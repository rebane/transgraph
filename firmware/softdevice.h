/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _SOFTDEVICE_H_
#define _SOFTDEVICE_H_

#include <stdint.h>
typedef int IRQn_Type;
#include "nrf_soc.h"
#include "nrf_sdm.h"
#include "ble_gap.h"
#include "ble.h"

#define SOFTDEVICE_CHAR_PROPS_READ     (1 << 0)
#define SOFTDEVICE_CHAR_PROPS_NOTIFY   (1 << 1)
#define SOFTDEVICE_CHAR_PROPS_INDICATE (1 << 2)
#define SOFTDEVICE_CHAR_PROPS_WRITE    (1 << 3)

#define SOFTDEVICE_VALUE_PROPS_READ    (1 << 0)
#define SOFTDEVICE_VALUE_PROPS_WRITE   (1 << 1)

#define SOFTDEVICE_EVENT_TYPE_SOC      1
#define SOFTDEVICE_EVENT_TYPE_BLE      2

struct softdevice_evt_s{
	uint8_t type;
	union{
		uint32_t soc_evt_id;
		struct{
			uint16_t ble_evt_len;
			ble_evt_t *ble_evt;
		};
	};
};

typedef struct softdevice_evt_s softdevice_evt_t;

uint32_t softdevice_enable(void *vectors);
uint32_t softdevice_disable();
uint32_t softdevice_nvic_enable_irq(uint8_t irq);
uint32_t softdevice_nvic_clear_pending_irq(uint8_t irq);
uint32_t softdevice_ble_enable(uint8_t service_changed);
uint32_t softdevice_ble_gap_device_name_set(uint8_t sm, uint8_t lv, void *name, uint16_t len);
uint32_t softdevice_ble_gap_ppcp_set(uint16_t min_conn_interval, uint16_t max_conn_interval, uint16_t slave_latency, uint16_t conn_sup_timeout);
uint32_t softdevice_ble_uuid_vs_add(void *p_vs_uuid, uint8_t *p_uuid_type);
uint32_t softdevice_ble_gatts_service_add(uint8_t type, uint16_t uuid, uint8_t uuid_type, uint16_t *p_handle);
uint32_t softdevice_ble_gatts_characteristic_add_simple(uint16_t service_handle, uint16_t uuid, uint8_t type, uint8_t char_props, uint8_t value_props, void *init_data, uint8_t len, uint16_t *value_handle);
uint32_t softdevice_ble_gatts_characteristic_add_user(uint16_t service_handle, uint16_t uuid, uint8_t type, uint8_t char_props, uint8_t value_props, void *init_data, uint8_t len, uint8_t max_len, uint16_t *value_handle);
uint32_t softdevice_ble_gatts_value_set(uint16_t conn_handle, uint16_t value_handle, void *p_data, uint16_t p_len, uint16_t offset);
uint32_t softdevice_ble_gatts_hvx(uint16_t conn_handle, uint16_t value_handle, uint8_t type, void *p_data, uint16_t p_len, uint16_t offset);
uint32_t softdevice_ble_gatts_hvx_user(uint16_t conn_handle, uint16_t value_handle, uint8_t type, uint16_t p_len);
uint32_t softdevice_ble_gap_adv_data_set(void *p_data, uint8_t dlen, void *p_sr_data, uint8_t srdlen);
uint32_t softdevice_ble_gap_adv_start(uint8_t type, uint8_t fp, uint16_t interval, uint16_t timeout);
softdevice_evt_t *softdevice_evt_get();
uint32_t softdevice_ble_gatts_sys_attr_set(uint16_t conn_handle, void *p_sys_attr_data, uint16_t len);
int softdevice_ble_gatts_value_is_notification_enabled(uint16_t conn_handle, uint16_t value_handle);
int softdevice_ble_gatts_value_is_indication_enabled(uint16_t conn_handle, uint16_t value_handle);
uint32_t softdevice_ble_gap_addr_get(uint8_t *addr);

#endif

