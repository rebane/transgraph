/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _SSD1309_HAL_H_
#define _SSD1309_HAL_H_

#include <stdint.h>

void ssd1309_hal_init();
void ssd1309_hal_cs(uint8_t level);
void ssd1309_hal_res(uint8_t level);
void ssd1309_hal_dc(uint8_t level);
void ssd1309_hal_en(uint8_t level);
void ssd1309_hal_sck(uint8_t level);
void ssd1309_hal_mosi(uint8_t level);
void ssd1309_hal_spi_send(uint8_t byte);
void ssd1309_hal_delay(uint32_t d);

#endif

