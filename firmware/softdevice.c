/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "softdevice.h"
#include <string.h>

static softdevice_evt_t softdevice_evt;
static uint8_t softdevice_evt_buffer[512] __attribute__((aligned(4)));

static void softdevice_assertion_handler(uint32_t pc, uint16_t line_num, const uint8_t *file_name)
{
	sd_nvic_SystemReset();
}

uint32_t softdevice_enable(void *vectors)
{
	uint32_t err;

	err = sd_softdevice_enable(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, softdevice_assertion_handler);
	sd_softdevice_vector_table_base_set((uint32_t)vectors);

	return err;
}

uint32_t softdevice_disable()
{
	return sd_softdevice_disable();
}

uint32_t softdevice_nvic_enable_irq(uint8_t irq)
{
	return sd_nvic_EnableIRQ(SWI2_INT);
}

uint32_t softdevice_nvic_clear_pending_irq(uint8_t irq)
{
	return sd_nvic_ClearPendingIRQ(irq);
}

uint32_t softdevice_ble_enable(uint8_t service_changed)
{
	ble_enable_params_t ble_enable_params;
	memset(&ble_enable_params, 0, sizeof(ble_enable_params));
	ble_enable_params.gatts_enable_params.service_changed = service_changed;

	return sd_ble_enable(&ble_enable_params);
}

uint32_t softdevice_ble_gap_device_name_set(uint8_t sm, uint8_t lv, void *name, uint16_t len)
{
	ble_gap_conn_sec_mode_t sec_mode;
	memset(&sec_mode, 0, sizeof(sec_mode));
	sec_mode.sm = sm;
	sec_mode.lv = lv;

	return sd_ble_gap_device_name_set(&sec_mode, (uint8_t *)name, len);
}

uint32_t softdevice_ble_gap_ppcp_set(uint16_t min_conn_interval, uint16_t max_conn_interval, uint16_t slave_latency, uint16_t conn_sup_timeout)
{
	ble_gap_conn_params_t conn_params;
	memset(&conn_params, 0, sizeof(conn_params));
	conn_params.min_conn_interval = min_conn_interval;
	conn_params.max_conn_interval = max_conn_interval;
	conn_params.slave_latency = slave_latency;
	conn_params.conn_sup_timeout = conn_sup_timeout;

	return sd_ble_gap_ppcp_set(&conn_params);
}

uint32_t softdevice_ble_uuid_vs_add(void *p_vs_uuid, uint8_t *p_uuid_type)
{
	ble_uuid128_t p_vs_uuid_rev;
	int i;

	for (i = 0; i < 16; i++)
		p_vs_uuid_rev.uuid128[i] = ((uint8_t *)p_vs_uuid)[15 - i];

	return sd_ble_uuid_vs_add(&p_vs_uuid_rev, p_uuid_type);
}

uint32_t softdevice_ble_gatts_service_add(uint8_t type, uint16_t uuid, uint8_t uuid_type, uint16_t *p_handle)
{
	ble_uuid_t ble_uuid;

	memset(&ble_uuid, 0, sizeof(ble_uuid));
	ble_uuid.uuid = uuid;
	ble_uuid.type = uuid_type;

	return sd_ble_gatts_service_add(type, &ble_uuid, p_handle);
}

uint32_t softdevice_ble_gatts_characteristic_add_simple(uint16_t service_handle, uint16_t uuid, uint8_t type, uint8_t char_props, uint8_t value_props, void *init_data, uint8_t len, uint16_t *value_handle)
{
	ble_gatts_char_md_t char_md;
	ble_gatts_attr_t attr_char_value;
	ble_uuid_t ble_uuid;
	ble_gatts_attr_md_t attr_md;
	ble_gatts_char_handles_t char_handles;
	uint32_t err;

	memset(&char_md, 0, sizeof(char_md));
	if (char_props & SOFTDEVICE_CHAR_PROPS_READ)
		char_md.char_props.read = 1;
	if (char_props & SOFTDEVICE_CHAR_PROPS_WRITE)
		char_md.char_props.write = 1;
	if (char_props & SOFTDEVICE_CHAR_PROPS_NOTIFY)
		char_md.char_props.notify = 1;
	if (char_props & SOFTDEVICE_CHAR_PROPS_INDICATE)
		char_md.char_props.indicate = 1;

	char_md.p_char_user_desc = NULL;
	char_md.p_char_pf = NULL;
	char_md.p_user_desc_md = NULL;
	char_md.p_cccd_md = NULL;
	char_md.p_sccd_md = NULL;

	memset(&ble_uuid, 0, sizeof(ble_uuid));
	ble_uuid.uuid = uuid;
	ble_uuid.type = type;

	memset(&attr_md, 0, sizeof(attr_md));
	if (value_props & SOFTDEVICE_VALUE_PROPS_READ)
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	if (value_props & SOFTDEVICE_VALUE_PROPS_WRITE)
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

	attr_md.vloc = BLE_GATTS_VLOC_STACK;
	attr_md.rd_auth = 0;
	attr_md.wr_auth = 0;
	attr_md.vlen = 0;

	memset(&attr_char_value, 0, sizeof(attr_char_value));
	attr_char_value.p_uuid = &ble_uuid;
	attr_char_value.p_attr_md = &attr_md;
	attr_char_value.init_len = len;
	attr_char_value.init_offs = 0;
	attr_char_value.max_len = len;
	attr_char_value.p_value = init_data;

	err = sd_ble_gatts_characteristic_add(service_handle, &char_md, &attr_char_value, &char_handles);
	if (value_handle != (void *)0)
		*value_handle = char_handles.value_handle;

	return err;
}

uint32_t softdevice_ble_gatts_characteristic_add_user(uint16_t service_handle, uint16_t uuid, uint8_t type, uint8_t char_props, uint8_t value_props, void *init_data, uint8_t len, uint8_t max_len, uint16_t *value_handle)
{
	ble_gatts_char_md_t char_md;
	ble_gatts_attr_t attr_char_value;
	ble_uuid_t ble_uuid;
	ble_gatts_attr_md_t attr_md;
	ble_gatts_char_handles_t char_handles;
	uint32_t err;

	memset(&char_md, 0, sizeof(char_md));
	if (char_props & SOFTDEVICE_CHAR_PROPS_READ)
		char_md.char_props.read = 1;
	if (char_props & SOFTDEVICE_CHAR_PROPS_WRITE)
		char_md.char_props.write = 1;
	if (char_props & SOFTDEVICE_CHAR_PROPS_NOTIFY)
		char_md.char_props.notify = 1;
	if (char_props & SOFTDEVICE_CHAR_PROPS_INDICATE)
		char_md.char_props.indicate = 1;

	char_md.p_char_user_desc = NULL;
	char_md.p_char_pf = NULL;
	char_md.p_user_desc_md = NULL;
	char_md.p_cccd_md = NULL;
	char_md.p_sccd_md = NULL;

	memset(&ble_uuid, 0, sizeof(ble_uuid));
	ble_uuid.uuid = uuid;
	ble_uuid.type = type;

	memset(&attr_md, 0, sizeof(attr_md));
	if (value_props & SOFTDEVICE_VALUE_PROPS_READ)
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	if (value_props & SOFTDEVICE_VALUE_PROPS_WRITE)
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

	attr_md.vloc = BLE_GATTS_VLOC_USER;
	attr_md.rd_auth = 0;
	attr_md.wr_auth = 0;
	attr_md.vlen = 1;

	memset(&attr_char_value, 0, sizeof(attr_char_value));
	attr_char_value.p_uuid = &ble_uuid;
	attr_char_value.p_attr_md = &attr_md;
	attr_char_value.init_len = len;
	attr_char_value.init_offs = 0;
	attr_char_value.max_len = max_len;
	attr_char_value.p_value = init_data;

	err = sd_ble_gatts_characteristic_add(service_handle, &char_md, &attr_char_value, &char_handles);
	if (value_handle != (void *)0)
		*value_handle = char_handles.value_handle;

	return err;
}

uint32_t softdevice_ble_gatts_value_set(uint16_t conn_handle, uint16_t value_handle, void *p_data, uint16_t p_len, uint16_t offset)
{
	ble_gatts_value_t gatts_value;

	gatts_value.len = p_len;
	gatts_value.offset = offset;
	gatts_value.p_value = p_data;

	return sd_ble_gatts_value_set(conn_handle, value_handle, &gatts_value);
}

uint32_t softdevice_ble_gatts_hvx(uint16_t conn_handle, uint16_t value_handle, uint8_t type, void *p_data, uint16_t p_len, uint16_t offset)
{
	ble_gatts_hvx_params_t params;

	memset(&params, 0, sizeof(params));
	params.handle = value_handle;
	params.type = type;
	params.p_data = (uint8_t *)p_data;
	params.p_len = &p_len;
	params.offset = offset;

	return sd_ble_gatts_hvx(conn_handle, &params);
}

uint32_t softdevice_ble_gap_adv_data_set(void *p_data, uint8_t dlen, void *p_sr_data, uint8_t srdlen)
{
	return sd_ble_gap_adv_data_set((uint8_t *)p_data, dlen, (uint8_t *)p_sr_data, srdlen);
}

uint32_t softdevice_ble_gap_adv_start(uint8_t type, uint8_t fp, uint16_t interval, uint16_t timeout)
{
	ble_gap_adv_params_t adv_params;

	memset(&adv_params, 0, sizeof(adv_params));
	adv_params.type = type;
	adv_params.fp = fp;
	adv_params.interval = interval;
	adv_params.timeout = timeout;

	return sd_ble_gap_adv_start(&adv_params);
}

softdevice_evt_t *softdevice_evt_get()
{
	if (sd_evt_get(&softdevice_evt.soc_evt_id) == NRF_SUCCESS) {
		softdevice_evt.type = SOFTDEVICE_EVENT_TYPE_SOC;
		return &softdevice_evt;
	}
	softdevice_evt.ble_evt_len = 512;
	if (sd_ble_evt_get(softdevice_evt_buffer, &softdevice_evt.ble_evt_len) == NRF_SUCCESS) {
		softdevice_evt.type = SOFTDEVICE_EVENT_TYPE_BLE;
		softdevice_evt.ble_evt = (ble_evt_t *)&softdevice_evt_buffer;
		return &softdevice_evt;
	}
	return (softdevice_evt_t *)0;
}

uint32_t softdevice_ble_gatts_sys_attr_set(uint16_t conn_handle, void *p_sys_attr_data, uint16_t len)
{
	return sd_ble_gatts_sys_attr_set(conn_handle, (uint8_t *)p_sys_attr_data, len, 0);
}

int softdevice_ble_gatts_value_is_notification_enabled(uint16_t conn_handle, uint16_t value_handle)
{
	ble_gatts_value_t v;
	uint8_t c[2];

	v.len = 2;
	v.offset = 0;
	v.p_value = c;

	if (sd_ble_gatts_value_get(conn_handle, value_handle + 1, &v) != NRF_SUCCESS)
		return 0;

	return (c[0] & 1);
}

int softdevice_ble_gatts_value_is_indication_enabled(uint16_t conn_handle, uint16_t value_handle)
{
	ble_gatts_value_t v;
	uint8_t c[2];

	v.len = 2;
	v.offset = 0;
	v.p_value = c;

	if (sd_ble_gatts_value_get(conn_handle, value_handle + 1, &v) != NRF_SUCCESS)
		return 0;

	return (c[0] & 2);
}

uint32_t softdevice_ble_gap_addr_get(uint8_t *addr)
{
	ble_gap_addr_t gap_addr;
	uint32_t err;

	err = sd_ble_gap_address_get(&gap_addr);
	addr[0] = gap_addr.addr[5];
	addr[1] = gap_addr.addr[4];
	addr[2] = gap_addr.addr[3];
	addr[3] = gap_addr.addr[2];
	addr[4] = gap_addr.addr[1];
	addr[5] = gap_addr.addr[0];

	return err;
}

