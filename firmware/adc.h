/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _ADC_H_
#define _ADC_H_

#include <stdint.h>

#define ADC_MUX_IN0  8
#define ADC_MUX_IN1  9
#define ADC_MUX_IN2  10
#define ADC_MUX_IN3  11
#define ADC_MUX_IN01 6
#define ADC_MUX_IN10 0
#define ADC_MUX_IN23 7
#define ADC_MUX_IN32 5

void adc_init(int ref_mv);
void adc_enable(int on);
uint32_t adc_get_raw(uint8_t mux);
int adc_get_mv(uint8_t mux);
int adc_get_mv_ref(uint8_t mux, int ref_mv);
uint32_t adc_vdd_get_raw();
int adc_vdd_get_mv();

#endif

