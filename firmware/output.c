/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "output.h"
#include <stdio.h>
#include "mtimer.h"
#include "ssd1309.h"
#include "bluetooth.h"
#include "io.h"
#include "measure.h"

void oled_refresh();

static int output_bi_ua;
static int output_cu_mv_min;
static int output_cu_mv_max;
static int output_ci_ma_scale;
static int output_initialized = 0;

void output_plot(int def, int bi_ua, int cu_mv_min, int cu_mv_max, int ci_ma_scale)
{
	int ci_ma, cu_mv, p;
	int berr, cerr;
	int i;

	ssd1309_cls();
	ssd1309_puts(4, 4, "OUTPUT", 1);
	measure_enable(1);
	measure_collector_polarity_set(MEASURE_POLARITY_NORMAL);
	measure_base_polarity_set(MEASURE_POLARITY_NORMAL);
	p = measure_get_cb_polarity();
	if (p == MEASURE_POLARITY_ERROR) {
		measure_enable(0);
		ssd1309_puts(64, 4, "ERROR", 1);
		oled_refresh();
	} else if (p == MEASURE_POLARITY_REVERSED) {
		ssd1309_puts(64, 4, "PNP", 1);
		measure_base_polarity_set(p);
		measure_base_vdd_set_mv(0);
		measure_collector_polarity_set(p);
		measure_collector_vdd_set_mv(0);
		io_e(IO_E_HIGH);
	} else {
		ssd1309_puts(64, 4, "NPN", 1);
		io_e(IO_E_LOW);
	}

	if (def) {
		if (output_initialized) {
			bi_ua = output_bi_ua;
			cu_mv_min = output_cu_mv_min;
			cu_mv_max = output_cu_mv_max;
			ci_ma_scale = output_ci_ma_scale;
		} else {
			bi_ua = 5000;
			cu_mv_min = 10;
			cu_mv_max = 3000;
			ci_ma_scale = 200;
		}
	} else {
		output_bi_ua = bi_ua;
		output_cu_mv_min = cu_mv_min;
		output_cu_mv_max = cu_mv_max;
		output_ci_ma_scale = ci_ma_scale;
		output_initialized = 1;
	}

	bluetooth_data_started(BLUETOOTH_START_OUTPUT, 128, bi_ua, cu_mv_min, cu_mv_max, ci_ma_scale);
	if (p == MEASURE_POLARITY_ERROR) {
		bluetooth_data_error(0);
		return;
	} else if (p == MEASURE_POLARITY_REVERSED) {
		bluetooth_data_transistor_type(BLUETOOTH_TRANSISTOR_TYPE_PNP);
	} else {
		bluetooth_data_transistor_type(BLUETOOTH_TRANSISTOR_TYPE_NPN);
	}

	oled_refresh();

	berr = cerr = 0;

	cerr |= !measure_collector_set_mv(cu_mv_min);
	berr |= !measure_base_set_ua(bi_ua);

	for (i = 0; i < 128; i++) {
		cu_mv = cu_mv_min + ((i * (cu_mv_max - cu_mv_min)) / 127);
		cerr |= !measure_collector_trim_mv(cu_mv);
		berr |= !measure_base_trim_ua(bi_ua);
		ci_ma = measure_collector_get_ma();
		ssd1309_psetr(i, 63 - ((ci_ma * 63) / ci_ma_scale), 1);
		bluetooth_data_data(i, (berr << 2) | (cerr << 1), bi_ua, cu_mv, ci_ma);
	}
	measure_enable(0);
	bluetooth_data_stopped();
	printf("BERR: %d, CERR: %d, I: %d\n", berr, cerr, i);
}

