/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include <stdint.h>

void platform_init();
void platform_bootstrap(void *start, uint32_t boot_param);
void platform_reboot(uint32_t boot_param);
uint32_t platform_boot_param_get();

#endif

