# mtimer

mtimer is a framework for hardware millisecond timer to easily poll timeouts.

## Usage

First it is needed to initialize the library with a hardware timer function pointer.

```C
extern uint32_t timer_get(uint16_t *msec); // hardware timer function that returns elapsed time in seconds and writes elapsed milliseconds to msec
mtimer_init(timer_get);
```

## Example

```C
mtimer_t timer;

mtimer_init(timer_get);

mtimer_timeout_clear(&timer);

while(1)
{
	if(mtimer_timeout(&timer))
	{
		printf("TICK\n");
		mtimer_timeout_add(&timer, 1000);
	}
}
```

## License

Copyright © 2019, [Veiko Rütter](https://bitbucket.org/rebane/).
Released under the [MIT License](LICENSE).

