#ifndef _NRF51_UART_H_
#define _NRF51_UART_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t STARTRX;
	volatile uint32_t STOPRX;
	volatile uint32_t STARTTX;
	volatile uint32_t STOPTX;
	uint32_t RESERVED1[3];
	volatile uint32_t SUSPEND;
	uint32_t RESERVED2[56];
	volatile uint32_t CTS;
	volatile uint32_t NCTS;
	volatile uint32_t RXDRDY;
	uint32_t RESERVED3[4];
	volatile uint32_t TXDRDY;
	uint32_t RESERVED4[1];
	volatile uint32_t ERROR;
	uint32_t RESERVED5[7];
	volatile uint32_t RXTO;
	uint32_t RESERVED6[110];
	volatile uint32_t INTEN;
	volatile uint32_t INTENSET;
	volatile uint32_t INTENCLR;
	uint32_t RESERVED7[93];
	volatile uint32_t ERRORSRC;
	uint32_t RESERVED8[31];
	volatile uint32_t ENABLE;
	uint32_t RESERVED9[1];
	volatile uint32_t PSELRTS;
	volatile uint32_t PSELTXD;
	volatile uint32_t PSELCTS;
	volatile uint32_t PSELRXD;
	volatile uint32_t RXD;
	volatile uint32_t TXD;
	uint32_t RESERVED10[1];
	volatile uint32_t BAUDRATE;
	uint32_t RESERVED11[17];
	volatile uint32_t CONFIG;
}__attribute__((packed)) UART_TypeDef;

#define UART0             ((UART_TypeDef *)0x40002000)

// INTEN, INTENSET, INTENCLR
#define UART_CTS          (((uint32_t)0x01) << 0)
#define UART_NCTS         (((uint32_t)0x01) << 1)
#define UART_RXDRDY       (((uint32_t)0x01) << 2)
#define UART_TXDRDY       (((uint32_t)0x01) << 7)
#define UART_ERROR        (((uint32_t)0x01) << 9)
#define UART_RXTO         (((uint32_t)0x01) << 17)

// ERRORSRC
#define UART_OVERRUN      (((uint32_t)0x01) << 0)
#define UART_ERRORSRC_PARITY (((uint32_t)0x01) << 1)
#define UART_FRAMING      (((uint32_t)0x01) << 2)
#define UART_BREAK        (((uint32_t)0x01) << 3)

// ENABLE
#define UART_ENABLE       (((uint32_t)0x07) << 0)

// CONFIG
#define UART_HWFC         (((uint32_t)0x01) << 0)
#define UART_CONFIG_PARITY (((uint32_t)0x07) << 1)

#endif

