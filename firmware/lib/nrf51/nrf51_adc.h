#ifndef _NRF51_ADC_H_
#define _NRF51_ADC_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t START;
	volatile uint32_t STOP;
	uint32_t RESERVED1[62];
	volatile uint32_t END;
	uint32_t RESERVED2[127];
	volatile uint32_t INTEN;
	volatile uint32_t INTENSET;
	volatile uint32_t INTENCLR;
	uint32_t RESERVED3[61];
	volatile uint32_t BUSY;
	uint32_t RESERVED4[63];
	volatile uint32_t ENABLE;
	volatile uint32_t CONFIG;
	volatile uint32_t RESULT;
}__attribute__((packed)) ADC_TypeDef;

#define ADC               ((ADC_TypeDef *)0x40007000)

// INTEN, INTENSET, INTENCLR
#define ADC_END           (((uint32_t)0x01) << 0)

// BUSY
#define ADC_BUSY          (((uint32_t)0x01) << 0)

// ENABLE
#define ADC_ENABLE        (((uint32_t)0x03) << 0)

// CONFIG
#define ADC_RES           (((uint32_t)0x03) << 0)
#define ADC_INPSEL        (((uint32_t)0x07) << 2)
#define ADC_REFSEL        (((uint32_t)0x03) << 5)
#define ADC_PSEL          (((uint32_t)0xFF) << 8)
#define ADC_EXTREFSEL     (((uint32_t)0x03) << 16)

// RESULT
#define ADC_RESULT        (((uint32_t)0x03FF) << 0)

#endif

