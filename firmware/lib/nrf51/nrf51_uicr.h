#ifndef _NRF51_UICR_H_
#define _NRF51_UICR_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t CLENR0;
	volatile uint32_t RBPCONF;
	volatile uint32_t XTALFREQ;
	uint32_t RESERVED1[1];
	volatile uint32_t FWID;
	union{
		volatile uint32_t BOOTLOADERADDR;
		volatile uint32_t NRFFW[15];
		struct{
			uint32_t RESERVED2[1];
			volatile uint32_t NRFFW1;
			volatile uint32_t NRFFW2;
			volatile uint32_t NRFFW3;
			volatile uint32_t NRFFW4;
			volatile uint32_t NRFFW5;
			volatile uint32_t NRFFW6;
			volatile uint32_t NRFFW7;
			volatile uint32_t NRFFW8;
			volatile uint32_t NRFFW9;
			volatile uint32_t NRFFW10;
			volatile uint32_t NRFFW11;
			volatile uint32_t NRFFW12;
			volatile uint32_t NRFFW13;
			volatile uint32_t NRFFW14;
		}__attribute__((packed));
	};
	union{
		volatile uint32_t NRFHW[12];
		struct{
			volatile uint32_t NRFHW0;
			volatile uint32_t NRFHW1;
			volatile uint32_t NRFHW2;
			volatile uint32_t NRFHW3;
			volatile uint32_t NRFHW4;
			volatile uint32_t NRFHW5;
			volatile uint32_t NRFHW6;
			volatile uint32_t NRFHW7;
			volatile uint32_t NRFHW8;
			volatile uint32_t NRFHW9;
			volatile uint32_t NRFHW10;
			volatile uint32_t NRFHW11;
		}__attribute__((packed));
	};
	union{
		volatile uint32_t CUSTOMER[32];
		struct{
			volatile uint32_t CUSTOMER0;
			volatile uint32_t CUSTOMER1;
			volatile uint32_t CUSTOMER2;
			volatile uint32_t CUSTOMER3;
			volatile uint32_t CUSTOMER4;
			volatile uint32_t CUSTOMER5;
			volatile uint32_t CUSTOMER6;
			volatile uint32_t CUSTOMER7;
			volatile uint32_t CUSTOMER8;
			volatile uint32_t CUSTOMER9;
			volatile uint32_t CUSTOMER10;
			volatile uint32_t CUSTOMER11;
			volatile uint32_t CUSTOMER12;
			volatile uint32_t CUSTOMER13;
			volatile uint32_t CUSTOMER14;
			volatile uint32_t CUSTOMER15;
			volatile uint32_t CUSTOMER16;
			volatile uint32_t CUSTOMER17;
			volatile uint32_t CUSTOMER18;
			volatile uint32_t CUSTOMER19;
			volatile uint32_t CUSTOMER20;
			volatile uint32_t CUSTOMER21;
			volatile uint32_t CUSTOMER22;
			volatile uint32_t CUSTOMER23;
			volatile uint32_t CUSTOMER24;
			volatile uint32_t CUSTOMER25;
			volatile uint32_t CUSTOMER26;
			volatile uint32_t CUSTOMER27;
			volatile uint32_t CUSTOMER28;
			volatile uint32_t CUSTOMER29;
			volatile uint32_t CUSTOMER30;
			volatile uint32_t CUSTOMER31;
		}__attribute__((packed));
	};
}__attribute__((packed)) UICR_TypeDef;

typedef struct{
	uint32_t CLENR0;
	uint32_t RBPCONF;
	uint32_t XTALFREQ;
	uint32_t RESERVED;
	uint32_t FWID;
	uint32_t BOOTLOADERADDR;
}__attribute__((packed)) UICR_Header_TypeDef;

#define UICR              ((UICR_TypeDef *)0x10001000)

// PPFC
// #define FICR_PPFC         (((uint32_t)0xFF) << 0)

#endif

