#ifndef _NRF51_FICR_H_
#define _NRF51_FICR_H_

#include <stdint.h>

typedef struct{
	uint32_t RESERVED1[4];
	volatile uint32_t CODEPAGESIZE;
	volatile uint32_t CODESIZE;
	uint32_t RESERVED2[4];
	volatile uint32_t CLENR0;
	volatile uint32_t PPFC;
	uint32_t RESERVED3[1];
	volatile uint32_t NUMRAMBLOCK;
	union{
		volatile uint32_t SIZERAMBLOCK[4];
		struct{
			union{
				volatile uint32_t SIZERAMBLOCKS;
				volatile uint32_t SIZERAMBLOCK0;
			};
			volatile uint32_t SIZERAMBLOCK1;
			volatile uint32_t SIZERAMBLOCK2;
			volatile uint32_t SIZERAMBLOCK3;
		}__attribute__((packed));
	};
	uint32_t RESERVED4[5];
	volatile uint32_t CONFIGID;
	union{
		volatile uint32_t DEVICEID[2];
		struct{
			volatile uint32_t DEVICEID0;
			volatile uint32_t DEVICEID1;
		}__attribute__((packed));
	};
	uint32_t RESERVED5[6];
	union{
		volatile uint32_t ER[4];
		struct{
			volatile uint32_t ER0;
			volatile uint32_t ER1;
			volatile uint32_t ER2;
			volatile uint32_t ER3;
		}__attribute__((packed));
	};
	union{
		volatile uint32_t IR[4];
		struct{
			volatile uint32_t IR0;
			volatile uint32_t IR1;
			volatile uint32_t IR2;
			volatile uint32_t IR3;
		}__attribute__((packed));
	};
	volatile uint32_t DEVICEADDRTYPE;
	union{
		volatile uint32_t DEVICEADDR[2];
		struct{
			volatile uint32_t DEVICEADDR0;
			volatile uint32_t DEVICEADDR1;
		}__attribute__((packed));
	};
	volatile uint32_t OVERRIDEEN;
	union{
		volatile uint32_t NRF_1MBIT[5];
		struct{
			volatile uint32_t NRF_1MBIT0;
			volatile uint32_t NRF_1MBIT1;
			volatile uint32_t NRF_1MBIT2;
			volatile uint32_t NRF_1MBIT3;
			volatile uint32_t NRF_1MBIT4;
		}__attribute__((packed));
	};
	uint32_t RESERVED6[10];
	union{
		volatile uint32_t BLE_1MBIT[5];
		struct{
			volatile uint32_t BLE_1MBIT0;
			volatile uint32_t BLE_1MBIT1;
			volatile uint32_t BLE_1MBIT2;
			volatile uint32_t BLE_1MBIT3;
			volatile uint32_t BLE_1MBIT4;
		}__attribute__((packed));
	};
}__attribute__((packed)) FICR_TypeDef;

#define FICR              ((FICR_TypeDef *)0x10000000)

// PPFC
#define FICR_PPFC         (((uint32_t)0xFF) << 0)

#endif

