#ifndef _NRF51_POWER_H_
#define _NRF51_POWER_H_

#include <stdint.h>

typedef struct{
	uint32_t RESERVED1[30];
	volatile uint32_t CONSTLAT;
	volatile uint32_t LOWPWR;
	uint32_t RESERVED2[34];
	volatile uint32_t POFWARN;
	uint32_t RESERVED3[126];
	volatile uint32_t INTENSET;
	volatile uint32_t INTENCLR;
	uint32_t RESERVED4[61];
	volatile uint32_t RESETREAS;
	uint32_t RESERVED5[9];
	volatile uint32_t RAMSTATUS;
	uint32_t RESERVED6[53];
	volatile uint32_t SYSTEMOFF;
	uint32_t RESERVED7[3];
	volatile uint32_t POFCON;
	uint32_t RESERVED8[2];
	volatile uint32_t GPREGRET;
	uint32_t RESERVED9[1];
	volatile uint32_t RAMON;
	uint32_t RESERVED10[7];
	volatile uint32_t RESET;
	uint32_t RESERVED11[3];
	volatile uint32_t RAMONB;
	uint32_t RESERVED12[8];
	volatile uint32_t DCDCEN;
}__attribute__((packed)) POWER_TypeDef;

#define POWER             ((POWER_TypeDef *)0x40000000)

// POWER_RESET
#define POWER_RESET_RESET (((uint32_t)0x01) << 0)

#endif

