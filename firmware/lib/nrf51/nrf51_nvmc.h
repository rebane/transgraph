#ifndef _NRF51_NVMC_H_
#define _NRF51_NVMC_H_

#include <stdint.h>

typedef struct{
	uint32_t RESERVED1[256];
	volatile uint32_t READY;
	uint32_t RESERVED2[64];
	volatile uint32_t CONFIG;
	union{
		volatile uint32_t ERASEPAGE;
		volatile uint32_t ERASEPCR1;
	};
	volatile uint32_t ERASEALL;
	volatile uint32_t ERASEPCR0;
	volatile uint32_t ERASEUICR;
}__attribute__((packed)) NVMC_TypeDef;

#define NVMC              ((NVMC_TypeDef *)0x4001E000)

// READY
#define NVMC_READY        (((uint32_t)0x01) << 0)

// CONFIG
#define NVMC_WEN          (((uint32_t)0x03) << 0)

// ERASEALL
#define NVMC_ERASEALL     (((uint32_t)0x01) << 0)

// ERASEUICR
#define NVMC_ERASEUICR    (((uint32_t)0x01) << 0)

#endif

