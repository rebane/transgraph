#ifndef _NRF51_TWI_H_
#define _NRF51_TWI_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t STARTRX;
	uint32_t RESERVED1[1];
	volatile uint32_t STARTTX;
	uint32_t RESERVED2[2];
	volatile uint32_t STOP;
	uint32_t RESERVED3[1];
	volatile uint32_t SUSPEND;
	volatile uint32_t RESUME;
	uint32_t RESERVED4[56];
	volatile uint32_t STOPPED;
	volatile uint32_t RXDREADY;
	uint32_t RESERVED5[4];
	volatile uint32_t TXDSENT;
	uint32_t RESERVED6[1];
	volatile uint32_t ERROR;
	uint32_t RESERVED7[4];
	volatile uint32_t BB;
	uint32_t RESERVED8[49];
	volatile uint32_t SHORTS;
	uint32_t RESERVED9[63];
	volatile uint32_t INTEN;
	volatile uint32_t INTENSET;
	volatile uint32_t INTENCLR;
	uint32_t RESERVED10[110];
	volatile uint32_t ERRORSRC;
	uint32_t RESERVED11[14];
	volatile uint32_t ENABLE;
	uint32_t RESERVED12[1];
	volatile uint32_t PSELSCL;
	volatile uint32_t PSELSDA;
	uint32_t RESERVED13[2];
	volatile uint32_t RXD;
	volatile uint32_t TXD;
	uint32_t RESERVED14[1];
	volatile uint32_t FREQUENCY;
	uint32_t RESERVED15[24];
	volatile uint32_t ADDRESS;
}__attribute__((packed)) TWI_TypeDef;

#define TWI0              ((TWI_TypeDef *)0x40003000)
#define TWI1              ((TWI_TypeDef *)0x40004000)

// SHORTS
#define TWI_BB_SUSPEND    (((uint32_t)0x01) << 0)
#define TWI_BB_STOP       (((uint32_t)0x01) << 1)

// INTEN, INTENSET, INTENCLR
#define TWI_STOPPED       (((uint32_t)0x01) << 1)
#define TWI_RXDREADY      (((uint32_t)0x01) << 2)
#define TWI_TXDSENT       (((uint32_t)0x01) << 7)
#define TWI_ERROR         (((uint32_t)0x01) << 9)
#define TWI_BB            (((uint32_t)0x01) << 14)

// ERRORSRC
#define TWI_OVERRUN       (((uint32_t)0x01) << 0)
#define TWI_ANACK         (((uint32_t)0x01) << 1)
#define TWI_DNACK         (((uint32_t)0x01) << 2)

// ENABLE
#define TWI_ENABLE        (((uint32_t)0x07) << 0)

// FREQUENCY
#define TWI_FREQUENCY_K100 0x01980000
#define TWI_FREQUENCY_K250 0x04000000
#define TWI_FREQUENCY_K400 0x06680000

#endif

