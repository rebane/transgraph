#ifndef _NRF51_SPI_H_
#define _NRF51_SPI_H_

#include <stdint.h>

typedef struct{
	uint32_t RESERVED1[66];
	volatile uint32_t READY;
	uint32_t RESERVED2[125];
	volatile uint32_t INTEN;
	volatile uint32_t INTENSET;
	volatile uint32_t INTENCLR;
	uint32_t RESERVED3[125];
	volatile uint32_t ENABLE;
	uint32_t RESERVED4[1];
	volatile uint32_t PSELSCK;
	volatile uint32_t PSELMOSI;
	volatile uint32_t PSELMISO;
	uint32_t RESERVED5[1];
	volatile uint32_t RXD;
	volatile uint32_t TXD;
	uint32_t RESERVED6[1];
	volatile uint32_t FREQUENCY;
	uint32_t RESERVED7[11];
	volatile uint32_t CONFIG;
}__attribute__((packed)) SPI_TypeDef;

#define SPI0                               ((SPI_TypeDef *)0x40003000)
#define SPI1                               ((SPI_TypeDef *)0x40004000)

// INTEN
#define SPI_INTEN_READY                    (((uint32_t)0x01) << 2)

// INTENSET
#define SPI_INTENSET_READY                 (((uint32_t)0x01) << 2)

// INTENCLR
#define SPI_INTENCLR_READY                 (((uint32_t)0x01) << 2)

// ENABLE
#define SPI_ENABLE_ENABLE                  (((uint32_t)0x07) << 0)
#define SPI_ENABLE_ENABLE_DISABLED         0
#define SPI_ENABLE_ENABLE_ENABLED          1

// PSELSCK
#define SPI_PSELSCK_PSELSCK                (((uint32_t)0xFFFFFFFF) << 0)
#define SPI_PSELSCK_PSELSCK_DISCONNECTED   0xFFFFFFFF

// PSELMOSI
#define SPI_PSELMOSI_PSELMOSI              (((uint32_t)0xFFFFFFFF) << 0)
#define SPI_PSELMOSI_PSELMOSI_DISCONNECTED 0xFFFFFFFF

// PSELMISO
#define SPI_PSELMISO_PSELMISO              (((uint32_t)0xFFFFFFFF) << 0)
#define SPI_PSELMISO_PSELMISO_DISCONNECTED 0xFFFFFFFF

// RXD
#define SPI_RXD_RXD                        (((uint32_t)0xFF) << 0)

// TXD
#define SPI_TXD_TXD                        (((uint32_t)0xFF) << 0)

// FREQUENCY
#define SPI_FREQUENCY_FREQUENCY            (((uint32_t)0xFFFFFF) << 0)
// #define SPI_FREQUENCY_FREQUENCY_K125       0x02000000
// #define SPI_FREQUENCY_FREQUENCY_K250       0x04000000
// #define SPI_FREQUENCY_FREQUENCY_K500       0x08000000
// #define SPI_FREQUENCY_FREQUENCY_M1         0x10000000
// #define SPI_FREQUENCY_FREQUENCY_M2         0x20000000
// #define SPI_FREQUENCY_FREQUENCY_M4         0x40000000
// #define SPI_FREQUENCY_FREQUENCY_M8         0x80000000

// CONFIG
#define SPI_CONFIG_ORDER                   (((uint32_t)0x01) << 0)
#define SPI_CONFIG_CPHA                    (((uint32_t)0x01) << 1)
#define SPI_CONFIG_CPOL                    (((uint32_t)0x01) << 2)
#define SPI_CONFIG_ORDER_MSB_FIRST         0
#define SPI_CONFIG_ORDER_LSB_FIRST         1
#define SPI_CONFIG_CPHA_LEADING            0
#define SPI_CONFIG_CPHA_TRAILING           1
#define SPI_CONFIG_CPOL_ACTIVE_HIGH        0
#define SPI_CONFIG_CPOL_ACTIVE_LOW         1

#endif

