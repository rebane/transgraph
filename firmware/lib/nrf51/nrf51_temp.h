#ifndef _NRF51_TEMP_H_
#define _NRF51_TEMP_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t START;
	volatile uint32_t STOP;
	uint32_t RESERVED1[62];
	volatile uint32_t DATARDY;
	uint32_t RESERVED2[127];
	volatile uint32_t INTEN;
	volatile uint32_t INTENSET;
	volatile uint32_t INTENCLR;
	uint32_t RESERVED3[127];
	volatile uint32_t TEMPERATURE;
}__attribute__((packed)) TEMP_TypeDef;

#define TEMP              ((TEMP_TypeDef *)0x4000C000)

// INTEN, INTENSET, INTENCLR
#define TEMP_DATARDY      (((uint32_t)0x01) << 0)

#endif

