#ifndef _NRF51_H_
#define _NRF51_H_

#include <stdint.h>

#include "nrf51_power.h"
#include "nrf51_ficr.h"
#include "nrf51_uicr.h"
#include "nrf51_interrupt.h"
#include "nrf51_clock.h"
#include "nrf51_nvmc.h"
#include "nrf51_gpio.h"
#include "nrf51_timer.h"
#include "nrf51_uart.h"
#include "nrf51_spi.h"
#include "nrf51_twi.h"
#include "nrf51_temp.h"
#include "nrf51_adc.h"

#endif

