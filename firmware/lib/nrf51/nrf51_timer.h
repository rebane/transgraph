#ifndef _NRF51_TIMER_H_
#define _NRF51_TIMER_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t START;
	volatile uint32_t STOP;
	volatile uint32_t COUNT;
	volatile uint32_t CLEAR;
	volatile uint32_t SHUTDOWN;
	uint32_t RESERVED1[11];
	union{
		volatile uint32_t CAPTURE[4];
		struct{
			volatile uint32_t CAPTURE0;
			volatile uint32_t CAPTURE1;
			volatile uint32_t CAPTURE2;
			volatile uint32_t CAPTURE3;
		}__attribute__((packed));
	};
	uint32_t RESERVED2[60];
	union{
		volatile uint32_t COMPARE[4];
		struct{
			volatile uint32_t COMPARE0;
			volatile uint32_t COMPARE1;
			volatile uint32_t COMPARE2;
			volatile uint32_t COMPARE3;
		}__attribute__((packed));
	};
	uint32_t RESERVED3[44];
	volatile uint32_t SHORTS;
	uint32_t RESERVED4[64];
	volatile uint32_t INTENSET;
	volatile uint32_t INTENCLR;
	uint32_t RESERVED5[126];
	volatile uint32_t MODE;
	volatile uint32_t BITMODE;
	uint32_t RESERVED6[1];
	volatile uint32_t PRESCALER;
	uint32_t RESERVED7[11];
	union{
		volatile uint32_t CC[4];
		struct{
			volatile uint32_t CC0;
			volatile uint32_t CC1;
			volatile uint32_t CC2;
			volatile uint32_t CC3;
		}__attribute__((packed));
	};
}__attribute__((packed)) TIMER_TypeDef;

#define TIMER0            ((TIMER_TypeDef *)0x40008000)
#define TIMER1            ((TIMER_TypeDef *)0x40009000)
#define TIMER2            ((TIMER_TypeDef *)0x4000A000)

// SHORTS
#define TIMER_COMPARE0_CLEAR (((uint32_t)0x01) << 0)
#define TIMER_COMPARE1_CLEAR (((uint32_t)0x01) << 1)
#define TIMER_COMPARE2_CLEAR (((uint32_t)0x01) << 2)
#define TIMER_COMPARE3_CLEAR (((uint32_t)0x01) << 3)
#define TIMER_COMPARE0_STOP  (((uint32_t)0x01) << 8)
#define TIMER_COMPARE1_STOP  (((uint32_t)0x01) << 9)
#define TIMER_COMPARE2_STOP  (((uint32_t)0x01) << 10)
#define TIMER_COMPARE3_STOP  (((uint32_t)0x01) << 11)

// INTENSET, INTENCLR
#define TIMER_COMPARE0       (((uint32_t)0x01) << 16)
#define TIMER_COMPARE1       (((uint32_t)0x01) << 17)
#define TIMER_COMPARE2       (((uint32_t)0x01) << 18)
#define TIMER_COMPARE3       (((uint32_t)0x01) << 19)

// MODE
#define TIMER_MODE           (((uint32_t)0x01) << 0)

// BITMODE
#define TIMER_BITMODE        (((uint32_t)0x03) << 0)

// PRESCALER
#define TIMER_PRESCALER      (((uint32_t)0x0F) << 0)

#endif

