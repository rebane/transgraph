#ifndef _NRF51_INTERRUPT_H_
#define _NRF51_INTERRUPT_H_

#define POWER_CLOCK_INT 0
#define RADIO_INT       1
#define UART0_INT       2
#define SPI0_INT        3
#define TWI0_INT        3
#define SPI1_INT        4
#define TWI1_INT        4

#define GPIOTE_INT      6
#define ADC_INT         7
#define TIMER0_INT      8
#define TIMER1_INT      9
#define TIMER2_INT      10
#define RTC0_INT        11
#define TEMP_INT        12
#define RNG_INT         13
#define ECB_INT         14
#define CCM_INT         15
#define AAR_INT         15
#define CCM_AAR_INT     15
#define WDT_INT         16
#define RTC1_INT        17
#define QDEC_INT        18
#define LPCOMP_COMP_INT 19
#define SWI0_INT        20
#define SWI1_INT        21
#define SWI2_INT        22
#define SWI3_INT        23
#define SWI4_INT        24
#define SWI5_INT        25

#endif

