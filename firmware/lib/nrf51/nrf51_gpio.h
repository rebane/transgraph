#ifndef _NRF51_GPIO_H_
#define _NRF51_GPIO_H_

#include <stdint.h>

typedef struct{
	uint32_t RESERVED1[321];
	volatile uint32_t OUT;
	volatile uint32_t OUTSET;
	volatile uint32_t OUTCLR;
	volatile uint32_t IN;
	volatile uint32_t DIR;
	volatile uint32_t DIRSET;
	volatile uint32_t DIRCLR;
	uint32_t RESERVED2[120];
	union{
		volatile uint32_t PIN_CNF[32];
		struct{
			volatile uint32_t PIN_CNF0;
			volatile uint32_t PIN_CNF1;
			volatile uint32_t PIN_CNF2;
			volatile uint32_t PIN_CNF3;
			volatile uint32_t PIN_CNF4;
			volatile uint32_t PIN_CNF5;
			volatile uint32_t PIN_CNF6;
			volatile uint32_t PIN_CNF7;
			volatile uint32_t PIN_CNF8;
			volatile uint32_t PIN_CNF9;
			volatile uint32_t PIN_CNF10;
			volatile uint32_t PIN_CNF11;
			volatile uint32_t PIN_CNF12;
			volatile uint32_t PIN_CNF13;
			volatile uint32_t PIN_CNF14;
			volatile uint32_t PIN_CNF15;
			volatile uint32_t PIN_CNF16;
			volatile uint32_t PIN_CNF17;
			volatile uint32_t PIN_CNF18;
			volatile uint32_t PIN_CNF19;
			volatile uint32_t PIN_CNF20;
			volatile uint32_t PIN_CNF21;
			volatile uint32_t PIN_CNF22;
			volatile uint32_t PIN_CNF23;
			volatile uint32_t PIN_CNF24;
			volatile uint32_t PIN_CNF25;
			volatile uint32_t PIN_CNF26;
			volatile uint32_t PIN_CNF27;
			volatile uint32_t PIN_CNF28;
			volatile uint32_t PIN_CNF29;
			volatile uint32_t PIN_CNF30;
			volatile uint32_t PIN_CNF31;
		}__attribute__((packed));
	};
}__attribute__((packed)) GPIO_TypeDef;

#define GPIO              ((GPIO_TypeDef *)0x50000000)

// GPIO
#define GPIO_PIN0         (((uint32_t)0x01) << 0)
#define GPIO_PIN1         (((uint32_t)0x01) << 1)
#define GPIO_PIN2         (((uint32_t)0x01) << 2)
#define GPIO_PIN3         (((uint32_t)0x01) << 3)
#define GPIO_PIN4         (((uint32_t)0x01) << 4)
#define GPIO_PIN5         (((uint32_t)0x01) << 5)
#define GPIO_PIN6         (((uint32_t)0x01) << 6)
#define GPIO_PIN7         (((uint32_t)0x01) << 7)
#define GPIO_PIN8         (((uint32_t)0x01) << 8)
#define GPIO_PIN9         (((uint32_t)0x01) << 9)
#define GPIO_PIN10        (((uint32_t)0x01) << 10)
#define GPIO_PIN11        (((uint32_t)0x01) << 11)
#define GPIO_PIN12        (((uint32_t)0x01) << 12)
#define GPIO_PIN13        (((uint32_t)0x01) << 13)
#define GPIO_PIN14        (((uint32_t)0x01) << 14)
#define GPIO_PIN15        (((uint32_t)0x01) << 15)
#define GPIO_PIN16        (((uint32_t)0x01) << 16)
#define GPIO_PIN17        (((uint32_t)0x01) << 17)
#define GPIO_PIN18        (((uint32_t)0x01) << 18)
#define GPIO_PIN19        (((uint32_t)0x01) << 19)
#define GPIO_PIN20        (((uint32_t)0x01) << 20)
#define GPIO_PIN21        (((uint32_t)0x01) << 21)
#define GPIO_PIN22        (((uint32_t)0x01) << 22)
#define GPIO_PIN23        (((uint32_t)0x01) << 23)
#define GPIO_PIN24        (((uint32_t)0x01) << 24)
#define GPIO_PIN25        (((uint32_t)0x01) << 25)
#define GPIO_PIN26        (((uint32_t)0x01) << 26)
#define GPIO_PIN27        (((uint32_t)0x01) << 27)
#define GPIO_PIN28        (((uint32_t)0x01) << 28)
#define GPIO_PIN29        (((uint32_t)0x01) << 29)
#define GPIO_PIN30        (((uint32_t)0x01) << 30)
#define GPIO_PIN31        (((uint32_t)0x01) << 31)

#define GPIO_DIR          (((uint32_t)0x01) << 0)
#define GPIO_INPUT        (((uint32_t)0x01) << 1)
#define GPIO_PULL         (((uint32_t)0x03) << 2)
#define GPIO_DRIVE        (((uint32_t)0x07) << 8)
#define GPIO_SENSE        (((uint32_t)0x03) << 16)

#endif
