#ifndef _NRF51_CLOCK_H_
#define _NRF51_CLOCK_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t HFCLKSTART;
	volatile uint32_t HFCLKSTOP;
	volatile uint32_t LFCLKSTART;
	volatile uint32_t LFCLKSTOP;
	volatile uint32_t CAL;
	volatile uint32_t CTSTART;
	volatile uint32_t CTSTOP;
	uint32_t RESERVED1[57];
	volatile uint32_t HFCLKSTARTED;
	volatile uint32_t LFCLKSTARTED;
	uint32_t RESERVED2[1];
	volatile uint32_t DONE;
	volatile uint32_t CTTO;
	uint32_t RESERVED3[124];
	volatile uint32_t INTENSET;
	volatile uint32_t INTENCLR;
	uint32_t RESERVED4[63];
	volatile uint32_t HFCLKRUN;
	volatile uint32_t HFCLKSTAT;
	uint32_t RESERVED5[1];
	volatile uint32_t LFCLKRUN;
	volatile uint32_t LFCLKSTAT;
	volatile uint32_t LFCLKSRCCOPY;
	uint32_t RESERVED6[62];
	volatile uint32_t LFCLKSRC;
	uint32_t RESERVED7[7];
	volatile uint32_t CTIV;
	uint32_t RESERVED8[5];
	volatile uint32_t XTALFREQ;
}__attribute__((packed)) CLOCK_TypeDef;

#define CLOCK             ((CLOCK_TypeDef *)0x40000000)

// INTENSET, INTENCLR
#define CLOCK_HFCLKSTARTED (((uint32_t)0x01) << 0)
#define CLOCK_LFCLKSTARTED (((uint32_t)0x01) << 1)
#define CLOCK_DONE         (((uint32_t)0x01) << 3)
#define CLOCK_CTTO         (((uint32_t)0x01) << 4)

// HFCLKRUN, LFCLKRUN
#define CLOCK_STATUS      (((uint32_t)0x01) << 0)

// HFCLKSTAT, LFCLKSTAT, LFCLKSRCCOPY, LFCLKSRC
#define CLOCK_SRC         (((uint32_t)0x03) << 0)
#define CLOCK_STATE       (((uint32_t)0x01) << 16)

// CTIV
#define CLOCK_CTIV        (((uint32_t)0x7F) << 0)

// XTALFREQ
#define CLOCK_XTALFREQ    (((uint32_t)0xFF) << 0)

#define CLOCK_XTALFREQ_16MHZ 0xFF
#define CLOCK_XTALFREQ_32MHZ 0x00
#define CLOCK_HFCLKSRC_RC    0
#define CLOCK_HFCLKSRC_XTAL  1
#define CLOCK_LFCLKSRC_RC    0
#define CLOCK_LFCLKSRC_XTAL  1
#define CLOCK_LFCLKSRC_SYNTH 2
#define CLOCK_STATE_STOP     0
#define CLOCK_STATE_RUN      1

#endif

