/*
 * Startup routines to boot up Cortex-M mcu's
 *
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <rebane@alkohol.ee> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.        Veiko Rütter
 * ----------------------------------------------------------------------------
 */

#ifndef _CORTEX_H_
#define _CORTEX_H_

#include <stdint.h>

typedef struct {
	volatile uint32_t ISER[8];
	uint32_t RESERVED1[24];
	volatile uint32_t ICER[8];
	uint32_t RESERVED2[24];
	volatile uint32_t ISPR[8];
	uint32_t RESERVED3[24];
	volatile uint32_t ICPR[8];
	uint32_t RESERVED4[24];
	volatile uint32_t IABR[8];
	uint32_t RESERVED5[56];
	volatile uint32_t IPR[32];
} NVIC_TypeDef;

typedef struct {
	uint32_t RESERVED1[2];
	volatile uint32_t ACTLR;
	uint32_t RESERVED2[829];
	volatile const uint32_t CPUID;
	volatile uint32_t ICSR;
	volatile uint32_t VTOR;
	volatile uint32_t AIRCR;
	volatile uint32_t SCR;
	volatile uint32_t CCR;
	volatile uint32_t SHPR[3];
	volatile uint32_t SHCSR;
	volatile uint32_t CFSR;
	volatile uint32_t HFSR;
	volatile uint32_t DFSR;
	volatile uint32_t MMFAR;
	volatile uint32_t BFAR;
	volatile uint32_t AFSR;
	volatile const uint32_t PFR[2];
	volatile const uint32_t DFR;
	volatile const uint32_t AFR;
	volatile const uint32_t MMFR[4];
	volatile const uint32_t ISAR[5];
	uint32_t RESERVED3[5];
	volatile uint32_t CPACR;
} SCB_TypeDef;

typedef struct {
	volatile uint32_t CSR;
	volatile uint32_t RVR;
	volatile uint32_t CVR;
	volatile uint32_t CALIB;
} SYSTICK_TypeDef;

#define NVIC                                ((NVIC_TypeDef *)0xE000E100)
#define SCB                                 ((SCB_TypeDef *)0xE000E000)
#define SYSTICK                             ((SYSTICK_TypeDef *)0xE000E010)

// SCB_AIRCR
#define SCB_AIRCR_VECTRESET                 (((uint32_t)0x0001) << 0)
#define SCB_AIRCR_VECTCLRACTIVE             (((uint32_t)0x0001) << 1)
#define SCB_AIRCR_SYSRESETREQ               (((uint32_t)0x0001) << 2)
#define SCB_AIRCR_PRIGROUP                  (((uint32_t)0x0007) << 8)
#define SCB_AIRCR_VECTKEY                   (((uint32_t)0xFFFF) << 16)
#define SCB_AIRCR_VECTKEYSTAT               (((uint32_t)0xFFFF) << 16)

// SCB_SCR
#define SCB_SCR_SLEEPONEXIT                 (((uint32_t)0x01) << 1)
#define SCB_SCR_SLEEPDEEP                   (((uint32_t)0x01) << 2)
#define SCB_SCR_SEVONPEND                   (((uint32_t)0x01) << 4)

// SCB_CPACR
#define SCB_CPACR_CP10                      (((uint32_t)0x03) << 20)
#define SCB_CPACR_CP11                      (((uint32_t)0x03) << 22)

// SYSTICK_CSR
#define SYSTICK_CSR_EN                      (((uint32_t)0x01) << 0)
#define SYSTICK_CSR_ENABLE                  (((uint32_t)0x01) << 0)
#define SYSTICK_CSR_IEN                     (((uint32_t)0x01) << 1)
#define SYSTICK_CSR_TICKINT                 (((uint32_t)0x01) << 1)
#define SYSTICK_CSR_CLKSOURCE               (((uint32_t)0x01) << 2)
#define SYSTICK_CSR_COUNTFLAG               (((uint32_t)0x01) << 16)

#define cortex_interrupt_set_priority(i, p) (NVIC->IPR[(i) >> 2] = ((NVIC->IPR[(i) >> 2] & ~(((uint32_t)0xFF) << (((i) & 0x03) << 3))) | (((uint32_t)p) << (((i) & 0x03) << 3))))
#define cortex_interrupt_enable(i)          (NVIC->ISER[(i) >> 5] = (((uint32_t)0x01) << ((i) & 0x1F)))
#define cortex_interrupt_disable(i)         (NVIC->ICER[(i) >> 5] = (((uint32_t)0x01) << ((i) & 0x1F)))
#define cortex_interrupt_clear(i)           (NVIC->ICPR[(i) >> 5] = (((uint32_t)0x01) << ((i) & 0x1F)))
#define cortex_interrupt_set(i)             (NVIC->ISPR[(i) >> 5] = (((uint32_t)0x01) << ((i) & 0x1F)))
#define cortex_interrupts_disable()         do{ __asm__("cpsid f"); __asm__("cpsid i"); }while(0)
#define cortex_interrupts_enable()          do{ __asm__("cpsie f"); __asm__("cpsie i"); }while(0)

#define interrupts_disable()                do{ __asm__("cpsid f"); __asm__("cpsid i"); }while(0)
#define interrupts_enable()                 do{ __asm__("cpsie f"); __asm__("cpsie i"); }while(0)

#define CORTEX_ISR(n)                       _CORTEX_ISR(n)
#define _CORTEX_ISR(n)                      void __attribute__((interrupt)) CORTEX_INTERRUPT_##n##_Handler()

void cortex_bootstrap(void *start) __attribute__ ((noreturn));
void cortex_reboot() __attribute__ ((noreturn));

#endif

