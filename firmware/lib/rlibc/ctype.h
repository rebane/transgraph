#ifndef _CTYPE_H_
#define _CTYPE_H_

#ifdef isupper
#undef isupper
#endif

static inline int isupper(int c){
	return((c >=  'A') && (c <= 'Z'));
}

#ifdef islower
#undef islower
#endif

static inline int islower(int c){
	return((c >=  'a') && (c <= 'z'));
}

#ifdef isalpha
#undef isalpha
#endif

static inline int isalpha(int c){
	return(isupper(c) || islower(c));
}

#ifdef isdigit
#undef isdigit
#endif

static inline int isdigit(int c){
	return((c >= '0') && (c <= '9'));
}

#ifdef isalnum
#undef isalnum
#endif

static inline int isalnum(int c){
	return(isalpha(c) || isdigit(c));
}

#ifdef isascii
#undef isascii
#endif

static inline int isascii(int c){
	return((c > 0) && (c <= 0x7F));
}

#ifdef iscntrl
#undef iscntrl
#endif

static inline int iscntrl(int c){
	return((c >= 0) && ((c <= 0x1F) || (c == 0x7F)));
}

#ifdef isprint
#undef isprint
#endif

static inline int isprint(int c){
	return((c >= ' ') && (c <= '~'));
}

#ifdef isgraph
#undef isgraph
#endif

static inline int isgraph(int c){
	return((c != ' ') && isprint(c));
}

#ifdef ispunct
#undef ispunct
#endif

static inline int ispunct(int c){
	return(((c > ' ') && (c <= '~')) && !isalnum(c));
}

#ifdef isspace
#undef isspace
#endif

static inline int isspace(int c){
	return((c ==  ' ') || (c == '\f') || (c == '\n') || (c == '\r') || (c == '\t') || (c == '\v'));
}

#ifdef isxupper
#undef isxupper
#endif

static inline int isxupper(int c){
	return(isdigit(c) || ((c >= 'A') && (c <= 'F')));
}

#ifdef isxlower
#undef isxlower
#endif

static inline int isxlower(int c){
	return(isdigit(c) || ((c >= 'a') && (c <= 'f')));
}

#ifdef isxdigit
#undef isxdigit
#endif

static inline int isxdigit(int c){
	return(isdigit(c) || ((c >= 'A') && (c <= 'F')) || ((c >= 'a') && (c <= 'f')));
}

#ifdef toupper
#undef toupper
#endif

static inline int toupper(int c){
	return(islower(c) ? (c - 'a' + 'A') : c);
}

#ifdef tolower
#undef tolower
#endif

static inline int tolower(int c){
	return(isupper(c) ? (c - 'A' + 'a') : c);
}

#endif

