/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _OUTPUT_H_
#define _OUTPUT_H_

void output_plot(int def, int bi_ua, int cu_mv_min, int cu_mv_max, int ci_ma_scale);

#endif

