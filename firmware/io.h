/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _IO_H_
#define _IO_H_

#define IO_E_IDLE 0
#define IO_E_HIGH 1
#define IO_E_LOW  2

void io_init();
void io_e(int state);
void io_5v(int on);

#endif

