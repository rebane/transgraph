/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _INPUT_H_
#define _INPUT_H_

void input_plot(int def, int cu_mv, int bu_mv_min, int bu_mv_max, int bi_ua_scale);

#endif

