/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "dac.h"
#include "cortex.h"
#include "nrf51.h"
#include "mask.h"

#define DAC_INCDEC_STEP 50

static int dac_ref_mv;
uint16_t dac_channel_a_raw;
uint16_t dac_channel_b_raw;

static void dac_en(uint8_t level);
static void dac_clr(uint8_t level);
static void dac_ldac(uint8_t level);
static void dac_sync(uint8_t level);
static void dac_mosi(uint8_t level);
static void dac_sck(uint8_t level);
static void dac_spi_send(uint32_t data);
static void dac_command(uint8_t cmd, uint8_t addr, uint16_t data);

void dac_init(int ref_mv)
{
	dac_ref_mv = ref_mv;
	dac_channel_a_raw = dac_channel_b_raw = 0;

	dac_en(0);
	dac_clr(0);
	GPIO->DIR |= GPIO_PIN15 | GPIO_PIN14 | GPIO_PIN10 | GPIO_PIN9 | GPIO_PIN7 | GPIO_PIN5;
	dac_en(0);
	dac_clr(0);
	dac_ldac(1);
	dac_sync(1);
	dac_mosi(0);
	dac_sck(1);
}

void dac_enable(int on)
{
	if (on) {
		dac_sck(1);
		dac_clr(1);
		dac_ldac(0);
		dac_command(4, 0, (0 << 4) | 3);
		dac_command(3, 7, 0);
		dac_en(1);
	} else {
		dac_en(0);
		dac_command(4, 0, (1 << 4) | 3);
		dac_clr(0);
		dac_ldac(1);
		dac_sync(1);
		dac_mosi(0);
		dac_sck(0);
	}
}

void dac_set_raw(uint8_t channel, uint16_t value)
{
	dac_command(3, channel, value);
}

int dac_inc(uint8_t channel)
{
	int d;

	if (channel == DAC_CHANNEL_A) {
		if (dac_channel_a_raw == 65535)
			return 0;

		d = 65535 - dac_channel_a_raw;
		if (d > DAC_INCDEC_STEP)
			d = DAC_INCDEC_STEP;
		dac_command(3, DAC_CHANNEL_A, dac_channel_a_raw + d);
	} else if (channel == DAC_CHANNEL_B) {
		if (dac_channel_b_raw == 65535)
			return 0;

		d = 65535 - dac_channel_b_raw;
		if (d > DAC_INCDEC_STEP)
			d = DAC_INCDEC_STEP;
		dac_command(3, DAC_CHANNEL_B, dac_channel_b_raw + d);
	}
	return 1;
}

int dac_dec(uint8_t channel)
{
	int d;

	if (channel == DAC_CHANNEL_A) {
		if (dac_channel_a_raw == 0)
			return 0;

		d = dac_channel_a_raw;
		if (d > DAC_INCDEC_STEP)
			d = DAC_INCDEC_STEP;
		dac_command(3, DAC_CHANNEL_A, dac_channel_a_raw - d);
	} else if (channel == DAC_CHANNEL_B) {
		if (dac_channel_b_raw == 0)
			return 0;

		d = dac_channel_b_raw;
		if (d > DAC_INCDEC_STEP)
			d = DAC_INCDEC_STEP;
		dac_command(3, DAC_CHANNEL_B, dac_channel_b_raw - d);
	}
	return 1;
}

void dac_set_mv(uint8_t channel, int mv)
{
	dac_set_mv_ref(channel, mv, dac_ref_mv);
}

void dac_set_mv_ref(uint8_t channel, int mv, int ref_mv)
{
	if (mv > ref_mv)
		mv = ref_mv;

	dac_command(3, channel, ((uint32_t)65535 * mv) / ref_mv);
}

static void dac_en(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN5;
	else
		GPIO->OUT &= ~GPIO_PIN5;
}

static void dac_clr(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN7;
	else
		GPIO->OUT &= ~GPIO_PIN7;
}

static void dac_ldac(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN9;
	else
		GPIO->OUT &= ~GPIO_PIN9;
}

static void dac_sync(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN10;
	else
		GPIO->OUT &= ~GPIO_PIN10;
}

static void dac_mosi(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN14;
	else
		GPIO->OUT &= ~GPIO_PIN14;
}

static void dac_sck(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN15;
	else
		GPIO->OUT &= ~GPIO_PIN15;
}

static void dac_spi_send(uint32_t data)
{
	int i;

	dac_sync(0);
	for (i = 23; i >= 0; i--) {
		dac_mosi((data >> i) & 1);
		dac_sck(0);
		dac_sck(1);
	}
	dac_sync(1);
}

static void dac_command(uint8_t cmd, uint8_t addr, uint16_t data)
{
	dac_spi_send((((uint32_t)cmd) << 19) | (((uint32_t)addr) << 16) | data);
	if (cmd == 3) {
		if ((addr == DAC_CHANNEL_A) || (addr == DAC_CHANNEL_AB))
			dac_channel_a_raw = data;
		if ((addr == DAC_CHANNEL_B) || (addr == DAC_CHANNEL_AB))
			dac_channel_b_raw = data;
	}
}

