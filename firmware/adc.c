/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "adc.h"
#include "cortex.h"
#include "nrf51.h"
#include "mask.h"

#define ADS1220_CR0            0
#define ADS1220_CR1            1
#define ADS1220_CR2            2
#define ADS1220_CR3            3

#define ADS1220_CR0_MUX        (((uint8_t)0x0F) << 4)
#define ADS1220_CR0_GAIN       (((uint8_t)0x07) << 1)
#define ADS1220_CR0_PGA_BYPASS (((uint8_t)0x01) << 0)

#define ADS1220_CR1_DR         (((uint8_t)0x07) << 5)
#define ADS1220_CR1_MODE       (((uint8_t)0x03) << 3)
#define ADS1220_CR1_CM         (((uint8_t)0x01) << 2)
#define ADS1220_CR1_TS         (((uint8_t)0x01) << 1)
#define ADS1220_CR1_BCS        (((uint8_t)0x01) << 0)

#define ADS1220_CR2_VREF       (((uint8_t)0x03) << 6)
#define ADS1220_CR2_5060       (((uint8_t)0x03) << 4)
#define ADS1220_CR2_PSW        (((uint8_t)0x01) << 3)
#define ADS1220_CR2_IDAC       (((uint8_t)0x07) << 0)

#define ADS1220_CR3_I1MUX      (((uint8_t)0x07) << 5)
#define ADS1220_CR3_I2MUX      (((uint8_t)0x07) << 2)
#define ADS1220_CR3_DRDYM      (((uint8_t)0x01) << 1)

static int adc_ref_mv;

static void adc_cs(uint8_t level);
static void adc_sck(uint8_t level);
static void adc_mosi(uint8_t level);
static uint8_t adc_drdy();
static uint8_t adc_miso();
static uint8_t adc_spi_transfer(uint8_t byte);
static void __attribute__((used)) adc_reset();
static void adc_start_sync();
static void adc_powerdown();
static uint32_t adc_rdata();
static uint8_t __attribute__((used)) adc_rreg(uint8_t reg);
static void adc_wreg(uint8_t reg, uint8_t value);
static void adc_wait();

void adc_init(int ref_mv)
{
	adc_ref_mv = ref_mv;

	adc_cs(1);
	adc_sck(0);
	adc_mosi(0);
	GPIO->DIR |= GPIO_PIN3 | GPIO_PIN12 | GPIO_PIN13;
	adc_cs(1);
	adc_sck(0);
	adc_mosi(0);

	GPIO->PIN_CNF[2] = 0;
	GPIO->DIR &= ~GPIO_PIN2;
	GPIO->PIN_CNF[2] = 0;

	GPIO->PIN_CNF[1] = 0;
	GPIO->DIR &= ~GPIO_PIN1;
	GPIO->PIN_CNF[1] = 0;
}

void adc_enable(int on)
{
	if (on) {
		adc_wreg(ADS1220_CR1, mask8(ADS1220_CR1_DR, 6) | mask8(ADS1220_CR1_MODE, 2) | mask8(ADS1220_CR1_CM, 0) | mask8(ADS1220_CR1_TS, 0) | mask8(ADS1220_CR1_BCS, 0));
		adc_wreg(ADS1220_CR2, mask8(ADS1220_CR2_VREF, 3) | mask8(ADS1220_CR2_5060, 1) | mask8(ADS1220_CR2_PSW, 0) | mask8(ADS1220_CR2_IDAC, 0));
		adc_wreg(ADS1220_CR3, mask8(ADS1220_CR3_I1MUX, 0) | mask8(ADS1220_CR3_I2MUX, 0) | mask8(ADS1220_CR3_DRDYM, 0));
	} else {
		adc_wreg(ADS1220_CR0, mask8(ADS1220_CR0_MUX, 0) | mask8(ADS1220_CR0_GAIN, 0) | mask8(ADS1220_CR0_PGA_BYPASS, 1));
		adc_wreg(ADS1220_CR1, mask8(ADS1220_CR1_DR, 0) | mask8(ADS1220_CR1_MODE, 0) | mask8(ADS1220_CR1_CM, 0) | mask8(ADS1220_CR1_TS, 0) | mask8(ADS1220_CR1_BCS, 0));
		adc_wreg(ADS1220_CR2, mask8(ADS1220_CR2_VREF, 0) | mask8(ADS1220_CR2_5060, 0) | mask8(ADS1220_CR2_PSW, 1) | mask8(ADS1220_CR2_IDAC, 0));
		adc_wreg(ADS1220_CR3, mask8(ADS1220_CR3_I1MUX, 0) | mask8(ADS1220_CR3_I2MUX, 0) | mask8(ADS1220_CR3_DRDYM, 0));
		adc_powerdown();
	}
}

uint32_t adc_get_raw(uint8_t mux)
{
	adc_wreg(ADS1220_CR2, mask8(ADS1220_CR2_VREF, 3) | mask8(ADS1220_CR2_5060, 1) | mask8(ADS1220_CR2_PSW, 0) | mask8(ADS1220_CR2_IDAC, 0));
	adc_wreg(ADS1220_CR0, mask8(ADS1220_CR0_MUX, mux) | mask8(ADS1220_CR0_GAIN, 0) | mask8(ADS1220_CR0_PGA_BYPASS, 1));
	adc_start_sync();
	adc_wait();
	return adc_rdata();
}

int adc_get_mv(uint8_t mux)
{
	return adc_get_mv_ref(mux, adc_ref_mv);
}

int adc_get_mv_ref(uint8_t mux, int ref_mv)
{
	uint32_t value;

	value = adc_get_raw(mux);
	if (value >= 0x800000) {
		value = 0x1000000 - value;
		return -(((int64_t)ref_mv * value) / 0x800000);
	}
	return (((int64_t)ref_mv * value) / 0x800000);
}

uint32_t adc_vdd_get_raw()
{
	adc_wreg(ADS1220_CR2, mask8(ADS1220_CR2_VREF, 0) | mask8(ADS1220_CR2_5060, 1) | mask8(ADS1220_CR2_PSW, 0) | mask8(ADS1220_CR2_IDAC, 0));
	adc_wreg(ADS1220_CR0, mask8(ADS1220_CR0_MUX, 13) | mask8(ADS1220_CR0_GAIN, 0) | mask8(ADS1220_CR0_PGA_BYPASS, 1));
	adc_start_sync();
	adc_wait();
	return adc_rdata();
}

int adc_vdd_get_mv()
{
	return adc_vdd_get_raw() / 1024;
}

static void adc_cs(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN3;
	else
		GPIO->OUT &= ~GPIO_PIN3;
}

static void adc_sck(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN13;
	else
		GPIO->OUT &= ~GPIO_PIN13;
}

static void adc_mosi(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN12;
	else
		GPIO->OUT &= ~GPIO_PIN12;
}

static uint8_t adc_drdy()
{
	return !!(GPIO->IN & GPIO_PIN2);
}

static uint8_t adc_miso()
{
	return !!(GPIO->IN & GPIO_PIN1);
}

static uint8_t adc_spi_transfer(uint8_t byte)
{
	uint8_t data = 0;
	int i;

	for (i = 7; i >= 0; i--) {
		adc_sck(1);
		if (adc_miso())
			data |= (1 << i);

		adc_mosi((byte >> i) & 1);
		adc_sck(0);
	}

	return data;
}

static void adc_reset()
{
	adc_cs(0);
	adc_spi_transfer(0x06);
	adc_cs(1);
}

static void adc_start_sync()
{
	adc_cs(0);
	adc_spi_transfer(0x08);
	adc_cs(1);
}

static void adc_powerdown()
{
	adc_cs(0);
	adc_spi_transfer(0x02);
	adc_cs(1);
}

static uint32_t adc_rdata()
{
	uint32_t value;

	adc_cs(0);
	adc_spi_transfer(0x10);
	value = ((uint32_t)adc_spi_transfer(0x00)) << 16;
	value |= ((uint32_t)adc_spi_transfer(0x00)) << 8;
	value |= adc_spi_transfer(0x00);
	adc_cs(1);

	return value;
}

static uint8_t adc_rreg(uint8_t reg)
{
	uint8_t value;

	adc_cs(0);
	adc_spi_transfer(0x20 | ((reg & 0x03) << 2));
	value = adc_spi_transfer(0x00);
	adc_cs(1);

	return value;
}

static void adc_wreg(uint8_t reg, uint8_t value)
{
	adc_cs(0);
	adc_spi_transfer(0x40 | ((reg & 0x03) << 2));
	adc_spi_transfer(value);
	adc_cs(1);
}

static void adc_wait()
{
	volatile uint32_t i;

	for (i = 0; i < 100000; i++) {
		if (!adc_drdy())
			break;
	}
}

