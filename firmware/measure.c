/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "measure.h"
#include <stdio.h>
#include "mtimer.h"
#include "dac.h"
#include "adc.h"
#include "io.h"

#define MEASURE_BASE_R      470
#define MEASURE_COLLECTOR_R 22

static int measure_ref_mv;
static int measure_collector_polarity;
static int measure_base_polarity;

void measure_init()
{
	measure_collector_polarity = MEASURE_POLARITY_NORMAL;
	measure_base_polarity = MEASURE_POLARITY_NORMAL;
}

void measure_collector_polarity_set(int polarity)
{
	measure_collector_polarity = polarity;
}

void measure_base_polarity_set(int polarity)
{
	measure_base_polarity = polarity;
}

void measure_enable(int on)
{
	if (on) {
		dac_enable(1);
		measure_ref_mv = adc_vdd_get_mv();
	} else {
		io_e(IO_E_IDLE);
		dac_enable(0);
	}
}

int measure_get_cb_polarity()
{
	int mv;

	dac_set_mv_ref(DAC_CHANNEL_A, 0, measure_ref_mv);
	dac_set_mv_ref(DAC_CHANNEL_B, 1000, measure_ref_mv);
	mtimer_sleep(50);
	mv = adc_get_mv_ref(ADC_MUX_IN0, measure_ref_mv);
	dac_set_mv_ref(DAC_CHANNEL_B, 0, measure_ref_mv);
	if (mv > 13)
		return MEASURE_POLARITY_NORMAL;

	dac_set_mv_ref(DAC_CHANNEL_A, 1000, measure_ref_mv);
	mtimer_sleep(50);
	mv = adc_get_mv_ref(ADC_MUX_IN3, measure_ref_mv);
	dac_set_mv_ref(DAC_CHANNEL_A, 0, measure_ref_mv);
	if (mv > 300)
		return MEASURE_POLARITY_REVERSED;

	return MEASURE_POLARITY_ERROR;
}

int measure_ref_get_mv()
{
	return measure_ref_mv;
}

int measure_collector_get_mv()
{
	int mv;

	mv = adc_get_mv_ref(ADC_MUX_IN0, measure_ref_mv);
	if (measure_collector_polarity == MEASURE_POLARITY_NORMAL)
		return mv;

	return measure_ref_mv - mv;
}

int measure_base_get_mv()
{
	int mv;

	mv = adc_get_mv_ref(ADC_MUX_IN3, measure_ref_mv);
	if (measure_base_polarity == MEASURE_POLARITY_NORMAL)
		return mv;

	return measure_ref_mv - mv;
}

int measure_collector_get_ma()
{
	int mv;

	mv = adc_get_mv_ref(ADC_MUX_IN01, measure_ref_mv);
	if (mv < 0)
		mv = -mv;

	return mv / MEASURE_COLLECTOR_R;
}

int measure_base_get_ua()
{
	int mv;

	mv = adc_get_mv_ref(ADC_MUX_IN23, measure_ref_mv);
	if (mv < 0)
		mv = -mv;

	return (mv * 1000) / MEASURE_BASE_R;
}

void measure_collector_vdd_set_mv(int mv)
{
	if (measure_collector_polarity == MEASURE_POLARITY_NORMAL)
		dac_set_mv_ref(DAC_CHANNEL_A, mv, measure_ref_mv);
	else
		dac_set_mv_ref(DAC_CHANNEL_A, measure_ref_mv - mv, measure_ref_mv);
}

void measure_base_vdd_set_mv(int mv)
{
	if (measure_base_polarity == MEASURE_POLARITY_NORMAL)
		dac_set_mv_ref(DAC_CHANNEL_B, mv, measure_ref_mv);
	else
		dac_set_mv_ref(DAC_CHANNEL_B, measure_ref_mv - mv, measure_ref_mv);
}

extern uint16_t dac_channel_a_raw;
extern uint16_t dac_channel_b_raw;

int measure_collector_set_mv(int mv)
{
	measure_collector_vdd_set_mv(mv);
	return measure_collector_trim_mv(mv);
}

int measure_base_set_mv(int mv)
{
	measure_base_vdd_set_mv(mv);
	return measure_base_trim_mv(mv);
}

int measure_collector_trim_mv(int mv)
{
	if (measure_collector_get_mv() < mv) {
		while (measure_collector_get_mv() < mv) {
			if (measure_collector_polarity == MEASURE_POLARITY_NORMAL) {
				if (!dac_inc(DAC_CHANNEL_A))
					return 0;
			} else {
				if (!dac_dec(DAC_CHANNEL_A))
					return 0;
			}
		}
	} else {
		while (measure_collector_get_mv() > mv) {
			if (measure_collector_polarity == MEASURE_POLARITY_NORMAL) {
				if (!dac_dec(DAC_CHANNEL_A))
					return 0;
			} else {
				if (!dac_inc(DAC_CHANNEL_A))
					return 0;
			}
		}
	}
	return 1;
}

int measure_base_trim_mv(int mv)
{
	if (measure_base_get_mv() < mv) {
		while (measure_base_get_mv() < mv) {
			if (measure_base_polarity == MEASURE_POLARITY_NORMAL) {
				if (!dac_inc(DAC_CHANNEL_B))
					return 0;
			} else {
				if (!dac_dec(DAC_CHANNEL_B))
					return 0;
			}
		}
	} else {
		while (measure_base_get_mv() > mv) {
			if (measure_base_polarity == MEASURE_POLARITY_NORMAL) {
				if (!dac_dec(DAC_CHANNEL_B))
					return 0;
			} else {
				if (!dac_inc(DAC_CHANNEL_B))
					return 0;
			}
		}
	}
	return 1;
}

int measure_collector_set_ma(int ma)
{
	int mv, r;

	mv = ma * MEASURE_COLLECTOR_R;
	measure_collector_vdd_set_mv(mv);
	r = mv / measure_collector_get_ma();
	if (r > MEASURE_COLLECTOR_R)
		r = MEASURE_COLLECTOR_R + (((r - MEASURE_COLLECTOR_R) * 2) / 3);
	else
		r = MEASURE_COLLECTOR_R - (((MEASURE_COLLECTOR_R - r) * 2) / 3);

	mv = ma * r;
	measure_collector_vdd_set_mv(mv);
	return measure_collector_trim_ma(ma);
}

int measure_base_set_ua(int ua)
{
	int mv, r;

	mv = (ua * MEASURE_BASE_R) / 1000;
	measure_base_vdd_set_mv(mv);
	r = (mv * 1000) / measure_base_get_ua();
	if (r > MEASURE_BASE_R)
		r = MEASURE_BASE_R + (((r - MEASURE_BASE_R) * 2) / 3);
	else
		r = MEASURE_BASE_R - (((MEASURE_BASE_R - r) * 2) / 3);

	mv = (ua * r) / 1000;
	measure_base_vdd_set_mv(mv);
	return measure_base_trim_ua(ua);
}

int measure_collector_trim_ma(int ma)
{
	if (measure_collector_get_ma() < ma) {
		while (measure_collector_get_ma() < ma) {
			if (measure_collector_polarity == MEASURE_POLARITY_NORMAL) {
				if (!dac_inc(DAC_CHANNEL_A))
					return 0;
			} else {
				if (!dac_dec(DAC_CHANNEL_A))
					return 0;
			}
		}
	} else {
		while (measure_collector_get_ma() > ma) {
			if (measure_collector_polarity == MEASURE_POLARITY_NORMAL) {
				if (!dac_dec(DAC_CHANNEL_A))
					return 0;
			} else {
				if (!dac_inc(DAC_CHANNEL_A))
					return 0;
			}
		}
	}
	return 1;
}

int measure_base_trim_ua(int ua)
{
	if (measure_base_get_ua() < ua) {
		while (measure_base_get_ua() < ua) {
			if (measure_base_polarity == MEASURE_POLARITY_NORMAL) {
				if (!dac_inc(DAC_CHANNEL_B))
					return 0;
			} else {
				if (!dac_dec(DAC_CHANNEL_B))
					return 0;
			}
		}
	} else {
		while (measure_base_get_ua() > ua) {
			if (measure_base_polarity == MEASURE_POLARITY_NORMAL) {
				if (!dac_dec(DAC_CHANNEL_B))
					return 0;
			} else {
				if (!dac_inc(DAC_CHANNEL_B))
					return 0;
			}
		}
	}
	return 1;
}

