/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _TIMER_H_
#define _TIMER_H_

#include <stdint.h>

void timer_init();
uint32_t timer_get(uint16_t *msec);
void timer_buzzer_set(uint16_t msec);

#endif

