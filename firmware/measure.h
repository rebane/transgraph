/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _MEASURE_H_
#define _MEASURE_H_

#define MEASURE_POLARITY_ERROR    (-1)
#define MEASURE_POLARITY_NORMAL   0
#define MEASURE_POLARITY_REVERSED 1

void measure_init();
void measure_collector_polarity_set(int polarity);
void measure_base_polarity_set(int polarity);
void measure_enable(int on);
int measure_get_cb_polarity();
int measure_ref_get_mv();
int measure_collector_get_mv();
int measure_base_get_mv();
int measure_collector_get_ma();
int measure_base_get_ua();
void measure_collector_vdd_set_mv(int mv);
void measure_base_vdd_set_mv(int mv);
int measure_collector_set_mv(int mv);
int measure_base_set_mv(int mv);
int measure_collector_trim_mv(int mv);
int measure_base_trim_mv(int mv);
int measure_collector_set_ma(int ma);
int measure_base_set_ua(int ua);
int measure_collector_trim_ma(int ma);
int measure_base_trim_ua(int ua);

#endif

