/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include <stdint.h>
#include <stdio.h>
#include "cortex.h"
#include "nrf51.h"
#include "mask.h"
#include "platform.h"
#include "timer.h"
#include "serial.h"
#include "bluetooth.h"
#include "mtimer.h"
#include "softdevice.h"
#include "ssd1309.h"
#include "button.h"
#include "dac.h"
#include "adc.h"
#include "io.h"
#include "measure.h"
#include "input.h"
#include "output.h"
#include "beta.h"

static int main_bluetooth_connected;

void oled_refresh();
void splash();

UICR_Header_TypeDef __uicr __attribute__((section(".uicr"))) = {
	.CLENR0 = 0x00020000,
	.RBPCONF = 0xFFFF0000,
	.XTALFREQ = 0xFFFFFFFF,
	.RESERVED = 0xFFFFFFFF,
	.FWID = 0x1234,
	.BOOTLOADERADDR = 0xFFFFFFFF,
};

int main()
{
	mtimer_t t, p, o;
	uint8_t x, b0;
	uint16_t a, b, c, s;

	main_bluetooth_connected = 0;
	platform_init();
	timer_init();
	mtimer_init(timer_get);
	serial_init();
	interrupts_enable();

	mtimer_sleep(200);
	POWER->RESET = POWER_RESET_RESET; /* ei tea kas seda võib ette poole tõsta?!!!??!!! */

	ssd1309_init();
	button_init();
	dac_init(5000);
	adc_init(5000);
	io_init();
	measure_init();

	// pwr
	GPIO->DIR |= GPIO_PIN17;
	GPIO->OUT |= GPIO_PIN17;
	timer_buzzer_set(50);

	bluetooth_init();

	splash();

	io_5v(1);
	mtimer_sleep(100);
	dac_enable(1);
	adc_enable(1);

	bluetooth_advertise_set();
	bluetooth_start();

	mtimer_timeout_clear(&t);
	mtimer_timeout_set(&p, 3000);
	mtimer_timeout_set(&o, 120000);
	b0 = 1;

	while (1) {
		if (mtimer_timeout(&t)) {
			bluetooth_data_uptime(mtimer_get(NULL));
			printf("NRF51 UPTIME: %lu, ADC1-1: %d, ADC1-2: %d, ADC2-1: %d, ADC2-2: %d, VDD: %d\n", (unsigned long int)mtimer_get(NULL),
				(int)adc_get_mv(ADC_MUX_IN1),
				(int)adc_get_mv(ADC_MUX_IN0),
				(int)adc_get_mv(ADC_MUX_IN2),
				(int)adc_get_mv(ADC_MUX_IN3),
				(int)adc_vdd_get_mv()
			);
//			printf("1) %d\n", adc_get_mv(ADC_MUX_IN23));
//			printf("2) %d\n", adc_get_mv(ADC_MUX_IN32));
			mtimer_timeout_add(&t, 1000);
		}
		if (bluetooth_connected()) {
			if (!main_bluetooth_connected) {
				mtimer_timeout_set(&o, 600000);
				main_bluetooth_connected = 1;
				oled_refresh();
				timer_buzzer_set(30);
			}
		} else {
			if (main_bluetooth_connected) {
				mtimer_timeout_set(&o, 120000);
				main_bluetooth_connected = 0;
				oled_refresh();
				timer_buzzer_set(30);
			}
		}
		if (serial_read(&x, 1) == 1) {
			if (x == 'x') {
				printf("ASD\n");
			} else if (x == '0') {
				io_e(IO_E_IDLE);
				printf("IO E IDLE\n");
			} else if (x == '1') {
				io_e(IO_E_HIGH);
				printf("IO E HIGH\n");
			} else if (x == '2') {
				io_e(IO_E_LOW);
				printf("IO E LOW\n");
			} else if (x == 'i') {
				printf("INPUT\n");
				input_plot(1, 0, 0, 0, 0);
				bluetooth_command_flush();
			} else if (x == 'o') {
				printf("OUTPUT\n");
				output_plot(1, 0, 0, 0, 0);
				bluetooth_command_flush();
			} else if (x == 'b') {
				printf("BETA\n");
				beta_plot(1, 0, 0, 0, 0);
				bluetooth_command_flush();
			} else if (x == 'p') {
				measure_enable(1);
				printf("POLARITY: %d\n", measure_get_cb_polarity());
			} else if (x == 'P') {
				GPIO->OUT &= ~GPIO_PIN17;
			} else if (x == 't') {
				// dac_set(DAC_CHANNEL_B, 65535);
				dac_set_mv_ref(DAC_CHANNEL_B, 4000, adc_vdd_get_mv());
				printf("DAC A HIGH\n");
			} else if (x == 'T') {
				// dac_set(DAC_CHANNEL_B, 0);
				dac_set_mv_ref(DAC_CHANNEL_B, 0, adc_vdd_get_mv());
				printf("DAC A LOW\n");
			} else if (x == 'y') {
				// dac_set(DAC_CHANNEL_B, 32767);
				dac_set_mv_ref(DAC_CHANNEL_B, 2000, adc_vdd_get_mv());
				printf("DAC A MID\n");
			}
		}
		if (mtimer_timeout(&o)) {
			timer_buzzer_set(300);
			ssd1309_cls();
			ssd1309_refresh();
			mtimer_sleep(300);
			GPIO->OUT &= ~GPIO_PIN17;
			while (button_get(0));
			cortex_reboot();
		}
		if (button_get(0)) {
			if (!b0) {
				mtimer_timeout_set(&o, 120000);
				timer_buzzer_set(30);
				splash();
			}

			b0 = 1;
			if (mtimer_timeout(&p)) {
				timer_buzzer_set(300);
				ssd1309_cls();
				ssd1309_refresh();
				mtimer_sleep(300);
				GPIO->OUT &= ~GPIO_PIN17;
				while (button_get(0));
				cortex_reboot();
			}
		} else {
			mtimer_timeout_set(&p, 3000);
			b0 = 0;
		}
		if (button_get(1)) {
			mtimer_timeout_set(&o, 120000);
			timer_buzzer_set(30);
			printf("INPUT\n");
			input_plot(1, 0, 0, 0, 0);
			bluetooth_command_flush();
			timer_buzzer_set(30);
		} else if (button_get(2)) {
			mtimer_timeout_set(&o, 120000);
			timer_buzzer_set(30);
			printf("OUTPUT\n");
			output_plot(1, 0, 0, 0, 0);
			bluetooth_command_flush();
			timer_buzzer_set(30);
		} else if (button_get(3)) {
			mtimer_timeout_set(&o, 120000);
			timer_buzzer_set(30);
			printf("BETA\n");
			beta_plot(1, 0, 0, 0, 0);
			bluetooth_command_flush();
			timer_buzzer_set(30);
		}
		if (bluetooth_command_get_start_input(&a, &b, &c, &s)) {
			mtimer_timeout_set(&o, 600000);
			timer_buzzer_set(30);
			printf("INPUT\n");
			input_plot(0, a, b, c, s);
			bluetooth_command_flush();
			timer_buzzer_set(30);
		}
		if (bluetooth_command_get_start_output(&a, &b, &c, &s)) {
			mtimer_timeout_set(&o, 600000);
			timer_buzzer_set(30);
			printf("OUTPUT\n");
			output_plot(0, a, b, c, s);
			bluetooth_command_flush();
			timer_buzzer_set(30);
		}
		if (bluetooth_command_get_start_beta(&a, &b, &c, &s)) {
			mtimer_timeout_set(&o, 600000);
			timer_buzzer_set(30);
			printf("BETA\n");
			beta_plot(0, a, b, c, s);
			bluetooth_command_flush();
			timer_buzzer_set(30);
		}
	}
}

void splash()
{
	uint8_t mac[6];
	char buffer[4];
	int i;

	softdevice_ble_gap_addr_get(mac);

	ssd1309_cls();
	ssd1309_puts(36, 0, "Taltech", 1);
	ssd1309_puts(17, 20, "Veiko R\01tter", 1);
	ssd1309_puts(49, 30, "2022", 1);

	for (i = 0; i < 6; i++) {
		snprintf(buffer, 3, "%02X", (unsigned int)mac[i]);
		buffer[2] = 0;
		ssd1309_puts(6 + (i * 20), 55, buffer, 1);
	}

	for (i = 0; i < 5; i++) {
		ssd1309_pset(23 + (i * 20), 56, 1);
		ssd1309_pset(23 + (i * 20), 60, 1);
	}

	oled_refresh();
}

void oled_refresh()
{
	if (main_bluetooth_connected)
		ssd1309_puts(118, 4, "\x03", 1);
	else
		ssd1309_puts(118, 4, "\x03", 0);

	ssd1309_refresh();
}

ssize_t write_stdout(const void *buf, size_t count)
{
	size_t i;

	for (i = 0; i < count; i++) {
		while (serial_write(&(((uint8_t *)buf)[i]), 1) != 1);
	}
	return count;
}

