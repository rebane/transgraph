/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "io.h"
#include "cortex.h"
#include "nrf51.h"
#include "mask.h"

void io_init()
{
	GPIO->OUT &= ~GPIO_PIN8;
	GPIO->OUT &= ~GPIO_PIN11;
	GPIO->DIR |= GPIO_PIN8;
	GPIO->DIR |= GPIO_PIN11;
	GPIO->OUT &= ~GPIO_PIN8;
	GPIO->OUT &= ~GPIO_PIN11;

	GPIO->OUT &= ~GPIO_PIN16;
	GPIO->DIR |= GPIO_PIN16;
	GPIO->OUT &= ~GPIO_PIN16;
}

void io_e(int state)
{
	if (state == IO_E_IDLE) {
		GPIO->OUT &= ~GPIO_PIN8;
		GPIO->OUT &= ~GPIO_PIN11;
	} else if (state == IO_E_HIGH) {
		GPIO->OUT |= GPIO_PIN8;
		GPIO->OUT &= ~GPIO_PIN11;
	} else if (state == IO_E_LOW) {
		GPIO->OUT |= GPIO_PIN11;
	}
}

void io_5v(int on)
{
	if (on)
		GPIO->OUT |= GPIO_PIN16;
	else
		GPIO->OUT &= ~GPIO_PIN16;
}

