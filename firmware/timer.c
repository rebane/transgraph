/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "timer.h"
#include "cortex.h"
#include "nrf51.h"
#include "mask.h"

static volatile uint32_t timer_sec;
static volatile uint16_t timer_msec;
static volatile uint16_t timer_buzzer;

void timer_init()
{
	cortex_interrupt_disable(TIMER1_INT);

	timer_sec = 0;
	timer_msec = 0;
	timer_buzzer = 0;

	GPIO->OUT &= ~GPIO_PIN0;
	GPIO->DIR |= GPIO_PIN0;
	GPIO->OUT &= ~GPIO_PIN0;

	TIMER1->STOP = 1;
	TIMER1->SHORTS = TIMER_COMPARE0_CLEAR;
	mask32_set(TIMER1->BITMODE, TIMER_BITMODE, 1);
	TIMER1->CC0 = 125;
	TIMER1->PRESCALER = 7;
	TIMER1->MODE = 0;
	TIMER1->INTENSET = TIMER_COMPARE0;
	TIMER1->START = 1;

	cortex_interrupt_set_priority(TIMER1_INT, 3);
	cortex_interrupt_clear(TIMER1_INT);
	cortex_interrupt_enable(TIMER1_INT);
}

uint32_t timer_get(uint16_t *msec)
{
	uint32_t sec;

	TIMER1->INTENCLR = TIMER_COMPARE0;
	sec = timer_sec;
	*msec = timer_msec;
	TIMER1->INTENSET = TIMER_COMPARE0;

	return sec;
}

void timer_buzzer_set(uint16_t msec)
{
	timer_buzzer = msec;
}

CORTEX_ISR(TIMER1_INT)
{
	timer_msec++;
	if (timer_msec == 1000) {
		timer_sec++;
		timer_msec = 0;
	}
	if (timer_buzzer) {
		timer_buzzer--;
		GPIO->OUT |= GPIO_PIN0;
	} else {
		GPIO->OUT &= ~GPIO_PIN0;
	}
	TIMER1->COMPARE0 = 0;
	cortex_interrupt_clear(TIMER1_INT);
}

