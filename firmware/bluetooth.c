/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "bluetooth.h"
#include <stdio.h>
#include "cortex.h"
#include "nrf51.h"
#include "platform.h"
#include "mtimer.h"
#include "softdevice.h"

// ble-service:     95b618a7-119a-495a-ae96-24a401edaf36
// ble-commands:    95b618a8-119a-495a-ae96-24a401edaf36
// ble-status:      95b618a9-119a-495a-ae96-24a401edaf36

/*
gatttool -t random -I -b FA:04:E7:E5:3F:37
char-write-req 0x0011 0200
char-write-req 0x000e 0100000000000000
*/

static char *bluetooth_uuid_base = "\x95\xb6\x18\xa7\x11\x9a\x49\x5a\xae\x96\x24\xa4\x01\xed\xaf\x36";
static uint8_t bluetooth_uuid_type;
static uint16_t bluetooth_conn_handle;
static uint16_t bluetooth_ble_service_handle;
static uint16_t bluetooth_value_handle_cmd, bluetooth_value_handle_data;
static uint8_t bluetooth_value_command[20];
static volatile uint8_t bluetooth_value_command_len;
static uint8_t bluetooth_value_data[20];
static uint8_t bluetooth_transaction_id;

static void bluetooth_data(int try, uint8_t event, uint8_t len);
static void bluetooth_evt(ble_evt_t *evt, uint16_t len);

void bluetooth_init()
{
	bluetooth_conn_handle = BLE_CONN_HANDLE_INVALID;
	bluetooth_value_command_len = 0;
	bluetooth_value_data[0] = 0;
	bluetooth_transaction_id = 1;

	softdevice_nvic_enable_irq(SWI2_INT);
	softdevice_ble_enable(1);
	softdevice_ble_gap_device_name_set(0, 0, "TRANSGRAPH", 10);
	softdevice_ble_gap_ppcp_set(8, 16, 0, 400);
	softdevice_ble_uuid_vs_add(bluetooth_uuid_base, &bluetooth_uuid_type);

	// ble service
	softdevice_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, 0x18A7, bluetooth_uuid_type, &bluetooth_ble_service_handle);
	//   ble commands
	softdevice_ble_gatts_characteristic_add_simple(bluetooth_ble_service_handle, 0x18A8, bluetooth_uuid_type, SOFTDEVICE_CHAR_PROPS_WRITE, SOFTDEVICE_VALUE_PROPS_WRITE, "", 20, &bluetooth_value_handle_cmd);
	//   ble data
	softdevice_ble_gatts_characteristic_add_user(bluetooth_ble_service_handle, 0x18A9, bluetooth_uuid_type, SOFTDEVICE_CHAR_PROPS_INDICATE | SOFTDEVICE_CHAR_PROPS_NOTIFY, SOFTDEVICE_VALUE_PROPS_READ, bluetooth_value_data, 0, 20, &bluetooth_value_handle_data);
}

void bluetooth_advertise_set()
{
	uint8_t buffer_adv[32], buffer_scan[32];

	buffer_adv[0] = 0x02;
	buffer_adv[1] = 0x01;
	buffer_adv[2] = 0x06;
	buffer_adv[3] = 11;
	buffer_adv[4] = 0x09;
	buffer_adv[5] = 'T';
	buffer_adv[6] = 'R';
	buffer_adv[7] = 'A';
	buffer_adv[8] = 'N';
	buffer_adv[9] = 'S';
	buffer_adv[10] = 'G';
	buffer_adv[11] = 'R';
	buffer_adv[12] = 'A';
	buffer_adv[13] = 'P';
	buffer_adv[14] = 'H';

	buffer_scan[0] = 17;
	buffer_scan[1] = 0x21;
	buffer_scan[2] = bluetooth_uuid_base[15];
	buffer_scan[3] = bluetooth_uuid_base[14];
	buffer_scan[4] = bluetooth_uuid_base[13];
	buffer_scan[5] = bluetooth_uuid_base[12];
	buffer_scan[6] = bluetooth_uuid_base[11];
	buffer_scan[7] = bluetooth_uuid_base[10];
	buffer_scan[8] = bluetooth_uuid_base[9];
	buffer_scan[9] = bluetooth_uuid_base[8];
	buffer_scan[10] = bluetooth_uuid_base[7];
	buffer_scan[11] = bluetooth_uuid_base[6];
	buffer_scan[12] = bluetooth_uuid_base[5];
	buffer_scan[13] = bluetooth_uuid_base[4];
	buffer_scan[14] = bluetooth_uuid_base[3];
	buffer_scan[15] = bluetooth_uuid_base[2];
	buffer_scan[16] = bluetooth_uuid_base[1];
	buffer_scan[17] = bluetooth_uuid_base[0];

	softdevice_ble_gap_adv_data_set(buffer_adv, 15, buffer_scan, 18);
}

void bluetooth_start()
{
	softdevice_ble_gap_adv_start(BLE_GAP_ADV_TYPE_ADV_IND, BLE_GAP_ADV_FP_ANY, 1000, 0);
}

void bluetooth_command_flush()
{
	bluetooth_value_command_len = 0;
}

int bluetooth_command_get_start_input(uint16_t *cu_mv, uint16_t *bu_mv_min, uint16_t *bu_mv_max, uint16_t *bi_ua_scale)
{
	if ((bluetooth_value_command_len != 10) || (bluetooth_value_command[0] != BLUETOOTH_EVENT_START))
		return 0;

	if (bluetooth_value_command[1] != BLUETOOTH_START_INPUT)
		return 0;

	*cu_mv = ((uint16_t)(bluetooth_value_command[2] << 8)) | bluetooth_value_command[3];
	*bu_mv_min = ((uint16_t)(bluetooth_value_command[4] << 8)) | bluetooth_value_command[5];
	*bu_mv_max = ((uint16_t)(bluetooth_value_command[6] << 8)) | bluetooth_value_command[7];
	*bi_ua_scale = ((uint16_t)(bluetooth_value_command[8] << 8)) | bluetooth_value_command[9];
	bluetooth_value_command_len = 0;
	return 1;
}

int bluetooth_command_get_start_output(uint16_t *bi_ua, uint16_t *cu_mv_min, uint16_t *cu_mv_max, uint16_t *ci_ma_scale)
{
	if ((bluetooth_value_command_len != 10) || (bluetooth_value_command[0] != BLUETOOTH_EVENT_START))
		return 0;

	if (bluetooth_value_command[1] != BLUETOOTH_START_OUTPUT)
		return 0;

	*bi_ua = ((uint16_t)(bluetooth_value_command[2] << 8)) | bluetooth_value_command[3];
	*cu_mv_min = ((uint16_t)(bluetooth_value_command[4] << 8)) | bluetooth_value_command[5];
	*cu_mv_max = ((uint16_t)(bluetooth_value_command[6] << 8)) | bluetooth_value_command[7];
	*ci_ma_scale = ((uint16_t)(bluetooth_value_command[8] << 8)) | bluetooth_value_command[9];
	bluetooth_value_command_len = 0;
	return 1;
}

int bluetooth_command_get_start_beta(uint16_t *cu_mv, uint16_t *bi_ua_min, uint16_t *bi_ua_max, uint16_t *ci_ma_scale)
{
	if ((bluetooth_value_command_len != 10) || (bluetooth_value_command[0] != BLUETOOTH_EVENT_START))
		return 0;

	if (bluetooth_value_command[1] != BLUETOOTH_START_BETA)
		return 0;

	*cu_mv = ((uint16_t)(bluetooth_value_command[2] << 8)) | bluetooth_value_command[3];
	*bi_ua_min = ((uint16_t)(bluetooth_value_command[4] << 8)) | bluetooth_value_command[5];
	*bi_ua_max = ((uint16_t)(bluetooth_value_command[6] << 8)) | bluetooth_value_command[7];
	*ci_ma_scale = ((uint16_t)(bluetooth_value_command[8] << 8)) | bluetooth_value_command[9];
	bluetooth_value_command_len = 0;
	return 1;
}

void bluetooth_data_uptime(uint32_t uptime)
{
	bluetooth_value_data[2] = (uptime >> 24) & 0xFF;
	bluetooth_value_data[3] = (uptime >> 16) & 0xFF;
	bluetooth_value_data[4] = (uptime >> 8) & 0xFF;
	bluetooth_value_data[5] = (uptime >> 0) & 0xFF;

	bluetooth_data(0, BLUETOOTH_EVENT_UPTIME, 4);
}

void bluetooth_data_started(uint8_t type, uint16_t size, uint16_t a, uint16_t b, uint16_t c, uint16_t s)
{
	bluetooth_transaction_id++;
	if (bluetooth_transaction_id == 0)
		bluetooth_transaction_id = 1;

	bluetooth_value_data[2] = bluetooth_transaction_id;
	bluetooth_value_data[3] = type;
	bluetooth_value_data[4] = (size >> 8) & 0xFF;
	bluetooth_value_data[5] = (size >> 0) & 0xFF;
	bluetooth_value_data[6] = (a >> 8) & 0xFF;
	bluetooth_value_data[7] = (a >> 0) & 0xFF;
	bluetooth_value_data[8] = (b >> 8) & 0xFF;
	bluetooth_value_data[9] = (b >> 0) & 0xFF;
	bluetooth_value_data[10] = (c >> 8) & 0xFF;
	bluetooth_value_data[11] = (c >> 0) & 0xFF;
	bluetooth_value_data[12] = (s >> 8) & 0xFF;
	bluetooth_value_data[13] = (s >> 0) & 0xFF;
	bluetooth_data(1, BLUETOOTH_EVENT_STARTED, 12);
}

void bluetooth_data_error(uint8_t error)
{
	bluetooth_value_data[2] = bluetooth_transaction_id;
	bluetooth_value_data[3] = error;
	bluetooth_data(1, BLUETOOTH_EVENT_ERROR, 2);
}

void bluetooth_data_transistor_type(uint8_t transistor_type)
{
	bluetooth_value_data[2] = bluetooth_transaction_id;
	bluetooth_value_data[3] = transistor_type;
	bluetooth_data(1, BLUETOOTH_EVENT_TRANSISTOR_TYPE, 2);
}

void bluetooth_data_data(uint16_t index, uint8_t status, uint16_t a, uint16_t b, uint16_t c)
{
	bluetooth_value_data[2] = bluetooth_transaction_id;
	bluetooth_value_data[3] = (index >> 8) & 0xFF;
	bluetooth_value_data[4] = (index >> 0) & 0xFF;
	bluetooth_value_data[5] = status;
	bluetooth_value_data[6] = (a >> 8) & 0xFF;
	bluetooth_value_data[7] = (a >> 0) & 0xFF;
	bluetooth_value_data[8] = (b >> 8) & 0xFF;
	bluetooth_value_data[9] = (b >> 0) & 0xFF;
	bluetooth_value_data[10] = (c >> 8) & 0xFF;
	bluetooth_value_data[11] = (c >> 0) & 0xFF;

	bluetooth_data(1, BLUETOOTH_EVENT_DATA, 10);
}

void bluetooth_data_stopped()
{
	bluetooth_value_data[2] = bluetooth_transaction_id;
	bluetooth_data(1, BLUETOOTH_EVENT_STOPPED, 1);
}

int bluetooth_connected()
{
	return (bluetooth_conn_handle != BLE_CONN_HANDLE_INVALID);
}

static void bluetooth_data(int try, uint8_t event, uint8_t len)
{
	uint32_t e;
	uint8_t t;
	int i;

	bluetooth_value_data[0]++;
	bluetooth_value_data[1] = event;
	if (softdevice_ble_gatts_value_is_notification_enabled(bluetooth_conn_handle, bluetooth_value_handle_data))
		t = BLE_GATT_HVX_NOTIFICATION;
	else
		t = BLE_GATT_HVX_INDICATION;

	for (i = 0; i < 16; i++) {
		e = softdevice_ble_gatts_hvx(bluetooth_conn_handle, bluetooth_value_handle_data, t, bluetooth_value_data, 2 + len, 0);
		if (try && (e == 17))
			mtimer_sleep(10);
		else
			break;
	}
}

static void bluetooth_evt(ble_evt_t *evt, uint16_t len)
{
	uint8_t value_len;

	if (evt->header.evt_id == BLE_GAP_EVT_CONNECTED) {
		bluetooth_conn_handle = evt->evt.gap_evt.conn_handle;
		softdevice_ble_gatts_sys_attr_set(bluetooth_conn_handle, NULL, 0);
	} else if (evt->header.evt_id == BLE_GAP_EVT_DISCONNECTED) {
		bluetooth_conn_handle = BLE_CONN_HANDLE_INVALID;
		softdevice_ble_gap_adv_start(BLE_GAP_ADV_TYPE_ADV_IND, BLE_GAP_ADV_FP_ANY, 1000, 0);
	} else if (evt->header.evt_id == BLE_GATTS_EVT_WRITE) {
		if (evt->evt.gatts_evt.params.write.handle == bluetooth_value_handle_cmd) {
			value_len = evt->evt.gatts_evt.params.write.len;
			if ((evt->evt.gatts_evt.params.write.offset == 0) && (value_len >= 2)) {
				if (value_len > 20)
					value_len = 20;

				memcpy(bluetooth_value_command, evt->evt.gatts_evt.params.write.data, value_len);
				bluetooth_value_command_len = value_len;
			}
		}
	} else if (evt->header.evt_id == BLE_GAP_EVT_RSSI_CHANGED) {
		bluetooth_conn_handle = evt->evt.gap_evt.conn_handle;
	}
}

CORTEX_ISR(SWI2_INT)
{
	softdevice_evt_t *evt;

	while ((evt = softdevice_evt_get()) != NULL) {
		if(evt->type == SOFTDEVICE_EVENT_TYPE_BLE){
			bluetooth_evt(evt->ble_evt, evt->ble_evt_len);
		}
	}
	softdevice_nvic_clear_pending_irq(SWI2_INT);
}

