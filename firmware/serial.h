/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <stdint.h>

void serial_init();
int16_t serial_read(void *buf, uint16_t count);
int16_t serial_write(const void *buf, uint16_t count);

#endif

