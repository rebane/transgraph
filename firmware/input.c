/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "input.h"
#include <stdio.h>
#include "mtimer.h"
#include "ssd1309.h"
#include "bluetooth.h"
#include "io.h"
#include "measure.h"

void oled_refresh();

static int input_cu_mv;
static int input_bu_mv_min;
static int input_bu_mv_max;
static int input_bi_ua_scale;
static int input_initialized = 0;

void input_plot(int def, int cu_mv, int bu_mv_min, int bu_mv_max, int bi_ua_scale)
{
	int bu_mv, bi_ua, p;
	int cerr, berr;
	int i;

	ssd1309_cls();
	ssd1309_puts(4, 4, "INPUT", 1);
	ssd1309_puts(118, 4, "\x03", 1);
	measure_enable(1);
	measure_collector_polarity_set(MEASURE_POLARITY_NORMAL);
	measure_base_polarity_set(MEASURE_POLARITY_NORMAL);
	p = measure_get_cb_polarity();
	if (p == MEASURE_POLARITY_ERROR) {
		measure_enable(0);
		ssd1309_puts(64, 4, "ERROR", 1);
		oled_refresh();
	} else if (p == MEASURE_POLARITY_REVERSED) {
		ssd1309_puts(64, 4, "PNP", 1);
		measure_base_polarity_set(p);
		measure_base_vdd_set_mv(0);
		measure_collector_polarity_set(p);
		measure_collector_vdd_set_mv(0);
		io_e(IO_E_HIGH);
	} else {
		ssd1309_puts(64, 4, "NPN", 1);
		io_e(IO_E_LOW);
	}

	if (def) {
		if (input_initialized) {
			cu_mv = input_cu_mv;
			bu_mv_min = input_bu_mv_min;
			bu_mv_max = input_bu_mv_max;
			bi_ua_scale = input_bi_ua_scale;
		} else {
			if (p == MEASURE_POLARITY_NORMAL) {
				cu_mv = 300;
				bu_mv_min = 750;
				bu_mv_max = 980; // measure_ref_get_mv() - 3000;
			} else {
				cu_mv = 1000;
				bu_mv_min = 0;
				bu_mv_max = 700; // measure_ref_get_mv() - 3000;
			}
			bi_ua_scale = 10000;
		}
	} else {
		input_cu_mv = cu_mv;
		input_bu_mv_min = bu_mv_min;
		input_bu_mv_max = bu_mv_max;
		input_bi_ua_scale = bi_ua_scale;
		input_initialized = 1;
	}

	bluetooth_data_started(BLUETOOTH_START_INPUT, 128, cu_mv, bu_mv_min, bu_mv_max, bi_ua_scale);
	if (p == MEASURE_POLARITY_ERROR) {
		bluetooth_data_error(0);
		return;
	} else if (p == MEASURE_POLARITY_REVERSED) {
		bluetooth_data_transistor_type(BLUETOOTH_TRANSISTOR_TYPE_PNP);
	} else {
		bluetooth_data_transistor_type(BLUETOOTH_TRANSISTOR_TYPE_NPN);
	}

	oled_refresh();

	berr = cerr = 0;

	berr |= !measure_base_set_mv(bu_mv_min);
	cerr |= !measure_collector_set_mv(cu_mv);

	for (i = 0; i < 128; i++) {
	//	if (berr || cerr)
	//		break;

		bu_mv = bu_mv_min + ((i * (bu_mv_max - bu_mv_min)) / 127);
		berr |= !measure_base_trim_mv(bu_mv);
		cerr |= !measure_collector_trim_mv(cu_mv);
		bi_ua = measure_base_get_ua();
		ssd1309_psetr(i, 63 - ((bi_ua * 63) / bi_ua_scale), 1);
		bluetooth_data_data(i, (cerr << 2) | (berr << 1), cu_mv, bu_mv, bi_ua);
	}
	measure_enable(0);
	bluetooth_data_stopped();
	printf("BERR: %d, CERR: %d, I: %d\n", berr, cerr, i);
}

