/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "ssd1309.h"
#include "ssd1309_hal.h"

extern const uint8_t ssd1309_font[128][8];

static uint8_t ssd1309_buffer[128 * 8];

static void ssd1309_reset();
static void ssd1309_data(uint8_t *data, int len);
static void ssd1309_cmd(uint8_t cmd);

void ssd1309_init()
{
	ssd1309_cls();

	ssd1309_hal_init();
	ssd1309_hal_en(1);
	ssd1309_reset();

	// Turn display off
	ssd1309_cmd(0xAE); // SSD1306_DISPLAYOFF

	ssd1309_cmd(0xD5); // SSD1306_SETDISPLAYCLOCKDIV
	ssd1309_cmd(0x80);

	ssd1309_cmd(0xA8); // SSD1306_SETMULTIPLEX
	ssd1309_cmd(0x3F);
    
	ssd1309_cmd(0xD3); // SSD1306_SETDISPLAYOFFSET
	ssd1309_cmd(0x00);
    
	ssd1309_cmd(0x40 | 0x00); // SSD1306_SETSTARTLINE
    
	// Internal charge pump
	ssd1309_cmd(0x8D); // SSD1306_CHARGEPUMP
	ssd1309_cmd(0x14);
    
	// Page addressing mode
	ssd1309_cmd(0x20); // SSD1306_MEMORYMODE
	ssd1309_cmd(0x02);
    
	ssd1309_cmd(0xA0 | 0x1); // SSD1306_SEGREMAP

	ssd1309_cmd(0xC8); // SSD1306_COMSCANDEC

	ssd1309_cmd(0xDA); // SSD1306_SETCOMPINS
	ssd1309_cmd(0x12);

	// Max contrast
	ssd1309_cmd(0x81); // SSD1306_SETCONTRAST
	ssd1309_cmd(0x8F);

	ssd1309_cmd(0xD9); // SSD1306_SETPRECHARGE
	ssd1309_cmd(0xF1);

	ssd1309_cmd(0xDB); // SSD1306_SETVCOMDETECT
	ssd1309_cmd(0x40);

	ssd1309_cmd(0xA4); // SSD1306_DISPLAYALLON_RESUME

	// Non-inverted display
	ssd1309_cmd(0xA6); // SSD1306_NORMALDISPLAY

	ssd1309_refresh();

	// Turn display back on
	ssd1309_cmd(0xAF); // SSD1306_DISPLAYON
}

void ssd1309_cls()
{
	int i;

	for (i = 0; i < 128 * 8; i++)
		ssd1309_buffer[i] = 0x00;
}

void ssd1309_pset(int x, int y, int on)
{
	if (on)
		ssd1309_buffer[(x & 0x7F) + ((y & 0x38) << 4)] |= (1 << (y & 0x07));
	else
		ssd1309_buffer[(x & 0x7F) + ((y & 0x38) << 4)] &= ~(1 << (y & 0x07));
}

void ssd1309_psetr(int x, int y, int on)
{
	int i;

	if (x < 0)
		x = 0;

	if (x > 127)
		x = 127;

	if (y < 0)
		y = 0;

	if (y > 63)
		y = 63;

	i = (x & 0x7F) + ((y & 0x38) << 4);

	if (on)
		ssd1309_buffer[i] |= (1 << (y & 0x07));
	else
		ssd1309_buffer[i] &= ~(1 << (y & 0x07));

	ssd1309_cmd(0xB0 + (y >> 3));
	ssd1309_cmd(0x00 + ((x >> 0) & 0x0F));
	ssd1309_cmd(0x10 + ((x >> 4) & 0x0F));
	ssd1309_data(&ssd1309_buffer[i], 1);
}

void ssd1309_putc(uint8_t x, uint8_t y, int c, int on)
{
	int i, j;
	uint8_t f;

	for (j = 0; j < 8; j++) {
		f = ssd1309_font[c][j];
		for (i = 0; i < 8; i++) {
			if (f & (1 << i))
				ssd1309_pset(x + (i * 1) + 0, y + (j * 1) + 0, on);
			else
				ssd1309_pset(x + (i * 1) + 0, y + (j * 1) + 0, 0);
		}
	}
}

void ssd1309_puts(uint8_t x, uint8_t y, const char *s, int on)
{
	int i;

	for (i = 0; s[i]; i++) {
		ssd1309_putc(x + (i * 8), y, s[i], on);
	}
}

void ssd1309_refresh()
{
	int i = 0;

	for (i = 0; i < 128 * 8; i += 128) {
		ssd1309_cmd(0xB0 + (i >> 7));
		ssd1309_cmd(0x00);
		ssd1309_cmd(0x10);
		ssd1309_data(&ssd1309_buffer[i], 128);
	}
}

static void ssd1309_reset()
{
	ssd1309_hal_cs(1);
	ssd1309_hal_res(0);
	ssd1309_hal_delay(500);
	ssd1309_hal_res(1);
	ssd1309_hal_delay(500);
}

static void ssd1309_cmd(uint8_t cmd)
{
	ssd1309_hal_cs(0);
	ssd1309_hal_dc(0);
	ssd1309_hal_spi_send(cmd);
	ssd1309_hal_cs(1);
}

static void ssd1309_data(uint8_t *data, int len)
{
	int i;

	ssd1309_hal_cs(0);
	ssd1309_hal_dc(1);
	for (i = 0; i < len; i++)
		ssd1309_hal_spi_send(data[i]);

	ssd1309_hal_cs(1);
}

