/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "ssd1309_hal.h"
#include <stdint.h>
#include "cortex.h"
#include "nrf51.h"
#include "mask.h"

void ssd1309_hal_init()
{
	GPIO->DIR |= GPIO_PIN21 | GPIO_PIN22 | GPIO_PIN23 | GPIO_PIN24 | GPIO_PIN25 | GPIO_PIN28;
}

void ssd1309_hal_cs(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN22;
	else
		GPIO->OUT &= ~GPIO_PIN22;
}

void ssd1309_hal_res(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN23;
	else
		GPIO->OUT &= ~GPIO_PIN23;
}

void ssd1309_hal_dc(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN24;
	else
		GPIO->OUT &= ~GPIO_PIN24;
}

void ssd1309_hal_en(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN21;
	else
		GPIO->OUT &= ~GPIO_PIN21;
}

void ssd1309_hal_sck(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN25;
	else
		GPIO->OUT &= ~GPIO_PIN25;
}

void ssd1309_hal_mosi(uint8_t level)
{
	if (level)
		GPIO->OUT |= GPIO_PIN28;
	else
		GPIO->OUT &= ~GPIO_PIN28;
}

void ssd1309_hal_spi_send(uint8_t byte)
{
	int i;

	for (i = 7; i >= 0; i--) {
		ssd1309_hal_mosi((byte >> i) & 1);
		ssd1309_hal_sck(1);
		ssd1309_hal_sck(0);
	}
}

void ssd1309_hal_delay(uint32_t d)
{
	volatile uint32_t i;

	for (i = 0; i < d; i++)
		__asm__("nop");
}

