/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "serial.h"
#include "cortex.h"
#include "nrf51.h"
#include "mask.h"

#define SERIAL_SEND_FIFO_LEN 128
#define SERIAL_RECV_FIFO_LEN 512

static volatile uint16_t serial_send_fifo_in, serial_send_fifo_out, serial_send_fifo_len;
static volatile uint16_t serial_recv_fifo_in, serial_recv_fifo_out, serial_recv_fifo_len;

static volatile uint8_t serial_send_start;
static volatile uint8_t serial_send_fifo[SERIAL_SEND_FIFO_LEN], serial_recv_fifo[SERIAL_RECV_FIFO_LEN];

void serial_init()
{
	cortex_interrupt_disable(UART0_INT);

	serial_send_start = 0;
	serial_send_fifo_in = serial_send_fifo_out = serial_send_fifo_len = 0;
	serial_recv_fifo_in = serial_recv_fifo_out = serial_recv_fifo_len = 0;

	UART0->ENABLE = 0;
	UART0->PSELTXD = 29;
	UART0->PSELRXD = 30;
	UART0->CONFIG = 0;
	UART0->BAUDRATE = 0x01D7E000;
	UART0->INTENSET = UART_RXDRDY;
	mask32_set(UART0->ENABLE, UART_ENABLE, 4);
	UART0->STARTTX = 1;
	UART0->STARTRX = 1;

	cortex_interrupt_set_priority(UART0_INT, 3);
	cortex_interrupt_clear(UART0_INT);
	cortex_interrupt_enable(UART0_INT);
}

int16_t serial_read(void *buf, uint16_t count)
{
	uint16_t len;

	if (!count || !serial_recv_fifo_len)
		return 0;

	cortex_interrupt_disable(UART0_INT);
	for (len = 0; serial_recv_fifo_len && (len < count) && (len < 32767); len++) {
		((uint8_t *)buf)[len] = serial_recv_fifo[serial_recv_fifo_out];
		serial_recv_fifo_len--;
		serial_recv_fifo_out++;
		if (serial_recv_fifo_out == SERIAL_RECV_FIFO_LEN)
			serial_recv_fifo_out = 0;
	}
	cortex_interrupt_enable(UART0_INT);
	return len;
}

int16_t serial_write(const void *buf, uint16_t count)
{
	uint16_t len;

	if (!count)
		return 0;

	cortex_interrupt_disable(UART0_INT);
	for (len = 0; (serial_send_fifo_len < SERIAL_SEND_FIFO_LEN) && (len < count) && (len < 32767); len++) {
		serial_send_fifo[serial_send_fifo_in] = ((uint8_t *)buf)[len];
		serial_send_fifo_len++;
		serial_send_fifo_in++;
		if (serial_send_fifo_in == SERIAL_SEND_FIFO_LEN)
			serial_send_fifo_in = 0;
	}
	if (!serial_send_start) {
		UART0->TXD = serial_send_fifo[serial_send_fifo_out];
		serial_send_fifo_out++;
		serial_send_fifo_len--;
		serial_send_start = 1;
		if (serial_send_fifo_out == SERIAL_SEND_FIFO_LEN)
			serial_send_fifo_out = 0;
	}
	UART0->INTENSET = UART_TXDRDY;
	cortex_interrupt_enable(UART0_INT);

	return len;
}

CORTEX_ISR(UART0_INT)
{
	volatile uint32_t c;
	if (UART0->TXDRDY) {
		if (serial_send_fifo_len) {
			UART0->TXD = serial_send_fifo[serial_send_fifo_out];
			serial_send_fifo_out++;
			serial_send_fifo_len--;
			if (serial_send_fifo_out == SERIAL_SEND_FIFO_LEN)
				serial_send_fifo_out = 0;
		} else {
			serial_send_start = 0;
			UART0->INTENCLR = UART_TXDRDY;
		}
		UART0->TXDRDY = 0;
	}
	if (UART0->RXDRDY) {
		UART0->RXDRDY = 0; // kindlasti tuleb see enne teha kui lugeda UART0->RXD, datasheet lk 153
		c = UART0->RXD;
		if (serial_recv_fifo_len < SERIAL_RECV_FIFO_LEN) {
		        serial_recv_fifo[serial_recv_fifo_in] = (c & 0xFF);
		        serial_recv_fifo_in++;
		        serial_recv_fifo_len++;
			if (serial_recv_fifo_in == SERIAL_RECV_FIFO_LEN)
				serial_recv_fifo_in = 0;
		}
	}
	cortex_interrupt_clear(UART0_INT);
}

