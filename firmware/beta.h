/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _BETA_H_
#define _BETA_H_

void beta_plot(int def, int cu_mv, int bi_ua_min, int bi_ua_max, int ci_ma_scale);

#endif

