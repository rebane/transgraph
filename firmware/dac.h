/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _DAC_H_
#define _DAC_H_

#include <stdint.h>

#define DAC_CHANNEL_A  0
#define DAC_CHANNEL_B  1
#define DAC_CHANNEL_AB 7

void dac_init(int ref_mv);
void dac_enable(int on);
void dac_set_raw(uint8_t channel, uint16_t value);
int dac_inc(uint8_t channel);
int dac_dec(uint8_t channel);
void dac_set_mv(uint8_t channel, int mv);
void dac_set_mv_ref(uint8_t channel, int mv, int ref_mv);

#endif

