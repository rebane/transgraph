/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "platform.h"
#include <stdio.h>
#include "cortex.h"
#include "nrf51.h"
#include "mask.h"
#include "softdevice.h"

extern uint8_t __text_beg__;

static uint8_t platform_is_manual_peripheral_setup_needed(void);

void platform_init()
{
	*(uint32_t volatile *)0x40000524 = 0xF000F; // RAM ON
	if (platform_is_manual_peripheral_setup_needed()) {
		*(uint32_t volatile *)0x40000504 = 0xC007FFDF;
		*(uint32_t volatile *)0x40006C18 = 0x00008000;
	}

	__asm__("cpsie i");
	softdevice_enable((void *)&__text_beg__);
}

static uint8_t platform_is_manual_peripheral_setup_needed(void)
{
	if ((((*(uint32_t *)0xF0000FE0) & 0x000000FF) == 0x1) && (((*(uint32_t *)0xF0000FE4) & 0x0000000F) == 0x0)) {
		if ((((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x00) && (((*(uint32_t *)0xF0000FEC) & 0x000000F0) == 0x0))
			return 1;
		if ((((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x10) && (((*(uint32_t *)0xF0000FEC) & 0x000000F0) == 0x0))
			return 1;
		if ((((*(uint32_t *)0xF0000FE8) & 0x000000F0) == 0x30) && (((*(uint32_t *)0xF0000FEC) & 0x000000F0) == 0x0))
			return 1;
	}
	return 0;
}

