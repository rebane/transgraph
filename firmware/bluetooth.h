/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _BLUETOOTH_H_
#define _BLUETOOTH_H_

#include <stdint.h>

#define BLUETOOTH_EVENT_UPTIME          0
#define BLUETOOTH_EVENT_START           1
#define BLUETOOTH_EVENT_STARTED         2
#define BLUETOOTH_EVENT_ERROR           3
#define BLUETOOTH_EVENT_TRANSISTOR_TYPE 4
#define BLUETOOTH_EVENT_DATA            5
#define BLUETOOTH_EVENT_STOPPED         6

#define BLUETOOTH_START_INPUT           0
#define BLUETOOTH_START_OUTPUT          1
#define BLUETOOTH_START_BETA            2

#define BLUETOOTH_TRANSISTOR_TYPE_NPN   0
#define BLUETOOTH_TRANSISTOR_TYPE_PNP   1

void bluetooth_init();
void bluetooth_advertise_set();
void bluetooth_start();
void bluetooth_command_flush();
int bluetooth_command_get_start_input(uint16_t *cu_mv, uint16_t *bu_mv_min, uint16_t *bu_mv_max, uint16_t *bi_ua_scale);
int bluetooth_command_get_start_output(uint16_t *bi_ua, uint16_t *cu_mv_min, uint16_t *cu_mv_max, uint16_t *ci_ma_scale);
int bluetooth_command_get_start_beta(uint16_t *cu_mv, uint16_t *bi_ua_min, uint16_t *bi_ua_max, uint16_t *ci_ma_scale);
void bluetooth_data_uptime(uint32_t uptime);
void bluetooth_data_started(uint8_t type, uint16_t size, uint16_t a, uint16_t b, uint16_t c, uint16_t s);
void bluetooth_data_error(uint8_t error);
void bluetooth_data_transistor_type(uint8_t transistor_type);
void bluetooth_data_data(uint16_t index, uint8_t status, uint16_t a, uint16_t b, uint16_t c);
void bluetooth_data_stopped();
int bluetooth_connected();

#endif

