/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "button.h"
#include "cortex.h"
#include "nrf51.h"
#include "mask.h"

void button_init()
{
	GPIO->PIN_CNF[20] = 0;
	GPIO->DIR &= ~GPIO_PIN20;
	GPIO->PIN_CNF[20] = 0;

	GPIO->PIN_CNF[18] = mask32(GPIO_PULL, 3);
	GPIO->DIR &= ~GPIO_PIN18;
	GPIO->PIN_CNF[18] = mask32(GPIO_PULL, 3);

	GPIO->PIN_CNF[19] = mask32(GPIO_PULL, 3);
	GPIO->DIR &= ~GPIO_PIN19;
	GPIO->PIN_CNF[19] = mask32(GPIO_PULL, 3);

	GPIO->PIN_CNF[4] = 0;
	GPIO->DIR &= ~GPIO_PIN4;
	GPIO->PIN_CNF[4] = 0;
}

int button_get(int button)
{
	if (button == 0)
		return !!(GPIO->IN & GPIO_PIN20);
	if (button == 1)
		return !(GPIO->IN & GPIO_PIN18);
	if (button == 2)
		return !(GPIO->IN & GPIO_PIN19);
	if (button == 3)
		return !!(GPIO->IN & GPIO_PIN4);
	return 0;
}

