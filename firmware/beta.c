/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "beta.h"
#include <stdio.h>
#include "mtimer.h"
#include "ssd1309.h"
#include "bluetooth.h"
#include "io.h"
#include "measure.h"

void oled_refresh();

static int beta_cu_mv;
static int beta_bi_ua_min;
static int beta_bi_ua_max;
static int beta_ci_ma_scale;
static int beta_initialized = 0;

void beta_plot(int def, int cu_mv, int bi_ua_min, int bi_ua_max, int ci_ma_scale)
{
	int ci_ma, bi_ua, p;
	int berr, cerr;
	int i;

	ssd1309_cls();
	ssd1309_puts(4, 4, "BETA", 1);
	measure_enable(1);
	measure_collector_polarity_set(MEASURE_POLARITY_NORMAL);
	measure_base_polarity_set(MEASURE_POLARITY_NORMAL);
	p = measure_get_cb_polarity();
	if (p == MEASURE_POLARITY_ERROR) {
		measure_enable(0);
		ssd1309_puts(64, 4, "ERROR", 1);
		oled_refresh();
	} else if (p == MEASURE_POLARITY_REVERSED) {
		ssd1309_puts(64, 4, "PNP", 1);
		measure_base_polarity_set(p);
		measure_base_vdd_set_mv(0);
		measure_collector_polarity_set(p);
		measure_collector_vdd_set_mv(0);
		io_e(IO_E_HIGH);
	} else {
		ssd1309_puts(64, 4, "NPN", 1);
		io_e(IO_E_LOW);
	}

	if (def) {
		if (beta_initialized) {
			cu_mv = beta_cu_mv;
			bi_ua_min = beta_bi_ua_min;
			bi_ua_max = beta_bi_ua_max;
			ci_ma_scale = beta_ci_ma_scale;
		} else {
			cu_mv = 3000;
			bi_ua_min = 0;
			bi_ua_max = 300;
			ci_ma_scale = 100;
		}
	} else {
		beta_cu_mv = cu_mv;
		beta_bi_ua_min = bi_ua_min;
		beta_bi_ua_max = bi_ua_max;
		beta_ci_ma_scale = ci_ma_scale;
		beta_initialized = 1;
	}

	bluetooth_data_started(BLUETOOTH_START_BETA, 128, cu_mv, bi_ua_min, bi_ua_max, ci_ma_scale);
	if (p == MEASURE_POLARITY_ERROR) {
		bluetooth_data_error(0);
		return;
	} else if (p == MEASURE_POLARITY_REVERSED) {
		bluetooth_data_transistor_type(BLUETOOTH_TRANSISTOR_TYPE_PNP);
	} else {
		bluetooth_data_transistor_type(BLUETOOTH_TRANSISTOR_TYPE_NPN);
	}

	oled_refresh();

	berr = cerr = 0;

	berr |= !measure_base_set_ua(bi_ua_min);
	cerr |= !measure_collector_set_mv(cu_mv);

	for (i = 0; i < 128; i++) {
		bi_ua = bi_ua_min + ((i * (bi_ua_max - bi_ua_min)) / 127);
		berr |= !measure_base_trim_ua(bi_ua);
		cerr |= !measure_collector_trim_mv(cu_mv);
		ci_ma = measure_collector_get_ma();
		ssd1309_psetr(i, 63 - ((ci_ma * 63) / ci_ma_scale), 1);
		bluetooth_data_data(i, (cerr << 2) | (berr << 1), cu_mv, bi_ua, ci_ma);
	}
	measure_enable(0);
	bluetooth_data_stopped();
	printf("BERR: %d, CERR: %d\n", berr, cerr);
}

