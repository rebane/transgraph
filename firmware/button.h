/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _BUTTON_H_
#define _BUTTON_H_

void button_init();
int button_get(int button);

#endif

