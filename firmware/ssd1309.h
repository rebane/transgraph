/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _SSD1309_H_
#define _SSD1309_H_

#include <stdint.h>

void ssd1309_init();
void ssd1309_cls();
void ssd1309_refresh();
void ssd1309_pset(int x, int y, int on);
void ssd1309_psetr(int x, int y, int on);
void ssd1309_putc(uint8_t x, uint8_t y, int c, int on);
void ssd1309_puts(uint8_t x, uint8_t y, const char *s, int on);

#endif

