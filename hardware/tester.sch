<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="7" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="13" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="ATT_MISO" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="punktiir" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE">
<packages>
<package name="SOT-23">
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-2.1" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-1.6" y1="-1.9" x2="1.6" y2="-1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="-1.9" x2="1.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="1.6" y1="0.7" x2="0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="0.6" y1="0.7" x2="0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="0.6" y1="1.9" x2="-0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="-0.6" y1="1.9" x2="-0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="-0.6" y1="0.7" x2="-1.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-1.9" width="0.2" layer="51"/>
<text x="-1.3" y="-0.7" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.8" y1="0.7" x2="-1.5" y2="0.7" width="0.2" layer="21"/>
<wire x1="-1.5" y1="0.7" x2="-1.5" y2="-0.1" width="0.2" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.5" y2="0.7" width="0.2" layer="21"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="-0.1" width="0.2" layer="21"/>
</package>
<package name="TO-92">
<description>&lt;b&gt;TO 92&lt;/b&gt;</description>
<wire x1="-2.1" y1="-1.6" x2="2.1" y2="-1.6" width="0.2" layer="21"/>
<pad name="3" x="1.27" y="0" drill="0.8" diameter="1.4"/>
<pad name="2" x="0" y="1.905" drill="0.8" diameter="1.4"/>
<pad name="1" x="-1.27" y="0" drill="0.8" diameter="1.5"/>
<text x="1.9" y="-2.1" size="0.8128" layer="25" font="vector" rot="R180">&gt;Name</text>
<wire x1="-2.1" y1="-1.6" x2="2.1" y2="-1.6" width="0.2" layer="51"/>
<wire x1="2.1" y1="-1.6" x2="-2.1" y2="-1.6" width="0.2" layer="51" curve="258"/>
<text x="-1.8" y="0" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="2.1" y1="-1.6" x2="-2.1" y2="-1.6" width="0.2" layer="21" curve="258"/>
</package>
<package name="SOT-323">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="1" y1="-0.1" x2="1" y2="0.5" width="0.2" layer="21"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.1" width="0.2" layer="21"/>
<smd name="3" x="0" y="0.9" dx="0.5" dy="1" layer="1"/>
<smd name="1" x="-0.65" y="-0.9" dx="0.5" dy="1" layer="1"/>
<smd name="2" x="0.65" y="-0.9" dx="0.5" dy="1" layer="1"/>
<text x="1.5" y="-0.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1" y1="0.5" x2="-0.6" y2="0.5" width="0.2" layer="21"/>
<wire x1="1" y1="0.5" x2="0.6" y2="0.5" width="0.2" layer="21"/>
<wire x1="-1" y1="-1.3" x2="-0.4" y2="-1.3" width="0.2" layer="51"/>
<wire x1="-0.4" y1="-1.3" x2="-0.4" y2="-0.5" width="0.2" layer="51"/>
<wire x1="-0.4" y1="-0.5" x2="0.4" y2="-0.5" width="0.2" layer="51"/>
<wire x1="0.4" y1="-0.5" x2="0.4" y2="-1.3" width="0.2" layer="51"/>
<wire x1="0.4" y1="-1.3" x2="1" y2="-1.3" width="0.2" layer="51"/>
<wire x1="1" y1="-1.3" x2="1" y2="0.5" width="0.2" layer="51"/>
<wire x1="1" y1="0.5" x2="0.3" y2="0.5" width="0.2" layer="51"/>
<wire x1="0.3" y1="0.5" x2="0.3" y2="1.3" width="0.2" layer="51"/>
<wire x1="0.3" y1="1.3" x2="-0.3" y2="1.3" width="0.2" layer="51"/>
<wire x1="-0.3" y1="1.3" x2="-0.3" y2="0.5" width="0.2" layer="51"/>
<wire x1="-0.3" y1="0.5" x2="-1" y2="0.5" width="0.2" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-1.3" width="0.2" layer="51"/>
<text x="-0.8" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
</package>
<package name="0603[1608-METRIC]">
<smd name="P$1" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.2" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="0.8" width="0.2" layer="21"/>
</package>
<package name="0402[1005-METRIC]">
<smd name="P$1" x="-0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<smd name="P$2" x="0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-0.6" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.85" y1="0.5" x2="-0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="-0.85" y1="-0.5" x2="0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="-0.5" x2="0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="0.5" x2="-0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.2" layer="51"/>
</package>
<package name="0805[2012-METRIC]">
<smd name="P$1" x="-1.1" y="0" dx="1.4" dy="1" layer="1" rot="R90"/>
<smd name="P$2" x="1.1" y="0" dx="1.4" dy="1" layer="1" rot="R90"/>
<text x="-1.6" y="1.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.2" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.2" layer="51"/>
<wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.2" layer="51"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.2" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.8" y1="1" x2="1.8" y2="1" width="0.2" layer="21"/>
<wire x1="1.8" y1="1" x2="1.8" y2="-1" width="0.2" layer="21"/>
<wire x1="1.8" y1="-1" x2="-1.8" y2="-1" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-1" x2="-1.8" y2="1" width="0.2" layer="21"/>
</package>
<package name="0201[0603-METRIC]">
<smd name="P$1" x="-0.25" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<smd name="P$2" x="0.25" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<text x="-1.6" y="0.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-0.5" y="-0.15" size="0.3048" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.75" y1="0.45" x2="0.75" y2="0.45" width="0.2" layer="21"/>
<wire x1="0.75" y1="0.45" x2="0.75" y2="-0.45" width="0.2" layer="21"/>
<wire x1="0.75" y1="-0.45" x2="-0.75" y2="-0.45" width="0.2" layer="21"/>
<wire x1="-0.75" y1="-0.45" x2="-0.75" y2="0.45" width="0.2" layer="21"/>
<wire x1="-0.7" y1="0.4" x2="0.7" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.7" y1="0.4" x2="0.7" y2="-0.4" width="0.2" layer="51"/>
<wire x1="0.7" y1="-0.4" x2="-0.7" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-0.7" y1="-0.4" x2="-0.7" y2="0.4" width="0.2" layer="51"/>
</package>
<package name="1206[3216-METRIC]">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<text x="-1.2" y="1.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="P$1" x="-1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<smd name="P$2" x="1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<wire x1="-2.3" y1="-1.1" x2="2.3" y2="-1.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="-1.1" x2="2.3" y2="1.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="1.1" x2="-2.3" y2="1.1" width="0.2" layer="51"/>
<wire x1="-2.3" y1="1.1" x2="-2.3" y2="-1.1" width="0.2" layer="51"/>
<text x="-1.5" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2.5" y1="1.3" x2="2.5" y2="1.3" width="0.2" layer="21"/>
<wire x1="2.5" y1="1.3" x2="2.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="2.5" y1="-1.3" x2="-2.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="-2.5" y1="-1.3" x2="-2.5" y2="1.3" width="0.2" layer="21"/>
</package>
<package name="SOT-523">
<smd name="3" x="0" y="0.645" dx="0.4" dy="0.51" layer="1"/>
<smd name="2" x="0.5" y="-0.645" dx="0.4" dy="0.51" layer="1"/>
<smd name="1" x="-0.5" y="-0.645" dx="0.4" dy="0.51" layer="1"/>
<text x="-1.2" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-0.8" y1="-1" x2="-0.2" y2="-1" width="0.2" layer="51"/>
<wire x1="-0.2" y1="-1" x2="-0.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="-0.2" y1="-0.6" x2="0.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="0.2" y1="-0.6" x2="0.2" y2="-1" width="0.2" layer="51"/>
<wire x1="0.2" y1="-1" x2="0.8" y2="-1" width="0.2" layer="51"/>
<wire x1="0.8" y1="-1" x2="0.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.3" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="1" width="0.2" layer="51"/>
<wire x1="0.3" y1="1" x2="-0.3" y2="1" width="0.2" layer="51"/>
<wire x1="-0.3" y1="1" x2="-0.3" y2="0.4" width="0.2" layer="51"/>
<wire x1="-0.3" y1="0.4" x2="-0.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="-0.8" y2="-1" width="0.2" layer="51"/>
<text x="-0.5" y="-0.3" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.5" y1="0.4" x2="-0.8" y2="0.4" width="0.2" layer="21"/>
<wire x1="-0.8" y1="0.4" x2="-0.8" y2="-0.1" width="0.2" layer="21"/>
<wire x1="0.5" y1="0.4" x2="0.8" y2="0.4" width="0.2" layer="21"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.1" width="0.2" layer="21"/>
</package>
<package name="SOT-223">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.2" layer="21"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.2" layer="21"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.2" layer="21"/>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.2" layer="21"/>
<smd name="1" x="-2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="2" x="0" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="3" x="2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="4" x="0" y="3.099" dx="3.6" dy="2.2" layer="1"/>
<text x="-3.7" y="-1.8" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.2" layer="51"/>
</package>
<package name="2512[6332-METRIC]">
<text x="-1.2" y="2.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="P$1" x="-2.6" y="0" dx="3.6" dy="2.4" layer="1" rot="R90"/>
<smd name="P$2" x="2.6" y="0" dx="3.6" dy="2.4" layer="1" rot="R90"/>
<wire x1="-3.9" y1="-1.9" x2="3.9" y2="-1.9" width="0.2" layer="51"/>
<wire x1="3.9" y1="-1.9" x2="3.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="3.9" y1="1.9" x2="-3.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="-3.9" y1="1.9" x2="-3.9" y2="-1.9" width="0.2" layer="51"/>
<text x="-1.5" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-4.1" y1="2.1" x2="4.1" y2="2.1" width="0.2" layer="21"/>
<wire x1="4.1" y1="2.1" x2="4.1" y2="-2.1" width="0.2" layer="21"/>
<wire x1="4.1" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="2.1" width="0.2" layer="21"/>
</package>
<package name="1812[4532-METRIC]">
<smd name="P$1" x="-2.25" y="0" dx="4" dy="2.1" layer="1" rot="R90"/>
<smd name="P$2" x="2.25" y="0" dx="4" dy="2.1" layer="1" rot="R90"/>
<text x="-1.1" y="2.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.6" y1="2.3" x2="3.6" y2="2.3" width="0.2" layer="21"/>
<wire x1="3.6" y1="2.3" x2="3.6" y2="-2.3" width="0.2" layer="21"/>
<wire x1="3.6" y1="-2.3" x2="-3.6" y2="-2.3" width="0.2" layer="21"/>
<wire x1="-3.6" y1="-2.3" x2="-3.6" y2="2.3" width="0.2" layer="21"/>
<wire x1="-3.4" y1="2.1" x2="3.4" y2="2.1" width="0.2" layer="51"/>
<wire x1="3.4" y1="2.1" x2="3.4" y2="-2.1" width="0.2" layer="51"/>
<wire x1="3.4" y1="-2.1" x2="-3.4" y2="-2.1" width="0.2" layer="51"/>
<wire x1="-3.4" y1="-2.1" x2="-3.4" y2="2.1" width="0.2" layer="51"/>
<text x="-1.1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="0603[1608-METRIC]-0OHM-ON">
<smd name="P$1" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<polygon width="0" layer="29">
<vertex x="-1.2" y="0.6"/>
<vertex x="1.2" y="0.6"/>
<vertex x="1.2" y="-0.6"/>
<vertex x="-1.2" y="-0.6"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="-0.4" y="0.4"/>
<vertex x="0" y="0"/>
<vertex x="-0.4" y="-0.4"/>
<vertex x="-1" y="-0.4"/>
<vertex x="-1" y="0.4"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0.15" y="0.4"/>
<vertex x="1" y="0.4"/>
<vertex x="1" y="-0.4"/>
<vertex x="0.15" y="-0.4"/>
<vertex x="0.45" y="-0.1"/>
<vertex x="0.45" y="0.1"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-0.3" y="0.4"/>
<vertex x="-0.3" y="-0.4"/>
<vertex x="0.3" y="-0.4"/>
<vertex x="0.3" y="0.4"/>
</polygon>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.2" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="0.8" width="0.2" layer="21"/>
</package>
<package name="HC49">
<wire x1="-3.302" y1="-2.413" x2="3.302" y2="-2.413" width="0.2" layer="21"/>
<wire x1="-3.302" y1="2.413" x2="3.302" y2="2.413" width="0.2" layer="21"/>
<wire x1="-3.302" y1="2.413" x2="-3.302" y2="-2.413" width="0.2" layer="21" curve="180"/>
<wire x1="3.302" y1="-2.413" x2="3.302" y2="2.413" width="0.2" layer="21" curve="180"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-1.6" y="-1.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.302" y1="-2.413" x2="3.302" y2="-2.413" width="0.2" layer="51"/>
<wire x1="-3.302" y1="2.413" x2="3.302" y2="2.413" width="0.2" layer="51"/>
<wire x1="-3.302" y1="2.413" x2="-3.302" y2="-2.413" width="0.2" layer="51" curve="180"/>
<wire x1="3.302" y1="-2.413" x2="3.302" y2="2.413" width="0.2" layer="51" curve="180"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="3.2X1.5">
<description>3.2x1.5mm crystal</description>
<smd name="P$1" x="-1.25" y="0" dx="1.1" dy="1.9" layer="1"/>
<smd name="P$2" x="1.25" y="0" dx="1.1" dy="1.9" layer="1"/>
<text x="-1.2" y="1.4" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.7" y1="0.9" x2="1.7" y2="0.9" width="0.2" layer="51"/>
<wire x1="1.7" y1="0.9" x2="1.7" y2="-0.9" width="0.2" layer="51"/>
<wire x1="1.7" y1="-0.9" x2="-1.7" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-1.7" y1="-0.9" x2="-1.7" y2="0.9" width="0.2" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.9" y1="1.1" x2="1.9" y2="1.1" width="0.2" layer="21"/>
<wire x1="1.9" y1="1.1" x2="1.9" y2="-1.1" width="0.2" layer="21"/>
<wire x1="1.9" y1="-1.1" x2="-1.9" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-1.9" y1="-1.1" x2="-1.9" y2="1.1" width="0.2" layer="21"/>
</package>
<package name="3.2X2.5">
<wire x1="-0.6" y1="1.7" x2="0.6" y2="1.7" width="0.2" layer="21"/>
<wire x1="2" y1="0.3" x2="2" y2="-0.3" width="0.2" layer="21"/>
<wire x1="0.6" y1="-1.7" x2="-0.6" y2="-1.7" width="0.2" layer="21"/>
<wire x1="-2" y1="0.3" x2="-2" y2="-0.3" width="0.2" layer="21"/>
<smd name="1" x="-1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="3" x="1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="4" x="-1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<text x="-1" y="2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.2" layer="51"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.2" layer="51"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.2" layer="51"/>
<text x="-1.3" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="2X1.2">
<smd name="P$1" x="-0.45" y="0" dx="0.4" dy="0.6" layer="1"/>
<smd name="P$2" x="0.45" y="0" dx="0.4" dy="0.6" layer="1"/>
<text x="-1.2" y="1.4" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<text x="-0.6" y="-0.3" size="0.6096" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.9" y1="0.5" x2="0.9" y2="0.5" width="0.2" layer="21"/>
<wire x1="0.9" y1="0.5" x2="0.9" y2="-0.5" width="0.2" layer="21"/>
<wire x1="0.9" y1="-0.5" x2="-0.9" y2="-0.5" width="0.2" layer="21"/>
<wire x1="-0.9" y1="-0.5" x2="-0.9" y2="0.5" width="0.2" layer="21"/>
</package>
<package name="HC49UP">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="3.4" y1="-1.2" x2="-3.4" y2="-1.2" width="0.2" layer="51"/>
<wire x1="3.4" y1="-2" x2="-3.4" y2="-2" width="0.2" layer="51"/>
<wire x1="-3.4" y1="1.2" x2="3.4" y2="1.2" width="0.2" layer="51"/>
<wire x1="-3.4" y1="2" x2="3.4" y2="2" width="0.2" layer="51"/>
<smd name="1" x="-4.8" y="0" dx="5.3" dy="1.9" layer="1"/>
<smd name="2" x="4.8" y="0" dx="5.3" dy="1.9" layer="1"/>
<text x="-1.9" y="2.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.4" y1="1.2" x2="-3.4" y2="-1.2" width="0.2" layer="51" curve="180"/>
<wire x1="3.4" y1="-1.2" x2="3.4" y2="1.2" width="0.2" layer="51" curve="180"/>
<wire x1="-3.4" y1="2" x2="-3.4" y2="-2" width="0.2" layer="51" curve="180"/>
<wire x1="3.4" y1="2" x2="3.4" y2="-2" width="0.2" layer="51" curve="-180"/>
<wire x1="-5.8" y1="2.4" x2="5.8" y2="2.4" width="0.2" layer="51"/>
<wire x1="5.8" y1="2.4" x2="5.8" y2="-2.4" width="0.2" layer="51"/>
<wire x1="5.8" y1="-2.4" x2="-5.8" y2="-2.4" width="0.2" layer="51"/>
<wire x1="-5.8" y1="-2.4" x2="-5.8" y2="2.4" width="0.2" layer="51"/>
<wire x1="-5.8" y1="1.3" x2="-5.8" y2="2.4" width="0.2" layer="21"/>
<wire x1="-5.8" y1="2.4" x2="5.8" y2="2.4" width="0.2" layer="21"/>
<wire x1="5.8" y1="2.4" x2="5.8" y2="1.3" width="0.2" layer="21"/>
<wire x1="-5.8" y1="-1.3" x2="-5.8" y2="-2.4" width="0.2" layer="21"/>
<wire x1="-5.8" y1="-2.4" x2="5.8" y2="-2.4" width="0.2" layer="21"/>
<wire x1="5.8" y1="-2.4" x2="5.8" y2="-1.3" width="0.2" layer="21"/>
<text x="-1.9" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="2X1.6">
<wire x1="-0.6" y1="1.2" x2="0.6" y2="1.2" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.3" x2="1.4" y2="-0.3" width="0.2" layer="21"/>
<wire x1="0.6" y1="-1.2" x2="-0.6" y2="-1.2" width="0.2" layer="21"/>
<wire x1="-1.4" y1="0.3" x2="-1.4" y2="-0.3" width="0.2" layer="21"/>
<smd name="1" x="-0.75" y="-0.6" dx="0.7" dy="0.6" layer="1"/>
<smd name="3" x="0.75" y="0.6" dx="0.7" dy="0.6" layer="1"/>
<smd name="4" x="-0.75" y="0.6" dx="0.7" dy="0.6" layer="1"/>
<smd name="2" x="0.75" y="-0.6" dx="0.7" dy="0.6" layer="1"/>
<text x="-1.5" y="1.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="-1.2" x2="1.4" y2="-1.2" width="0.2" layer="51"/>
<wire x1="1.4" y1="-1.2" x2="1.4" y2="1.2" width="0.2" layer="51"/>
<wire x1="1.4" y1="1.2" x2="-1.4" y2="1.2" width="0.2" layer="51"/>
<wire x1="-1.4" y1="1.2" x2="-1.4" y2="-1.2" width="0.2" layer="51"/>
<text x="-1.1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="7X5">
<wire x1="-3.9" y1="2.7" x2="3.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="3.9" y1="0.3" x2="3.9" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="3.9" y1="-2.7" x2="-3.9" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="0.3" x2="-3.9" y2="-0.3" width="0.2032" layer="21"/>
<smd name="1" x="-3" y="-1.27" dx="2" dy="1.4" layer="1"/>
<smd name="3" x="3" y="1.27" dx="2" dy="1.4" layer="1"/>
<smd name="4" x="-3" y="1.27" dx="2" dy="1.4" layer="1"/>
<smd name="2" x="3" y="-1.27" dx="2" dy="1.4" layer="1"/>
<text x="-1.4" y="3.1" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.9" y1="2.7" x2="-3.9" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.9" y1="2.7" x2="3.9" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.9" y1="-2.3" x2="3.9" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="-2.3" x2="-3.9" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="2.7" x2="-3.9" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-3.9" y1="-2.7" x2="3.9" y2="-2.7" width="0.2" layer="51"/>
<wire x1="3.9" y1="-2.7" x2="3.9" y2="2.7" width="0.2" layer="51"/>
<wire x1="3.9" y1="2.7" x2="-3.9" y2="2.7" width="0.2" layer="51"/>
<text x="-1.4" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="0603[1608-METRIC]-DIODE">
<smd name="C" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="A" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-1.5" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.2" layer="51"/>
<text x="-0.3" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.9" y1="0.3" x2="-0.9" y2="0" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0" x2="-0.9" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0" x2="-0.6" y2="0.3" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0.3" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.6" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.6" y1="-0.3" x2="-0.9" y2="0" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0" x2="-1" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.5" y2="0" width="0.1" layer="51"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.8" x2="-1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="-0.3" y1="0" x2="0.2" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.4" x2="-0.3" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0" x2="0.1" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.2" x2="0.1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.2" x2="-0.3" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0" x2="0" y2="0.1" width="0.127" layer="21"/>
<wire x1="0" y1="0.1" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0" y1="-0.1" x2="-0.3" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.3" x2="0.1" y2="-0.3" width="0.127" layer="21"/>
</package>
<package name="SOD-323">
<smd name="C" x="-1.25" y="0" dx="0.8" dy="0.6" layer="1"/>
<smd name="A" x="1.25" y="0" dx="0.8" dy="0.6" layer="1"/>
<wire x1="-1.2" y1="0.7" x2="1.2" y2="0.7" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-0.7" x2="1.2" y2="-0.7" width="0.2" layer="21"/>
<wire x1="-0.5" y1="0.4" x2="-0.5" y2="-0.4" width="0.2" layer="21"/>
<text x="-1.3" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-0.1" y1="0" x2="0.4" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.4" x2="0.4" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.4" x2="-0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1" y1="0" x2="0.3" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.2" x2="0.3" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.2" x2="-0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1" y1="0" x2="0.2" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.1" x2="0.2" y2="0" width="0.127" layer="21"/>
<wire x1="0.2" y1="0" x2="0.2" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.1" x2="-0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1" y1="0" x2="0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-1.8" y1="0.4" x2="-1.4" y2="0.4" width="0.2" layer="51"/>
<wire x1="-1.4" y1="0.4" x2="-1.4" y2="0.8" width="0.2" layer="51"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.2" layer="51"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="0.4" width="0.2" layer="51"/>
<wire x1="1.4" y1="0.4" x2="1.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="1.8" y1="0.4" x2="1.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="1.8" y1="-0.4" x2="1.4" y2="-0.4" width="0.2" layer="51"/>
<wire x1="1.4" y1="-0.4" x2="1.4" y2="-0.8" width="0.2" layer="51"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.2" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-1.4" y1="-0.4" x2="-1.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-1.8" y1="-0.4" x2="-1.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.2" y2="0" width="0.1" layer="51"/>
<wire x1="-1.2" y1="0" x2="-1.2" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-1.2" y1="0" x2="-1.2" y2="0.3" width="0.1" layer="51"/>
<wire x1="-1.2" y1="0" x2="-0.8" y2="0.3" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0.3" x2="-0.8" y2="0" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0" x2="-0.8" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-0.3" x2="-1.2" y2="0" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0" x2="-0.5" y2="0" width="0.1" layer="51"/>
<text x="-0.4" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
</package>
<package name="SMA-DIODE">
<wire x1="-2.2" y1="1.8" x2="2.2" y2="1.8" width="0.2" layer="21"/>
<wire x1="2.2" y1="-1.8" x2="-2.2" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-2.2" y1="1.8" x2="-2.2" y2="1.3" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-1.8" x2="-2.2" y2="-1.3" width="0.2" layer="21"/>
<wire x1="2.2" y1="-1.8" x2="2.2" y2="-1.3" width="0.2" layer="21" curve="-1.90441"/>
<wire x1="2.2" y1="1.8" x2="2.2" y2="1.3" width="0.2" layer="21"/>
<smd name="C" x="-2" y="0" dx="2" dy="2" layer="1"/>
<smd name="A" x="2" y="0" dx="2" dy="2" layer="1"/>
<text x="-1.2" y="2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<rectangle x1="-1.5" y1="1.25" x2="-0.75" y2="1.75" layer="21"/>
<rectangle x1="-1.5" y1="-1.75" x2="-0.75" y2="-1.25" layer="21"/>
<wire x1="0.1" y1="0" x2="0.6" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.6" y1="0.4" x2="0.6" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.4" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.5" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.5" y1="0.2" x2="0.5" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.5" y1="-0.2" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.4" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.1" x2="0.4" y2="0" width="0.127" layer="21"/>
<wire x1="0.4" y1="0" x2="0.4" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.4" y2="0" width="0.127" layer="21"/>
<wire x1="-2.9" y1="0.9" x2="-2.2" y2="0.9" width="0.2" layer="51"/>
<wire x1="-2.2" y1="0.9" x2="-2.2" y2="1.8" width="0.2" layer="51"/>
<wire x1="-2.2" y1="1.8" x2="2.2" y2="1.8" width="0.2" layer="51"/>
<wire x1="2.2" y1="1.8" x2="2.2" y2="0.9" width="0.2" layer="51"/>
<wire x1="2.2" y1="0.9" x2="2.9" y2="0.9" width="0.2" layer="51"/>
<wire x1="2.9" y1="0.9" x2="2.9" y2="-0.9" width="0.2" layer="51"/>
<wire x1="2.9" y1="-0.9" x2="2.2" y2="-0.9" width="0.2" layer="51"/>
<wire x1="2.2" y1="-0.9" x2="2.2" y2="-1.8" width="0.2" layer="51"/>
<wire x1="2.2" y1="-1.8" x2="-2.2" y2="-1.8" width="0.2" layer="51"/>
<wire x1="-2.2" y1="-1.8" x2="-2.2" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-2.2" y1="-0.9" x2="-2.9" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-2.9" y1="-0.9" x2="-2.9" y2="0.9" width="0.2" layer="51"/>
<rectangle x1="-1.5" y1="1.25" x2="-0.75" y2="1.75" layer="51"/>
<rectangle x1="-1.5" y1="-1.75" x2="-0.75" y2="-1.25" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="MICROMELF">
<smd name="C" x="-0.95" y="0" dx="0.9" dy="1.4" layer="1"/>
<smd name="A" x="0.95" y="0" dx="0.9" dy="1.4" layer="1"/>
<text x="-1.2" y="1.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-0.3" y1="0" x2="0.2" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.4" x2="-0.3" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0" x2="0.1" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.2" x2="0.1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.2" x2="-0.3" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0" x2="0" y2="0.1" width="0.127" layer="21"/>
<wire x1="0" y1="0.1" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0" y1="-0.1" x2="-0.3" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1" x2="1.7" y2="1" width="0.2" layer="21"/>
<wire x1="1.7" y1="1" x2="1.7" y2="-1" width="0.2" layer="21"/>
<wire x1="1.7" y1="-1" x2="-1.7" y2="-1" width="0.2" layer="21"/>
<wire x1="-1.7" y1="-1" x2="-1.7" y2="1" width="0.2" layer="21"/>
<wire x1="-1.6" y1="0.9" x2="-1.2" y2="0.9" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.9" x2="1.6" y2="0.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="0.9" x2="1.6" y2="-0.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="-0.9" x2="-1.2" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-0.9" x2="-1.6" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-0.9" x2="-1.6" y2="0.9" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.9" x2="-1.2" y2="-0.9" width="0.2" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="POWER_DI_123">
<smd name="C" x="-0.95" y="0" dx="2.2" dy="1.4" layer="1"/>
<smd name="A" x="1.6" y="0" dx="0.9" dy="1.4" layer="1"/>
<text x="-1.6" y="1.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="1" x2="-1.4" y2="1.2" width="0.2" layer="21"/>
<wire x1="-1.4" y1="1.2" x2="1.4" y2="1.2" width="0.2" layer="21"/>
<wire x1="1.4" y1="1.2" x2="1.4" y2="1" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-1" x2="-1.4" y2="-1.2" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-1.2" x2="1.4" y2="-1.2" width="0.2" layer="21"/>
<wire x1="1.4" y1="-1.2" x2="1.4" y2="-1" width="0.2" layer="21"/>
<wire x1="0.4" y1="0" x2="0.9" y2="-0.4" width="0.2" layer="21"/>
<wire x1="0.9" y1="-0.4" x2="0.9" y2="0.4" width="0.2" layer="21"/>
<wire x1="0.9" y1="0.4" x2="0.4" y2="0" width="0.2" layer="21"/>
<wire x1="0.4" y1="0" x2="0.6" y2="-0.1" width="0.2" layer="21"/>
<wire x1="0.6" y1="-0.1" x2="0.8" y2="-0.2" width="0.2" layer="21"/>
<wire x1="0.8" y1="-0.2" x2="0.8" y2="0.2" width="0.2" layer="21"/>
<wire x1="0.8" y1="0.2" x2="0.6" y2="-0.1" width="0.2" layer="21"/>
<text x="-0.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2" y1="1.2" x2="-2" y2="-1.2" width="0.2" layer="51"/>
<wire x1="-2" y1="-1.2" x2="2" y2="-1.2" width="0.2" layer="51"/>
<wire x1="2" y1="-1.2" x2="2" y2="1.2" width="0.2" layer="51"/>
<wire x1="2" y1="1.2" x2="-2" y2="1.2" width="0.2" layer="51"/>
<wire x1="-1.7" y1="0" x2="-1.5" y2="0" width="0.1" layer="51"/>
<wire x1="-1.5" y1="0.4" x2="-1.5" y2="0" width="0.1" layer="51"/>
<wire x1="-1.5" y1="0" x2="-1.5" y2="-0.4" width="0.1" layer="51"/>
<wire x1="-1.5" y1="0" x2="-1" y2="0.4" width="0.1" layer="51"/>
<wire x1="-1" y1="0.4" x2="-1" y2="0" width="0.1" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.4" width="0.1" layer="51"/>
<wire x1="-1" y1="-0.4" x2="-1.5" y2="0" width="0.1" layer="51"/>
<wire x1="-1" y1="0" x2="-0.8" y2="0" width="0.1" layer="51"/>
</package>
<package name="0402[1005-METRIC]-DIODE">
<smd name="C" x="-0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<smd name="A" x="0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-0.3" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.85" y1="0.5" x2="-0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="-0.85" y1="-0.5" x2="0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="-0.5" x2="0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="0.5" x2="-0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="0.1" y1="0.2" x2="-0.1" y2="0" width="0.2" layer="21"/>
<wire x1="-0.1" y1="0" x2="0.1" y2="-0.2" width="0.2" layer="21"/>
<wire x1="0.1" y1="-0.2" x2="0.1" y2="0.2" width="0.2" layer="21"/>
<wire x1="-0.65" y1="0" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.55" y1="0" x2="-0.35" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0.2" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.6" y2="-0.2" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.4" y2="0.2" width="0.1" layer="51"/>
<wire x1="-0.4" y1="0.2" x2="-0.4" y2="-0.2" width="0.1" layer="51"/>
<wire x1="-0.4" y1="-0.2" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.55" y2="0" width="0.1" layer="51"/>
<wire x1="-0.55" y1="0" x2="-0.45" y2="0.1" width="0.1" layer="51"/>
<wire x1="-0.45" y1="0.1" x2="-0.45" y2="-0.1" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.2" layer="51"/>
</package>
<package name="SOD-123">
<description>SOD-123 0.91x1.22 mm pad</description>
<wire x1="-0.1" y1="0" x2="0.6" y2="0.4" width="0.2" layer="21"/>
<wire x1="0.6" y1="0.4" x2="0.6" y2="-0.4" width="0.2" layer="21"/>
<wire x1="0.6" y1="-0.4" x2="-0.1" y2="0" width="0.2" layer="21"/>
<wire x1="-1.3" y1="0.9" x2="1.3" y2="0.9" width="0.2" layer="21"/>
<wire x1="1.3" y1="-0.9" x2="-1.3" y2="-0.9" width="0.2" layer="21"/>
<smd name="A" x="1.65" y="0" dx="0.9" dy="1.2" layer="1"/>
<smd name="C" x="-1.65" y="0" dx="0.9" dy="1.2" layer="1"/>
<text x="-1.2" y="1.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<rectangle x1="-0.9" y1="-0.8" x2="-0.4" y2="0.8" layer="21"/>
<wire x1="0" y1="0" x2="0.5" y2="0.2" width="0.2" layer="21"/>
<wire x1="0.5" y1="0.2" x2="0.5" y2="0" width="0.2" layer="21"/>
<wire x1="0.5" y1="0" x2="0.5" y2="-0.2" width="0.2" layer="21"/>
<wire x1="0.5" y1="-0.2" x2="0.2" y2="0" width="0.2" layer="21"/>
<wire x1="0.2" y1="0" x2="0.5" y2="0" width="0.2" layer="21"/>
<wire x1="1.3" y1="0" x2="2" y2="0.4" width="0.2" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-0.4" width="0.2" layer="51"/>
<wire x1="2" y1="-0.4" x2="1.3" y2="0" width="0.2" layer="51"/>
<wire x1="-2.4" y1="0.9" x2="2.4" y2="0.9" width="0.2" layer="51"/>
<wire x1="2.4" y1="-0.9" x2="-2.4" y2="-0.9" width="0.2" layer="51"/>
<wire x1="1.4" y1="0" x2="1.9" y2="0.2" width="0.2" layer="51"/>
<wire x1="1.9" y1="0.2" x2="1.9" y2="0" width="0.2" layer="51"/>
<wire x1="1.9" y1="0" x2="1.9" y2="-0.2" width="0.2" layer="51"/>
<wire x1="1.9" y1="-0.2" x2="1.6" y2="0" width="0.2" layer="51"/>
<wire x1="1.6" y1="0" x2="1.9" y2="0" width="0.2" layer="51"/>
<wire x1="-2.4" y1="0.9" x2="-2.4" y2="-0.9" width="0.2" layer="51"/>
<wire x1="2.4" y1="-0.9" x2="2.4" y2="0.9" width="0.2" layer="51"/>
<wire x1="1.3" y1="0.4" x2="1.3" y2="-0.4" width="0.2" layer="51"/>
<text x="-2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="SOIC-8">
<smd name="1" x="-1.905" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-0.635" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="0.635" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="1.905" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.905" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="0.635" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<smd name="7" x="-0.635" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<smd name="8" x="-1.905" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<text x="-2.9" y="-1.9" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="-2.5" y="-1.5" size="1.27" layer="21" font="vector" ratio="15" rot="R180">&gt;o</text>
<wire x1="-2.5" y1="1.6" x2="2.5" y2="1.6" width="0.2" layer="21"/>
<wire x1="2.5" y1="1.6" x2="2.5" y2="-1.6" width="0.2" layer="21"/>
<wire x1="2.5" y1="-1.6" x2="-2" y2="-1.6" width="0.2" layer="21"/>
<wire x1="-2" y1="-1.6" x2="-2.5" y2="-1.605" width="0.2" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-2.5" y1="-1.1" x2="-2.5" y2="-1.605" width="0.2" layer="21"/>
<wire x1="-2" y1="-1.6" x2="-2.5" y2="-1.1" width="0.2" layer="21"/>
<text x="-1.4" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2.6" y1="-1.7" x2="-2.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="-2.6" y1="1.7" x2="-2.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="-2.3" y1="1.7" x2="-2.3" y2="3.5" width="0.2" layer="51"/>
<wire x1="-2.3" y1="3.5" x2="-1.6" y2="3.5" width="0.2" layer="51"/>
<wire x1="-1.6" y1="3.5" x2="-1.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="-1.6" y1="1.7" x2="-1" y2="1.7" width="0.2" layer="51"/>
<wire x1="-1" y1="1.7" x2="-1" y2="3.5" width="0.2" layer="51"/>
<wire x1="-1" y1="3.5" x2="-0.3" y2="3.5" width="0.2" layer="51"/>
<wire x1="-0.3" y1="3.5" x2="-0.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="-0.3" y1="1.7" x2="0.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="0.3" y1="1.7" x2="0.3" y2="3.5" width="0.2" layer="51"/>
<wire x1="0.3" y1="3.5" x2="1" y2="3.5" width="0.2" layer="51"/>
<wire x1="1" y1="3.5" x2="1" y2="1.7" width="0.2" layer="51"/>
<wire x1="1" y1="1.7" x2="1.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="1.6" y1="1.7" x2="1.6" y2="3.5" width="0.2" layer="51"/>
<wire x1="1.6" y1="3.5" x2="2.3" y2="3.5" width="0.2" layer="51"/>
<wire x1="2.3" y1="3.5" x2="2.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="2.3" y1="1.7" x2="2.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="2.6" y1="1.7" x2="2.6" y2="-1.7" width="0.2" layer="51"/>
<wire x1="2.6" y1="-1.7" x2="2.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="2.3" y1="-1.7" x2="2.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="2.3" y1="-3.5" x2="1.6" y2="-3.5" width="0.2" layer="51"/>
<wire x1="1.6" y1="-3.5" x2="1.6" y2="-1.7" width="0.2" layer="51"/>
<wire x1="1.6" y1="-1.7" x2="1" y2="-1.7" width="0.2" layer="51"/>
<wire x1="1" y1="-1.7" x2="1" y2="-3.5" width="0.2" layer="51"/>
<wire x1="1" y1="-3.5" x2="0.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="0.3" y1="-3.5" x2="0.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="0.3" y1="-1.7" x2="-0.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-0.3" y1="-1.7" x2="-0.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-0.3" y1="-3.5" x2="-1" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-1" y1="-3.5" x2="-1" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-1" y1="-1.7" x2="-1.6" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-1.7" x2="-1.6" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-3.5" x2="-2.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-2.3" y1="-3.5" x2="-2.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-2.3" y1="-1.7" x2="-2.6" y2="-1.7" width="0.2" layer="51"/>
<circle x="-2" y="-1.2" radius="0.282840625" width="0.2" layer="51"/>
</package>
<package name="DIP-8">
<wire x1="-5.1" y1="-0.6" x2="-5.1" y2="0.6" width="0.2" layer="21" curve="180"/>
<wire x1="-5.1" y1="-0.6" x2="-5.1" y2="-2.8" width="0.2" layer="21"/>
<wire x1="5.1" y1="-2.8" x2="5.1" y2="2.8" width="0.2" layer="21"/>
<wire x1="-5.1" y1="2.8" x2="-5.1" y2="0.6" width="0.2" layer="21"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-5.7" y="-1.5" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-4.8" y1="2.8" x2="-5.1" y2="2.8" width="0.2" layer="21"/>
<wire x1="4.8" y1="2.8" x2="5.1" y2="2.8" width="0.2" layer="21"/>
<wire x1="4.8" y1="-2.8" x2="5.1" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-4.8" y1="-2.8" x2="-5.1" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-5.1" y1="-0.6" x2="-5.1" y2="0.6" width="0.2" layer="51" curve="180"/>
<wire x1="-5.1" y1="-0.6" x2="-5.1" y2="-3.8" width="0.2" layer="51"/>
<wire x1="-5.1" y1="3.8" x2="-5.1" y2="0.6" width="0.2" layer="51"/>
<wire x1="-5.1" y1="3.8" x2="5.1" y2="3.8" width="0.2" layer="51"/>
<wire x1="5.1" y1="3.8" x2="5.1" y2="-3.8" width="0.2" layer="51"/>
<wire x1="5.1" y1="-3.8" x2="-5.1" y2="-3.8" width="0.2" layer="51"/>
<text x="-1.7" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="SOT-23-8">
<wire x1="1.8" y1="0.8" x2="1.8" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-0.8" x2="-1.8" y2="0.8" width="0.2" layer="21"/>
<smd name="1" x="-0.975" y="-1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="2" x="-0.325" y="-1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="3" x="0.325" y="-1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="6" x="0.325" y="1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="8" x="-0.975" y="1.3" dx="0.45" dy="1.2" layer="1"/>
<text x="-2.4" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<smd name="7" x="-0.325" y="1.3" dx="0.45" dy="1.2" layer="1"/>
<text x="-1.7" y="-2.7" size="1.27" layer="21" font="vector" ratio="15" rot="R90">&gt;o</text>
<wire x1="-1.9" y1="-1.9" x2="1.9" y2="-1.9" width="0.2" layer="51"/>
<wire x1="1.9" y1="-1.9" x2="1.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="1.9" y1="1.9" x2="-1.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="-1.9" y1="1.9" x2="-1.9" y2="-1.9" width="0.2" layer="51"/>
<circle x="-1.325" y="-1.3" radius="0.282840625" width="0.2" layer="51"/>
<text x="-1.3" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<smd name="4" x="0.975" y="-1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="5" x="0.975" y="1.3" dx="0.45" dy="1.2" layer="1"/>
</package>
<package name="WIRE_SOLDER">
<pad name="P$1" x="0" y="0" drill="1"/>
<hole x="-3" y="0" drill="1.5"/>
<text x="1.4" y="-0.4" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3" y1="1.2" x2="0" y2="1.2" width="0.2" layer="51"/>
<wire x1="0" y1="1.2" x2="0" y2="-1.2" width="0.2" layer="51" curve="-180"/>
<wire x1="0" y1="-1.2" x2="-3" y2="-1.2" width="0.2" layer="51"/>
<wire x1="-3" y1="-1.2" x2="-3" y2="1.2" width="0.2" layer="51" curve="-180"/>
<text x="-2.1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-3" y1="1.2" x2="0" y2="1.2" width="0.2" layer="21"/>
<wire x1="0" y1="1.2" x2="0" y2="-1.2" width="0.2" layer="21" curve="-180"/>
<wire x1="0" y1="-1.2" x2="-3" y2="-1.2" width="0.2" layer="21"/>
<wire x1="-3" y1="-1.2" x2="-3" y2="1.2" width="0.2" layer="21" curve="-180"/>
<wire x1="-3" y1="1.2" x2="0" y2="1.2" width="0.2" layer="22"/>
<wire x1="0" y1="1.2" x2="0" y2="-1.2" width="0.2" layer="22" curve="-180"/>
<wire x1="0" y1="-1.2" x2="-3" y2="-1.2" width="0.2" layer="22"/>
<wire x1="-3" y1="-1.2" x2="-3" y2="1.2" width="0.2" layer="22" curve="-180"/>
<wire x1="-3" y1="1.2" x2="0" y2="1.2" width="0.2" layer="52"/>
<wire x1="0" y1="1.2" x2="0" y2="-1.2" width="0.2" layer="52" curve="-180"/>
<wire x1="0" y1="-1.2" x2="-3" y2="-1.2" width="0.2" layer="52"/>
<wire x1="-3" y1="-1.2" x2="-3" y2="1.2" width="0.2" layer="52" curve="-180"/>
<circle x="-3" y="0" radius="0.6" width="0.2" layer="52"/>
<circle x="0" y="0" radius="0.6" width="0.2" layer="52"/>
<text x="1.4" y="-1.6" size="0.8128" layer="25" font="vector">&gt;Value</text>
<text x="-2.1" y="-0.7" size="0.8128" layer="51" font="vector">&gt;Value</text>
</package>
<package name="12X12">
<smd name="1" x="3.75" y="0" dx="11" dy="4" layer="1" rot="R270"/>
<smd name="2" x="-3.75" y="0" dx="11" dy="4" layer="1" rot="R270"/>
<text x="-1.8" y="6.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-6" y1="6" x2="6" y2="6" width="0.2" layer="51"/>
<wire x1="6" y1="6" x2="6" y2="-6" width="0.2" layer="51"/>
<wire x1="6" y1="-6" x2="-6" y2="-6" width="0.2" layer="51"/>
<wire x1="-6" y1="-6" x2="-6" y2="6" width="0.2" layer="51"/>
<wire x1="-6" y1="6" x2="6" y2="6" width="0.2" layer="21"/>
<wire x1="6" y1="6" x2="6" y2="-6" width="0.2" layer="21"/>
<wire x1="6" y1="-6" x2="-6" y2="-6" width="0.2" layer="21"/>
<wire x1="-6" y1="-6" x2="-6" y2="6" width="0.2" layer="21"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="5X5">
<smd name="1" x="-1.85" y="0" dx="5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="1.85" y="0" dx="5" dy="1.8" layer="1" rot="R90"/>
<text x="-1.8" y="3.1" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3" y1="2.7" x2="3" y2="2.7" width="0.2" layer="51"/>
<wire x1="3" y1="2.7" x2="3" y2="-2.7" width="0.2" layer="51"/>
<wire x1="3" y1="-2.7" x2="-3" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-3" y1="-2.7" x2="-3" y2="2.7" width="0.2" layer="51"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-3" y1="2.7" x2="3" y2="2.7" width="0.2" layer="21"/>
<wire x1="3" y1="2.7" x2="3" y2="-2.7" width="0.2" layer="21"/>
<wire x1="3" y1="-2.7" x2="-3" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-3" y1="-2.7" x2="-3" y2="2.7" width="0.2" layer="21"/>
</package>
<package name="3.5/7.5">
<pad name="P$1" x="-1.75" y="0" drill="0.7"/>
<pad name="P$2" x="1.75" y="0" drill="0.7"/>
<circle x="0" y="0" radius="3.8" width="0.2" layer="21"/>
<text x="-1.7" y="4.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="3.8" width="0.2" layer="51"/>
</package>
<package name="PS8D43">
<smd name="1" x="-4" y="0" dx="3" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="4" y="0" dx="3" dy="1.8" layer="1" rot="R90"/>
<wire x1="-3.7" y1="1.8" x2="3.7" y2="1.8" width="0.2" layer="21" curve="-130"/>
<wire x1="3.7" y1="-1.8" x2="-3.7" y2="-1.8" width="0.2" layer="21" curve="-130"/>
<text x="-1.7" y="4.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="4.159325" width="0.2" layer="51"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="7X7">
<smd name="1" x="-2.525" y="0" dx="2.4" dy="6.4" layer="1"/>
<smd name="2" x="2.525" y="0" dx="2.4" dy="6.4" layer="1"/>
<text x="-1.8" y="3.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-4" y1="3.6" x2="4" y2="3.6" width="0.2" layer="21"/>
<wire x1="4" y1="3.6" x2="4" y2="-3.6" width="0.2" layer="21"/>
<wire x1="4" y1="-3.6" x2="-4" y2="-3.6" width="0.2" layer="21"/>
<wire x1="-4" y1="-3.6" x2="-4" y2="3.6" width="0.2" layer="21"/>
<wire x1="-4" y1="3.6" x2="-4" y2="-3.6" width="0.2" layer="51"/>
<wire x1="-4" y1="-3.6" x2="4" y2="-3.6" width="0.2" layer="51"/>
<wire x1="4" y1="-3.6" x2="4" y2="3.6" width="0.2" layer="51"/>
<wire x1="4" y1="3.6" x2="-4" y2="3.6" width="0.2" layer="51"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="3X3">
<smd name="1" x="-0.95" y="0" dx="1.2" dy="3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.2" dy="3" layer="1"/>
<text x="-1.7" y="2.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.85" y1="1.85" x2="1.85" y2="1.85" width="0.2" layer="21"/>
<wire x1="1.85" y1="1.85" x2="1.85" y2="-1.85" width="0.2" layer="21"/>
<wire x1="1.85" y1="-1.85" x2="-1.85" y2="-1.85" width="0.2" layer="21"/>
<wire x1="-1.85" y1="-1.85" x2="-1.85" y2="1.85" width="0.2" layer="21"/>
<wire x1="-1.85" y1="1.85" x2="1.85" y2="1.85" width="0.2" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.85" y2="-1.85" width="0.2" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="-1.85" y2="-1.85" width="0.2" layer="51"/>
<wire x1="-1.85" y1="-1.85" x2="-1.85" y2="1.85" width="0.2" layer="51"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="8X8">
<smd name="1" x="-2.75" y="0" dx="8" dy="2.5" layer="1" rot="R90"/>
<smd name="2" x="2.75" y="0" dx="8" dy="2.5" layer="1" rot="R90"/>
<text x="-1.9" y="4.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-4.2" y1="4.2" x2="4.2" y2="4.2" width="0.2" layer="51"/>
<wire x1="4.2" y1="4.2" x2="4.2" y2="-4.2" width="0.2" layer="51"/>
<wire x1="4.2" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2" layer="51"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="4.2" width="0.2" layer="51"/>
<wire x1="-4.2" y1="4.2" x2="4.2" y2="4.2" width="0.2" layer="21"/>
<wire x1="4.2" y1="4.2" x2="4.2" y2="-4.2" width="0.2" layer="21"/>
<wire x1="4.2" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="4.2" width="0.2" layer="21"/>
<text x="-1.9" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="4X4">
<smd name="1" x="-1.35" y="0" dx="1.2" dy="4" layer="1"/>
<smd name="2" x="1.35" y="0" dx="1.2" dy="4" layer="1"/>
<text x="-1.6" y="2.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-2.2" y1="2.3" x2="2.2" y2="2.3" width="0.2" layer="21"/>
<wire x1="2.2" y1="2.3" x2="2.2" y2="-2.3" width="0.2" layer="21"/>
<wire x1="2.2" y1="-2.3" x2="-2.2" y2="-2.3" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-2.3" x2="-2.2" y2="2.3" width="0.2" layer="21"/>
<wire x1="-2.2" y1="2.3" x2="2.2" y2="2.3" width="0.2" layer="51"/>
<wire x1="2.2" y1="2.3" x2="2.2" y2="-2.3" width="0.2" layer="51"/>
<wire x1="2.2" y1="-2.3" x2="-2.2" y2="-2.3" width="0.2" layer="51"/>
<wire x1="-2.2" y1="-2.3" x2="-2.2" y2="2.3" width="0.2" layer="51"/>
<text x="-1.6" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="6X6">
<smd name="1" x="-2.15" y="0" dx="6" dy="2" layer="1" rot="R90"/>
<smd name="2" x="2.15" y="0" dx="6" dy="2" layer="1" rot="R90"/>
<text x="-1.7" y="3.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.4" y1="3.2" x2="3.4" y2="3.2" width="0.2" layer="21"/>
<wire x1="3.4" y1="3.2" x2="3.4" y2="-3.2" width="0.2" layer="21"/>
<wire x1="3.4" y1="-3.2" x2="-3.4" y2="-3.2" width="0.2" layer="21"/>
<wire x1="-3.4" y1="-3.2" x2="-3.4" y2="3.2" width="0.2" layer="21"/>
<wire x1="-3.4" y1="3.2" x2="3.4" y2="3.2" width="0.2" layer="51"/>
<wire x1="3.4" y1="3.2" x2="3.4" y2="-3.2" width="0.2" layer="51"/>
<wire x1="3.4" y1="-3.2" x2="-3.4" y2="-3.2" width="0.2" layer="51"/>
<wire x1="-3.4" y1="-3.2" x2="-3.4" y2="3.2" width="0.2" layer="51"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="2X2">
<smd name="1" x="-0.65" y="0" dx="0.7" dy="2" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="2" layer="1"/>
<wire x1="-1.3" y1="-1.3" x2="1.3" y2="-1.3" width="0.2" layer="21"/>
<wire x1="1.3" y1="-1.3" x2="1.3" y2="1.3" width="0.2" layer="21"/>
<wire x1="1.3" y1="1.3" x2="-1.3" y2="1.3" width="0.2" layer="21"/>
<wire x1="-1.3" y1="1.3" x2="-1.3" y2="-1.3" width="0.2" layer="21"/>
<text x="-1.8" y="1.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.3" y1="1.3" x2="1.3" y2="1.3" width="0.2" layer="51"/>
<wire x1="1.3" y1="1.3" x2="1.3" y2="-1.3" width="0.2" layer="51"/>
<wire x1="1.3" y1="-1.3" x2="-1.3" y2="-1.3" width="0.2" layer="51"/>
<wire x1="-1.3" y1="-1.3" x2="-1.3" y2="1.3" width="0.2" layer="51"/>
<text x="-1.1" y="-0.3" size="0.6096" layer="51" font="vector">&gt;Name</text>
</package>
<package name="5/8">
<pad name="P$1" x="-2.5" y="0" drill="1"/>
<pad name="P$2" x="2.5" y="0" drill="1"/>
<circle x="0" y="0" radius="4.1" width="0.2" layer="21"/>
<text x="-1.7" y="4.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="4.1" width="0.2" layer="51"/>
</package>
<package name="1206[3216-METRIC]-POL">
<smd name="P" x="-1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<smd name="N" x="1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<text x="-1.1" y="1.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-2.1" y1="-1.1" x2="-1.9" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-1.7" y1="-1.1" x2="2.3" y2="-1.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="-1.1" x2="2.3" y2="1.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="1.1" x2="-1.7" y2="1.1" width="0.2" layer="51"/>
<wire x1="-1.7" y1="1.1" x2="-1.9" y2="1.1" width="0.2" layer="51"/>
<wire x1="-2.1" y1="1.1" x2="-2.3" y2="1.1" width="0.2" layer="51"/>
<wire x1="-1.7" y1="1.1" x2="-1.7" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-1.7" y1="-1.1" x2="-1.9" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-1.9" y1="-1.1" x2="-1.9" y2="1.1" width="0.2" layer="51"/>
<wire x1="-1.9" y1="1.1" x2="-2.1" y2="1.1" width="0.2" layer="51"/>
<wire x1="-2.1" y1="1.1" x2="-2.1" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-2.1" y1="-1.1" x2="-2.3" y2="-1.1" width="0.2" layer="51"/>
<wire x1="-2.3" y1="-1.1" x2="-2.3" y2="1.1" width="0.2" layer="51"/>
<wire x1="-1.4" y1="0" x2="-0.8" y2="0" width="0.2" layer="51"/>
<wire x1="-1.1" y1="0.3" x2="-1.1" y2="-0.3" width="0.2" layer="51"/>
<text x="-0.6" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2.5" y1="1.3" x2="-0.5" y2="1.3" width="0.2" layer="21"/>
<wire x1="-0.5" y1="1.3" x2="-0.3" y2="1.3" width="0.2" layer="21"/>
<wire x1="-0.3" y1="1.3" x2="2.5" y2="1.3" width="0.2" layer="21"/>
<wire x1="2.5" y1="1.3" x2="2.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="2.5" y1="-1.3" x2="-0.3" y2="-1.3" width="0.2" layer="21"/>
<wire x1="-0.3" y1="-1.3" x2="-0.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="-0.5" y1="-1.3" x2="-2.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="-2.5" y1="-1.3" x2="-2.5" y2="1.3" width="0.2" layer="21"/>
<wire x1="-0.5" y1="1.3" x2="-0.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="-0.3" y1="1.3" x2="-0.3" y2="-1.3" width="0.2" layer="21"/>
</package>
<package name="E2-5">
<pad name="-" x="-1" y="0" drill="0.8" shape="octagon"/>
<pad name="+" x="1" y="0" drill="0.8"/>
<circle x="0" y="0" radius="2.6" width="0.2" layer="21"/>
<wire x1="0.6" y1="1.4" x2="1.4" y2="1.4" width="0.2" layer="21"/>
<wire x1="1" y1="1.8" x2="1" y2="1" width="0.2" layer="21"/>
<text x="-1.2" y="3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="2.6" width="0.2" layer="51"/>
<wire x1="1" y1="1.8" x2="1" y2="1" width="0.2" layer="51"/>
<wire x1="0.6" y1="1.4" x2="1.4" y2="1.4" width="0.2" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="C-STYLE">
<pad name="+" x="-3" y="0" drill="1.1"/>
<pad name="-" x="3" y="0" drill="1.1" shape="octagon"/>
<circle x="0" y="0" radius="11" width="0.2" layer="21"/>
<wire x1="-4.8" y1="0" x2="-6.4" y2="0" width="0.2" layer="21"/>
<wire x1="-5.6" y1="0.8" x2="-5.6" y2="-0.8" width="0.2" layer="21"/>
<wire x1="6.4" y1="0" x2="4.8" y2="0" width="0.2" layer="21"/>
<text x="-1.7" y="2.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="11" width="0.2" layer="51"/>
<wire x1="-5.6" y1="0.8" x2="-5.6" y2="-0.8" width="0.2" layer="51"/>
<wire x1="-4.8" y1="0" x2="-6.4" y2="0" width="0.2" layer="51"/>
<wire x1="6.4" y1="0" x2="4.8" y2="0" width="0.2" layer="51"/>
<text x="-1.7" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<circle x="-3" y="0" radius="0.70710625" width="0.2" layer="51"/>
<circle x="3" y="0" radius="0.70710625" width="0.2" layer="51"/>
</package>
<package name="E3.5-8">
<pad name="-" x="-1.75" y="0" drill="0.8" shape="octagon"/>
<pad name="+" x="1.75" y="0" drill="0.8"/>
<circle x="0" y="0" radius="4.1" width="0.2" layer="21"/>
<wire x1="1.4" y1="1.7" x2="2.2" y2="1.7" width="0.2" layer="21"/>
<wire x1="1.8" y1="2.1" x2="1.8" y2="1.3" width="0.2" layer="21"/>
<text x="-1.7" y="4.4" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="4.1" width="0.2" layer="51"/>
<wire x1="1.8" y1="2.1" x2="1.8" y2="1.3" width="0.2" layer="51"/>
<wire x1="1.4" y1="1.7" x2="2.2" y2="1.7" width="0.2" layer="51"/>
<text x="-1.7" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="E5-10">
<pad name="-" x="-2.5" y="0" drill="1" diameter="2" shape="octagon"/>
<pad name="+" x="2.5" y="0" drill="1" diameter="2"/>
<circle x="0" y="0" radius="5.1" width="0.2" layer="21"/>
<wire x1="1.4" y1="1.7" x2="2.2" y2="1.7" width="0.2" layer="21"/>
<wire x1="1.8" y1="2.1" x2="1.8" y2="1.3" width="0.2" layer="21"/>
<text x="-1.7" y="5.4" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="5.1" width="0.2" layer="51"/>
<wire x1="1.8" y1="2.1" x2="1.8" y2="1.3" width="0.2" layer="51"/>
<wire x1="1.4" y1="1.7" x2="2.2" y2="1.7" width="0.2" layer="51"/>
<text x="-1.7" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="SOT-23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;p&gt;
package type OT</description>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-2.1" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-1.6" y1="-1.9" x2="1.6" y2="-1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="-1.9" x2="1.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="1.9" x2="0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="0.6" y1="1.9" x2="0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="0.6" y1="0.7" x2="-0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="-0.6" y1="0.7" x2="-0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="-0.6" y1="1.9" x2="-1.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="-1.6" y1="1.9" x2="-1.6" y2="-1.9" width="0.2" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-0.4" y1="0.8" x2="0.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.2" layer="21"/>
</package>
<package name="FIDUCIAL">
<polygon width="0" layer="29">
<vertex x="1" y="0" curve="180"/>
<vertex x="-1" y="0" curve="180"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="0" y="-1"/>
<vertex x="0" y="-0.6" curve="-180"/>
<vertex x="0" y="0.6"/>
<vertex x="0" y="1" curve="180"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="0" y="1"/>
<vertex x="0" y="0.6" curve="-180"/>
<vertex x="0" y="-0.6"/>
<vertex x="0" y="-1" curve="180"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0" y="-0.4" curve="180"/>
<vertex x="0" y="0.4" curve="180"/>
</polygon>
<circle x="0" y="0" radius="0.9" width="0.2" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="51"/>
</package>
<package name="FIDUCIAL_BORDER">
<polygon width="0" layer="29">
<vertex x="1" y="0" curve="180"/>
<vertex x="-1" y="0" curve="180"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="0" y="-1"/>
<vertex x="0" y="-0.6" curve="-180"/>
<vertex x="0" y="0.6"/>
<vertex x="0" y="1" curve="180"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="0" y="1"/>
<vertex x="0" y="0.6" curve="-180"/>
<vertex x="0" y="-0.6"/>
<vertex x="0" y="-1" curve="180"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0" y="-0.4" curve="180"/>
<vertex x="0" y="0.4" curve="180"/>
</polygon>
<circle x="0" y="0" radius="0.9" width="0.2" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="51"/>
<polygon width="0.2" layer="1">
<vertex x="0" y="1.1" curve="180"/>
<vertex x="0" y="-1.1"/>
<vertex x="0" y="-2.3" curve="-180"/>
<vertex x="0" y="2.3"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0" y="-1.1" curve="180"/>
<vertex x="0" y="1.1"/>
<vertex x="0" y="2.3" curve="-180"/>
<vertex x="0" y="-2.3"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="TRANSISTOR-NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="5.08" y="-5.08" size="1.778" layer="96" font="vector">&gt;Value</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="5.08" y="3.556" size="1.778" layer="95" font="vector">&gt;Name</text>
</symbol>
<symbol name="RESISTOR">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="-2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="3.81" y="1.524" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="MOSFET-N">
<wire x1="-3.6576" y1="2.413" x2="-3.6576" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="-2.0066" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="1.905" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="3.175" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.762" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="3.175" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.762" x2="3.429" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.762" x2="1.651" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="0" x2="-0.762" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0.508" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0.254" x2="-1.778" y2="0" width="0.3048" layer="94"/>
<wire x1="-1.778" y1="0" x2="-0.889" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="-0.889" y1="-0.254" x2="-0.889" y2="0" width="0.3048" layer="94"/>
<wire x1="-0.889" y1="0" x2="-1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="2.54" y="3.556" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="2.54" y="-5.08" size="1.778" layer="96" font="vector">&gt;Value</text>
<rectangle x1="-2.794" y1="-2.54" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.54" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="0" visible="off" length="point" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="0" y1="-1.905" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.0254" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.0254" y2="-1.905" width="0.1524" layer="94"/>
</symbol>
<symbol name="MOSFET-P">
<wire x1="-3.6576" y1="-2.413" x2="-3.6576" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-2.0066" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.905" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="3.175" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="1.905" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.762" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="1.905" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.762" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.762" x2="3.429" y2="0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.508" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="0" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="0.254" x2="-0.254" y2="0" width="0.3048" layer="94"/>
<wire x1="-0.254" y1="0" x2="-1.143" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="2.54" y="3.556" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="2.54" y="-5.08" size="1.778" layer="96" font="vector">&gt;Value</text>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.54" layer="94"/>
<rectangle x1="-2.794" y1="-2.54" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="0" visible="off" length="point" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="CAPACITOR">
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<rectangle x1="-0.762" y1="-2.032" x2="-0.254" y2="2.032" layer="94"/>
<rectangle x1="0.254" y1="-2.032" x2="0.762" y2="2.032" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-4.572" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="CRYSTAL">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-4.572" size="1.778" layer="96" font="vector">&gt;Value</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CRYSTAL-GND">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-4.572" size="1.778" layer="96" font="vector">&gt;Value</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="GND" x="0" y="-2.54" visible="off" length="point"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.524" width="0.254" layer="94"/>
</symbol>
<symbol name="DIODE">
<pin name="C" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="A" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="1.27" y1="1.016" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.016" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="1.016" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.016" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="3.81" y="1.778" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96" font="vector">&gt;Value</text>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="8PIN-CHIP">
<pin name="4" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="5" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-10.16" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="6.35" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-6.858" y="4.064" size="1.778" layer="95" font="vector">&gt;P1</text>
<text x="-6.858" y="1.524" size="1.778" layer="95" font="vector">&gt;P2</text>
<text x="-6.858" y="-1.016" size="1.778" layer="95" font="vector">&gt;P3</text>
<text x="-6.858" y="-3.556" size="1.778" layer="95" font="vector">&gt;P4</text>
<text x="6.858" y="-3.556" size="1.778" layer="95" font="vector" rot="MR0">&gt;P5</text>
<text x="-4.318" y="9.144" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-3.302" y="-6.604" size="1.778" layer="95" font="vector" rot="MR180">&gt;Value</text>
<pin name="6" x="10.16" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="7" x="10.16" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="8" x="10.16" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<text x="6.858" y="-1.016" size="1.778" layer="95" font="vector" rot="MR0">&gt;P6</text>
<text x="6.858" y="1.524" size="1.778" layer="95" font="vector" rot="MR0">&gt;P7</text>
<text x="6.858" y="4.064" size="1.778" layer="95" font="vector" rot="MR0">&gt;P8</text>
<wire x1="-7.62" y1="6.35" x2="-6.35" y2="7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="WIRE_SOLDER">
<circle x="0" y="0" radius="0.762" width="0.254" layer="94"/>
<pin name="P$1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="0" y="2.54" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="7.62" y="-2.54" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
<symbol name="FERRITE/INDUCTOR">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="3.81" y="1.524" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96" font="vector">&gt;Value</text>
<rectangle x1="-2.667" y1="-1.016" x2="2.667" y2="0.508" layer="94"/>
<rectangle x1="-2.667" y1="0.762" x2="2.667" y2="1.016" layer="94"/>
</symbol>
<symbol name="CAPACITOR-POL">
<pin name="N" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="P" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<rectangle x1="-0.762" y1="-2.032" x2="-0.254" y2="2.032" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-4.572" size="1.778" layer="96" font="vector">&gt;Value</text>
<rectangle x1="0.254" y1="-2.032" x2="0.3556" y2="2.032" layer="94"/>
<rectangle x1="0.6604" y1="-2.032" x2="0.762" y2="2.032" layer="94"/>
<rectangle x1="0.3556" y1="1.9304" x2="0.6604" y2="2.032" layer="94"/>
<rectangle x1="0.3556" y1="-2.032" x2="0.6604" y2="-1.9304" layer="94"/>
<rectangle x1="1.5748" y1="-1.7272" x2="1.7272" y2="-0.6604" layer="94"/>
<rectangle x1="1.5748" y1="-1.7272" x2="1.7272" y2="-0.6604" layer="94" rot="R90"/>
</symbol>
<symbol name="SOT23-5">
<pin name="4" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="5" x="10.16" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-6.858" y="1.524" size="1.778" layer="95" font="vector">&gt;P1</text>
<text x="-6.858" y="-1.016" size="1.778" layer="95" font="vector">&gt;P2</text>
<text x="-6.858" y="-3.556" size="1.778" layer="95" font="vector">&gt;P3</text>
<text x="6.858" y="-3.556" size="1.778" layer="95" font="vector" rot="MR0">&gt;P4</text>
<text x="6.858" y="1.524" size="1.778" layer="95" font="vector" rot="MR0">&gt;P5</text>
<text x="-4.318" y="6.604" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-4.318" y="-8.636" size="1.778" layer="95" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="FIDUCIAL">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<circle x="0" y="0" radius="1.04726875" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.915809375" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.915809375" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.803215625" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.567959375" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.359209375" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.127" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TRANSISTOR-NPN" prefix="T" uservalue="yes">
<description>TRANSISTOR NPN</description>
<gates>
<gate name="T" symbol="TRANSISTOR-NPN" x="0" y="0"/>
</gates>
<devices>
<device name="-SOT-23" package="SOT-23">
<connects>
<connect gate="T" pin="B" pad="1"/>
<connect gate="T" pin="C" pad="3"/>
<connect gate="T" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO-92" package="TO-92">
<connects>
<connect gate="T" pin="B" pad="2"/>
<connect gate="T" pin="C" pad="3"/>
<connect gate="T" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT-323" package="SOT-323">
<connects>
<connect gate="T" pin="B" pad="1"/>
<connect gate="T" pin="C" pad="3"/>
<connect gate="T" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>RESISTOR</description>
<gates>
<gate name="R" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805[2012-METRIC]" package="0805[2012-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0201[0603-METRIC]" package="0201[0603-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206[3216-METRIC]" package="1206[3216-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2512[6332-METRIC]" package="2512[6332-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-N" prefix="T">
<gates>
<gate name="G$1" symbol="MOSFET-N" x="0" y="0"/>
</gates>
<devices>
<device name="-SOT-23" package="SOT-23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT-523" package="SOT-523">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-P" prefix="T" uservalue="yes">
<description>MOSFET-P</description>
<gates>
<gate name="T" symbol="MOSFET-P" x="0" y="0"/>
</gates>
<devices>
<device name="-SOT-23" package="SOT-23">
<connects>
<connect gate="T" pin="D" pad="3"/>
<connect gate="T" pin="G" pad="1"/>
<connect gate="T" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT-523" package="SOT-523">
<connects>
<connect gate="T" pin="D" pad="3"/>
<connect gate="T" pin="G" pad="1"/>
<connect gate="T" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT-223" package="SOT-223">
<connects>
<connect gate="T" pin="D" pad="2"/>
<connect gate="T" pin="G" pad="1"/>
<connect gate="T" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>CAPACITOR</description>
<gates>
<gate name="C" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1812[4532-METRIC]" package="1812[4532-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603[1608-METRIC]-0OHM" package="0603[1608-METRIC]-0OHM-ON">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206[3216-METRIC]" package="1206[3216-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805[2012-METRIC]" package="0805[2012-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0201[0603-METRIC]" package="0201[0603-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL" prefix="Q" uservalue="yes">
<description>CRYSTAL</description>
<gates>
<gate name="Q" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="-HC49" package="HC49">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3.2X1.5" package="3.2X1.5">
<connects>
<connect gate="Q" pin="1" pad="P$1"/>
<connect gate="Q" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3.2X2.5" package="3.2X2.5">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2X1.2" package="2X1.2">
<connects>
<connect gate="Q" pin="1" pad="P$1"/>
<connect gate="Q" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-HC49UP" package="HC49UP">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL-GND" prefix="Q">
<description>CRYSTAL</description>
<gates>
<gate name="Q" symbol="CRYSTAL-GND" x="0" y="0"/>
</gates>
<devices>
<device name="-3.2X2.5" package="3.2X2.5">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
<connect gate="Q" pin="GND" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2X1.6" package="2X1.6">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
<connect gate="Q" pin="GND" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7X5" package="7X5">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
<connect gate="Q" pin="GND" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>DIODE</description>
<gates>
<gate name="D" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]-DIODE">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD-323" package="SOD-323">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMA" package="SMA-DIODE">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MICROMELF" package="MICROMELF">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-POWER-DI-123" package="POWER_DI_123">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]-DIODE">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD-123" package="SOD-123">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="8PIN-CHIP">
<gates>
<gate name="G$1" symbol="8PIN-CHIP" x="0" y="0"/>
</gates>
<devices>
<device name="-SOIC-8" package="SOIC-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-DIP-8" package="DIP-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT-23-8" package="SOT-23-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WIRE_SOLDER">
<gates>
<gate name="G$1" symbol="WIRE_SOLDER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WIRE_SOLDER">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FERRITE/INDUCTOR" prefix="L" uservalue="yes">
<description>FERRITE/INDUCTOR</description>
<gates>
<gate name="L" symbol="FERRITE/INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-12X12" package="12X12">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5X5" package="5X5">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3.5/7.5" package="3.5/7.5">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PS8D43" package="PS8D43">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7X7" package="7X7">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3X3" package="3X3">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206[3216-METRIC]" package="1206[3216-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805[2012-METRIC]" package="0805[2012-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-8X8" package="8X8">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4X4" package="4X4">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6X6" package="6X6">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0201[0603-METRIC]" package="0201[0603-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2X2" package="2X2">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5/8" package="5/8">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR-POL" prefix="C" uservalue="yes">
<description>CAPACITOR-POL</description>
<gates>
<gate name="C" symbol="CAPACITOR-POL" x="0" y="0"/>
</gates>
<devices>
<device name="-1206[3216-METRIC][CASE-A]" package="1206[3216-METRIC]-POL">
<connects>
<connect gate="C" pin="N" pad="N"/>
<connect gate="C" pin="P" pad="P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-E2-5" package="E2-5">
<connects>
<connect gate="C" pin="N" pad="-"/>
<connect gate="C" pin="P" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-C-STYLE" package="C-STYLE">
<connects>
<connect gate="C" pin="N" pad="-"/>
<connect gate="C" pin="P" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-E3.5-8" package="E3.5-8">
<connects>
<connect gate="C" pin="N" pad="-"/>
<connect gate="C" pin="P" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-E5-10" package="E5-10">
<connects>
<connect gate="C" pin="N" pad="-"/>
<connect gate="C" pin="P" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOT23-5">
<gates>
<gate name="G$1" symbol="SOT23-5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23-5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" prefix="FIDUCIAL">
<description>FIDUCIAL</description>
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-BORDER" package="FIDUCIAL_BORDER">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_POWER">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="+5V">
<pin name="+5V" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="VIN">
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VIN" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VIN">
<gates>
<gate name="G$1" symbol="VIN" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="oled">
<packages>
<package name="OLED">
<pad name="5" x="-1.27" y="23.3" drill="1"/>
<pad name="4" x="-3.81" y="23.3" drill="1"/>
<pad name="3" x="-6.35" y="23.3" drill="1"/>
<pad name="2" x="-8.89" y="23.3" drill="1"/>
<pad name="1" x="-11.43" y="23.3" drill="1"/>
<pad name="6" x="1.27" y="23.3" drill="1"/>
<pad name="7" x="3.81" y="23.3" drill="1"/>
<pad name="8" x="6.35" y="23.3" drill="1"/>
<pad name="9" x="8.89" y="23.3" drill="1"/>
<pad name="10" x="11.43" y="23.3" drill="1"/>
<hole x="-24.9" y="24.4" drill="2.6"/>
<hole x="24.9" y="24.4" drill="2.6"/>
<hole x="24.9" y="-24.4" drill="2.6"/>
<hole x="-24.9" y="-24.4" drill="2.6"/>
<wire x1="-31" y1="18.7" x2="31" y2="18.7" width="0.2" layer="51"/>
<wire x1="31" y1="18.7" x2="31" y2="-19.5" width="0.2" layer="51"/>
<wire x1="31" y1="-19.5" x2="-31" y2="-19.5" width="0.2" layer="51"/>
<wire x1="-31" y1="-19.5" x2="-31" y2="18.7" width="0.2" layer="51"/>
<wire x1="-28" y1="16.75" x2="28" y2="16.75" width="0.2" layer="51"/>
<wire x1="28" y1="16.75" x2="28" y2="-10.75" width="0.2" layer="51"/>
<wire x1="28" y1="-10.75" x2="-28" y2="-10.75" width="0.2" layer="51"/>
<wire x1="-28" y1="-10.75" x2="-28" y2="16.75" width="0.2" layer="51"/>
<smd name="FCC4" x="-7.5" y="-3.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<smd name="FCC3" x="-7.5" y="-2.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<smd name="FCC2" x="-7.5" y="-1.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<smd name="FCC1" x="-7.5" y="-0.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<smd name="FCCG2" x="-9.95" y="-12.05" dx="2.5" dy="2.8" layer="16" rot="R90"/>
<smd name="FCCG1" x="-9.95" y="2.45" dx="2.5" dy="2.8" layer="16" rot="R90"/>
<smd name="FCC5" x="-7.5" y="-4.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<smd name="FCC6" x="-7.5" y="-5.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<smd name="FCC7" x="-7.5" y="-6.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<smd name="FCC8" x="-7.5" y="-7.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<smd name="FCC9" x="-7.5" y="-8.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<smd name="FCC10" x="-7.5" y="-9.3" dx="0.6" dy="1.7" layer="16" rot="R90"/>
<polygon width="0.2" layer="40">
<vertex x="-11.3" y="3.8"/>
<vertex x="0" y="3.8"/>
<vertex x="0" y="-0.1"/>
<vertex x="0" y="-13.4"/>
<vertex x="-11.2" y="-13.4"/>
<vertex x="-30.9" y="-13.4"/>
<vertex x="-30.9" y="2.3"/>
<vertex x="-30.9" y="3.8"/>
</polygon>
<circle x="-24.9" y="24.4" radius="3" width="0.127" layer="41"/>
<circle x="24.9" y="24.4" radius="3" width="0.127" layer="41"/>
<circle x="-24.9" y="-24.4" radius="3" width="0.127" layer="41"/>
<circle x="24.9" y="-24.4" radius="3" width="0.127" layer="41"/>
<circle x="-24.9" y="24.4" radius="3" width="0.127" layer="42"/>
<circle x="24.9" y="24.4" radius="3" width="0.127" layer="42"/>
<circle x="24.9" y="-24.4" radius="3" width="0.127" layer="42"/>
<circle x="-24.9" y="-24.4" radius="3" width="0.127" layer="42"/>
<wire x1="-8.2" y1="2.8" x2="-6" y2="2.8" width="0.2" layer="22"/>
<wire x1="-6" y1="2.8" x2="-6" y2="-12.4" width="0.2" layer="22"/>
<wire x1="-6" y1="-12.4" x2="-8.2" y2="-12.4" width="0.2" layer="22"/>
<wire x1="-11.7" y1="2.8" x2="-12" y2="2.8" width="0.2" layer="22"/>
<wire x1="-12" y1="2.8" x2="-12" y2="-12.4" width="0.2" layer="22"/>
<wire x1="-12" y1="-12.4" x2="-11.7" y2="-12.4" width="0.2" layer="22"/>
<text x="-12.3" y="-2.2" size="0.8128" layer="26" font="vector" rot="MR270">&gt;Name</text>
<wire x1="-11.7" y1="2.8" x2="-12" y2="2.8" width="0.2" layer="52"/>
<wire x1="-12" y1="2.8" x2="-12" y2="-12.4" width="0.2" layer="52"/>
<wire x1="-12" y1="-12.4" x2="-11.7" y2="-12.4" width="0.2" layer="52"/>
<wire x1="-8.2" y1="-12.4" x2="-6" y2="-12.4" width="0.2" layer="52"/>
<wire x1="-6" y1="-12.4" x2="-6" y2="2.8" width="0.2" layer="52"/>
<wire x1="-6" y1="2.8" x2="-8.2" y2="2.8" width="0.2" layer="52"/>
<text x="-7.3" y="-5.1" size="0.8128" layer="52" font="vector" rot="MR0">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="OLED">
<pin name="VCC@1" x="-5.08" y="10.16" visible="pin" length="short" direction="pas"/>
<pin name="VCC@2" x="-5.08" y="7.62" visible="pin" length="short" direction="pas"/>
<pin name="EN" x="-5.08" y="5.08" visible="pin" length="short" direction="pas"/>
<pin name="GND@4" x="-5.08" y="2.54" visible="pin" length="short" direction="pas"/>
<pin name="CS" x="-5.08" y="0" visible="pin" length="short" direction="pas"/>
<pin name="RES" x="-5.08" y="-2.54" visible="pin" length="short" direction="pas"/>
<pin name="D/C" x="-5.08" y="-5.08" visible="pin" length="short" direction="pas"/>
<pin name="GND@8" x="-5.08" y="-7.62" visible="pin" length="short" direction="pas"/>
<pin name="SCK" x="-5.08" y="-10.16" visible="pin" length="short" direction="pas"/>
<pin name="DIN" x="-5.08" y="-12.7" visible="pin" length="short" direction="pas"/>
<wire x1="-2.54" y1="12.7" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="-15.24" x2="-2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-2.54" y2="12.7" width="0.254" layer="94"/>
<text x="-2.54" y="15.24" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="5.08" y="-17.78" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
<pin name="GND" x="2.54" y="-17.78" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="OLED">
<gates>
<gate name="G$1" symbol="OLED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="OLED">
<connects>
<connect gate="G$1" pin="CS" pad="5 FCC5"/>
<connect gate="G$1" pin="D/C" pad="7 FCC7"/>
<connect gate="G$1" pin="DIN" pad="10 FCC10"/>
<connect gate="G$1" pin="EN" pad="3 FCC3"/>
<connect gate="G$1" pin="GND" pad="FCCG1 FCCG2"/>
<connect gate="G$1" pin="GND@4" pad="4 FCC4"/>
<connect gate="G$1" pin="GND@8" pad="8 FCC8"/>
<connect gate="G$1" pin="RES" pad="6 FCC6"/>
<connect gate="G$1" pin="SCK" pad="9 FCC9"/>
<connect gate="G$1" pin="VCC@1" pad="1 FCC1"/>
<connect gate="G$1" pin="VCC@2" pad="2 FCC2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="battery">
<packages>
<package name="BATTERY">
<wire x1="-26" y1="13.5" x2="26" y2="13.5" width="0.2" layer="51"/>
<wire x1="26" y1="13.5" x2="26" y2="-13.5" width="0.2" layer="51"/>
<wire x1="26" y1="-13.5" x2="-26" y2="-13.5" width="0.2" layer="51"/>
<wire x1="-26" y1="-13.5" x2="-26" y2="13.5" width="0.2" layer="51"/>
<text x="-3.2" y="-2.2" size="3.81" layer="51" font="vector">9V</text>
</package>
</packages>
<symbols>
<symbol name="BATTERY">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<text x="-1.524" y="-0.762" size="1.778" layer="94" font="vector">9V</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BATTERY">
<gates>
<gate name="G$1" symbol="BATTERY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BATTERY">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="banana">
<packages>
<package name="BANANA">
<circle x="0" y="0" radius="5.6" width="0.2" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="BANANA">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BANANA">
<gates>
<gate name="G$1" symbol="BANANA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BANANA">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_PINHEAD">
<packages>
<package name="PINHEADER-04X1-2.54">
<pad name="P$1" x="-3.81" y="0" drill="1.016" shape="square"/>
<pad name="P$2" x="-1.27" y="0" drill="1.016"/>
<pad name="P$3" x="1.27" y="0" drill="1.016"/>
<pad name="P$4" x="3.81" y="0" drill="1.016"/>
<wire x1="-4.318" y1="1.27" x2="4.318" y2="1.27" width="0.127" layer="37"/>
<wire x1="-4.318" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="0.508" width="0.127" layer="37"/>
<wire x1="5.08" y1="-0.508" x2="5.08" y2="0.508" width="0.127" layer="37"/>
<text x="-1.2" y="1.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-4.318" y1="1.27" x2="4.318" y2="1.27" width="0.127" layer="38"/>
<wire x1="-4.318" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="38"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="0.508" width="0.127" layer="38"/>
<wire x1="5.08" y1="-0.508" x2="5.08" y2="0.508" width="0.127" layer="38"/>
<wire x1="-5" y1="1.2" x2="5" y2="1.2" width="0.2" layer="51"/>
<wire x1="5" y1="1.2" x2="5" y2="-1.2" width="0.2" layer="51"/>
<wire x1="5" y1="-1.2" x2="-5" y2="-1.2" width="0.2" layer="51"/>
<wire x1="-5" y1="-1.2" x2="-5" y2="1.2" width="0.2" layer="51"/>
<wire x1="-4.5" y1="0.7" x2="-3.1" y2="0.7" width="0.2" layer="51"/>
<wire x1="-3.1" y1="0.7" x2="-3.1" y2="-0.7" width="0.2" layer="51"/>
<wire x1="-3.1" y1="-0.7" x2="-4.5" y2="-0.7" width="0.2" layer="51"/>
<wire x1="-4.5" y1="-0.7" x2="-4.5" y2="0.7" width="0.2" layer="51"/>
<text x="-1.6" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="PINHEADER-04X1_N-2.54">
<pad name="P$1" x="-3.81" y="0" drill="1.016"/>
<pad name="P$2" x="-1.27" y="0" drill="1.016"/>
<pad name="P$3" x="1.27" y="0" drill="1.016"/>
<pad name="P$4" x="3.81" y="0" drill="1.016"/>
<wire x1="-4.318" y1="1.27" x2="4.318" y2="1.27" width="0.127" layer="37"/>
<wire x1="-4.318" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="0.508" width="0.127" layer="37"/>
<wire x1="5.08" y1="-0.508" x2="5.08" y2="0.508" width="0.127" layer="37"/>
<text x="-1.6" y="1.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-4.318" y1="1.27" x2="4.318" y2="1.27" width="0.127" layer="38"/>
<wire x1="-4.318" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="38"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="0.508" width="0.127" layer="38"/>
<wire x1="5.08" y1="-0.508" x2="5.08" y2="0.508" width="0.127" layer="38"/>
<wire x1="-5.1" y1="1.3" x2="5.1" y2="1.3" width="0.2" layer="21"/>
<wire x1="5.1" y1="1.3" x2="5.1" y2="-1.3" width="0.2" layer="21"/>
<wire x1="5.1" y1="-1.3" x2="-5.1" y2="-1.3" width="0.2" layer="21"/>
<wire x1="-5.1" y1="-1.3" x2="-5.1" y2="1.3" width="0.2" layer="21"/>
<wire x1="-5.1" y1="1.3" x2="5.1" y2="1.3" width="0.2" layer="22"/>
<wire x1="5.1" y1="1.3" x2="5.1" y2="-1.3" width="0.2" layer="22"/>
<wire x1="5.1" y1="-1.3" x2="-5.1" y2="-1.3" width="0.2" layer="22"/>
<wire x1="-5.1" y1="-1.3" x2="-5.1" y2="1.3" width="0.2" layer="22"/>
<wire x1="-5.1" y1="1.3" x2="5.1" y2="1.3" width="0.2" layer="51"/>
<wire x1="5.1" y1="1.3" x2="5.1" y2="-1.3" width="0.2" layer="51"/>
<wire x1="5.1" y1="-1.3" x2="-5.1" y2="-1.3" width="0.2" layer="51"/>
<wire x1="-5.1" y1="-1.3" x2="-5.1" y2="1.3" width="0.2" layer="51"/>
<text x="-1.4" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="PINHEADER-01X1-2.54">
<pad name="P$1" x="0" y="0" drill="1.016" shape="square"/>
<wire x1="-0.508" y1="1.27" x2="0.508" y2="1.27" width="0.127" layer="37"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="37"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.508" width="0.127" layer="37"/>
<text x="-1.2" y="1.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-0.508" y1="1.27" x2="0.508" y2="1.27" width="0.127" layer="38"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.127" layer="38"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="38"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.127" layer="38"/>
<wire x1="-0.508" y1="1.27" x2="0.508" y2="1.27" width="0.2" layer="22"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.2" layer="22"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.2" layer="22"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.2" layer="22"/>
<wire x1="-0.508" y1="1.27" x2="0.508" y2="1.27" width="0.2" layer="21"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.2" layer="21"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.2" layer="21"/>
<text x="-0.9" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="1.360146875" width="0.2" layer="51"/>
</package>
<package name="PINHEADER-01X1_N-2.54">
<pad name="P$1" x="0" y="0" drill="1.016"/>
<wire x1="-0.508" y1="1.27" x2="0.508" y2="1.27" width="0.127" layer="37"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="37"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.508" width="0.127" layer="37"/>
<text x="-1.27" y="1.778" size="1.27" layer="25">&gt;Name</text>
<wire x1="-0.508" y1="1.27" x2="0.508" y2="1.27" width="0.127" layer="38"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.127" layer="38"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="38"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.127" layer="38"/>
</package>
</packages>
<symbols>
<symbol name="PINHEADER-04X1">
<pin name="P$1" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$2" x="2.54" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="8.89" size="1.778" layer="95">&gt;Name</text>
<text x="-1.778" y="4.064" size="1.778" layer="95">1</text>
<text x="-1.778" y="1.524" size="1.778" layer="95">2</text>
<pin name="P$3" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$4" x="2.54" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95">3</text>
<text x="-1.778" y="-3.556" size="1.778" layer="95">4</text>
</symbol>
<symbol name="PINHEADER-01X1">
<pin name="P$1" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="3.81" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-1.778" y="-1.016" size="1.778" layer="95">1</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHEADER-04X1" prefix="JP" uservalue="yes">
<description>PINHEADER-2.54-04X1</description>
<gates>
<gate name="JP" symbol="PINHEADER-04X1" x="0" y="0"/>
</gates>
<devices>
<device name="-2.54" package="PINHEADER-04X1-2.54">
<connects>
<connect gate="JP" pin="P$1" pad="P$1"/>
<connect gate="JP" pin="P$2" pad="P$2"/>
<connect gate="JP" pin="P$3" pad="P$3"/>
<connect gate="JP" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.54N" package="PINHEADER-04X1_N-2.54">
<connects>
<connect gate="JP" pin="P$1" pad="P$1"/>
<connect gate="JP" pin="P$2" pad="P$2"/>
<connect gate="JP" pin="P$3" pad="P$3"/>
<connect gate="JP" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEADER-01X1" prefix="JP" uservalue="yes">
<description>PINHEADER-2.54-01X1</description>
<gates>
<gate name="JP" symbol="PINHEADER-01X1" x="0" y="0"/>
</gates>
<devices>
<device name="-2.54" package="PINHEADER-01X1-2.54">
<connects>
<connect gate="JP" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.54N" package="PINHEADER-01X1_N-2.54">
<connects>
<connect gate="JP" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="nordic">
<description>Generated from &lt;b&gt;t.sch&lt;/b&gt;&lt;p&gt;
by exp-project-lbr.ulp</description>
<packages>
<package name="NORDICSEMI_2450BM14A0002">
<wire x1="-0.9" y1="-0.6" x2="0.9" y2="-0.6" width="0.2" layer="51"/>
<wire x1="0.9" y1="-0.6" x2="0.9" y2="0.6" width="0.2" layer="51"/>
<wire x1="0.9" y1="0.6" x2="-0.9" y2="0.6" width="0.2" layer="51"/>
<wire x1="-0.9" y1="0.6" x2="-0.9" y2="-0.6" width="0.2" layer="51"/>
<rectangle x1="0.39" y1="-0.075" x2="0.63" y2="0.08" layer="51"/>
<smd name="1" x="0.5" y="0.425" dx="0.55" dy="0.25" layer="1" rot="R90"/>
<smd name="2" x="0" y="0.425" dx="0.55" dy="0.25" layer="1" rot="R90"/>
<smd name="3" x="-0.5" y="0.425" dx="0.55" dy="0.25" layer="1" rot="R90"/>
<smd name="4" x="-0.5" y="-0.425" dx="0.55" dy="0.25" layer="1" rot="R90"/>
<smd name="5" x="0" y="-0.425" dx="0.55" dy="0.25" layer="1" rot="R90"/>
<smd name="6" x="0.5" y="-0.425" dx="0.55" dy="0.25" layer="1" rot="R90"/>
<text x="-1.5" y="1.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-0.9" y1="0.7" x2="-1.1" y2="0.7" width="0.2" layer="21"/>
<wire x1="-1.1" y1="0.7" x2="-1.1" y2="-0.7" width="0.2" layer="21"/>
<wire x1="-1.1" y1="-0.7" x2="-0.9" y2="-0.7" width="0.2" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.1" y2="0.7" width="0.2" layer="21"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.7" width="0.2" layer="21"/>
<wire x1="1.1" y1="-0.7" x2="0.9" y2="-0.7" width="0.2" layer="21"/>
<text x="-0.7" y="-0.3" size="0.6096" layer="51" font="vector">&gt;Name</text>
</package>
<package name="NORDICSEMI_QFN48">
<wire x1="-3.1" y1="2.6" x2="-3.1" y2="2.9" width="0.2" layer="21"/>
<wire x1="-3.1" y1="2.9" x2="-3.1" y2="3.1" width="0.2" layer="21"/>
<wire x1="-3.1" y1="3.1" x2="-2.6" y2="3.1" width="0.2" layer="21"/>
<wire x1="3.1" y1="2.6" x2="3.1" y2="3.1" width="0.2" layer="21"/>
<wire x1="3.1" y1="-3.1" x2="2.6" y2="-3.1" width="0.2" layer="21"/>
<wire x1="-3.1" y1="-2.6" x2="-3.1" y2="-3.1" width="0.2" layer="21"/>
<wire x1="2.6" y1="3.1" x2="3.1" y2="3.1" width="0.2" layer="21"/>
<wire x1="3.1" y1="-3.1" x2="3.1" y2="-2.6" width="0.2" layer="21"/>
<wire x1="-2.6" y1="-3.1" x2="-3.1" y2="-3.1" width="0.2" layer="21"/>
<smd name="1" x="-3.2" y="2.2" dx="1.2" dy="0.2" layer="1"/>
<smd name="2" x="-3.2" y="1.8" dx="1.2" dy="0.2" layer="1"/>
<smd name="3" x="-3.2" y="1.4" dx="1.2" dy="0.2" layer="1"/>
<smd name="4" x="-3.2" y="1" dx="1.2" dy="0.2" layer="1"/>
<smd name="5" x="-3.2" y="0.6" dx="1.2" dy="0.2" layer="1"/>
<smd name="6" x="-3.2" y="0.2" dx="1.2" dy="0.2" layer="1"/>
<smd name="7" x="-3.2" y="-0.2" dx="1.2" dy="0.2" layer="1"/>
<smd name="8" x="-3.2" y="-0.6" dx="1.2" dy="0.2" layer="1"/>
<smd name="9" x="-3.2" y="-1" dx="1.2" dy="0.2" layer="1"/>
<smd name="10" x="-3.2" y="-1.4" dx="1.2" dy="0.2" layer="1"/>
<smd name="11" x="-3.2" y="-1.8" dx="1.2" dy="0.2" layer="1"/>
<smd name="12" x="-3.2" y="-2.2" dx="1.2" dy="0.2" layer="1"/>
<smd name="13" x="-2.2" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="14" x="-1.8" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="15" x="-1.4" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="16" x="-1" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="17" x="-0.6" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="18" x="-0.2" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="19" x="0.2" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="20" x="0.6" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="21" x="1" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="22" x="1.4" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="23" x="1.8" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="24" x="2.2" y="-3.2" dx="1.2" dy="0.2" layer="1" rot="R90"/>
<smd name="25" x="3.2" y="-2.2" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="26" x="3.2" y="-1.8" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="27" x="3.2" y="-1.4" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="28" x="3.2" y="-1" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="29" x="3.2" y="-0.6" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="30" x="3.2" y="-0.2" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="31" x="3.2" y="0.2" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="32" x="3.2" y="0.6" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="33" x="3.2" y="1" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="34" x="3.2" y="1.4" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="35" x="3.2" y="1.8" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="36" x="3.2" y="2.2" dx="1.2" dy="0.2" layer="1" rot="R180"/>
<smd name="37" x="2.2" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="38" x="1.8" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="39" x="1.4" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="40" x="1" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="41" x="0.6" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="42" x="0.2" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="43" x="-0.2" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="44" x="-0.6" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="45" x="-1" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="46" x="-1.4" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="47" x="-1.8" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="48" x="-2.2" y="3.2" dx="1.2" dy="0.2" layer="1" rot="R270"/>
<smd name="GND" x="0" y="0" dx="4.2" dy="4.2" layer="1"/>
<text x="-1.4" y="4.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.1" y1="2.6" x2="-2.6" y2="3.1" width="0.2" layer="21"/>
<wire x1="-2.6" y1="3.1" x2="-3.1" y2="2.9" width="0.2" layer="21"/>
<wire x1="-3.1" y1="2.9" x2="-2.9" y2="2.9" width="0.2" layer="21"/>
<text x="-3.2" y="3.2" size="0.8128" layer="21" font="vector" ratio="30" rot="R90">&gt;o</text>
<wire x1="-3.1" y1="3.1" x2="-3.1" y2="-3.1" width="0.2" layer="51"/>
<wire x1="-3.1" y1="-3.1" x2="3.1" y2="-3.1" width="0.2" layer="51"/>
<wire x1="3.1" y1="-3.1" x2="3.1" y2="3.1" width="0.2" layer="51"/>
<wire x1="3.1" y1="3.1" x2="-3.1" y2="3.1" width="0.2" layer="51"/>
<circle x="-2.4" y="2.4" radius="0.3" width="0.2" layer="51"/>
<text x="-1.7" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<polygon width="0.2" layer="41">
<vertex x="-2.7" y="0.2"/>
<vertex x="-0.2" y="0.2"/>
<vertex x="-0.2" y="2.7"/>
<vertex x="0.2" y="2.7"/>
<vertex x="0.2" y="1.5"/>
<vertex x="0.2" y="0.2"/>
<vertex x="2.7" y="0.2"/>
<vertex x="2.7" y="-0.2"/>
<vertex x="0.2" y="-0.2"/>
<vertex x="0.2" y="-2.7"/>
<vertex x="-0.2" y="-2.7"/>
<vertex x="-0.2" y="-2.3"/>
<vertex x="-0.2" y="-0.2"/>
<vertex x="-2.7" y="-0.2"/>
</polygon>
<wire x1="2.9" y1="1.2" x2="3.5" y2="1.2" width="0.2" layer="41"/>
<wire x1="-2.4" y1="-3.5" x2="-2.4" y2="-2.9" width="0.2" layer="41"/>
</package>
<package name="NORDICSEMI_2450AT18B100">
<wire x1="2.2" y1="0.9" x2="-2.2" y2="0.9" width="0.2" layer="51"/>
<wire x1="-2.2" y1="0.9" x2="-2.2" y2="-0.9" width="0.2" layer="51"/>
<wire x1="-2.2" y1="-0.9" x2="2.2" y2="-0.9" width="0.2" layer="51"/>
<wire x1="2.2" y1="-0.9" x2="2.2" y2="0.9" width="0.2" layer="51"/>
<wire x1="-1" y1="0.8" x2="1" y2="0.8" width="0.2" layer="21"/>
<wire x1="-1" y1="-0.8" x2="1" y2="-0.8" width="0.2" layer="21"/>
<rectangle x1="-1.2" y1="-0.1" x2="-0.5" y2="0.2" layer="21"/>
<smd name="FP" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1" rot="R90"/>
<smd name="NC" x="1.7" y="0" dx="1.6" dy="0.8" layer="1" rot="R90"/>
<text x="-1.4" y="1.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<rectangle x1="-1.2" y1="-0.1" x2="-0.5" y2="0.2" layer="51"/>
<text x="-0.5" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="NORDICSEMI_BALUN-JOHANSON">
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<pin name="BAL1" x="-2.54" y="10.16" length="short" direction="pas" rot="R270"/>
<pin name="BAL2" x="-2.54" y="-10.16" length="short" direction="pas" rot="R90"/>
<pin name="DC" x="0" y="-10.16" length="short" direction="pas" rot="R90"/>
<pin name="GND" x="0" y="10.16" length="short" direction="pas" rot="R270"/>
<pin name="NC" x="2.54" y="-10.16" length="short" direction="pas" rot="R90"/>
<pin name="UNBL" x="2.54" y="10.16" length="short" direction="pas" rot="R270"/>
<text x="-5.08" y="12.7" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-5.08" y="-15.24" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="NORDICSEMI_NRF51822">
<wire x1="-22.86" y1="-25.4" x2="-22.86" y2="22.86" width="0.254" layer="94"/>
<wire x1="-22.86" y1="22.86" x2="25.4" y2="22.86" width="0.254" layer="94"/>
<wire x1="25.4" y1="22.86" x2="25.4" y2="-25.4" width="0.254" layer="94"/>
<wire x1="25.4" y1="-25.4" x2="-22.86" y2="-25.4" width="0.254" layer="94"/>
<pin name="ANT1" x="27.94" y="0" length="short" direction="pas" rot="R180"/>
<pin name="ANT2" x="27.94" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="AVDD@35" x="27.94" y="10.16" length="short" direction="pwr" rot="R180"/>
<pin name="AVDD@36" x="27.94" y="12.7" length="short" direction="pwr" rot="R180"/>
<pin name="DCC" x="-25.4" y="10.16" length="short" direction="pas"/>
<pin name="DEC1" x="10.16" y="25.4" length="short" direction="pas" rot="R270"/>
<pin name="DEC2" x="27.94" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="PAD" x="-5.08" y="-7.62" visible="pin" length="short" direction="pwr"/>
<pin name="P0.00" x="-25.4" y="5.08" length="short"/>
<pin name="P0.01" x="-25.4" y="2.54" length="short"/>
<pin name="P0.02" x="-25.4" y="0" length="short"/>
<pin name="P0.03" x="-25.4" y="-2.54" length="short"/>
<pin name="P0.04" x="-25.4" y="-5.08" length="short"/>
<pin name="P0.05" x="-25.4" y="-7.62" length="short"/>
<pin name="P0.06" x="-25.4" y="-10.16" length="short"/>
<pin name="P0.07" x="-25.4" y="-12.7" length="short"/>
<pin name="P0.08" x="-10.16" y="-27.94" length="short" rot="R90"/>
<pin name="P0.09" x="-7.62" y="-27.94" length="short" rot="R90"/>
<pin name="P0.10" x="-5.08" y="-27.94" length="short" rot="R90"/>
<pin name="P0.11" x="-2.54" y="-27.94" length="short" rot="R90"/>
<pin name="P0.12" x="0" y="-27.94" length="short" rot="R90"/>
<pin name="P0.13" x="2.54" y="-27.94" length="short" rot="R90"/>
<pin name="P0.14" x="5.08" y="-27.94" length="short" rot="R90"/>
<pin name="P0.15" x="7.62" y="-27.94" length="short" rot="R90"/>
<pin name="P0.16" x="10.16" y="-27.94" length="short" rot="R90"/>
<pin name="P0.17" x="27.94" y="-15.24" length="short" rot="R180"/>
<pin name="P0.18" x="27.94" y="-12.7" length="short" rot="R180"/>
<pin name="P0.19" x="27.94" y="-10.16" length="short" rot="R180"/>
<pin name="P0.20" x="27.94" y="-7.62" length="short" rot="R180"/>
<pin name="P0.21" x="7.62" y="25.4" length="short" rot="R270"/>
<pin name="P0.22" x="5.08" y="25.4" length="short" rot="R270"/>
<pin name="P0.23" x="2.54" y="25.4" length="short" rot="R270"/>
<pin name="P0.24" x="0" y="25.4" length="short" rot="R270"/>
<pin name="P0.25" x="-2.54" y="25.4" length="short" rot="R270"/>
<pin name="P0.26" x="-5.08" y="25.4" length="short" rot="R270"/>
<pin name="P0.27" x="-7.62" y="25.4" length="short" rot="R270"/>
<pin name="P0.28" x="-10.16" y="25.4" length="short" rot="R270"/>
<pin name="P0.29" x="-12.7" y="25.4" length="short" rot="R270"/>
<pin name="P0.30" x="-25.4" y="7.62" length="short"/>
<pin name="SWDCLK" x="15.24" y="-27.94" length="short" rot="R90"/>
<pin name="SWDIO/NRESET" x="12.7" y="-27.94" length="short" rot="R90"/>
<pin name="VDD@1" x="-25.4" y="12.7" length="short" direction="pwr"/>
<pin name="VDD@12" x="-25.4" y="-15.24" length="short" direction="pwr"/>
<pin name="VDD_PA" x="27.94" y="-2.54" length="short" direction="pwr" rot="R180"/>
<pin name="VSS@13" x="-12.7" y="-27.94" length="short" direction="pwr" rot="R90"/>
<pin name="VSS@33" x="27.94" y="5.08" length="short" direction="pwr" rot="R180"/>
<pin name="VSS@34" x="27.94" y="7.62" length="short" direction="pwr" rot="R180"/>
<pin name="XC1" x="15.24" y="25.4" length="short" direction="pas" rot="R270"/>
<pin name="XC2" x="12.7" y="25.4" length="short" direction="pas" rot="R270"/>
<text x="-5.08" y="7.62" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="2.54" y="5.08" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
<symbol name="NORDICSEMI_ANTENA-JOHANSON">
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="0.254" y1="5.08" x2="2.032" y2="7.112" width="0.254" layer="94" curve="-90"/>
<wire x1="-0.508" y1="5.08" x2="2.032" y2="7.874" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.27" y1="5.08" x2="2.032" y2="8.636" width="0.254" layer="94" curve="-90"/>
<wire x1="-2.032" y1="5.08" x2="-2.032" y2="5.334" width="0.254" layer="94"/>
<wire x1="-2.032" y1="5.334" x2="2.032" y2="9.398" width="0.254" layer="94" curve="-90"/>
<wire x1="-2.794" y1="5.08" x2="-2.794" y2="5.334" width="0.254" layer="94"/>
<wire x1="-2.794" y1="5.334" x2="2.032" y2="10.16" width="0.254" layer="94" curve="-90"/>
<wire x1="2.54" y1="5.08" x2="1.524" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.524" y1="5.08" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<pin name="FP" x="-2.54" y="0" visible="off" length="short"/>
<pin name="NC" x="7.62" y="0" visible="off" length="short" direction="nc" rot="R180"/>
<text x="5.08" y="7.62" size="1.778" layer="95" font="vector">&gt;Value</text>
<text x="-10.16" y="7.62" size="1.778" layer="95" font="vector">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NORDICSEMI_BALUN-2450BM">
<description>General Specifications:
Part Number 2450BM14A0002
Insertion Loss 2.0 dB max.
Frequency (MHz) 2400 - 2500
Return Loss 9.5 dB min.
Unbalanced Impedance 50 Ω
Phase Difference 160° ± 15°
Amplitude Difference 3.5 ± 1.5 dB 
Reel Quanity 4,000
Operating Temperature -40 to +85°C
Attenuation: Common Mode 2.45 GHz 
Impedance Matched Balun-Filter: Optimized for Nordic s Chipset nRF24L01/+
Differential Balanced Impedance Conjugate match to Nordic 
nRF24L01/nRF24L01+
Attenuation: Differential Mode 15 min.@4800~5000MHz
15 min.@7200~7500MHz'</description>
<gates>
<gate name="G$1" symbol="NORDICSEMI_BALUN-JOHANSON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NORDICSEMI_2450BM14A0002">
<connects>
<connect gate="G$1" pin="BAL1" pad="3"/>
<connect gate="G$1" pin="BAL2" pad="4"/>
<connect gate="G$1" pin="DC" pad="5"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="NC" pad="6"/>
<connect gate="G$1" pin="UNBL" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NORDICSEMI_NRF51822">
<description>Bluetooth 4.0 Low Energy/2.4Ghz
NORDIC SEMICONDUCTOR</description>
<gates>
<gate name="G$1" symbol="NORDICSEMI_NRF51822" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="NORDICSEMI_QFN48">
<connects>
<connect gate="G$1" pin="ANT1" pad="31"/>
<connect gate="G$1" pin="ANT2" pad="32"/>
<connect gate="G$1" pin="AVDD@35" pad="35"/>
<connect gate="G$1" pin="AVDD@36" pad="36"/>
<connect gate="G$1" pin="DCC" pad="2"/>
<connect gate="G$1" pin="DEC1" pad="39"/>
<connect gate="G$1" pin="DEC2" pad="29"/>
<connect gate="G$1" pin="P0.00" pad="4"/>
<connect gate="G$1" pin="P0.01" pad="5"/>
<connect gate="G$1" pin="P0.02" pad="6"/>
<connect gate="G$1" pin="P0.03" pad="7"/>
<connect gate="G$1" pin="P0.04" pad="8"/>
<connect gate="G$1" pin="P0.05" pad="9"/>
<connect gate="G$1" pin="P0.06" pad="10"/>
<connect gate="G$1" pin="P0.07" pad="11"/>
<connect gate="G$1" pin="P0.08" pad="14"/>
<connect gate="G$1" pin="P0.09" pad="15"/>
<connect gate="G$1" pin="P0.10" pad="16"/>
<connect gate="G$1" pin="P0.11" pad="17"/>
<connect gate="G$1" pin="P0.12" pad="18"/>
<connect gate="G$1" pin="P0.13" pad="19"/>
<connect gate="G$1" pin="P0.14" pad="20"/>
<connect gate="G$1" pin="P0.15" pad="21"/>
<connect gate="G$1" pin="P0.16" pad="22"/>
<connect gate="G$1" pin="P0.17" pad="25"/>
<connect gate="G$1" pin="P0.18" pad="26"/>
<connect gate="G$1" pin="P0.19" pad="27"/>
<connect gate="G$1" pin="P0.20" pad="28"/>
<connect gate="G$1" pin="P0.21" pad="40"/>
<connect gate="G$1" pin="P0.22" pad="41"/>
<connect gate="G$1" pin="P0.23" pad="42"/>
<connect gate="G$1" pin="P0.24" pad="43"/>
<connect gate="G$1" pin="P0.25" pad="44"/>
<connect gate="G$1" pin="P0.26" pad="45"/>
<connect gate="G$1" pin="P0.27" pad="46"/>
<connect gate="G$1" pin="P0.28" pad="47"/>
<connect gate="G$1" pin="P0.29" pad="48"/>
<connect gate="G$1" pin="P0.30" pad="3"/>
<connect gate="G$1" pin="PAD" pad="GND"/>
<connect gate="G$1" pin="SWDCLK" pad="24"/>
<connect gate="G$1" pin="SWDIO/NRESET" pad="23"/>
<connect gate="G$1" pin="VDD@1" pad="1"/>
<connect gate="G$1" pin="VDD@12" pad="12"/>
<connect gate="G$1" pin="VDD_PA" pad="30"/>
<connect gate="G$1" pin="VSS@13" pad="13"/>
<connect gate="G$1" pin="VSS@33" pad="33"/>
<connect gate="G$1" pin="VSS@34" pad="34"/>
<connect gate="G$1" pin="XC1" pad="37"/>
<connect gate="G$1" pin="XC2" pad="38"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NORDICSEMI_ANTENA-2450AT">
<description>General Specifications:
Part Number 2450AT18B100 
Input Power 3W max.
Frequency Range 2400 - 2500 Mhz
Impedance 50 Ω
Peak Gain 0.5 dBi typ. (XZ-V) 
Operating Temperature -40 to +85°C
Average Gain -0.5 dBi typ. (XZ-V) 
Reel Quanity 3,000
Return Loss 9.5 dB min</description>
<gates>
<gate name="G$1" symbol="NORDICSEMI_ANTENA-JOHANSON" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="NORDICSEMI_2450AT18B100">
<connects>
<connect gate="G$1" pin="FP" pad="FP"/>
<connect gate="G$1" pin="NC" pad="NC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ad5663">
<packages>
<package name="MSOP-10">
<description>Exposed Pad 0.5mm pitch</description>
<wire x1="-1.7" y1="1.4" x2="1.7" y2="1.4" width="0.2" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-1.4" width="0.2" layer="21"/>
<wire x1="1.7" y1="-1.4" x2="-1" y2="-1.4" width="0.2" layer="21"/>
<wire x1="-1" y1="-1.4" x2="-1.7" y2="-1.4" width="0.2" layer="21"/>
<wire x1="-1.7" y1="-1.4" x2="-1.7" y2="-0.7" width="0.2" layer="21"/>
<smd name="10" x="-1" y="2.2445" dx="0.31" dy="0.89" layer="1"/>
<smd name="8" x="0" y="2.2445" dx="0.31" dy="0.89" layer="1"/>
<smd name="7" x="0.5" y="2.2445" dx="0.31" dy="0.89" layer="1"/>
<smd name="9" x="-0.5" y="2.2445" dx="0.31" dy="0.89" layer="1"/>
<smd name="6" x="1" y="2.2445" dx="0.31" dy="0.89" layer="1"/>
<smd name="5" x="1" y="-2.2445" dx="0.31" dy="0.89" layer="1"/>
<smd name="4" x="0.5" y="-2.2445" dx="0.31" dy="0.89" layer="1"/>
<smd name="3" x="0" y="-2.2445" dx="0.31" dy="0.89" layer="1"/>
<smd name="2" x="-0.5" y="-2.2445" dx="0.31" dy="0.89" layer="1"/>
<smd name="1" x="-1" y="-2.2445" dx="0.31" dy="0.89" layer="1"/>
<text x="-1.7" y="3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<rectangle x1="-0.085" y1="1.5" x2="0.085" y2="2.45" layer="51"/>
<rectangle x1="-1.085" y1="-2.45" x2="-0.915" y2="-1.5" layer="51"/>
<rectangle x1="-0.585" y1="-2.45" x2="-0.415" y2="-1.5" layer="51"/>
<rectangle x1="-0.085" y1="-2.45" x2="0.085" y2="-1.5" layer="51"/>
<rectangle x1="0.415" y1="-2.45" x2="0.585" y2="-1.5" layer="51"/>
<rectangle x1="0.915" y1="-2.45" x2="1.085" y2="-1.5" layer="51"/>
<rectangle x1="-1.085" y1="1.5" x2="-0.915" y2="2.45" layer="51"/>
<rectangle x1="-0.585" y1="1.5" x2="-0.415" y2="2.45" layer="51"/>
<rectangle x1="0.415" y1="1.5" x2="0.585" y2="2.45" layer="51"/>
<rectangle x1="0.915" y1="1.5" x2="1.085" y2="2.45" layer="51"/>
<wire x1="-1.7" y1="-0.7" x2="-1.7" y2="1.4" width="0.2" layer="21"/>
<wire x1="-1.7" y1="1.4" x2="1.7" y2="1.4" width="0.2" layer="51"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-1.4" width="0.2" layer="51"/>
<wire x1="1.7" y1="-1.4" x2="-1.7" y2="-1.4" width="0.2" layer="51"/>
<wire x1="-1.7" y1="-1.4" x2="-1.7" y2="1.4" width="0.2" layer="51"/>
<circle x="-1.2" y="-0.9" radius="0.27" width="0.2" layer="51"/>
<text x="-0.9" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.7" y1="-0.7" x2="-1" y2="-1.4" width="0.2" layer="21"/>
<text x="-1.8" y="-1.5" size="0.8128" layer="21" font="vector" ratio="30" rot="R180">&gt;o</text>
</package>
</packages>
<symbols>
<symbol name="AD5663">
<pin name="OUTA" x="-12.7" y="5.08" length="short" direction="pas"/>
<pin name="OUTB" x="-12.7" y="2.54" length="short" direction="pas"/>
<pin name="GND" x="-12.7" y="0" length="short" direction="pwr"/>
<pin name="/LDAC" x="-12.7" y="-2.54" length="short" direction="in"/>
<pin name="/CLR" x="-12.7" y="-5.08" length="short" direction="in"/>
<pin name="/SYNC" x="12.7" y="-5.08" length="short" direction="in" rot="R180"/>
<pin name="SCK" x="12.7" y="-2.54" length="short" direction="in" rot="R180"/>
<pin name="MOSI" x="12.7" y="0" length="short" direction="in" rot="R180"/>
<pin name="VDD" x="12.7" y="2.54" length="short" direction="pwr" rot="R180"/>
<pin name="VREF" x="12.7" y="5.08" length="short" direction="pwr" rot="R180"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="10.16" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="5.08" y="-10.16" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AD5663">
<gates>
<gate name="G$1" symbol="AD5663" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MSOP-10">
<connects>
<connect gate="G$1" pin="/CLR" pad="5"/>
<connect gate="G$1" pin="/LDAC" pad="4"/>
<connect gate="G$1" pin="/SYNC" pad="6"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="MOSI" pad="8"/>
<connect gate="G$1" pin="OUTA" pad="1"/>
<connect gate="G$1" pin="OUTB" pad="2"/>
<connect gate="G$1" pin="SCK" pad="7"/>
<connect gate="G$1" pin="VDD" pad="9"/>
<connect gate="G$1" pin="VREF" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ads1120">
<packages>
<package name="TSSOP-16">
<wire x1="-2.8" y1="2.2" x2="2.8" y2="2.2" width="0.2" layer="51"/>
<wire x1="2.8" y1="2.2" x2="2.8" y2="-2.2" width="0.2" layer="51"/>
<wire x1="2.8" y1="-2.2" x2="-2.8" y2="-2.2" width="0.2" layer="51"/>
<wire x1="-2.8" y1="-2.2" x2="-2.8" y2="2.2" width="0.2" layer="51"/>
<wire x1="-2.8" y1="-1.8" x2="-2.8" y2="1.8" width="0.2" layer="21"/>
<wire x1="2.8" y1="1.8" x2="2.8" y2="-1.8" width="0.2" layer="21"/>
<circle x="-1.9" y="-1.3" radius="0.3" width="0.2" layer="51"/>
<smd name="1" x="-2.275" y="-3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="2" x="-1.625" y="-3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="3" x="-0.975" y="-3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="4" x="-0.325" y="-3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="5" x="0.325" y="-3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="6" x="0.975" y="-3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="7" x="1.625" y="-3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="8" x="2.275" y="-3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="9" x="2.275" y="3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="10" x="1.625" y="3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="11" x="0.975" y="3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="12" x="0.325" y="3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="13" x="-0.325" y="3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="14" x="-0.975" y="3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="15" x="-1.625" y="3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<smd name="16" x="-2.275" y="3" dx="1.43" dy="0.35" layer="1" rot="R90"/>
<text x="-3.2" y="-1.8" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<rectangle x1="-2.4" y1="-3.2" x2="-2.155" y2="-2.2" layer="51"/>
<rectangle x1="-1.75" y1="-3.2" x2="-1.505" y2="-2.2" layer="51"/>
<rectangle x1="-1.1" y1="-3.2" x2="-0.855" y2="-2.2" layer="51"/>
<rectangle x1="-0.45" y1="-3.2" x2="-0.205" y2="-2.2" layer="51"/>
<rectangle x1="0.2" y1="-3.2" x2="0.445" y2="-2.2" layer="51"/>
<rectangle x1="0.85" y1="-3.2" x2="1.095" y2="-2.2" layer="51"/>
<rectangle x1="1.5" y1="-3.2" x2="1.745" y2="-2.2" layer="51"/>
<rectangle x1="2.15" y1="-3.2" x2="2.395" y2="-2.2" layer="51"/>
<rectangle x1="2.155" y1="2.2" x2="2.4" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="1.505" y1="2.2" x2="1.75" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="0.855" y1="2.2" x2="1.1" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="0.205" y1="2.2" x2="0.45" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="-0.445" y1="2.2" x2="-0.2" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="-1.095" y1="2.2" x2="-0.85" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="-1.745" y1="2.2" x2="-1.5" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="-2.395" y1="2.2" x2="-2.15" y2="3.2" layer="51" rot="R180"/>
<wire x1="-2.8" y1="1.8" x2="2.8" y2="1.8" width="0.2" layer="21"/>
<wire x1="-2.8" y1="-1.8" x2="2.8" y2="-1.8" width="0.2" layer="21"/>
<text x="-1.9" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<text x="-2.9" y="-2.3" size="0.8128" layer="21" font="vector" ratio="30" rot="R180">&gt;o</text>
<wire x1="-0.65" y1="-2.5" x2="-0.65" y2="-3.5" width="0.2" layer="41"/>
<wire x1="0" y1="-2.5" x2="0" y2="-3.5" width="0.2" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="ADS1220">
<pin name="SCK" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="/CS" x="-12.7" y="5.08" length="short" direction="in"/>
<pin name="CLK" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="DGND" x="-12.7" y="0" length="short" direction="pwr"/>
<pin name="AVSS" x="-12.7" y="-2.54" length="short" direction="pwr"/>
<pin name="AIN3" x="-12.7" y="-5.08" length="short" direction="pas"/>
<pin name="AIN2" x="-12.7" y="-7.62" length="short" direction="pas"/>
<pin name="REFN0" x="-12.7" y="-10.16" length="short" direction="pas"/>
<pin name="REFP0" x="12.7" y="-10.16" length="short" direction="pas" rot="R180"/>
<pin name="AIN1" x="12.7" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="AIN0" x="12.7" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="AVDD" x="12.7" y="-2.54" length="short" direction="pwr" rot="R180"/>
<pin name="DVDD" x="12.7" y="0" length="short" direction="pwr" rot="R180"/>
<pin name="/DRDY" x="12.7" y="2.54" length="short" direction="out" rot="R180"/>
<pin name="MISO" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="MOSI" x="12.7" y="7.62" length="short" direction="in" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-5.08" y="12.7" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="5.08" y="-15.24" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADS1220">
<gates>
<gate name="G$1" symbol="ADS1220" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP-16">
<connects>
<connect gate="G$1" pin="/CS" pad="2"/>
<connect gate="G$1" pin="/DRDY" pad="14"/>
<connect gate="G$1" pin="AIN0" pad="11"/>
<connect gate="G$1" pin="AIN1" pad="10"/>
<connect gate="G$1" pin="AIN2" pad="7"/>
<connect gate="G$1" pin="AIN3" pad="6"/>
<connect gate="G$1" pin="AVDD" pad="12"/>
<connect gate="G$1" pin="AVSS" pad="5"/>
<connect gate="G$1" pin="CLK" pad="3"/>
<connect gate="G$1" pin="DGND" pad="4"/>
<connect gate="G$1" pin="DVDD" pad="13"/>
<connect gate="G$1" pin="MISO" pad="15"/>
<connect gate="G$1" pin="MOSI" pad="16"/>
<connect gate="G$1" pin="REFN0" pad="8"/>
<connect gate="G$1" pin="REFP0" pad="9"/>
<connect gate="G$1" pin="SCK" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="max4231">
<packages>
<package name="SOT-23-6">
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.2" layer="21"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="6" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-2.1" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<smd name="5" x="0" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-1.4" y="-2.7" size="1.27" layer="21" font="vector" ratio="15" rot="R90">&gt;o</text>
<wire x1="-1.6" y1="-1.9" x2="1.6" y2="-1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="-1.9" x2="1.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="1.9" x2="-1.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="-1.6" y1="1.9" x2="-1.6" y2="-1.9" width="0.2" layer="51"/>
<circle x="-1" y="-1.3" radius="0.282840625" width="0.2" layer="51"/>
<text x="-1.3" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="OPAMP">
<pin name="-" x="-7.62" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="+" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="OUT" x="12.7" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="V+" x="2.54" y="5.08" visible="pad" length="point" direction="pwr" rot="R270"/>
<pin name="V-" x="2.54" y="-5.08" visible="pad" length="point" direction="pwr" rot="R90"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-4.318" y="1.778" size="1.778" layer="95" font="vector">-</text>
<text x="-4.318" y="-3.302" size="1.778" layer="95" font="vector">+</text>
<text x="1.016" y="0.254" size="1.778" layer="95" font="vector">V+</text>
<text x="1.016" y="-2.286" size="1.778" layer="95" font="vector">V-</text>
<text x="-7.62" y="7.62" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96" font="vector">&gt;Value</text>
<wire x1="2.54" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.1778" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-5.08" width="0.1778" layer="94"/>
<pin name="/SHDN" x="0" y="-5.08" visible="pad" length="point" direction="in" rot="R90"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.429" width="0.1778" layer="94"/>
<text x="0.254" y="-2.794" size="1.778" layer="95" font="vector" rot="R90">/SD</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="OPAMP">
<gates>
<gate name="G$1" symbol="OPAMP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23-6">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="3"/>
<connect gate="G$1" pin="/SHDN" pad="5"/>
<connect gate="G$1" pin="OUT" pad="4"/>
<connect gate="G$1" pin="V+" pad="6"/>
<connect gate="G$1" pin="V-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="button">
<packages>
<package name="BUTTON">
<wire x1="3.1" y1="1" x2="3.1" y2="-1" width="0.2" layer="21"/>
<wire x1="-3.1" y1="1" x2="-3.1" y2="-1" width="0.2" layer="21"/>
<pad name="A@1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="A@2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="B@1" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="B@2" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="-1.4" y="3.6" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.1" y1="3" x2="3.1" y2="3" width="0.2" layer="51"/>
<wire x1="3.1" y1="3" x2="3.1" y2="-3" width="0.2" layer="51"/>
<wire x1="3.1" y1="-3" x2="-3.1" y2="-3" width="0.2" layer="51"/>
<wire x1="-3.1" y1="-3" x2="-3.1" y2="3" width="0.2" layer="51"/>
<circle x="0" y="0" radius="2.7" width="0.2" layer="51"/>
<text x="-1.3" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<pad name="A@3" x="0" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="B@3" x="0" y="-2.2606" drill="1.016" diameter="1.8796"/>
<wire x1="-2.3" y1="3" x2="-1" y2="3" width="0.2" layer="21"/>
<wire x1="1" y1="3" x2="2.3" y2="3" width="0.2" layer="21"/>
<wire x1="1" y1="-3" x2="2.3" y2="-3" width="0.2" layer="21"/>
<wire x1="-2.3" y1="-3" x2="-1" y2="-3" width="0.2" layer="21"/>
<circle x="0" y="0" radius="1.1" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="BUTTON">
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="B" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="95" font="vector">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BUTTON" prefix="SW" uservalue="yes">
<description>TACTILE SWITCH,
FARNELL: 1703878</description>
<gates>
<gate name="SW" symbol="BUTTON" x="0" y="0"/>
</gates>
<devices>
<device name="-TH" package="BUTTON">
<connects>
<connect gate="SW" pin="A" pad="A@1 A@2 A@3"/>
<connect gate="SW" pin="B" pad="B@1 B@2 B@3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jlink">
<packages>
<package name="2X5_1.27">
<wire x1="6.6" y1="2.7" x2="-6.6" y2="2.7" width="0.2" layer="21"/>
<wire x1="-6.6" y1="2.7" x2="-6.6" y2="-2.7" width="0.2" layer="21"/>
<pad name="6" x="0" y="0.635" drill="0.6"/>
<pad name="5" x="0" y="-0.635" drill="0.6"/>
<pad name="8" x="1.27" y="0.635" drill="0.6"/>
<pad name="7" x="1.27" y="-0.635" drill="0.6"/>
<pad name="10" x="2.54" y="0.635" drill="0.6"/>
<pad name="9" x="2.54" y="-0.635" drill="0.6"/>
<pad name="4" x="-1.27" y="0.635" drill="0.6"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.6"/>
<pad name="2" x="-2.54" y="0.635" drill="0.6"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.6" first="yes"/>
<text x="-1.4" y="3.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-6.6" y1="-2.7" x2="-1.2" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-2.7" x2="1.2" y2="-2.7" width="0.2" layer="21"/>
<wire x1="1.2" y1="-2.7" x2="6.6" y2="-2.7" width="0.2" layer="21"/>
<wire x1="6.6" y1="-2.7" x2="6.6" y2="2.7" width="0.2" layer="21"/>
<wire x1="-6.6" y1="2.7" x2="6.6" y2="2.7" width="0.2" layer="51"/>
<wire x1="6.6" y1="2.7" x2="6.6" y2="-2.7" width="0.2" layer="51"/>
<wire x1="6.6" y1="-2.7" x2="1.2" y2="-2.7" width="0.2" layer="51"/>
<wire x1="1.2" y1="-2.7" x2="-1.2" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-2.7" x2="-6.6" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-6.6" y1="-2.7" x2="-6.6" y2="2.7" width="0.2" layer="51"/>
<wire x1="-5.6" y1="1.8" x2="5.6" y2="1.8" width="0.2" layer="51"/>
<wire x1="5.6" y1="1.8" x2="5.6" y2="-1.8" width="0.2" layer="51"/>
<wire x1="5.6" y1="-1.8" x2="1.2" y2="-1.8" width="0.2" layer="51"/>
<wire x1="1.2" y1="-1.8" x2="1.2" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-2.7" x2="-1.2" y2="-1.8" width="0.2" layer="51"/>
<wire x1="-1.2" y1="-1.8" x2="-5.6" y2="-1.8" width="0.2" layer="51"/>
<wire x1="-5.6" y1="-1.8" x2="-5.6" y2="1.8" width="0.2" layer="51"/>
<wire x1="-5.6" y1="1.8" x2="5.6" y2="1.8" width="0.2" layer="21"/>
<wire x1="5.6" y1="1.8" x2="5.6" y2="-1.8" width="0.2" layer="21"/>
<wire x1="5.6" y1="-1.8" x2="1.2" y2="-1.8" width="0.2" layer="21"/>
<wire x1="1.2" y1="-1.8" x2="1.2" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-5.6" y1="1.8" x2="-5.6" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-5.6" y1="-1.8" x2="-1.2" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-1.8" x2="-1.2" y2="-2.7" width="0.2" layer="21"/>
<text x="-1.4" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="2X5_1.27">
<pin name="P$1" x="7.62" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$3" x="7.62" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="2.54" y="8.89" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="3.302" y="4.064" size="1.778" layer="95">1</text>
<text x="-1.778" y="4.064" size="1.778" layer="95">2</text>
<pin name="P$5" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$7" x="7.62" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="3.302" y="1.524" size="1.778" layer="95">3</text>
<text x="-1.778" y="1.524" size="1.778" layer="95">4</text>
<text x="3.302" y="-1.016" size="1.778" layer="95">5</text>
<text x="-1.778" y="-1.016" size="1.778" layer="95">6</text>
<text x="3.302" y="-3.556" size="1.778" layer="95">7</text>
<text x="-1.778" y="-3.556" size="1.778" layer="95">8</text>
<pin name="P$2" x="-5.08" y="5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$4" x="-5.08" y="2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$6" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$8" x="-5.08" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$9" x="7.62" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$10" x="-5.08" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="3.302" y="-6.096" size="1.778" layer="95">9</text>
<text x="-1.778" y="-6.096" size="1.778" layer="95">10</text>
<text x="5.08" y="-10.16" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="2X5_1.27">
<gates>
<gate name="G$1" symbol="2X5_1.27" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X5_1.27">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$10" pad="10"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="buzzer">
<description>Generated from &lt;b&gt;buzzer.sch&lt;/b&gt;&lt;p&gt;
by exp-project-lbr.ulp</description>
<packages>
<package name="KXG0905C_KXG0905C">
<circle x="0" y="0" radius="4.8" width="0.2" layer="21"/>
<wire x1="-2.5" y1="2.2" x2="-2.5" y2="1" width="0.2" layer="21"/>
<wire x1="-3.1" y1="1.6" x2="-1.9" y2="1.6" width="0.2" layer="21"/>
<pad name="N" x="2.5" y="0" drill="1"/>
<pad name="P" x="-2.5" y="0" drill="1"/>
<text x="-1.7" y="5.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1.6" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="4.8" width="0.2" layer="51"/>
<wire x1="-2.5" y1="2.2" x2="-2.5" y2="1" width="0.2" layer="51"/>
<wire x1="-3.1" y1="1.6" x2="-1.9" y2="1.6" width="0.2" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="KXG0905C_KXG0905C">
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.508" y1="1.524" x2="-0.508" y2="1.524" width="0.254" layer="94"/>
<pin name="N" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="P" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<text x="2.54" y="2.54" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="2.54" y="-2.54" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="KXG0905C_KXG0905C">
<gates>
<gate name="G$1" symbol="KXG0905C_KXG0905C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="KXG0905C_KXG0905C">
<connects>
<connect gate="G$1" pin="N" pad="N"/>
<connect gate="G$1" pin="P" pad="P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="logo">
<description>Generated from &lt;b&gt;logo.brd&lt;/b&gt;&lt;p&gt;
by exp-project-lbr.ulp</description>
<packages>
<package name="ESD_10.00">
<description>&lt;b&gt;Logo ESD 10 mm&lt;/b&gt;</description>
<polygon width="0.047" layer="21" spacing="0.2658">
<vertex x="0.0943" y="0.0943"/>
<vertex x="2.2156" y="3.8184"/>
<vertex x="3.1113" y="3.3942"/>
<vertex x="2.9699" y="3.1585"/>
<vertex x="2.9228" y="3.0642"/>
<vertex x="2.9228" y="2.6871"/>
<vertex x="3.0642" y="0.8014"/>
<vertex x="3.1585" y="0.8014"/>
<vertex x="3.2999" y="0.8957"/>
<vertex x="3.3942" y="1.0843"/>
<vertex x="3.4885" y="1.3671"/>
<vertex x="3.5356" y="1.7914"/>
<vertex x="3.5356" y="2.6871"/>
<vertex x="3.7713" y="3.017"/>
<vertex x="3.9127" y="3.017"/>
<vertex x="5.1855" y="2.4042"/>
<vertex x="5.1855" y="2.3571"/>
<vertex x="4.2899" y="1.5557"/>
<vertex x="4.1013" y="1.2728"/>
<vertex x="4.007" y="1.0843"/>
<vertex x="4.007" y="0.9428"/>
<vertex x="4.0542" y="0.8485"/>
<vertex x="4.1013" y="0.8014"/>
<vertex x="4.1956" y="0.8014"/>
<vertex x="4.5256" y="0.99"/>
<vertex x="4.9027" y="1.2257"/>
<vertex x="5.2327" y="1.4614"/>
<vertex x="5.657" y="1.7442"/>
<vertex x="6.0341" y="1.9799"/>
<vertex x="6.1284" y="1.9799"/>
<vertex x="10.0411" y="0.1886"/>
<vertex x="10.0411" y="0.0943"/>
</polygon>
<polygon width="0.047" layer="21" spacing="0.2658">
<vertex x="2.6399" y="4.337"/>
<vertex x="5.0913" y="8.5797"/>
<vertex x="6.9769" y="5.327"/>
<vertex x="6.9769" y="5.2327"/>
<vertex x="4.7141" y="4.8084"/>
<vertex x="4.667" y="4.7613"/>
<vertex x="3.677" y="3.8656"/>
<vertex x="3.5827" y="3.8656"/>
</polygon>
<polygon width="0.047" layer="21" spacing="0.2658">
<vertex x="4.5256" y="3.3942"/>
<vertex x="4.8556" y="3.6299"/>
<vertex x="4.9498" y="3.6299"/>
<vertex x="5.4213" y="3.4885"/>
<vertex x="5.5627" y="3.3942"/>
<vertex x="5.657" y="3.2528"/>
<vertex x="5.6098" y="3.0642"/>
<vertex x="5.5155" y="2.9228"/>
</polygon>
<polygon width="0.047" layer="21" spacing="0.2658">
<vertex x="9.2868" y="1.2257"/>
<vertex x="6.9298" y="2.3571"/>
<vertex x="6.9298" y="2.4042"/>
<vertex x="7.9669" y="3.5356"/>
</polygon>
</package>
<package name="MUELLTONNE_10.00">
<description>&lt;b&gt;Logo trash / Mülltonne 10 mm&lt;/b&gt;</description>
<circle x="7.2365" y="2.1002" radius="0.7497" width="0.1664" layer="21"/>
<circle x="8.1099" y="8.7753" radius="0.3743" width="0.1664" layer="21"/>
<circle x="7.2365" y="2.1002" radius="0.4159" width="0.1664" layer="21"/>
<wire x1="0.915" y1="0.4991" x2="10.4804" y2="10.3141" width="0.1664" layer="21"/>
<wire x1="0.6654" y1="10.2309" x2="9.815" y2="0.4159" width="0.1664" layer="21"/>
<wire x1="6.4047" y1="1.8299" x2="4.1589" y2="1.8299" width="0.1664" layer="21"/>
<wire x1="4.1589" y1="1.8299" x2="3.9094" y2="2.0795" width="0.1664" layer="21" curve="-89.97705"/>
<wire x1="3.9094" y1="2.0795" x2="3.4103" y2="8.4842" width="0.1664" layer="21"/>
<wire x1="3.4103" y1="8.4842" x2="3.4103" y2="8.6505" width="0.1664" layer="21"/>
<wire x1="3.4103" y1="8.6505" x2="3.6598" y2="9.1496" width="0.1664" layer="21" curve="-53.125511"/>
<wire x1="3.6598" y1="9.1496" x2="4.0757" y2="9.3159" width="0.1664" layer="21" curve="-30.138307"/>
<wire x1="4.0757" y1="9.3159" x2="5.157" y2="9.3991" width="0.1664" layer="21" curve="-4.66621"/>
<wire x1="5.157" y1="9.3991" x2="5.157" y2="9.5655" width="0.1664" layer="21"/>
<wire x1="5.157" y1="9.5655" x2="6.2384" y2="9.5655" width="0.1664" layer="21"/>
<wire x1="6.2384" y1="9.5655" x2="6.2384" y2="9.2328" width="0.1664" layer="21"/>
<wire x1="6.2384" y1="9.2328" x2="5.157" y2="9.2328" width="0.1664" layer="21"/>
<wire x1="5.157" y1="9.2328" x2="5.157" y2="9.3991" width="0.1664" layer="21"/>
<wire x1="6.3215" y1="9.3991" x2="6.4047" y2="9.3991" width="0.1664" layer="21"/>
<wire x1="6.4047" y1="9.3991" x2="7.1533" y2="9.3159" width="0.1664" layer="21" curve="-12.680383"/>
<wire x1="7.1533" y1="9.3159" x2="7.6524" y2="8.9832" width="0.1664" layer="21" curve="-42.008846"/>
<wire x1="7.6524" y1="8.9832" x2="7.7356" y2="8.7337" width="0.1664" layer="21" curve="-33.738056"/>
<wire x1="7.7356" y1="8.7337" x2="7.3197" y2="3.1608" width="0.1664" layer="21"/>
<wire x1="7.7148" y1="7.0701" x2="8.2346" y2="7.0701" width="0.1664" layer="21"/>
<wire x1="8.2346" y1="7.0701" x2="8.2346" y2="7.9227" width="0.1664" layer="21"/>
<wire x1="8.2346" y1="7.9227" x2="7.7564" y2="7.9227" width="0.1664" layer="21"/>
<wire x1="3.9925" y1="8.6505" x2="3.951" y2="9.3991" width="0.1664" layer="21"/>
<wire x1="3.951" y1="9.3991" x2="4.1173" y2="9.3991" width="0.1664" layer="21" curve="-192.62744"/>
<wire x1="4.1173" y1="9.3991" x2="4.0757" y2="8.6505" width="0.1664" layer="21"/>
<wire x1="4.0757" y1="8.6505" x2="3.9925" y2="8.6505" width="0.1664" layer="21"/>
<rectangle x1="2.8696" y1="8.4218" x2="7.7564" y2="8.7753" layer="21"/>
<rectangle x1="5.0323" y1="6.4463" x2="6.7166" y2="7.0285" layer="21"/>
<rectangle x1="3.9925" y1="1.2685" x2="4.5748" y2="1.8299" layer="21"/>
</package>
<package name="MULETONNE_COVER_10.00">
<circle x="7.2365" y="2.1002" radius="0.7497" width="0.1664" layer="104"/>
<circle x="8.1099" y="8.7753" radius="0.3743" width="0.1664" layer="104"/>
<circle x="7.2365" y="2.1002" radius="0.4159" width="0.1664" layer="104"/>
<wire x1="0.915" y1="0.4991" x2="10.4804" y2="10.3141" width="0.1664" layer="104"/>
<wire x1="0.6654" y1="10.2309" x2="9.815" y2="0.4159" width="0.1664" layer="104"/>
<wire x1="6.4047" y1="1.8299" x2="4.1589" y2="1.8299" width="0.1664" layer="104"/>
<wire x1="4.1589" y1="1.8299" x2="3.9094" y2="2.0795" width="0.1664" layer="104" curve="-89.97705"/>
<wire x1="3.9094" y1="2.0795" x2="3.4103" y2="8.4842" width="0.1664" layer="104"/>
<wire x1="3.4103" y1="8.4842" x2="3.4103" y2="8.6505" width="0.1664" layer="104"/>
<wire x1="3.4103" y1="8.6505" x2="3.6598" y2="9.1496" width="0.1664" layer="104" curve="-53.125511"/>
<wire x1="3.6598" y1="9.1496" x2="4.0757" y2="9.3159" width="0.1664" layer="104" curve="-30.138307"/>
<wire x1="4.0757" y1="9.3159" x2="5.157" y2="9.3991" width="0.1664" layer="104" curve="-4.66621"/>
<wire x1="5.157" y1="9.3991" x2="5.157" y2="9.5655" width="0.1664" layer="104"/>
<wire x1="5.157" y1="9.5655" x2="6.2384" y2="9.5655" width="0.1664" layer="104"/>
<wire x1="6.2384" y1="9.5655" x2="6.2384" y2="9.2328" width="0.1664" layer="104"/>
<wire x1="6.2384" y1="9.2328" x2="5.157" y2="9.2328" width="0.1664" layer="104"/>
<wire x1="5.157" y1="9.2328" x2="5.157" y2="9.3991" width="0.1664" layer="104"/>
<wire x1="6.3215" y1="9.3991" x2="6.4047" y2="9.3991" width="0.1664" layer="104"/>
<wire x1="6.4047" y1="9.3991" x2="7.1533" y2="9.3159" width="0.1664" layer="104" curve="-12.680383"/>
<wire x1="7.1533" y1="9.3159" x2="7.6524" y2="8.9832" width="0.1664" layer="104" curve="-42.008846"/>
<wire x1="7.6524" y1="8.9832" x2="7.7356" y2="8.7337" width="0.1664" layer="104" curve="-33.738056"/>
<wire x1="7.7356" y1="8.7337" x2="7.3197" y2="3.1608" width="0.1664" layer="104"/>
<wire x1="7.7148" y1="7.0701" x2="8.2346" y2="7.0701" width="0.1664" layer="104"/>
<wire x1="8.2346" y1="7.0701" x2="8.2346" y2="7.9227" width="0.1664" layer="104"/>
<wire x1="8.2346" y1="7.9227" x2="7.7564" y2="7.9227" width="0.1664" layer="104"/>
<wire x1="3.9925" y1="8.6505" x2="3.951" y2="9.3991" width="0.1664" layer="104"/>
<wire x1="3.951" y1="9.3991" x2="4.1173" y2="9.3991" width="0.1664" layer="104" curve="-192.62744"/>
<wire x1="4.1173" y1="9.3991" x2="4.0757" y2="8.6505" width="0.1664" layer="104"/>
<wire x1="4.0757" y1="8.6505" x2="3.9925" y2="8.6505" width="0.1664" layer="104"/>
<rectangle x1="2.8696" y1="8.4218" x2="7.7564" y2="8.7753" layer="104"/>
<rectangle x1="5.0323" y1="6.4463" x2="6.7166" y2="7.0285" layer="104"/>
<rectangle x1="3.9925" y1="1.2685" x2="4.5748" y2="1.8299" layer="104"/>
</package>
</packages>
<symbols>
<symbol name="ESD">
<text x="0" y="0" size="1.27" layer="94">ESD</text>
</symbol>
<symbol name="MUELLTONNE">
<text x="0" y="0" size="1.27" layer="94">MT</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESD">
<gates>
<gate name="G$1" symbol="ESD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESD_10.00">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MUELLTONNE">
<gates>
<gate name="G$1" symbol="MUELLTONNE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MUELLTONNE_10.00">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="COVER" package="MULETONNE_COVER_10.00">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
<part name="R28" library="REBANE" deviceset="RESISTOR" device="-2512[6332-METRIC]" value="22Ω"/>
<part name="R31" library="REBANE" deviceset="RESISTOR" device="-2512[6332-METRIC]" value="470Ω"/>
<part name="T7" library="REBANE" deviceset="MOSFET-N" device="-SOT-23" value="SI2310"/>
<part name="T5" library="REBANE" deviceset="MOSFET-P" device="-SOT-23" value="SI2319"/>
<part name="GND1" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND2" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND3" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="T6" library="REBANE" deviceset="TRANSISTOR-NPN" device="-SOT-23" value="BC847"/>
<part name="R30" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="R33" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="GND4" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R32" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="1k"/>
<part name="R34" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="GND5" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="FRAME2" library="frames" deviceset="A4L-LOC" device=""/>
<part name="OLED1" library="oled" deviceset="OLED" device="" value="M24S1309"/>
<part name="BATTERY" library="battery" deviceset="BATTERY" device=""/>
<part name="BANANA1" library="banana" deviceset="BANANA" device="" value="BLUE"/>
<part name="BANANA2" library="banana" deviceset="BANANA" device="" value="GREEN"/>
<part name="BANANA3" library="banana" deviceset="BANANA" device="" value="YELLOW"/>
<part name="SW1" library="button" deviceset="BUTTON" device="-TH"/>
<part name="SW2" library="button" deviceset="BUTTON" device="-TH"/>
<part name="SW3" library="button" deviceset="BUTTON" device="-TH"/>
<part name="SW4" library="button" deviceset="BUTTON" device="-TH"/>
<part name="J2" library="REBANE_PINHEAD" deviceset="PINHEADER-04X1" device="-2.54N"/>
<part name="U6" library="nordic" deviceset="NORDICSEMI_BALUN-2450BM" device="" value="2450BM14E0003"/>
<part name="U5" library="nordic" deviceset="NORDICSEMI_NRF51822" device="" value="NRF51822"/>
<part name="A1" library="nordic" deviceset="NORDICSEMI_ANTENA-2450AT" device="" value="2450AT18B100"/>
<part name="GND6" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C25" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="47nF"/>
<part name="C26" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="2.2nF"/>
<part name="GND7" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND8" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND9" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C20" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="1nF"/>
<part name="+3V1" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND10" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="Q1" library="REBANE" deviceset="CRYSTAL" device="-3.2X1.5" value="32.768kHz"/>
<part name="C18" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="12pF"/>
<part name="C15" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="12pF"/>
<part name="GND11" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND12" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="Q2" library="REBANE" deviceset="CRYSTAL-GND" device="-3.2X2.5" value="16MHz"/>
<part name="C16" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="12pF"/>
<part name="C19" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="12pF"/>
<part name="GND13" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND14" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND15" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C17" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND16" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND17" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V2" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C24" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND18" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="U2" library="ad5663" deviceset="AD5663" device=""/>
<part name="U3" library="ads1120" deviceset="ADS1220" device=""/>
<part name="U9" library="max4231" deviceset="OPAMP" device="" value="MAX4231AUT"/>
<part name="U8" library="max4231" deviceset="OPAMP" device="" value="MAX4231AUT"/>
<part name="R29" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="GND20" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="P+1" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="P+2" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="P+3" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="P+4" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="+3V3" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V4" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND21" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND22" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND23" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C23" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="C22" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="10uF"/>
<part name="GND24" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND25" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="D2" library="REBANE" deviceset="DIODE" device="-SOD-323" value="BAT20J"/>
<part name="D1" library="REBANE" deviceset="DIODE" device="-SOD-323" value="BAT20J"/>
<part name="R6" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="140k"/>
<part name="R8" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="78k7"/>
<part name="GND26" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R9" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="GND27" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="T2" library="REBANE" deviceset="MOSFET-N" device="-SOT-23" value="SI2310"/>
<part name="GND28" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R3" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="U$1" library="REBANE_POWER" deviceset="VIN" device=""/>
<part name="U$2" library="REBANE_POWER" deviceset="VIN" device=""/>
<part name="U1" library="REBANE" deviceset="8PIN-CHIP" device="-SOIC-8" value="ADP3334ARZ">
<attribute name="P1" value="GND"/>
<attribute name="P2" value="SD"/>
<attribute name="P3" value="IN"/>
<attribute name="P4" value="IN"/>
<attribute name="P5" value="OUT"/>
<attribute name="P6" value="OUT"/>
<attribute name="P7" value="FB"/>
<attribute name="P8" value="NC"/>
</part>
<part name="U$3" library="REBANE_POWER" deviceset="VIN" device=""/>
<part name="GND29" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C7" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="10uF"/>
<part name="GND30" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V5" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R4" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="140k"/>
<part name="R11" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="78k7"/>
<part name="GND31" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R22" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="GND32" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R19" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="GND33" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="T3" library="REBANE" deviceset="MOSFET-N" device="-SOT-23" value="SI2310"/>
<part name="GND34" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R15" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="U$4" library="REBANE_POWER" deviceset="VIN" device=""/>
<part name="U4" library="REBANE" deviceset="8PIN-CHIP" device="-SOIC-8" value="ADP3334ARZ">
<attribute name="P1" value="GND"/>
<attribute name="P2" value="SD"/>
<attribute name="P3" value="IN"/>
<attribute name="P4" value="IN"/>
<attribute name="P5" value="OUT"/>
<attribute name="P6" value="OUT"/>
<attribute name="P7" value="FB"/>
<attribute name="P8" value="NC"/>
</part>
<part name="U$5" library="REBANE_POWER" deviceset="VIN" device=""/>
<part name="GND35" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C14" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="10uF"/>
<part name="GND36" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R18" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="210k"/>
<part name="R20" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="64k9"/>
<part name="GND37" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="P+5" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="C1" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="10nF"/>
<part name="C2" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="10uF"/>
<part name="GND38" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C8" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="10nF"/>
<part name="C9" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="10uF"/>
<part name="GND39" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="WS1" library="REBANE" deviceset="WIRE_SOLDER" device="" value="+9V"/>
<part name="WS2" library="REBANE" deviceset="WIRE_SOLDER" device="" value="-9V"/>
<part name="U$8" library="REBANE_POWER" deviceset="VIN" device=""/>
<part name="GND40" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="WS4" library="REBANE" deviceset="WIRE_SOLDER" device="" value="B"/>
<part name="WS3" library="REBANE" deviceset="WIRE_SOLDER" device="" value="C"/>
<part name="WS5" library="REBANE" deviceset="WIRE_SOLDER" device="" value="E"/>
<part name="C28" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND41" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="P+6" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="C27" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND42" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="P+7" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="GND43" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND45" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="FRAME3" library="frames" deviceset="A4L-LOC" device=""/>
<part name="GND46" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="P+8" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="L2" library="REBANE" deviceset="FERRITE/INDUCTOR" device="-0603[1608-METRIC]" value="FB"/>
<part name="C13" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="C12" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND47" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND48" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND49" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="P+9" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="L1" library="REBANE" deviceset="FERRITE/INDUCTOR" device="-0603[1608-METRIC]" value="FB"/>
<part name="C4" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="C5" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND50" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND51" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R12" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="T1" library="REBANE" deviceset="MOSFET-P" device="-SOT-23" value="SI2319"/>
<part name="U$6" library="REBANE_POWER" deviceset="VIN" device=""/>
<part name="R7" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="140k"/>
<part name="R10" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="78k7"/>
<part name="GND52" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C6" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND53" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V6" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R24" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="GND44" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C10" library="REBANE" deviceset="CAPACITOR-POL" device="-1206[3216-METRIC][CASE-A]" value="100uF"/>
<part name="C11" library="REBANE" deviceset="CAPACITOR-POL" device="-1206[3216-METRIC][CASE-A]" value="100uF"/>
<part name="GND54" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND55" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="C3" library="REBANE" deviceset="CAPACITOR-POL" device="-1206[3216-METRIC][CASE-A]" value="100uF"/>
<part name="GND56" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="U7" library="REBANE" deviceset="SOT23-5" device="" value="SN74LVC1G38D">
<attribute name="P1" value="A"/>
<attribute name="P2" value="B"/>
<attribute name="P3" value="GND"/>
<attribute name="P4" value="Y"/>
<attribute name="P5" value="VCC"/>
</part>
<part name="GND57" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="+3V7" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="C21" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND58" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="J1" library="jlink" deviceset="2X5_1.27" device=""/>
<part name="+3V8" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND59" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R5" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="GND60" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R1" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="+3V9" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V10" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R2" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="+3V11" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R21" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="R23" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="+3V12" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="GND61" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R27" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="220"/>
<part name="+3V13" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="BZ1" library="buzzer" deviceset="KXG0905C_KXG0905C" device="" value="KXG0905C"/>
<part name="D3" library="REBANE" deviceset="DIODE" device="-SOD-323" value="BAT20J"/>
<part name="T4" library="REBANE" deviceset="TRANSISTOR-NPN" device="-SOT-23" value="BC847"/>
<part name="R25" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="1k"/>
<part name="GND19" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R26" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="+3V14" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="R13" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="120"/>
<part name="R16" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="220"/>
<part name="GND62" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="R14" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="120"/>
<part name="R17" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="220"/>
<part name="GND63" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="FIDUCIAL1" library="REBANE" deviceset="FIDUCIAL" device=""/>
<part name="FIDUCIAL2" library="REBANE" deviceset="FIDUCIAL" device=""/>
<part name="FIDUCIAL3" library="REBANE" deviceset="FIDUCIAL" device=""/>
<part name="FIDUCIAL4" library="REBANE" deviceset="FIDUCIAL" device=""/>
<part name="JP1" library="REBANE_PINHEAD" deviceset="PINHEADER-01X1" device="-2.54"/>
<part name="JP2" library="REBANE_PINHEAD" deviceset="PINHEADER-01X1" device="-2.54"/>
<part name="GND64" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="GND65" library="REBANE_POWER" deviceset="GND" device=""/>
<part name="U$7" library="logo" deviceset="ESD" device=""/>
<part name="U$9" library="logo" deviceset="MUELLTONNE" device=""/>
<part name="U$10" library="logo" deviceset="MUELLTONNE" device="COVER"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="129.54" y1="7.62" x2="7.62" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="81.28" width="0.1524" layer="94" style="longdash"/>
<wire x1="129.54" y1="81.28" x2="7.62" y2="81.28" width="0.1524" layer="94" style="longdash"/>
<wire x1="129.54" y1="7.62" x2="129.54" y2="81.28" width="0.1524" layer="94" style="longdash"/>
<wire x1="129.54" y1="86.36" x2="7.62" y2="86.36" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="170.18" x2="7.62" y2="86.36" width="0.1524" layer="94" style="longdash"/>
<wire x1="129.54" y1="170.18" x2="129.54" y2="86.36" width="0.1524" layer="94" style="longdash"/>
<wire x1="129.54" y1="170.18" x2="7.62" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="170.18" x2="134.62" y2="104.14" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="170.18" x2="251.46" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="104.14" x2="251.46" y2="104.14" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="170.18" x2="251.46" y2="104.14" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="99.06" x2="134.62" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="99.06" x2="251.46" y2="99.06" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="27.94" x2="251.46" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="99.06" x2="251.46" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<text x="10.16" y="167.64" size="5.08" layer="94" font="vector" rot="MR180">3V3 SUPPLY</text>
<text x="10.16" y="78.74" size="5.08" layer="94" font="vector" rot="MR180">5V SUPPLY</text>
<text x="137.16" y="167.64" size="5.08" layer="94" font="vector" rot="MR180">DAC</text>
<text x="137.16" y="96.52" size="5.08" layer="94" font="vector" rot="MR180">ADC</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="SW1" gate="SW" x="27.94" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="27.432" y="139.446" size="1.778" layer="95" font="vector" rot="MR270"/>
</instance>
<instance part="D2" gate="D" x="35.56" y="127" smashed="yes" rot="R180">
<attribute name="NAME" x="33.782" y="127.508" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="40.64" y="125.476" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="D1" gate="D" x="35.56" y="132.08" smashed="yes" rot="R180">
<attribute name="NAME" x="33.782" y="132.588" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="37.338" y="132.588" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R6" gate="R" x="27.94" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="27.432" y="116.332" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="27.432" y="122.428" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R8" gate="R" x="27.94" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="27.432" y="98.552" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="27.432" y="104.648" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND26" gate="1" x="27.94" y="91.44"/>
<instance part="R9" gate="R" x="40.64" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="40.132" y="98.552" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="40.132" y="104.648" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND27" gate="1" x="40.64" y="91.44"/>
<instance part="T2" gate="G$1" x="48.26" y="127" smashed="yes">
<attribute name="NAME" x="48.768" y="129.54" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="48.768" y="124.46" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="GND28" gate="1" x="48.26" y="91.44"/>
<instance part="R3" gate="R" x="48.26" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="47.752" y="139.192" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="47.752" y="145.288" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="U$1" gate="G$1" x="27.94" y="160.02"/>
<instance part="U$2" gate="G$1" x="48.26" y="160.02"/>
<instance part="U1" gate="G$1" x="91.44" y="137.16" smashed="yes" rot="MR180">
<attribute name="P1" x="84.582" y="133.096" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P2" x="84.582" y="135.636" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P3" x="84.582" y="138.176" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P4" x="84.582" y="140.716" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P5" x="98.298" y="140.716" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="P6" x="98.298" y="138.176" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="P7" x="98.298" y="135.636" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="P8" x="98.298" y="133.096" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="NAME" x="90.17" y="142.748" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="99.568" y="129.032" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U$3" gate="G$1" x="73.66" y="160.02"/>
<instance part="GND29" gate="1" x="78.74" y="91.44"/>
<instance part="C7" gate="C" x="73.66" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="73.152" y="100.33" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="73.152" y="102.87" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND30" gate="1" x="73.66" y="91.44"/>
<instance part="+3V5" gate="G$1" x="119.38" y="160.02"/>
<instance part="R4" gate="R" x="109.22" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="108.712" y="121.412" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="108.712" y="127.508" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R11" gate="R" x="109.22" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="108.712" y="98.552" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="108.712" y="104.648" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND31" gate="1" x="109.22" y="91.44"/>
<instance part="R19" gate="R" x="25.4" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="24.892" y="19.812" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="24.892" y="25.908" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND33" gate="1" x="25.4" y="12.7"/>
<instance part="T3" gate="G$1" x="33.02" y="40.64" smashed="yes">
<attribute name="NAME" x="33.528" y="43.18" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="33.528" y="38.1" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="GND34" gate="1" x="33.02" y="12.7"/>
<instance part="R15" gate="R" x="33.02" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="32.512" y="52.832" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="32.512" y="58.928" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="U$4" gate="G$1" x="33.02" y="71.12"/>
<instance part="U4" gate="G$1" x="60.96" y="50.8" smashed="yes" rot="MR180">
<attribute name="P1" x="54.102" y="46.736" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P2" x="54.102" y="49.276" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P3" x="54.102" y="51.816" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P4" x="54.102" y="54.356" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P5" x="67.818" y="54.356" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="P6" x="67.818" y="51.816" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="P7" x="67.818" y="49.276" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="P8" x="67.818" y="46.736" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="NAME" x="59.69" y="56.388" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="69.088" y="42.672" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U$5" gate="G$1" x="43.18" y="71.12"/>
<instance part="GND35" gate="1" x="48.26" y="12.7"/>
<instance part="C14" gate="C" x="43.18" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="42.672" y="21.59" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="42.672" y="24.13" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND36" gate="1" x="43.18" y="12.7"/>
<instance part="R18" gate="R" x="78.74" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="78.232" y="37.592" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="78.232" y="43.688" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R20" gate="R" x="78.74" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="78.232" y="19.812" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="78.232" y="25.908" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND37" gate="1" x="78.74" y="12.7"/>
<instance part="P+5" gate="1" x="88.9" y="71.12"/>
<instance part="C1" gate="C" x="114.3" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="113.792" y="123.19" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="113.792" y="125.73" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C2" gate="C" x="119.38" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="118.872" y="123.19" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="118.872" y="125.73" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND38" gate="1" x="119.38" y="91.44"/>
<instance part="C8" gate="C" x="83.82" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="83.312" y="39.37" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="83.312" y="41.91" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C9" gate="C" x="88.9" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="88.392" y="39.37" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="88.392" y="41.91" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND39" gate="1" x="88.9" y="12.7"/>
<instance part="WS1" gate="G$1" x="20.32" y="144.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="19.558" y="143.51" size="1.778" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="WS2" gate="G$1" x="20.32" y="99.06" smashed="yes" rot="R90">
<attribute name="VALUE" x="21.336" y="100.33" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="U$8" gate="G$1" x="20.32" y="160.02"/>
<instance part="GND40" gate="1" x="20.32" y="91.44"/>
<instance part="U2" gate="G$1" x="177.8" y="134.62" smashed="yes">
<attribute name="NAME" x="176.276" y="143.002" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="182.626" y="126.238" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="U3" gate="G$1" x="180.34" y="58.42" smashed="yes">
<attribute name="NAME" x="178.816" y="69.342" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="186.436" y="44.958" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND46" gate="1" x="165.1" y="33.02"/>
<instance part="P+8" gate="1" x="210.82" y="93.98"/>
<instance part="L2" gate="L" x="203.2" y="55.88" smashed="yes">
<attribute name="NAME" x="200.152" y="56.388" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="206.248" y="56.388" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C13" gate="C" x="210.82" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="210.312" y="39.37" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="210.312" y="41.91" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C12" gate="C" x="195.58" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="195.072" y="39.37" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="195.072" y="41.91" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND47" gate="1" x="195.58" y="33.02"/>
<instance part="GND48" gate="1" x="210.82" y="33.02"/>
<instance part="GND49" gate="1" x="162.56" y="109.22"/>
<instance part="P+9" gate="1" x="218.44" y="165.1"/>
<instance part="L1" gate="L" x="208.28" y="139.7" smashed="yes">
<attribute name="NAME" x="205.232" y="140.208" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="211.328" y="140.208" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C4" gate="C" x="193.04" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="192.532" y="118.11" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="192.532" y="120.65" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C5" gate="C" x="218.44" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="217.932" y="118.11" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="217.932" y="120.65" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND50" gate="1" x="193.04" y="109.22"/>
<instance part="GND51" gate="1" x="218.44" y="109.22"/>
<instance part="R12" gate="R" x="165.1" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="164.592" y="75.692" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="164.592" y="81.788" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="T1" gate="T" x="63.5" y="142.24" smashed="yes">
<attribute name="NAME" x="64.008" y="144.78" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="64.008" y="139.7" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="U$6" gate="G$1" x="63.5" y="160.02"/>
<instance part="R7" gate="R" x="63.5" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="62.992" y="121.412" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="62.992" y="127.508" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R10" gate="R" x="63.5" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="62.992" y="98.552" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="62.992" y="104.648" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND52" gate="1" x="63.5" y="91.44"/>
<instance part="C6" gate="C" x="58.42" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="57.912" y="100.33" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="57.912" y="102.87" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND53" gate="1" x="58.42" y="91.44"/>
<instance part="C10" gate="C" x="93.98" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="93.472" y="39.37" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="93.472" y="41.91" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C11" gate="C" x="99.06" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="98.552" y="39.37" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="98.552" y="41.91" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND54" gate="1" x="93.98" y="12.7"/>
<instance part="GND55" gate="1" x="99.06" y="12.7"/>
<instance part="C3" gate="C" x="124.46" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="123.952" y="123.19" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="123.952" y="125.73" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND56" gate="1" x="124.46" y="91.44"/>
<instance part="R5" gate="R" x="160.02" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="159.512" y="116.332" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="159.512" y="122.428" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND60" gate="1" x="160.02" y="109.22"/>
<instance part="R1" gate="R" x="160.02" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="159.512" y="146.812" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="159.512" y="152.908" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="+3V9" gate="G$1" x="160.02" y="165.1"/>
<instance part="+3V10" gate="G$1" x="165.1" y="93.98"/>
<instance part="R2" gate="R" x="195.58" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="195.072" y="146.812" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="195.072" y="152.908" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="+3V11" gate="G$1" x="195.58" y="165.1"/>
<instance part="R13" gate="R" x="220.98" y="63.5" smashed="yes">
<attribute name="NAME" x="217.932" y="64.008" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="224.028" y="64.008" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R16" gate="R" x="228.6" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="228.092" y="45.212" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="228.092" y="51.308" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND62" gate="1" x="228.6" y="33.02"/>
<instance part="R14" gate="R" x="220.98" y="60.96" smashed="yes">
<attribute name="NAME" x="217.932" y="61.468" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="224.028" y="61.468" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R17" gate="R" x="231.14" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="230.632" y="45.212" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="230.632" y="51.308" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND63" gate="1" x="231.14" y="33.02"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R8" gate="R" pin="P$1"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="27.94" y1="96.52" x2="27.94" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R9" gate="R" pin="P$1"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="40.64" y1="96.52" x2="40.64" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="G$1" pin="S"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="48.26" y1="121.92" x2="48.26" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="1"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="81.28" y1="132.08" x2="78.74" y2="132.08" width="0.1524" layer="91"/>
<wire x1="78.74" y1="132.08" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="C" pin="1"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="73.66" y1="99.06" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R11" gate="R" pin="P$1"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="109.22" y1="96.52" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R19" gate="R" pin="P$1"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="25.4" y1="17.78" x2="25.4" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T3" gate="G$1" pin="S"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="33.02" y1="35.56" x2="33.02" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="1"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="50.8" y1="45.72" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
<wire x1="48.26" y1="45.72" x2="48.26" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="C" pin="1"/>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="43.18" y1="20.32" x2="43.18" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R20" gate="R" pin="P$1"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="78.74" y1="17.78" x2="78.74" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="C" pin="1"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="119.38" y1="121.92" x2="119.38" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="C" pin="1"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="88.9" y1="38.1" x2="88.9" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="WS2" gate="G$1" pin="P$1"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="20.32" y1="96.52" x2="20.32" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="CLK"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="167.64" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<wire x1="165.1" y1="60.96" x2="165.1" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="DGND"/>
<wire x1="165.1" y1="58.42" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<wire x1="165.1" y1="55.88" x2="165.1" y2="35.56" width="0.1524" layer="91"/>
<wire x1="167.64" y1="58.42" x2="165.1" y2="58.42" width="0.1524" layer="91"/>
<junction x="165.1" y="58.42"/>
<pinref part="U3" gate="G$1" pin="AVSS"/>
<wire x1="167.64" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<junction x="165.1" y="55.88"/>
</segment>
<segment>
<pinref part="C12" gate="C" pin="1"/>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="195.58" y1="38.1" x2="195.58" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="C" pin="1"/>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="210.82" y1="38.1" x2="210.82" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="GND49" gate="1" pin="GND"/>
<wire x1="165.1" y1="134.62" x2="162.56" y2="134.62" width="0.1524" layer="91"/>
<wire x1="162.56" y1="134.62" x2="162.56" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="C" pin="1"/>
<pinref part="GND50" gate="1" pin="GND"/>
<wire x1="193.04" y1="116.84" x2="193.04" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="C" pin="1"/>
<pinref part="GND51" gate="1" pin="GND"/>
<wire x1="218.44" y1="116.84" x2="218.44" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R10" gate="R" pin="P$1"/>
<pinref part="GND52" gate="1" pin="GND"/>
<wire x1="63.5" y1="96.52" x2="63.5" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="C" pin="1"/>
<pinref part="GND53" gate="1" pin="GND"/>
<wire x1="58.42" y1="99.06" x2="58.42" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="C" pin="N"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="93.98" y1="38.1" x2="93.98" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="C" pin="N"/>
<pinref part="GND55" gate="1" pin="GND"/>
<wire x1="99.06" y1="38.1" x2="99.06" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="C" pin="N"/>
<pinref part="GND56" gate="1" pin="GND"/>
<wire x1="124.46" y1="121.92" x2="124.46" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R5" gate="R" pin="P$1"/>
<pinref part="GND60" gate="1" pin="GND"/>
<wire x1="160.02" y1="114.3" x2="160.02" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R16" gate="R" pin="P$1"/>
<pinref part="GND62" gate="1" pin="GND"/>
<wire x1="228.6" y1="43.18" x2="228.6" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R17" gate="R" pin="P$1"/>
<pinref part="GND63" gate="1" pin="GND"/>
<wire x1="231.14" y1="43.18" x2="231.14" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="5"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="101.6" y1="139.7" x2="109.22" y2="139.7" width="0.1524" layer="91"/>
<wire x1="109.22" y1="139.7" x2="114.3" y2="139.7" width="0.1524" layer="91"/>
<wire x1="114.3" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<wire x1="119.38" y1="139.7" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="6"/>
<pinref part="R4" gate="R" pin="P$2"/>
<wire x1="101.6" y1="137.16" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
<wire x1="109.22" y1="137.16" x2="109.22" y2="129.54" width="0.1524" layer="91"/>
<wire x1="109.22" y1="139.7" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
<junction x="109.22" y="137.16"/>
<junction x="109.22" y="139.7"/>
<pinref part="C1" gate="C" pin="2"/>
<wire x1="114.3" y1="127" x2="114.3" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C2" gate="C" pin="2"/>
<wire x1="119.38" y1="127" x2="119.38" y2="137.16" width="0.1524" layer="91"/>
<junction x="114.3" y="139.7"/>
<junction x="119.38" y="139.7"/>
<pinref part="C3" gate="C" pin="P"/>
<wire x1="119.38" y1="137.16" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<wire x1="124.46" y1="127" x2="124.46" y2="137.16" width="0.1524" layer="91"/>
<wire x1="124.46" y1="137.16" x2="119.38" y2="137.16" width="0.1524" layer="91"/>
<junction x="119.38" y="137.16"/>
</segment>
<segment>
<pinref part="R1" gate="R" pin="P$2"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<wire x1="160.02" y1="157.48" x2="160.02" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R12" gate="R" pin="P$2"/>
<wire x1="165.1" y1="86.36" x2="165.1" y2="83.82" width="0.1524" layer="91"/>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
<pinref part="R2" gate="R" pin="P$2"/>
<wire x1="195.58" y1="157.48" x2="195.58" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="D2" gate="D" pin="A"/>
<wire x1="33.02" y1="127" x2="27.94" y2="127" width="0.1524" layer="91"/>
<pinref part="SW1" gate="SW" pin="A"/>
<wire x1="27.94" y1="127" x2="27.94" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R6" gate="R" pin="P$2"/>
<wire x1="27.94" y1="124.46" x2="27.94" y2="127" width="0.1524" layer="91"/>
<junction x="27.94" y="127"/>
</segment>
</net>
<net name="SW1" class="0">
<segment>
<pinref part="R6" gate="R" pin="P$1"/>
<pinref part="R8" gate="R" pin="P$2"/>
<wire x1="27.94" y1="114.3" x2="27.94" y2="111.76" width="0.1524" layer="91"/>
<wire x1="27.94" y1="111.76" x2="27.94" y2="106.68" width="0.1524" layer="91"/>
<wire x1="27.94" y1="111.76" x2="15.24" y2="111.76" width="0.1524" layer="91"/>
<junction x="27.94" y="111.76"/>
<label x="15.24" y="111.76" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="PWR" class="0">
<segment>
<pinref part="D1" gate="D" pin="A"/>
<wire x1="33.02" y1="132.08" x2="15.24" y2="132.08" width="0.1524" layer="91"/>
<label x="15.24" y="132.08" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="D2" gate="D" pin="C"/>
<wire x1="38.1" y1="127" x2="40.64" y2="127" width="0.1524" layer="91"/>
<wire x1="40.64" y1="127" x2="43.18" y2="127" width="0.1524" layer="91"/>
<pinref part="R9" gate="R" pin="P$2"/>
<wire x1="40.64" y1="106.68" x2="40.64" y2="127" width="0.1524" layer="91"/>
<junction x="40.64" y="127"/>
<pinref part="T2" gate="G$1" pin="G"/>
<pinref part="D1" gate="D" pin="C"/>
<wire x1="38.1" y1="132.08" x2="40.64" y2="132.08" width="0.1524" layer="91"/>
<wire x1="40.64" y1="132.08" x2="40.64" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="VIN"/>
<pinref part="SW1" gate="SW" pin="B"/>
<wire x1="27.94" y1="152.4" x2="27.94" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VIN"/>
<pinref part="R3" gate="R" pin="P$2"/>
<wire x1="48.26" y1="152.4" x2="48.26" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="VIN"/>
<wire x1="73.66" y1="152.4" x2="73.66" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="3"/>
<wire x1="73.66" y1="139.7" x2="73.66" y2="137.16" width="0.1524" layer="91"/>
<wire x1="81.28" y1="137.16" x2="73.66" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="4"/>
<wire x1="81.28" y1="139.7" x2="73.66" y2="139.7" width="0.1524" layer="91"/>
<junction x="73.66" y="139.7"/>
<pinref part="C7" gate="C" pin="2"/>
<wire x1="73.66" y1="104.14" x2="73.66" y2="137.16" width="0.1524" layer="91"/>
<junction x="73.66" y="137.16"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="VIN"/>
<pinref part="R15" gate="R" pin="P$2"/>
<wire x1="33.02" y1="63.5" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="VIN"/>
<wire x1="43.18" y1="63.5" x2="43.18" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="3"/>
<wire x1="43.18" y1="53.34" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="4"/>
<wire x1="50.8" y1="53.34" x2="43.18" y2="53.34" width="0.1524" layer="91"/>
<junction x="43.18" y="53.34"/>
<pinref part="C14" gate="C" pin="2"/>
<wire x1="43.18" y1="25.4" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<junction x="43.18" y="50.8"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VIN"/>
<pinref part="WS1" gate="G$1" pin="P$1"/>
<wire x1="20.32" y1="152.4" x2="20.32" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="VIN"/>
<pinref part="T1" gate="T" pin="S"/>
<wire x1="63.5" y1="152.4" x2="63.5" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R3" gate="R" pin="P$1"/>
<pinref part="T2" gate="G$1" pin="D"/>
<wire x1="48.26" y1="137.16" x2="48.26" y2="134.62" width="0.1524" layer="91"/>
<wire x1="48.26" y1="134.62" x2="48.26" y2="132.08" width="0.1524" layer="91"/>
<wire x1="81.28" y1="134.62" x2="55.88" y2="134.62" width="0.1524" layer="91"/>
<junction x="48.26" y="134.62"/>
<pinref part="U1" gate="G$1" pin="2"/>
<pinref part="T1" gate="T" pin="G"/>
<wire x1="55.88" y1="134.62" x2="48.26" y2="134.62" width="0.1524" layer="91"/>
<wire x1="58.42" y1="142.24" x2="55.88" y2="142.24" width="0.1524" layer="91"/>
<wire x1="55.88" y1="142.24" x2="55.88" y2="134.62" width="0.1524" layer="91"/>
<junction x="55.88" y="134.62"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="7"/>
<wire x1="101.6" y1="134.62" x2="104.14" y2="134.62" width="0.1524" layer="91"/>
<wire x1="104.14" y1="134.62" x2="104.14" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R4" gate="R" pin="P$1"/>
<wire x1="104.14" y1="111.76" x2="109.22" y2="111.76" width="0.1524" layer="91"/>
<wire x1="109.22" y1="111.76" x2="109.22" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R11" gate="R" pin="P$2"/>
<wire x1="109.22" y1="114.3" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<wire x1="109.22" y1="106.68" x2="109.22" y2="111.76" width="0.1524" layer="91"/>
<junction x="109.22" y="111.76"/>
<pinref part="C1" gate="C" pin="1"/>
<wire x1="109.22" y1="114.3" x2="114.3" y2="114.3" width="0.1524" layer="91"/>
<wire x1="114.3" y1="114.3" x2="114.3" y2="121.92" width="0.1524" layer="91"/>
<junction x="109.22" y="114.3"/>
</segment>
</net>
<net name="5V_EN" class="0">
<segment>
<wire x1="15.24" y1="40.64" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="40.64" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R19" gate="R" pin="P$2"/>
<wire x1="25.4" y1="27.94" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<junction x="25.4" y="40.64"/>
<pinref part="T3" gate="G$1" pin="G"/>
<label x="15.24" y="40.64" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R15" gate="R" pin="P$1"/>
<pinref part="T3" gate="G$1" pin="D"/>
<wire x1="33.02" y1="50.8" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<wire x1="33.02" y1="48.26" x2="33.02" y2="45.72" width="0.1524" layer="91"/>
<wire x1="50.8" y1="48.26" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<junction x="33.02" y="48.26"/>
<pinref part="U4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="C8" gate="C" pin="1"/>
<wire x1="83.82" y1="33.02" x2="83.82" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="7"/>
<wire x1="71.12" y1="48.26" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
<wire x1="73.66" y1="48.26" x2="73.66" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R18" gate="R" pin="P$1"/>
<wire x1="73.66" y1="33.02" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R20" gate="R" pin="P$2"/>
<wire x1="78.74" y1="33.02" x2="78.74" y2="35.56" width="0.1524" layer="91"/>
<wire x1="78.74" y1="27.94" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<junction x="78.74" y="33.02"/>
<wire x1="83.82" y1="33.02" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<junction x="78.74" y="33.02"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="83.82" y1="53.34" x2="88.9" y2="53.34" width="0.1524" layer="91"/>
<wire x1="88.9" y1="53.34" x2="88.9" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C8" gate="C" pin="2"/>
<wire x1="83.82" y1="43.18" x2="83.82" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C9" gate="C" pin="2"/>
<wire x1="88.9" y1="43.18" x2="88.9" y2="50.8" width="0.1524" layer="91"/>
<junction x="83.82" y="53.34"/>
<junction x="88.9" y="53.34"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<pinref part="U4" gate="G$1" pin="5"/>
<wire x1="88.9" y1="50.8" x2="88.9" y2="53.34" width="0.1524" layer="91"/>
<wire x1="71.12" y1="53.34" x2="78.74" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="6"/>
<pinref part="R18" gate="R" pin="P$2"/>
<wire x1="71.12" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<wire x1="78.74" y1="50.8" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<wire x1="78.74" y1="53.34" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<junction x="78.74" y="50.8"/>
<wire x1="83.82" y1="53.34" x2="78.74" y2="53.34" width="0.1524" layer="91"/>
<junction x="78.74" y="53.34"/>
<pinref part="C11" gate="C" pin="P"/>
<wire x1="88.9" y1="50.8" x2="93.98" y2="50.8" width="0.1524" layer="91"/>
<wire x1="93.98" y1="50.8" x2="99.06" y2="50.8" width="0.1524" layer="91"/>
<wire x1="99.06" y1="50.8" x2="99.06" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C10" gate="C" pin="P"/>
<wire x1="93.98" y1="43.18" x2="93.98" y2="50.8" width="0.1524" layer="91"/>
<junction x="88.9" y="50.8"/>
<junction x="93.98" y="50.8"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="DVDD"/>
<pinref part="P+8" gate="1" pin="+5V"/>
<wire x1="210.82" y1="86.36" x2="210.82" y2="58.42" width="0.1524" layer="91"/>
<wire x1="210.82" y1="58.42" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<pinref part="L2" gate="L" pin="P$2"/>
<wire x1="208.28" y1="55.88" x2="210.82" y2="55.88" width="0.1524" layer="91"/>
<wire x1="210.82" y1="55.88" x2="210.82" y2="58.42" width="0.1524" layer="91"/>
<junction x="210.82" y="58.42"/>
<pinref part="C13" gate="C" pin="2"/>
<wire x1="210.82" y1="43.18" x2="210.82" y2="55.88" width="0.1524" layer="91"/>
<junction x="210.82" y="55.88"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VDD"/>
<pinref part="P+9" gate="1" pin="+5V"/>
<wire x1="190.5" y1="137.16" x2="218.44" y2="137.16" width="0.1524" layer="91"/>
<wire x1="218.44" y1="137.16" x2="218.44" y2="139.7" width="0.1524" layer="91"/>
<pinref part="L1" gate="L" pin="P$2"/>
<wire x1="218.44" y1="139.7" x2="218.44" y2="157.48" width="0.1524" layer="91"/>
<wire x1="213.36" y1="139.7" x2="218.44" y2="139.7" width="0.1524" layer="91"/>
<junction x="218.44" y="139.7"/>
<pinref part="C5" gate="C" pin="2"/>
<wire x1="218.44" y1="121.92" x2="218.44" y2="137.16" width="0.1524" layer="91"/>
<junction x="218.44" y="137.16"/>
</segment>
</net>
<net name="ADC1-2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="AIN0"/>
<wire x1="193.04" y1="53.34" x2="208.28" y2="53.34" width="0.1524" layer="91"/>
<label x="208.28" y="53.34" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ADC2-1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="AIN2"/>
<wire x1="167.64" y1="50.8" x2="142.24" y2="50.8" width="0.1524" layer="91"/>
<label x="142.24" y="50.8" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="ADC2-2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="AIN3"/>
<wire x1="167.64" y1="53.34" x2="142.24" y2="53.34" width="0.1524" layer="91"/>
<label x="142.24" y="53.34" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="ADC1-1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="AIN1"/>
<wire x1="193.04" y1="50.8" x2="208.28" y2="50.8" width="0.1524" layer="91"/>
<label x="208.28" y="50.8" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="ADC_SCK" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SCK"/>
<wire x1="167.64" y1="66.04" x2="142.24" y2="66.04" width="0.1524" layer="91"/>
<label x="142.24" y="66.04" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="ADC_CS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="/CS"/>
<wire x1="167.64" y1="63.5" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<label x="142.24" y="63.5" size="1.778" layer="95" font="vector"/>
<pinref part="R12" gate="R" pin="P$1"/>
<wire x1="165.1" y1="63.5" x2="142.24" y2="63.5" width="0.1524" layer="91"/>
<wire x1="165.1" y1="73.66" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<junction x="165.1" y="63.5"/>
</segment>
</net>
<net name="ADC_MOSI" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="MOSI"/>
<wire x1="193.04" y1="66.04" x2="208.28" y2="66.04" width="0.1524" layer="91"/>
<label x="208.28" y="66.04" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="AVDD"/>
<pinref part="L2" gate="L" pin="P$1"/>
<wire x1="198.12" y1="55.88" x2="195.58" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C12" gate="C" pin="2"/>
<wire x1="195.58" y1="55.88" x2="193.04" y2="55.88" width="0.1524" layer="91"/>
<wire x1="195.58" y1="43.18" x2="195.58" y2="55.88" width="0.1524" layer="91"/>
<junction x="195.58" y="55.88"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VREF"/>
<pinref part="L1" gate="L" pin="P$1"/>
<wire x1="203.2" y1="139.7" x2="193.04" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C4" gate="C" pin="2"/>
<wire x1="193.04" y1="139.7" x2="190.5" y2="139.7" width="0.1524" layer="91"/>
<wire x1="193.04" y1="121.92" x2="193.04" y2="139.7" width="0.1524" layer="91"/>
<junction x="193.04" y="139.7"/>
</segment>
</net>
<net name="DAC_MOSI" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="MOSI"/>
<wire x1="190.5" y1="134.62" x2="215.9" y2="134.62" width="0.1524" layer="91"/>
<label x="215.9" y="134.62" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="DAC_SCK" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SCK"/>
<wire x1="190.5" y1="132.08" x2="215.9" y2="132.08" width="0.1524" layer="91"/>
<label x="215.9" y="132.08" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="DAC_SYNC" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="/SYNC"/>
<wire x1="190.5" y1="129.54" x2="195.58" y2="129.54" width="0.1524" layer="91"/>
<label x="215.9" y="129.54" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="R2" gate="R" pin="P$1"/>
<wire x1="195.58" y1="129.54" x2="215.9" y2="129.54" width="0.1524" layer="91"/>
<wire x1="195.58" y1="144.78" x2="195.58" y2="129.54" width="0.1524" layer="91"/>
<junction x="195.58" y="129.54"/>
</segment>
</net>
<net name="DAC_LDAC" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="/LDAC"/>
<wire x1="165.1" y1="132.08" x2="160.02" y2="132.08" width="0.1524" layer="91"/>
<label x="142.24" y="132.08" size="1.778" layer="95" font="vector"/>
<pinref part="R1" gate="R" pin="P$1"/>
<wire x1="160.02" y1="132.08" x2="142.24" y2="132.08" width="0.1524" layer="91"/>
<wire x1="160.02" y1="144.78" x2="160.02" y2="132.08" width="0.1524" layer="91"/>
<junction x="160.02" y="132.08"/>
</segment>
</net>
<net name="DAC_CLR" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="/CLR"/>
<wire x1="165.1" y1="129.54" x2="160.02" y2="129.54" width="0.1524" layer="91"/>
<label x="142.24" y="129.54" size="1.778" layer="95" font="vector"/>
<pinref part="R5" gate="R" pin="P$2"/>
<wire x1="160.02" y1="129.54" x2="142.24" y2="129.54" width="0.1524" layer="91"/>
<wire x1="160.02" y1="124.46" x2="160.02" y2="129.54" width="0.1524" layer="91"/>
<junction x="160.02" y="129.54"/>
</segment>
</net>
<net name="DAC2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUTB"/>
<wire x1="165.1" y1="137.16" x2="142.24" y2="137.16" width="0.1524" layer="91"/>
<label x="142.24" y="137.16" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="DAC1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUTA"/>
<wire x1="165.1" y1="139.7" x2="142.24" y2="139.7" width="0.1524" layer="91"/>
<label x="142.24" y="139.7" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="BATT" class="0">
<segment>
<pinref part="R7" gate="R" pin="P$1"/>
<pinref part="R10" gate="R" pin="P$2"/>
<wire x1="63.5" y1="119.38" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<wire x1="63.5" y1="111.76" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
<wire x1="63.5" y1="111.76" x2="58.42" y2="111.76" width="0.1524" layer="91"/>
<junction x="63.5" y="111.76"/>
<label x="50.8" y="111.76" size="1.778" layer="95" font="vector"/>
<pinref part="C6" gate="C" pin="2"/>
<wire x1="58.42" y1="111.76" x2="50.8" y2="111.76" width="0.1524" layer="91"/>
<wire x1="58.42" y1="104.14" x2="58.42" y2="111.76" width="0.1524" layer="91"/>
<junction x="58.42" y="111.76"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="T1" gate="T" pin="D"/>
<pinref part="R7" gate="R" pin="P$2"/>
<wire x1="63.5" y1="137.16" x2="63.5" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="MISO"/>
<pinref part="R13" gate="R" pin="P$1"/>
<wire x1="193.04" y1="63.5" x2="215.9" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC_MISO" class="0">
<segment>
<pinref part="R13" gate="R" pin="P$2"/>
<pinref part="R16" gate="R" pin="P$2"/>
<wire x1="226.06" y1="63.5" x2="228.6" y2="63.5" width="0.1524" layer="91"/>
<wire x1="228.6" y1="63.5" x2="228.6" y2="53.34" width="0.1524" layer="91"/>
<wire x1="228.6" y1="63.5" x2="246.38" y2="63.5" width="0.1524" layer="91"/>
<junction x="228.6" y="63.5"/>
<label x="246.38" y="63.5" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="/DRDY"/>
<pinref part="R14" gate="R" pin="P$1"/>
<wire x1="193.04" y1="60.96" x2="215.9" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC_DRDY" class="0">
<segment>
<pinref part="R14" gate="R" pin="P$2"/>
<pinref part="R17" gate="R" pin="P$2"/>
<wire x1="226.06" y1="60.96" x2="231.14" y2="60.96" width="0.1524" layer="91"/>
<wire x1="231.14" y1="60.96" x2="231.14" y2="53.34" width="0.1524" layer="91"/>
<wire x1="231.14" y1="60.96" x2="246.38" y2="60.96" width="0.1524" layer="91"/>
<junction x="231.14" y="60.96"/>
<label x="246.38" y="60.96" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="149.86" y1="7.62" x2="7.62" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="116.84" x2="7.62" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="149.86" y1="116.84" x2="149.86" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="116.84" x2="149.86" y2="116.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="121.92" x2="149.86" y2="121.92" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="170.18" x2="149.86" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="170.18" x2="7.62" y2="121.92" width="0.1524" layer="94" style="longdash"/>
<wire x1="149.86" y1="170.18" x2="149.86" y2="121.92" width="0.1524" layer="94" style="longdash"/>
<wire x1="154.94" y1="170.18" x2="154.94" y2="111.76" width="0.1524" layer="94" style="longdash"/>
<wire x1="154.94" y1="170.18" x2="251.46" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="154.94" y1="111.76" x2="251.46" y2="111.76" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="111.76" x2="251.46" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<text x="10.16" y="167.64" size="5.08" layer="94" font="vector" rot="MR180">OLED/BUTTONS</text>
<wire x1="154.94" y1="106.68" x2="251.46" y2="106.68" width="0.1524" layer="94" style="longdash"/>
<wire x1="154.94" y1="73.66" x2="251.46" y2="73.66" width="0.1524" layer="94" style="longdash"/>
<wire x1="154.94" y1="73.66" x2="154.94" y2="106.68" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="73.66" x2="251.46" y2="106.68" width="0.1524" layer="94" style="longdash"/>
<text x="157.48" y="167.64" size="5.08" layer="94" font="vector" rot="MR180">BUZZER</text>
<text x="157.48" y="104.14" size="5.08" layer="94" font="vector" rot="MR180">DEBUG</text>
<wire x1="154.94" y1="68.58" x2="251.46" y2="68.58" width="0.1524" layer="94" style="longdash"/>
<wire x1="154.94" y1="27.94" x2="251.46" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="154.94" y1="27.94" x2="154.94" y2="68.58" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="27.94" x2="251.46" y2="68.58" width="0.1524" layer="94" style="longdash"/>
<text x="157.48" y="66.04" size="5.08" layer="94" font="vector" rot="MR180">RESET</text>
<text x="10.16" y="114.3" size="5.08" layer="94" font="vector" rot="MR180">MCU</text>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="OLED1" gate="G$1" x="50.8" y="147.32" smashed="yes" rot="MR90">
<attribute name="NAME" x="36.068" y="152.908" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="62.992" y="152.908" size="1.778" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="SW2" gate="SW" x="106.68" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="106.172" y="139.446" size="1.778" layer="95" font="vector" rot="MR270"/>
</instance>
<instance part="SW3" gate="SW" x="114.3" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="113.792" y="139.446" size="1.778" layer="95" font="vector" rot="MR270"/>
</instance>
<instance part="SW4" gate="SW" x="121.92" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="121.412" y="139.446" size="1.778" layer="95" font="vector" rot="MR270"/>
</instance>
<instance part="U6" gate="G$1" x="119.38" y="55.88" smashed="yes">
<attribute name="NAME" x="125.222" y="56.896" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="125.222" y="53.594" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="U5" gate="G$1" x="55.88" y="55.88" smashed="yes">
<attribute name="NAME" x="54.102" y="56.388" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="62.23" y="55.372" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="A1" gate="G$1" x="121.92" y="73.66" smashed="yes" rot="R90">
<attribute name="VALUE" x="125.222" y="74.93" size="1.778" layer="95" font="vector"/>
<attribute name="NAME" x="125.222" y="77.724" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND6" gate="1" x="119.38" y="71.12" rot="R180"/>
<instance part="C25" gate="C" x="106.68" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="106.172" y="29.21" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="106.172" y="31.75" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C26" gate="C" x="111.76" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="111.252" y="29.21" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="111.252" y="31.75" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND7" gate="1" x="106.68" y="12.7"/>
<instance part="GND8" gate="1" x="111.76" y="12.7"/>
<instance part="GND9" gate="1" x="101.6" y="12.7"/>
<instance part="C20" gate="C" x="106.68" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="106.172" y="69.85" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="106.172" y="72.39" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="+3V1" gate="G$1" x="101.6" y="111.76"/>
<instance part="GND10" gate="1" x="48.26" y="43.18"/>
<instance part="Q1" gate="Q" x="48.26" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="47.752" y="87.376" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="47.752" y="94.488" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C18" gate="C" x="35.56" y="83.82" smashed="yes">
<attribute name="NAME" x="34.29" y="84.328" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="36.83" y="84.328" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C15" gate="C" x="35.56" y="93.98" smashed="yes">
<attribute name="NAME" x="34.29" y="94.488" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="36.83" y="94.488" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND11" gate="1" x="22.86" y="93.98" rot="R270"/>
<instance part="GND12" gate="1" x="22.86" y="83.82" rot="R270"/>
<instance part="Q2" gate="Q" x="71.12" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="70.612" y="87.376" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="70.612" y="94.488" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C16" gate="C" x="78.74" y="93.98" smashed="yes">
<attribute name="NAME" x="77.47" y="94.488" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="80.01" y="94.488" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C19" gate="C" x="78.74" y="83.82" smashed="yes">
<attribute name="NAME" x="77.47" y="84.328" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="80.01" y="84.328" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND13" gate="1" x="88.9" y="83.82" rot="R90"/>
<instance part="GND14" gate="1" x="88.9" y="93.98" rot="R90"/>
<instance part="GND15" gate="1" x="43.18" y="12.7"/>
<instance part="C17" gate="C" x="66.04" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="65.532" y="87.63" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="65.532" y="90.17" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND16" gate="1" x="66.04" y="111.76" rot="R180"/>
<instance part="GND17" gate="1" x="88.9" y="88.9" rot="R90"/>
<instance part="+3V2" gate="G$1" x="27.94" y="111.76"/>
<instance part="C24" gate="C" x="27.94" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="27.432" y="29.21" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="27.432" y="31.75" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND18" gate="1" x="27.94" y="12.7"/>
<instance part="+3V3" gate="G$1" x="58.42" y="127" rot="R180"/>
<instance part="+3V4" gate="G$1" x="60.96" y="127" rot="R180"/>
<instance part="GND21" gate="1" x="53.34" y="127"/>
<instance part="GND22" gate="1" x="43.18" y="127"/>
<instance part="GND23" gate="1" x="30.48" y="127"/>
<instance part="C23" gate="C" x="22.86" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="22.352" y="29.21" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="22.352" y="31.75" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C22" gate="C" x="17.78" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="17.272" y="29.21" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="17.272" y="31.75" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND24" gate="1" x="22.86" y="12.7"/>
<instance part="GND25" gate="1" x="17.78" y="12.7"/>
<instance part="R22" gate="R" x="78.74" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="78.232" y="139.192" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="78.232" y="145.288" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND32" gate="1" x="78.74" y="127"/>
<instance part="GND43" gate="1" x="106.68" y="127"/>
<instance part="GND45" gate="1" x="114.3" y="127"/>
<instance part="+3V6" gate="G$1" x="121.92" y="165.1"/>
<instance part="R24" gate="R" x="132.08" y="134.62" smashed="yes">
<attribute name="NAME" x="129.032" y="135.128" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="135.128" y="135.128" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND44" gate="1" x="139.7" y="127"/>
<instance part="U7" gate="G$1" x="208.28" y="50.8" smashed="yes">
<attribute name="P1" x="201.422" y="52.324" size="1.778" layer="95" font="vector"/>
<attribute name="P2" x="201.422" y="49.784" size="1.778" layer="95" font="vector"/>
<attribute name="P3" x="201.422" y="47.244" size="1.778" layer="95" font="vector"/>
<attribute name="P4" x="215.138" y="47.244" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="P5" x="215.138" y="52.324" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="NAME" x="206.248" y="56.642" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="218.186" y="44.958" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="GND57" gate="1" x="195.58" y="30.48"/>
<instance part="+3V7" gate="G$1" x="223.52" y="63.5"/>
<instance part="C21" gate="C" x="223.52" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="223.012" y="36.83" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="223.012" y="39.37" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND58" gate="1" x="223.52" y="30.48"/>
<instance part="J1" gate="G$1" x="210.82" y="88.9" smashed="yes" rot="MR0">
<attribute name="NAME" x="208.026" y="97.282" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="215.646" y="80.518" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="+3V8" gate="G$1" x="200.66" y="104.14"/>
<instance part="GND59" gate="1" x="200.66" y="76.2"/>
<instance part="R21" gate="R" x="88.9" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="88.392" y="146.812" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="88.392" y="152.908" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R23" gate="R" x="83.82" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="83.312" y="139.192" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="83.312" y="145.288" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="+3V12" gate="G$1" x="88.9" y="165.1"/>
<instance part="GND61" gate="1" x="83.82" y="127"/>
<instance part="R27" gate="R" x="96.52" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="96.012" y="85.852" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="96.012" y="91.948" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="+3V13" gate="G$1" x="96.52" y="111.76"/>
<instance part="BZ1" gate="G$1" x="210.82" y="144.78" smashed="yes">
<attribute name="NAME" x="213.36" y="147.32" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="213.36" y="142.24" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="D3" gate="D" x="205.74" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="205.232" y="143.002" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="205.232" y="146.558" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="T4" gate="T" x="208.28" y="129.54" smashed="yes">
<attribute name="VALUE" x="211.328" y="127" size="1.778" layer="96" font="vector" rot="MR180"/>
<attribute name="NAME" x="211.328" y="132.08" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="R25" gate="R" x="198.12" y="129.54" smashed="yes">
<attribute name="NAME" x="195.072" y="130.048" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="201.168" y="130.048" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND19" gate="1" x="210.82" y="116.84"/>
<instance part="R26" gate="R" x="198.12" y="121.92" smashed="yes">
<attribute name="NAME" x="195.072" y="122.428" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="201.168" y="122.428" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="+3V14" gate="G$1" x="210.82" y="162.56"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="U6" gate="G$1" pin="GND"/>
<wire x1="119.38" y1="68.58" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C25" gate="C" pin="1"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="106.68" y1="27.94" x2="106.68" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="C" pin="1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="111.76" y1="27.94" x2="111.76" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VSS@33"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="83.82" y1="60.96" x2="101.6" y2="60.96" width="0.1524" layer="91"/>
<wire x1="101.6" y1="60.96" x2="101.6" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VSS@34"/>
<wire x1="83.82" y1="63.5" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<wire x1="101.6" y1="63.5" x2="101.6" y2="60.96" width="0.1524" layer="91"/>
<junction x="101.6" y="60.96"/>
<pinref part="C20" gate="C" pin="1"/>
<wire x1="106.68" y1="68.58" x2="106.68" y2="63.5" width="0.1524" layer="91"/>
<wire x1="106.68" y1="63.5" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<junction x="101.6" y="63.5"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PAD"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="50.8" y1="48.26" x2="48.26" y2="48.26" width="0.1524" layer="91"/>
<wire x1="48.26" y1="48.26" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="C" pin="1"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="25.4" y1="93.98" x2="33.02" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C18" gate="C" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="25.4" y1="83.82" x2="33.02" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="C19" gate="C" pin="2"/>
<wire x1="86.36" y1="83.82" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C16" gate="C" pin="2"/>
<wire x1="86.36" y1="93.98" x2="81.28" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VSS@13"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="43.18" y1="15.24" x2="43.18" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="C17" gate="C" pin="2"/>
<wire x1="66.04" y1="109.22" x2="66.04" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q2" gate="Q" pin="GND"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="73.66" y1="88.9" x2="86.36" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C24" gate="C" pin="1"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="27.94" y1="27.94" x2="27.94" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OLED1" gate="G$1" pin="GND@4"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="53.34" y1="129.54" x2="53.34" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OLED1" gate="G$1" pin="GND@8"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="43.18" y1="129.54" x2="43.18" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OLED1" gate="G$1" pin="GND"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="33.02" y1="149.86" x2="30.48" y2="149.86" width="0.1524" layer="91"/>
<wire x1="30.48" y1="149.86" x2="30.48" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C22" gate="C" pin="1"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="17.78" y1="27.94" x2="17.78" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C23" gate="C" pin="1"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="22.86" y1="27.94" x2="22.86" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SW2" gate="SW" pin="A"/>
<wire x1="106.68" y1="137.16" x2="106.68" y2="129.54" width="0.1524" layer="91"/>
<pinref part="GND43" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="SW3" gate="SW" pin="A"/>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="114.3" y1="137.16" x2="114.3" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R24" gate="R" pin="P$2"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="137.16" y1="134.62" x2="139.7" y2="134.62" width="0.1524" layer="91"/>
<wire x1="139.7" y1="134.62" x2="139.7" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="3"/>
<pinref part="GND57" gate="1" pin="GND"/>
<wire x1="198.12" y1="48.26" x2="195.58" y2="48.26" width="0.1524" layer="91"/>
<wire x1="195.58" y1="48.26" x2="195.58" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="C" pin="1"/>
<pinref part="GND58" gate="1" pin="GND"/>
<wire x1="223.52" y1="35.56" x2="223.52" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="P$3"/>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="203.2" y1="91.44" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="200.66" y1="91.44" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="P$5"/>
<wire x1="200.66" y1="88.9" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="200.66" y1="83.82" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="203.2" y1="88.9" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="P$9"/>
<wire x1="203.2" y1="83.82" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<junction x="200.66" y="88.9"/>
<junction x="200.66" y="83.82"/>
</segment>
<segment>
<pinref part="R23" gate="R" pin="P$1"/>
<pinref part="GND61" gate="1" pin="GND"/>
<wire x1="83.82" y1="137.16" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R22" gate="R" pin="P$1"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="78.74" y1="137.16" x2="78.74" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T4" gate="T" pin="E"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="210.82" y1="119.38" x2="210.82" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R26" gate="R" pin="P$2"/>
<wire x1="210.82" y1="121.92" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
<wire x1="203.2" y1="121.92" x2="210.82" y2="121.92" width="0.1524" layer="91"/>
<junction x="210.82" y="121.92"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="ANT2"/>
<wire x1="83.82" y1="58.42" x2="111.76" y2="58.42" width="0.1524" layer="91"/>
<wire x1="111.76" y1="58.42" x2="111.76" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="BAL1"/>
<wire x1="111.76" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
<wire x1="116.84" y1="68.58" x2="116.84" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="ANT1"/>
<wire x1="83.82" y1="55.88" x2="111.76" y2="55.88" width="0.1524" layer="91"/>
<wire x1="111.76" y1="55.88" x2="111.76" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="BAL2"/>
<wire x1="111.76" y1="43.18" x2="116.84" y2="43.18" width="0.1524" layer="91"/>
<wire x1="116.84" y1="43.18" x2="116.84" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="VDD_PA"/>
<wire x1="83.82" y1="53.34" x2="109.22" y2="53.34" width="0.1524" layer="91"/>
<wire x1="109.22" y1="53.34" x2="109.22" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="DC"/>
<wire x1="109.22" y1="40.64" x2="111.76" y2="40.64" width="0.1524" layer="91"/>
<wire x1="111.76" y1="40.64" x2="119.38" y2="40.64" width="0.1524" layer="91"/>
<wire x1="119.38" y1="40.64" x2="119.38" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C26" gate="C" pin="2"/>
<wire x1="111.76" y1="33.02" x2="111.76" y2="40.64" width="0.1524" layer="91"/>
<junction x="111.76" y="40.64"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="FP"/>
<pinref part="U6" gate="G$1" pin="UNBL"/>
<wire x1="121.92" y1="71.12" x2="121.92" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="DEC2"/>
<wire x1="83.82" y1="50.8" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<wire x1="106.68" y1="50.8" x2="106.68" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C25" gate="C" pin="2"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="AVDD@35"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="83.82" y1="66.04" x2="101.6" y2="66.04" width="0.1524" layer="91"/>
<wire x1="101.6" y1="66.04" x2="101.6" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="AVDD@36"/>
<wire x1="101.6" y1="68.58" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="78.74" x2="101.6" y2="104.14" width="0.1524" layer="91"/>
<wire x1="83.82" y1="68.58" x2="101.6" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C20" gate="C" pin="2"/>
<wire x1="106.68" y1="73.66" x2="106.68" y2="78.74" width="0.1524" layer="91"/>
<wire x1="106.68" y1="78.74" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<junction x="101.6" y="68.58"/>
<junction x="101.6" y="78.74"/>
</segment>
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="U5" gate="G$1" pin="VDD@1"/>
<wire x1="27.94" y1="104.14" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="27.94" y1="68.58" x2="30.48" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VDD@12"/>
<wire x1="30.48" y1="40.64" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<wire x1="27.94" y1="40.64" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<junction x="27.94" y="68.58"/>
<pinref part="C24" gate="C" pin="2"/>
<wire x1="27.94" y1="33.02" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<junction x="27.94" y="40.64"/>
<pinref part="C23" gate="C" pin="2"/>
<wire x1="22.86" y1="33.02" x2="22.86" y2="40.64" width="0.1524" layer="91"/>
<wire x1="22.86" y1="40.64" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C22" gate="C" pin="2"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="40.64" width="0.1524" layer="91"/>
<wire x1="17.78" y1="40.64" x2="22.86" y2="40.64" width="0.1524" layer="91"/>
<junction x="22.86" y="40.64"/>
</segment>
<segment>
<pinref part="OLED1" gate="G$1" pin="VCC@2"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="58.42" y1="134.62" x2="58.42" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OLED1" gate="G$1" pin="VCC@1"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<wire x1="60.96" y1="134.62" x2="60.96" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SW4" gate="SW" pin="B"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="121.92" y1="157.48" x2="121.92" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="5"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="218.44" y1="53.34" x2="223.52" y2="53.34" width="0.1524" layer="91"/>
<wire x1="223.52" y1="53.34" x2="223.52" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C21" gate="C" pin="2"/>
<wire x1="223.52" y1="40.64" x2="223.52" y2="53.34" width="0.1524" layer="91"/>
<junction x="223.52" y="53.34"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="P$1"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="203.2" y1="93.98" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<wire x1="200.66" y1="93.98" x2="200.66" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R21" gate="R" pin="P$2"/>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
<wire x1="88.9" y1="157.48" x2="88.9" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V13" gate="G$1" pin="+3V3"/>
<pinref part="R27" gate="R" pin="P$2"/>
<wire x1="96.52" y1="104.14" x2="96.52" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V14" gate="G$1" pin="+3V3"/>
<wire x1="210.82" y1="154.94" x2="210.82" y2="152.4" width="0.1524" layer="91"/>
<pinref part="D3" gate="D" pin="C"/>
<wire x1="210.82" y1="152.4" x2="205.74" y2="152.4" width="0.1524" layer="91"/>
<wire x1="205.74" y1="152.4" x2="205.74" y2="147.32" width="0.1524" layer="91"/>
<pinref part="BZ1" gate="G$1" pin="P"/>
<wire x1="210.82" y1="149.86" x2="210.82" y2="152.4" width="0.1524" layer="91"/>
<junction x="210.82" y="152.4"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="Q1" gate="Q" pin="1"/>
<pinref part="U5" gate="G$1" pin="P0.27"/>
<wire x1="48.26" y1="86.36" x2="48.26" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C18" gate="C" pin="2"/>
<wire x1="48.26" y1="83.82" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<wire x1="38.1" y1="83.82" x2="48.26" y2="83.82" width="0.1524" layer="91"/>
<junction x="48.26" y="83.82"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="Q1" gate="Q" pin="2"/>
<wire x1="48.26" y1="91.44" x2="48.26" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="P0.26"/>
<wire x1="48.26" y1="93.98" x2="50.8" y2="93.98" width="0.1524" layer="91"/>
<wire x1="50.8" y1="93.98" x2="50.8" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C15" gate="C" pin="2"/>
<wire x1="38.1" y1="93.98" x2="48.26" y2="93.98" width="0.1524" layer="91"/>
<junction x="48.26" y="93.98"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="XC2"/>
<wire x1="68.58" y1="81.28" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<pinref part="Q2" gate="Q" pin="2"/>
<wire x1="68.58" y1="93.98" x2="71.12" y2="93.98" width="0.1524" layer="91"/>
<wire x1="71.12" y1="93.98" x2="71.12" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C16" gate="C" pin="1"/>
<wire x1="76.2" y1="93.98" x2="71.12" y2="93.98" width="0.1524" layer="91"/>
<junction x="71.12" y="93.98"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="Q2" gate="Q" pin="1"/>
<pinref part="U5" gate="G$1" pin="XC1"/>
<wire x1="71.12" y1="86.36" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C19" gate="C" pin="1"/>
<wire x1="71.12" y1="83.82" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<wire x1="76.2" y1="83.82" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<junction x="71.12" y="83.82"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="C17" gate="C" pin="1"/>
<pinref part="U5" gate="G$1" pin="DEC1"/>
<wire x1="66.04" y1="86.36" x2="66.04" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OLED_EN" class="0">
<segment>
<pinref part="OLED1" gate="G$1" pin="EN"/>
<wire x1="55.88" y1="142.24" x2="55.88" y2="127" width="0.1524" layer="91"/>
<label x="55.88" y="127" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.21"/>
<wire x1="63.5" y1="81.28" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<label x="63.5" y="111.76" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
<segment>
<pinref part="R22" gate="R" pin="P$2"/>
<wire x1="78.74" y1="147.32" x2="78.74" y2="165.1" width="0.1524" layer="91"/>
<label x="78.74" y="165.1" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="OLED_CS" class="0">
<segment>
<pinref part="OLED1" gate="G$1" pin="CS"/>
<wire x1="50.8" y1="142.24" x2="50.8" y2="127" width="0.1524" layer="91"/>
<label x="50.8" y="127" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.22"/>
<wire x1="60.96" y1="81.28" x2="60.96" y2="111.76" width="0.1524" layer="91"/>
<label x="60.96" y="111.76" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
<segment>
<pinref part="R21" gate="R" pin="P$1"/>
<wire x1="88.9" y1="144.78" x2="88.9" y2="127" width="0.1524" layer="91"/>
<label x="88.9" y="127" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="OLED_RES" class="0">
<segment>
<pinref part="OLED1" gate="G$1" pin="RES"/>
<wire x1="48.26" y1="142.24" x2="48.26" y2="127" width="0.1524" layer="91"/>
<label x="48.26" y="127" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.23"/>
<wire x1="58.42" y1="81.28" x2="58.42" y2="111.76" width="0.1524" layer="91"/>
<label x="58.42" y="111.76" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
<segment>
<pinref part="R23" gate="R" pin="P$2"/>
<wire x1="83.82" y1="147.32" x2="83.82" y2="165.1" width="0.1524" layer="91"/>
<label x="83.82" y="165.1" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="OLED_D/C" class="0">
<segment>
<pinref part="OLED1" gate="G$1" pin="D/C"/>
<wire x1="45.72" y1="142.24" x2="45.72" y2="127" width="0.1524" layer="91"/>
<label x="45.72" y="127" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.24"/>
<wire x1="55.88" y1="81.28" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<label x="55.88" y="111.76" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="OLED_MOSI" class="0">
<segment>
<wire x1="38.1" y1="142.24" x2="38.1" y2="127" width="0.1524" layer="91"/>
<label x="38.1" y="127" size="1.778" layer="95" font="vector" rot="R90"/>
<pinref part="OLED1" gate="G$1" pin="DIN"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.28"/>
<wire x1="45.72" y1="81.28" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
<label x="45.72" y="111.76" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="OLED_SCK" class="0">
<segment>
<wire x1="40.64" y1="142.24" x2="40.64" y2="127" width="0.1524" layer="91"/>
<pinref part="OLED1" gate="G$1" pin="SCK"/>
<label x="40.64" y="127" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
<segment>
<wire x1="53.34" y1="81.28" x2="53.34" y2="111.76" width="0.1524" layer="91"/>
<label x="53.34" y="111.76" size="1.778" layer="95" font="vector" rot="MR270"/>
<pinref part="U5" gate="G$1" pin="P0.25"/>
</segment>
</net>
<net name="/SW2" class="0">
<segment>
<pinref part="SW2" gate="SW" pin="B"/>
<wire x1="106.68" y1="147.32" x2="106.68" y2="165.1" width="0.1524" layer="91"/>
<label x="106.68" y="165.1" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.18"/>
<wire x1="83.82" y1="43.18" x2="93.98" y2="43.18" width="0.1524" layer="91"/>
<label x="93.98" y="43.18" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="/SW3" class="0">
<segment>
<pinref part="SW3" gate="SW" pin="B"/>
<wire x1="114.3" y1="147.32" x2="114.3" y2="165.1" width="0.1524" layer="91"/>
<label x="114.3" y="165.1" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.19"/>
<wire x1="83.82" y1="45.72" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
<label x="93.98" y="45.72" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SW4" class="0">
<segment>
<pinref part="SW4" gate="SW" pin="A"/>
<wire x1="121.92" y1="137.16" x2="121.92" y2="134.62" width="0.1524" layer="91"/>
<label x="121.92" y="127" size="1.778" layer="95" font="vector" rot="R90"/>
<pinref part="R24" gate="R" pin="P$1"/>
<wire x1="121.92" y1="134.62" x2="121.92" y2="127" width="0.1524" layer="91"/>
<wire x1="127" y1="134.62" x2="121.92" y2="134.62" width="0.1524" layer="91"/>
<junction x="121.92" y="134.62"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="2"/>
<wire x1="198.12" y1="50.8" x2="175.26" y2="50.8" width="0.1524" layer="91"/>
<label x="175.26" y="50.8" size="1.778" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.04"/>
<wire x1="30.48" y1="50.8" x2="15.24" y2="50.8" width="0.1524" layer="91"/>
<label x="15.24" y="50.8" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="SWD/RST" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SWDIO/NRESET"/>
<wire x1="68.58" y1="27.94" x2="68.58" y2="25.4" width="0.1524" layer="91"/>
<label x="68.58" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
<pinref part="R27" gate="R" pin="P$1"/>
<wire x1="68.58" y1="25.4" x2="68.58" y2="12.7" width="0.1524" layer="91"/>
<wire x1="96.52" y1="83.82" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<wire x1="96.52" y1="25.4" x2="68.58" y2="25.4" width="0.1524" layer="91"/>
<junction x="68.58" y="25.4"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="4"/>
<wire x1="218.44" y1="48.26" x2="238.76" y2="48.26" width="0.1524" layer="91"/>
<label x="238.76" y="48.26" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="P$2"/>
<wire x1="215.9" y1="93.98" x2="238.76" y2="93.98" width="0.1524" layer="91"/>
<label x="238.76" y="93.98" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SWC" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SWDCLK"/>
<wire x1="71.12" y1="27.94" x2="71.12" y2="12.7" width="0.1524" layer="91"/>
<label x="71.12" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="P$4"/>
<wire x1="215.9" y1="91.44" x2="238.76" y2="91.44" width="0.1524" layer="91"/>
<label x="238.76" y="91.44" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="SW1" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="1"/>
<wire x1="198.12" y1="53.34" x2="175.26" y2="53.34" width="0.1524" layer="91"/>
<label x="175.26" y="53.34" size="1.778" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.20"/>
<wire x1="83.82" y1="48.26" x2="93.98" y2="48.26" width="0.1524" layer="91"/>
<label x="93.98" y="48.26" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P$6"/>
<wire x1="215.9" y1="88.9" x2="238.76" y2="88.9" width="0.1524" layer="91"/>
<label x="238.76" y="88.9" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.29"/>
<wire x1="43.18" y1="81.28" x2="43.18" y2="111.76" width="0.1524" layer="91"/>
<label x="43.18" y="111.76" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P$8"/>
<wire x1="215.9" y1="86.36" x2="238.76" y2="86.36" width="0.1524" layer="91"/>
<label x="238.76" y="86.36" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.30"/>
<wire x1="30.48" y1="63.5" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
<label x="15.24" y="63.5" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="PWR" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.17"/>
<wire x1="83.82" y1="40.64" x2="93.98" y2="40.64" width="0.1524" layer="91"/>
<label x="93.98" y="40.64" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="BZ1" gate="G$1" pin="N"/>
<pinref part="T4" gate="T" pin="C"/>
<wire x1="210.82" y1="139.7" x2="210.82" y2="137.16" width="0.1524" layer="91"/>
<pinref part="D3" gate="D" pin="A"/>
<wire x1="210.82" y1="137.16" x2="210.82" y2="134.62" width="0.1524" layer="91"/>
<wire x1="205.74" y1="142.24" x2="205.74" y2="137.16" width="0.1524" layer="91"/>
<wire x1="205.74" y1="137.16" x2="210.82" y2="137.16" width="0.1524" layer="91"/>
<junction x="210.82" y="137.16"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="T4" gate="T" pin="B"/>
<pinref part="R25" gate="R" pin="P$2"/>
<wire x1="203.2" y1="129.54" x2="205.74" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BUZZER" class="0">
<segment>
<pinref part="R26" gate="R" pin="P$1"/>
<wire x1="193.04" y1="121.92" x2="187.96" y2="121.92" width="0.1524" layer="91"/>
<wire x1="187.96" y1="121.92" x2="187.96" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R25" gate="R" pin="P$1"/>
<wire x1="187.96" y1="129.54" x2="193.04" y2="129.54" width="0.1524" layer="91"/>
<wire x1="187.96" y1="129.54" x2="175.26" y2="129.54" width="0.1524" layer="91"/>
<junction x="187.96" y="129.54"/>
<label x="175.26" y="129.54" size="1.778" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="P0.00"/>
<wire x1="30.48" y1="60.96" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<label x="15.24" y="60.96" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="5V_EN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.16"/>
<wire x1="66.04" y1="27.94" x2="66.04" y2="12.7" width="0.1524" layer="91"/>
<label x="66.04" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="DAC_SCK" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.15"/>
<wire x1="63.5" y1="27.94" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
<label x="63.5" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="DAC_MOSI" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.14"/>
<wire x1="60.96" y1="27.94" x2="60.96" y2="12.7" width="0.1524" layer="91"/>
<label x="60.96" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ADC_SCK" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.13"/>
<wire x1="58.42" y1="27.94" x2="58.42" y2="12.7" width="0.1524" layer="91"/>
<label x="58.42" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="ADC_MOSI" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.12"/>
<wire x1="55.88" y1="27.94" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<label x="55.88" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="IO2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.11"/>
<wire x1="53.34" y1="27.94" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
<label x="53.34" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="DAC_SYNC" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.10"/>
<wire x1="50.8" y1="27.94" x2="50.8" y2="12.7" width="0.1524" layer="91"/>
<label x="50.8" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="DAC_LDAC" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.09"/>
<wire x1="48.26" y1="27.94" x2="48.26" y2="12.7" width="0.1524" layer="91"/>
<label x="48.26" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="IO1" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.08"/>
<wire x1="45.72" y1="27.94" x2="45.72" y2="12.7" width="0.1524" layer="91"/>
<label x="45.72" y="12.7" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
</net>
<net name="BATT" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.06"/>
<wire x1="30.48" y1="45.72" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<label x="15.24" y="45.72" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="DAC_CLR" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.07"/>
<wire x1="30.48" y1="43.18" x2="15.24" y2="43.18" width="0.1524" layer="91"/>
<label x="15.24" y="43.18" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="DAC_EN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.05"/>
<wire x1="30.48" y1="48.26" x2="15.24" y2="48.26" width="0.1524" layer="91"/>
<label x="15.24" y="48.26" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="ADC_CS" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.03"/>
<wire x1="30.48" y1="53.34" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
<label x="15.24" y="53.34" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="ADC_DRDY" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.02"/>
<wire x1="30.48" y1="55.88" x2="15.24" y2="55.88" width="0.1524" layer="91"/>
<label x="15.24" y="55.88" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="ADC_MISO" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="P0.01"/>
<wire x1="30.48" y1="58.42" x2="15.24" y2="58.42" width="0.1524" layer="91"/>
<label x="15.24" y="58.42" size="1.778" layer="95" font="vector"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="170.18" y="157.48" size="2.54" layer="94">1. Vaikeolek</text>
<text x="175.26" y="152.4" size="2.54" layer="94">a) IO1 madal, IO2 madal (E lahti ühendatud)</text>
<text x="175.26" y="147.32" size="2.54" layer="94">b) DAC1 ja DAC2 võrdsed</text>
<text x="170.18" y="137.16" size="2.54" layer="94">2. Mõõtmise alustamine</text>
<text x="175.26" y="132.08" size="2.54" layer="94">a) Madala pingega teha kindlaks BC siire</text>
<text x="175.26" y="127" size="2.54" layer="94">b) Vastavalt BC siirdele lülitada õiges</text>
<text x="177.8" y="121.92" size="2.54" layer="94">järjekorras DAC1 js DAC2 kas kõrgeks</text>
<text x="177.8" y="116.84" size="2.54" layer="94">või madalaks</text>
<text x="175.26" y="111.76" size="2.54" layer="94">c) Vastavalt BC siirdele lülitada IO1 ja IO2</text>
<text x="177.8" y="106.68" size="2.54" layer="94">nii, et E oleks kas kõrge või madal</text>
<text x="170.18" y="96.52" size="2.54" layer="94">3. Mõõtmine</text>
<text x="170.18" y="86.36" size="2.54" layer="94">4. Mõõtmise lõpetamine</text>
<text x="175.26" y="81.28" size="2.54" layer="94">a) Lülitada IO1 ja IO2 madalaks</text>
<text x="175.26" y="76.2" size="2.54" layer="94">b) Vastavalt BC siirdele lülitada õiges</text>
<text x="177.8" y="71.12" size="2.54" layer="94">järjekorras DAC1 ja DAC2 võrdseks</text>
<wire x1="7.62" y1="170.18" x2="157.48" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="157.48" y1="170.18" x2="157.48" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="157.48" y1="7.62" x2="7.62" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="162.56" y1="170.18" x2="251.46" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="170.18" x2="251.46" y2="66.04" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="66.04" x2="162.56" y2="66.04" width="0.1524" layer="94" style="longdash"/>
<wire x1="162.56" y1="66.04" x2="162.56" y2="170.18" width="0.1524" layer="94" style="longdash"/>
<text x="91.44" y="45.72" size="1.778" layer="96" font="vector" rot="MR0">B</text>
<text x="91.44" y="48.26" size="1.778" layer="96" font="vector" rot="MR0">C</text>
<circle x="81.28" y="81.28" radius="9.1581" width="0.1524" layer="98"/>
<text x="10.16" y="167.64" size="5.08" layer="94" font="vector" rot="MR180">MEASUREMENT</text>
<wire x1="251.46" y1="60.96" x2="162.56" y2="60.96" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="27.94" x2="162.56" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="162.56" y1="60.96" x2="162.56" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="251.46" y1="60.96" x2="251.46" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<text x="165.1" y="58.42" size="5.08" layer="94" font="vector" rot="MR180">FIDUCIALS/HOLES/ETC</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="R28" gate="R" x="81.28" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="80.772" y="113.792" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="80.772" y="119.888" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R31" gate="R" x="60.96" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="57.912" y="81.788" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="64.008" y="81.788" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="T7" gate="G$1" x="111.76" y="60.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="111.252" y="63.5" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="111.252" y="58.42" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="T5" gate="T" x="111.76" y="76.2" smashed="yes" rot="MR0">
<attribute name="NAME" x="111.252" y="78.74" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="111.252" y="73.66" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="111.76" y="40.64"/>
<instance part="GND2" gate="1" x="35.56" y="104.14"/>
<instance part="GND3" gate="1" x="35.56" y="53.34"/>
<instance part="T6" gate="T" x="124.46" y="68.58" smashed="yes" rot="MR0">
<attribute name="VALUE" x="121.412" y="66.04" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="121.412" y="71.12" size="1.778" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="R30" gate="R" x="121.92" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="121.412" y="83.312" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="121.412" y="89.408" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R33" gate="R" x="134.62" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="134.112" y="47.752" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="134.112" y="53.848" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND4" gate="1" x="134.62" y="40.64"/>
<instance part="R32" gate="R" x="134.62" y="68.58" smashed="yes">
<attribute name="NAME" x="131.572" y="69.088" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="137.668" y="69.088" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R34" gate="R" x="142.24" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="141.732" y="47.752" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="141.732" y="53.848" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="142.24" y="40.64"/>
<instance part="U9" gate="G$1" x="33.02" y="81.28" smashed="yes">
<attribute name="NAME" x="36.068" y="84.328" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="37.084" y="78.994" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="U8" gate="G$1" x="33.02" y="132.08" smashed="yes">
<attribute name="NAME" x="36.068" y="135.128" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="37.084" y="129.794" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="R29" gate="R" x="33.02" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="32.512" y="111.252" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="32.512" y="117.348" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND20" gate="1" x="33.02" y="104.14"/>
<instance part="P+1" gate="1" x="35.56" y="152.4"/>
<instance part="P+2" gate="1" x="35.56" y="101.6"/>
<instance part="P+3" gate="1" x="111.76" y="104.14"/>
<instance part="P+4" gate="1" x="121.92" y="104.14"/>
<instance part="WS4" gate="G$1" x="76.2" y="81.28" smashed="yes">
<attribute name="VALUE" x="77.724" y="80.264" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="WS3" gate="G$1" x="81.28" y="86.36" smashed="yes" rot="R270">
<attribute name="VALUE" x="82.296" y="84.836" size="1.778" layer="96" font="vector" rot="MR270"/>
</instance>
<instance part="WS5" gate="G$1" x="81.28" y="76.2" smashed="yes" rot="R90">
<attribute name="VALUE" x="82.296" y="77.724" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="J2" gate="JP" x="81.28" y="45.72" smashed="yes">
<attribute name="NAME" x="78.486" y="54.102" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C28" gate="C" x="43.18" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="42.672" y="59.69" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="42.672" y="62.23" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND41" gate="1" x="43.18" y="53.34"/>
<instance part="P+6" gate="1" x="43.18" y="76.2"/>
<instance part="C27" gate="C" x="43.18" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="42.672" y="110.49" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="42.672" y="113.03" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND42" gate="1" x="43.18" y="104.14"/>
<instance part="P+7" gate="1" x="43.18" y="127"/>
<instance part="BATTERY" gate="G$1" x="241.3" y="35.56"/>
<instance part="BANANA1" gate="G$1" x="215.9" y="35.56"/>
<instance part="BANANA2" gate="G$1" x="223.52" y="35.56"/>
<instance part="BANANA3" gate="G$1" x="231.14" y="35.56"/>
<instance part="FIDUCIAL1" gate="G$1" x="170.18" y="35.56"/>
<instance part="FIDUCIAL2" gate="G$1" x="177.8" y="35.56"/>
<instance part="FIDUCIAL3" gate="G$1" x="185.42" y="35.56"/>
<instance part="FIDUCIAL4" gate="G$1" x="193.04" y="35.56"/>
<instance part="JP1" gate="JP" x="170.18" y="45.72" smashed="yes">
<attribute name="NAME" x="166.624" y="49.276" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="JP2" gate="JP" x="193.04" y="45.72" smashed="yes">
<attribute name="NAME" x="189.738" y="49.276" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND64" gate="1" x="177.8" y="45.72" rot="R90"/>
<instance part="GND65" gate="1" x="200.66" y="45.72" rot="R90"/>
<instance part="U$7" gate="G$1" x="213.36" y="43.18"/>
<instance part="U$9" gate="G$1" x="220.98" y="43.18"/>
<instance part="U$10" gate="G$1" x="228.6" y="43.18"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="T7" gate="G$1" pin="S"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="111.76" y1="55.88" x2="111.76" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="35.56" y1="106.68" x2="35.56" y2="127" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="V-"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="35.56" y1="55.88" x2="35.56" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="V-"/>
</segment>
<segment>
<pinref part="R33" gate="R" pin="P$1"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="134.62" y1="45.72" x2="134.62" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R34" gate="R" pin="P$1"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="142.24" y1="45.72" x2="142.24" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R29" gate="R" pin="P$1"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="33.02" y1="109.22" x2="33.02" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="C" pin="1"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="43.18" y1="58.42" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C27" gate="C" pin="1"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="43.18" y1="109.22" x2="43.18" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="JP" pin="P$1"/>
<pinref part="GND64" gate="1" pin="GND"/>
<wire x1="175.26" y1="45.72" x2="172.72" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="JP" pin="P$1"/>
<pinref part="GND65" gate="1" pin="GND"/>
<wire x1="198.12" y1="45.72" x2="195.58" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC1-2" class="0">
<segment>
<pinref part="R28" gate="R" pin="P$1"/>
<wire x1="81.28" y1="111.76" x2="81.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="81.28" y1="109.22" x2="81.28" y2="88.9" width="0.1524" layer="91"/>
<wire x1="81.28" y1="109.22" x2="96.52" y2="109.22" width="0.1524" layer="91"/>
<junction x="81.28" y="109.22"/>
<label x="96.52" y="109.22" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="WS3" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="J2" gate="JP" pin="P$2"/>
<wire x1="83.82" y1="48.26" x2="91.44" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC2-2" class="0">
<segment>
<pinref part="R31" gate="R" pin="P$1"/>
<wire x1="66.04" y1="81.28" x2="68.58" y2="81.28" width="0.1524" layer="91"/>
<wire x1="68.58" y1="81.28" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
<wire x1="68.58" y1="81.28" x2="68.58" y2="66.04" width="0.1524" layer="91"/>
<label x="68.58" y="66.04" size="1.778" layer="95" font="vector" rot="R90"/>
<junction x="68.58" y="81.28"/>
<pinref part="WS4" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="J2" gate="JP" pin="P$3"/>
<wire x1="83.82" y1="45.72" x2="91.44" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC1-1" class="0">
<segment>
<pinref part="R28" gate="R" pin="P$2"/>
<wire x1="81.28" y1="121.92" x2="81.28" y2="132.08" width="0.1524" layer="91"/>
<wire x1="81.28" y1="132.08" x2="96.52" y2="132.08" width="0.1524" layer="91"/>
<label x="96.52" y="132.08" size="1.778" layer="95" font="vector" rot="MR0"/>
<wire x1="45.72" y1="132.08" x2="48.26" y2="132.08" width="0.1524" layer="91"/>
<junction x="81.28" y="132.08"/>
<wire x1="48.26" y1="132.08" x2="81.28" y2="132.08" width="0.1524" layer="91"/>
<wire x1="25.4" y1="134.62" x2="22.86" y2="134.62" width="0.1524" layer="91"/>
<wire x1="22.86" y1="134.62" x2="22.86" y2="139.7" width="0.1524" layer="91"/>
<wire x1="22.86" y1="139.7" x2="48.26" y2="139.7" width="0.1524" layer="91"/>
<wire x1="48.26" y1="139.7" x2="48.26" y2="132.08" width="0.1524" layer="91"/>
<junction x="48.26" y="132.08"/>
<pinref part="U8" gate="G$1" pin="-"/>
<pinref part="U8" gate="G$1" pin="OUT"/>
</segment>
</net>
<net name="ADC2-1" class="0">
<segment>
<pinref part="R31" gate="R" pin="P$2"/>
<wire x1="55.88" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<wire x1="53.34" y1="81.28" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<label x="53.34" y="66.04" size="1.778" layer="95" font="vector" rot="R90"/>
<wire x1="45.72" y1="81.28" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<junction x="53.34" y="81.28"/>
<wire x1="48.26" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<wire x1="25.4" y1="83.82" x2="22.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="22.86" y1="83.82" x2="22.86" y2="88.9" width="0.1524" layer="91"/>
<wire x1="22.86" y1="88.9" x2="48.26" y2="88.9" width="0.1524" layer="91"/>
<wire x1="48.26" y1="88.9" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<junction x="48.26" y="81.28"/>
<pinref part="U9" gate="G$1" pin="-"/>
<pinref part="U9" gate="G$1" pin="OUT"/>
</segment>
</net>
<net name="DAC1" class="0">
<segment>
<wire x1="25.4" y1="129.54" x2="12.7" y2="129.54" width="0.1524" layer="91"/>
<label x="12.7" y="129.54" size="1.778" layer="95" font="vector"/>
<pinref part="U8" gate="G$1" pin="+"/>
</segment>
</net>
<net name="DAC2" class="0">
<segment>
<wire x1="25.4" y1="78.74" x2="12.7" y2="78.74" width="0.1524" layer="91"/>
<label x="12.7" y="78.74" size="1.778" layer="95" font="vector"/>
<pinref part="U9" gate="G$1" pin="+"/>
</segment>
</net>
<net name="IO2" class="0">
<segment>
<pinref part="T7" gate="G$1" pin="G"/>
<wire x1="116.84" y1="60.96" x2="121.92" y2="60.96" width="0.1524" layer="91"/>
<pinref part="T6" gate="T" pin="E"/>
<wire x1="121.92" y1="60.96" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<wire x1="134.62" y1="60.96" x2="152.4" y2="60.96" width="0.1524" layer="91"/>
<wire x1="121.92" y1="63.5" x2="121.92" y2="60.96" width="0.1524" layer="91"/>
<junction x="121.92" y="60.96"/>
<pinref part="R33" gate="R" pin="P$2"/>
<wire x1="134.62" y1="55.88" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<junction x="134.62" y="60.96"/>
<label x="152.4" y="60.96" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="T5" gate="T" pin="G"/>
<pinref part="T6" gate="T" pin="C"/>
<wire x1="116.84" y1="76.2" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<wire x1="121.92" y1="76.2" x2="121.92" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R30" gate="R" pin="P$1"/>
<wire x1="121.92" y1="81.28" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<junction x="121.92" y="76.2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="T6" gate="T" pin="B"/>
<pinref part="R32" gate="R" pin="P$1"/>
<wire x1="127" y1="68.58" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO1" class="0">
<segment>
<pinref part="R32" gate="R" pin="P$2"/>
<pinref part="R34" gate="R" pin="P$2"/>
<wire x1="139.7" y1="68.58" x2="142.24" y2="68.58" width="0.1524" layer="91"/>
<wire x1="142.24" y1="68.58" x2="142.24" y2="55.88" width="0.1524" layer="91"/>
<wire x1="142.24" y1="68.58" x2="152.4" y2="68.58" width="0.1524" layer="91"/>
<junction x="142.24" y="68.58"/>
<label x="152.4" y="68.58" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="DAC_EN" class="0">
<segment>
<pinref part="R29" gate="R" pin="P$2"/>
<pinref part="U8" gate="G$1" pin="/SHDN"/>
<wire x1="33.02" y1="119.38" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<wire x1="33.02" y1="124.46" x2="33.02" y2="127" width="0.1524" layer="91"/>
<wire x1="33.02" y1="124.46" x2="12.7" y2="124.46" width="0.1524" layer="91"/>
<junction x="33.02" y="124.46"/>
<label x="12.7" y="124.46" size="1.778" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="/SHDN"/>
<wire x1="33.02" y1="76.2" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
<wire x1="33.02" y1="73.66" x2="12.7" y2="73.66" width="0.1524" layer="91"/>
<label x="12.7" y="73.66" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="35.56" y1="144.78" x2="35.56" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="V+"/>
<pinref part="P+1" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="35.56" y1="93.98" x2="35.56" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="V+"/>
<pinref part="P+2" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="T5" gate="T" pin="S"/>
<wire x1="111.76" y1="96.52" x2="111.76" y2="81.28" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="R30" gate="R" pin="P$2"/>
<wire x1="121.92" y1="96.52" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<pinref part="P+4" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="P+6" gate="1" pin="+5V"/>
<pinref part="C28" gate="C" pin="2"/>
<wire x1="43.18" y1="68.58" x2="43.18" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+7" gate="1" pin="+5V"/>
<pinref part="C27" gate="C" pin="2"/>
<wire x1="43.18" y1="119.38" x2="43.18" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="E" class="0">
<segment>
<pinref part="J2" gate="JP" pin="P$1"/>
<wire x1="83.82" y1="50.8" x2="91.44" y2="50.8" width="0.1524" layer="91"/>
<label x="91.44" y="50.8" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J2" gate="JP" pin="P$4"/>
<wire x1="83.82" y1="43.18" x2="91.44" y2="43.18" width="0.1524" layer="91"/>
<label x="91.44" y="43.18" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="81.28" y1="73.66" x2="81.28" y2="68.58" width="0.1524" layer="91"/>
<wire x1="81.28" y1="68.58" x2="111.76" y2="68.58" width="0.1524" layer="91"/>
<pinref part="T5" gate="T" pin="D"/>
<wire x1="111.76" y1="68.58" x2="111.76" y2="71.12" width="0.1524" layer="91"/>
<pinref part="T7" gate="G$1" pin="D"/>
<wire x1="111.76" y1="68.58" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<junction x="111.76" y="68.58"/>
<pinref part="WS5" gate="G$1" pin="P$1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
