/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "ble.h"
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/l2cap.h>
#include <errno.h>

void ble_uuid2bin(void *dest, char *uuid){
	unsigned int d[16];
	sscanf(uuid, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X",
		&d[0], &d[1], &d[2], &d[3], &d[4], &d[5], &d[6], &d[7],
		&d[8], &d[9], &d[10], &d[11], &d[12], &d[13], &d[14], &d[15]
	);
	((uint8_t *)dest)[0] = d[15];
	((uint8_t *)dest)[1] = d[14];
	((uint8_t *)dest)[2] = d[13];
	((uint8_t *)dest)[3] = d[12];
	((uint8_t *)dest)[4] = d[11];
	((uint8_t *)dest)[5] = d[10];
	((uint8_t *)dest)[6] = d[9];
	((uint8_t *)dest)[7] = d[8];
	((uint8_t *)dest)[8] = d[7];
	((uint8_t *)dest)[9] = d[6];
	((uint8_t *)dest)[10] = d[5];
	((uint8_t *)dest)[11] = d[4];
	((uint8_t *)dest)[12] = d[3];
	((uint8_t *)dest)[13] = d[2];
	((uint8_t *)dest)[14] = d[1];
	((uint8_t *)dest)[15] = d[0];
}

int ble_scan_open(char *hci_dev){
	int dev_id, fd, i;
	le_set_scan_parameters_cp scan_params_cp;
	le_set_event_mask_cp event_mask_cp;
	le_set_scan_enable_cp scan_enable_cp;
	struct hci_request request;
	struct hci_filter filter;

	dev_id = hci_devid(hci_dev);
	if(dev_id < 0)return(-1);

	fd = hci_open_dev(dev_id);
	if(fd < 0)return(-1);

	memset(&scan_params_cp, 0, sizeof(le_set_scan_parameters_cp));
	scan_params_cp.type = 0x01;
	scan_params_cp.interval = htobs(0x0010);
	scan_params_cp.window = htobs(0x0010);
	scan_params_cp.own_bdaddr_type = 0x00;
	scan_params_cp.filter = 0x00;
	memset(&request, 0, sizeof(struct hci_request));
	request.ogf = OGF_LE_CTL;
	request.ocf = OCF_LE_SET_SCAN_PARAMETERS;
	request.cparam = &scan_params_cp;
	request.clen = LE_SET_SCAN_PARAMETERS_CP_SIZE;
	request.rparam = &i;
	request.rlen = 1;
	if(hci_send_req(fd, &request, 1000) < 0)goto error;

	memset(&event_mask_cp, 0, sizeof(le_set_event_mask_cp));
	for(i = 0; i < 8; i++)event_mask_cp.mask[i] = 0xFF;
	memset(&request, 0, sizeof(struct hci_request));
	request.ogf = OGF_LE_CTL;
	request.ocf = OCF_LE_SET_EVENT_MASK;
	request.cparam = &event_mask_cp;
	request.clen = LE_SET_EVENT_MASK_CP_SIZE;
	request.rparam = &i;
	request.rlen = 1;
	if(hci_send_req(fd, &request, 1000) < 0)goto error;

	memset(&scan_enable_cp, 0, sizeof(le_set_scan_enable_cp));
	scan_enable_cp.enable = 0x01;
	scan_enable_cp.filter_dup = 0x00;
	memset(&request, 0, sizeof(struct hci_request));
	request.ogf = OGF_LE_CTL;
	request.ocf = OCF_LE_SET_SCAN_ENABLE;
	request.cparam = &scan_enable_cp;
	request.clen = LE_SET_SCAN_ENABLE_CP_SIZE;
	request.rparam = &i;
	request.rlen = 1;
	if(hci_send_req(fd, &request, 1000) < 0)goto error;

	
	hci_filter_clear(&filter);
	hci_filter_set_ptype(HCI_EVENT_PKT, &filter);
	hci_filter_set_event(EVT_LE_META_EVENT, &filter);
	if(setsockopt(fd, SOL_HCI, HCI_FILTER, &filter, sizeof(struct hci_filter)) < 0)goto error;

	return(fd);
error:
	hci_close_dev(fd);
	return(-1);
}

uint8_t *ble_scan_ba(void *data, ssize_t len){
	evt_le_meta_event *meta_event;
	le_advertising_info *info;
	if(len < HCI_EVENT_HDR_SIZE)return(NULL);
	meta_event = (evt_le_meta_event *)(data + HCI_EVENT_HDR_SIZE + 1);
	if(meta_event->subevent != EVT_LE_ADVERTISING_REPORT)return(NULL);
	info = (le_advertising_info *)((void *)meta_event->data + 1);
	return((uint8_t *)&(info->bdaddr));
}

int ble_scan_ba_match(bdaddr_t *bdaddr, void *data, ssize_t len){
	evt_le_meta_event *meta_event;
	le_advertising_info *info;
	if(len < HCI_EVENT_HDR_SIZE)return(-1);
	meta_event = (evt_le_meta_event *)(data + HCI_EVENT_HDR_SIZE + 1);
	if(meta_event->subevent != EVT_LE_ADVERTISING_REPORT)return(-1);
	info = (le_advertising_info *)((void *)meta_event->data + 1);
	if(!bacmp(bdaddr, &(info->bdaddr)))return(1);
	return(0);
}

void ble_scan_close(int fd){
	int i;
	le_set_scan_enable_cp scan_enable_cp;
	struct hci_request request;

	memset(&scan_enable_cp, 0, sizeof(le_set_scan_enable_cp));
	scan_enable_cp.enable = 0x00;
	memset(&request, 0, sizeof(struct hci_request));
	request.ogf = OGF_LE_CTL;
	request.ocf = OCF_LE_SET_SCAN_ENABLE;
	request.cparam = &scan_enable_cp;
	request.clen = LE_SET_SCAN_ENABLE_CP_SIZE;
	request.rparam = &i;
	request.rlen = 1;
	hci_send_req(fd, &request, 1000);
	hci_close_dev(fd);
}

int ble_gatt_connect(char *hci_dev, char *dst_addr, int dst_addr_type){
	int dev_id, fd, l;
	char buffer[512];
	struct sockaddr_l2 addr;
	bdaddr_t dst_ba; // for search

	dev_id = hci_devid(hci_dev);
	if(dev_id < 0)return(-1);
	memset(&addr, 0, sizeof(addr));
	if(hci_devba(dev_id, &addr.l2_bdaddr) < 0)return(-1);

	// search
	fd = ble_scan_open(hci_dev);
	if(fd < 0)return(-1);
	str2ba(dst_addr, &dst_ba);
	while(1){
		l = read(fd, buffer, 512);
		if(l < 0){
			ble_scan_close(fd);
			return(-1);
		}
		if(ble_scan_ba_match(&dst_ba, buffer, l) > 0)break;
	}
	ble_scan_close(fd);
	// end of search

	fd = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
	if(fd < 0)return(-1);

	addr.l2_family = AF_BLUETOOTH;
	addr.l2_bdaddr_type = BLE_ADDR_TYPE_PUBLIC;
	addr.l2_cid = htobs(4);

	if(bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)goto error;

	str2ba(dst_addr, &addr.l2_bdaddr);
	addr.l2_bdaddr_type = dst_addr_type;
	addr.l2_cid = htobs(4);

	if(connect(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)goto error;
	return(fd);
error:
	ble_gatt_close(fd);
	return(-1);
}

void ble_gatt_close(int fd){
	shutdown(fd, SHUT_RDWR);
	close(fd);
}

int ble_gatt_cmd_read_by_handle(int fd, uint16_t handle){
	uint8_t buffer[4];
	buffer[0] = BLE_GATT_OP_READ_REQ;
	buffer[1] = (handle >> 0) & 0xFF;
	buffer[2] = (handle >> 8) & 0xFF;
	return(write(fd, buffer, 3));
}

int ble_gatt_cmd_read_by_uuid_cstr(int fd, uint16_t start_handle, uint16_t end_handle, char *uuid){ // 00001524-1212-efde-1523-785feabcd123
	uint8_t buffer[22];
	if(strlen(uuid) < 36)return(-EINVAL);
	buffer[0] = BLE_GATT_OP_READ_BY_TYPE_REQ;
	buffer[1] = (start_handle >> 0) & 0xFF;
	buffer[2] = (start_handle >> 8) & 0xFF;
	buffer[3] = (end_handle >> 0) & 0xFF;
	buffer[4] = (end_handle >> 8) & 0xFF;
	ble_uuid2bin(&buffer[5], uuid);
	return(write(fd, buffer, 21));
}

int ble_gatt_cmd_notify_set(int fd, uint16_t handle){
	return(ble_gatt_cmd_write(fd, (handle + 1), "\x01\x00", 2));
}

int ble_gatt_cmd_notify_clear(int fd, uint16_t handle){
	return(ble_gatt_cmd_write(fd, (handle + 1), "\x00\x00", 2));
}

int ble_gatt_cmd_notify_set_req(int fd, uint16_t handle){
	return(ble_gatt_cmd_write_req(fd, (handle + 1), "\x01\x00", 2));
}

int ble_gatt_cmd_notify_clear_req(int fd, uint16_t handle){
	return(ble_gatt_cmd_write_req(fd, (handle + 1), "\x00\x00", 2));
}

int ble_gatt_cmd_indicate_set_req(int fd, uint16_t handle){
	return(ble_gatt_cmd_write_req(fd, (handle + 1), "\x02\x00", 2));
}

int ble_gatt_cmd_write(int fd, uint16_t handle, void *buf, uint8_t count){
	uint8_t buffer[260];
	int i;
	buffer[0] = BLE_GATT_OP_WRITE_CMD;
	buffer[1] = (handle >> 0) & 0xFF;
	buffer[2] = (handle >> 8) & 0xFF;
	for(i = 0; i < count; i++){
		buffer[3 + i] = ((uint8_t *)buf)[i];
	}
	return(write(fd, buffer, 3 + count));
}

int ble_gatt_cmd_write_req(int fd, uint16_t handle, void *buf, uint8_t count){
	uint8_t buffer[260];
	int i;
	buffer[0] = BLE_GATT_OP_WRITE_REQ;
	buffer[1] = (handle >> 0) & 0xFF;
	buffer[2] = (handle >> 8) & 0xFF;
	for(i = 0; i < count; i++){
		buffer[3 + i] = ((uint8_t *)buf)[i];
	}
	return(write(fd, buffer, 3 + count));
}

