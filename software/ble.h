/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _BLE_H_
#define _BLE_H_

#include <stdint.h>
#include <bluetooth/bluetooth.h>

#define BLE_GATT_OP_ERROR                    0x01
#define BLE_GATT_OP_MTU_REQ                  0x02
#define BLE_GATT_OP_MTU_RESP                 0x03
#define BLE_GATT_OP_FIND_INFO_REQ            0x04
#define BLE_GATT_OP_FIND_INFO_RESP           0x05
#define BLE_GATT_OP_FIND_BY_TYPE_REQ         0x06
#define BLE_GATT_OP_FIND_BY_TYPE_RESP        0x07
#define BLE_GATT_OP_READ_BY_TYPE_REQ         0x08
#define BLE_GATT_OP_READ_BY_TYPE_RESP        0x09
#define BLE_GATT_OP_READ_REQ                 0x0A
#define BLE_GATT_OP_READ_RESP                0x0B
#define BLE_GATT_OP_READ_BLOB_REQ            0x0C
#define BLE_GATT_OP_READ_BLOB_RESP           0x0D
#define BLE_GATT_OP_READ_MULTI_REQ           0x0E
#define BLE_GATT_OP_READ_MULTI_RESP          0x0F
#define BLE_GATT_OP_READ_BY_GROUP_REQ        0x10
#define BLE_GATT_OP_READ_BY_GROUP_RESP       0x11
#define BLE_GATT_OP_WRITE_REQ                0x12
#define BLE_GATT_OP_WRITE_RESP               0x13
#define BLE_GATT_OP_WRITE_CMD                0x52
#define BLE_GATT_OP_PREP_WRITE_REQ           0x16
#define BLE_GATT_OP_PREP_WRITE_RESP          0x17
#define BLE_GATT_OP_EXEC_WRITE_REQ           0x18
#define BLE_GATT_OP_EXEC_WRITE_RESP          0x19
#define BLE_GATT_OP_HANDLE_NOTIFY            0x1B
#define BLE_GATT_OP_HANDLE_IND               0x1D
#define BLE_GATT_OP_HANDLE_CNF               0x1E
#define BLE_GATT_OP_SIGNED_WRITE_CMD         0xD2

#ifndef BDADDR_LE_PUBLIC
#define BDADDR_LE_PUBLIC                     0x01
#endif

#ifndef BDADDR_LE_RANDOM
#define BDADDR_LE_RANDOM                     0x02
#endif

#define BLE_ADDR_TYPE_PUBLIC                 (BDADDR_LE_PUBLIC)
#define BLE_ADDR_TYPE_RANDOM                 (BDADDR_LE_RANDOM)

void ble_uuid2bin(void *dest, char *uuid);

int ble_scan_open(char *hci_dev);
uint8_t *ble_scan_ba(void *data, ssize_t len);
int ble_scan_ba_match(bdaddr_t *bdaddr, void *data, ssize_t len);
void ble_scan_close(int fd);

int ble_gatt_connect(char *hci_dev, char *dst_addr, int dst_addr_type);
void ble_gatt_close(int fd);
int ble_gatt_cmd_notify_set(int fd, uint16_t handle);
int ble_gatt_cmd_notify_clear(int fd, uint16_t handle);
int ble_gatt_cmd_notify_set_req(int fd, uint16_t handle);
int ble_gatt_cmd_notify_clear_req(int fd, uint16_t handle);

int ble_gatt_cmd_indicate_set_req(int fd, uint16_t handle);

int ble_gatt_cmd_read_by_handle(int fd, uint16_t handle);
int ble_gatt_cmd_read_by_uuid_cstr(int fd, uint16_t start_handle, uint16_t end_handle, char *uuid);
int ble_gatt_cmd_write(int fd, uint16_t handle, void *buf, uint8_t count);
int ble_gatt_cmd_write_req(int fd, uint16_t handle, void *buf, uint8_t count);

#endif

