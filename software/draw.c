/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "draw.h"
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <gtk/gtk.h>
#include "gui.h"

static pthread_mutex_t draw_mutex;
static cairo_surface_t *draw_cs;
static cairo_t *draw_cr;

static cairo_status_t draw_write(void *closure, const unsigned char *data, unsigned int length);

void draw_init()
{
	pthread_mutex_init(&draw_mutex, NULL);
	draw_cs = cairo_image_surface_create(CAIRO_FORMAT_RGB24, 640, 480);
	draw_cr = cairo_create(draw_cs);

	draw_puts(80, 100, 24, "Arvutite ja süsteemide projekt, IAS1420", 0, 0, 255);
	draw_puts(150, 140, 24, "Tarkvara projekt, IAS1410", 0, 0, 255);
	draw_puts(270, 180, 24, "TalTech", 0, 0, 255);
	draw_puts(240, 260, 24, "Veiko Rütter", 0, 0, 255);
	draw_puts(260, 300, 24, "2021/2022", 0, 0, 255);
}

void draw_color(int red, int green, int blue)
{
	pthread_mutex_lock(&draw_mutex);
	cairo_set_source_rgb(draw_cr, (double)red / 255, (double)green / 255, (double)blue / 255);
	pthread_mutex_unlock(&draw_mutex);
}

void draw_cls()
{
	draw_color(0, 0, 0);
	pthread_mutex_lock(&draw_mutex);
	cairo_set_line_width(draw_cr, 1);
	cairo_rectangle(draw_cr, 0, 0, 640, 480);
	cairo_fill(draw_cr);
	cairo_stroke(draw_cr);
	pthread_mutex_unlock(&draw_mutex);
	gui_trigger_draw();
}

void draw_line(int x1, int y1, int x2, int y2, int red, int green, int blue)
{
	draw_color(red, green, blue);
	pthread_mutex_lock(&draw_mutex);
	cairo_set_line_width(draw_cr, 1);
	cairo_move_to(draw_cr, x1, y1);
	cairo_line_to(draw_cr, x2, y2);
	cairo_stroke(draw_cr);
	pthread_mutex_unlock(&draw_mutex);
	gui_trigger_draw();
}

void draw_pset(int x, int y, int red, int green, int blue)
{
	draw_color(red, green, blue);
	pthread_mutex_lock(&draw_mutex);
	cairo_rectangle(draw_cr, x, y, 1, 1);
	cairo_fill(draw_cr);
	cairo_stroke(draw_cr);
	pthread_mutex_unlock(&draw_mutex);
	gui_trigger_draw();
}

void draw_puts(int x, int y, int size, char *s, int red, int green, int blue)
{
	draw_color(red, green, blue);
	pthread_mutex_lock(&draw_mutex);
	cairo_select_font_face(draw_cr, "Georgia", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size(draw_cr, size);
	cairo_move_to(draw_cr, x, y);
	cairo_show_text(draw_cr, s);
	cairo_stroke(draw_cr);
	pthread_mutex_unlock(&draw_mutex);
	gui_trigger_draw();
}

void draw_scale(char *yn, int ys, int ye, char *xn, int xs, int xe)
{
	char buffer[16];

	draw_cls();

	draw_line(15, 20, 15, 465, 255, 0, 0);
	draw_line(15, 20, 11, 30, 255, 0, 0);
	draw_line(15, 20, 19, 30, 255, 0, 0);

	snprintf(buffer, 15, "%d", ys);
	draw_puts(5, 465, 10, buffer, 255, 0, 0);

	snprintf(buffer, 15, "%d", ye);
	draw_puts(20, 25, 10, buffer, 255, 0, 0);

	draw_puts(10, 13, 10, yn, 0, 0, 255);

	draw_line(630, 465, 15, 465, 255, 0, 0);
	draw_line(630, 465, 620, 461, 255, 0, 0);
	draw_line(630, 465, 620, 469, 255, 0, 0);

	snprintf(buffer, 15, "%d", xs);
	draw_puts(10, 477, 10, buffer, 255, 0, 0);

	snprintf(buffer, 15, "%d", xe);
	draw_puts(610, 477, 10, buffer, 255, 0, 0);

	draw_puts(590, 455, 10, xn, 0, 0, 255);
}

void draw_refresh(void *cairo)
{
	cairo_t *cr = (cairo_t *)cairo;

	pthread_mutex_lock(&draw_mutex);
	cairo_surface_flush(draw_cs);
	cairo_surface_mark_dirty(draw_cs);
	cairo_set_source_surface(cr, draw_cs, 0, 0);
	cairo_paint(cr);
	pthread_mutex_unlock(&draw_mutex);
}

int draw_save(char *path)
{
	int f;

	printf("DRAW PNG: %s\n", path);
	f = open(path, O_WRONLY | O_CREAT | O_TRUNC, 0600);
	if (f < 0)
		return -1;

	pthread_mutex_lock(&draw_mutex);
	cairo_surface_write_to_png_stream(draw_cs, draw_write, &f);
	pthread_mutex_unlock(&draw_mutex);

	close(f);

	return 0;
}

static cairo_status_t draw_write(void *closure, const unsigned char *data, unsigned int length)
{
	int f = *(int *)closure;

	write(f, data, length);
	printf("WRITE\n");

	return CAIRO_STATUS_SUCCESS;
}

