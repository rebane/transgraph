/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _TESTER_H_
#define _TESTER_H_

#include <stdint.h>

#define TESTER_EVENT_CONNECTED    0
#define TESTER_EVENT_DISCONNECTED 1

void tester_init(int ind_not, void (*bluetooth_scan_callback)(uint8_t *mac), void (*bluetooth_callback)(int event, int index, uint8_t transaction_id, uint32_t s, uint16_t a, uint16_t b, uint16_t c, uint16_t m), void (*tester_callback)(int event));
void tester_scan();
void tester_noscan();
void tester_connect(char *btaddr);
void tester_disconnect();
void tester_poll();

void tester_plot_input(uint16_t cu_mv, uint16_t bu_mv_min, uint16_t bu_mv_max, uint16_t bi_ua_scale);
void tester_plot_output(uint16_t bi_ua, uint16_t cu_mv_min, uint16_t cu_mv_max, uint16_t ci_ma_scale);
void tester_plot_beta(uint16_t cu_mv, uint16_t bi_ua_min, uint16_t bi_ua_max, uint16_t ci_ma_scale);

#endif

