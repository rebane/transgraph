/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "tester.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <glib.h>
#include "ble.h"
#include "bluetooth.h"

#define TESTER_MAC_LIST_MAX 16

enum {
	TESTER_STATE_IDLE = 0,
	TESTER_STATE_SCAN_START,
	TESTER_STATE_SCAN,
	TESTER_STATE_CONNECT,
	TESTER_STATE_CONNECTED,
	TESTER_STATE_DISCONNECT
};

static int tester_ind_not;
static volatile int tester_state;
static volatile int tester_disconnect_flag;
static volatile char *tester_btaddr;
static int tester_sock;
static pthread_t tester_thread;
static pthread_mutex_t tester_mutex;
static GQueue *tester_queue;

static int tester_mac_list_len;
static uint8_t tester_mac_list[TESTER_MAC_LIST_MAX][6];

static void (*tester_bluetooth_scan_callback)(uint8_t *mac);
static void (*tester_bluetooth_callback)(int event, int index, uint8_t transaction_id, uint32_t s, uint16_t a, uint16_t b, uint16_t c, uint16_t m);
static void (*tester_tester_callback)(int event);

static void *tester_loop(void *p);
static void tester_tester_callback_set(int event);
static void tester_parse(uint8_t *buffer, int l);
static int tester_sock_poll(int sock, int timeout);
static void tester_swap(uint16_t *a, uint16_t *b);

void tester_init(int ind_not, void (*bluetooth_scan_callback)(uint8_t *mac), void (*bluetooth_callback)(int event, int index, uint8_t transaction_id, uint32_t s, uint16_t a, uint16_t b, uint16_t c, uint16_t m), void (*tester_callback)(int event))
{
	tester_ind_not = ind_not;
	tester_bluetooth_scan_callback = bluetooth_scan_callback;
	tester_bluetooth_callback = bluetooth_callback;
	tester_tester_callback = tester_callback;
	tester_state = TESTER_STATE_IDLE;
	tester_disconnect_flag = 0;
	tester_btaddr = NULL;
	pthread_mutex_init(&tester_mutex, NULL);
	tester_queue = g_queue_new();

	tester_mac_list_len = 0;

	pthread_create(&tester_thread, NULL, tester_loop, NULL);
}

void tester_scan()
{
	if (tester_state != TESTER_STATE_IDLE)
		return;

	tester_state = TESTER_STATE_SCAN_START;
}

void tester_noscan()
{
	if ((tester_state != TESTER_STATE_SCAN_START) && (tester_state != TESTER_STATE_SCAN))
		return;

	tester_state = TESTER_STATE_IDLE;
}

void tester_connect(char *btaddr)
{
	if ((tester_state != TESTER_STATE_IDLE) && (tester_state != TESTER_STATE_SCAN_START) && (tester_state != TESTER_STATE_SCAN))
		return;

	free((char *)tester_btaddr);
	tester_btaddr = strdup(btaddr);
	tester_state = TESTER_STATE_CONNECT;
}

void tester_disconnect()
{
	if (tester_state == TESTER_STATE_IDLE) {
		tester_tester_callback_set(TESTER_EVENT_DISCONNECTED);
		return;
	}

	tester_disconnect_flag = 1;
}

void tester_poll()
{
	uint8_t *d;

	pthread_mutex_lock(&tester_mutex);
	d = (uint8_t *)g_queue_pop_tail(tester_queue);
	pthread_mutex_unlock(&tester_mutex);

	if (d == NULL)
		return;

	if (d[1] == 0)
		tester_tester_callback(d[2]);
	else if (d[1] == 1)
		tester_parse(&d[2], d[0]);
	else if (d[1] == 2)
		tester_bluetooth_scan_callback(&d[2]);

	g_free(d);
}

void tester_plot_input(uint16_t cu_mv, uint16_t bu_mv_min, uint16_t bu_mv_max, uint16_t bi_ua_scale)
{
	uint8_t buffer[8];

	if (tester_state != TESTER_STATE_CONNECTED)
		return;

	if (bu_mv_min > bu_mv_max)
		tester_swap(&bu_mv_min, &bu_mv_max);

	buffer[0] = BLUETOOTH_EVENT_START;
	buffer[1] = BLUETOOTH_START_INPUT;
	buffer[2] = (cu_mv >> 8) & 0xFF;
	buffer[3] = (cu_mv >> 0) & 0xFF;
	buffer[4] = (bu_mv_min >> 8) & 0xFF;
	buffer[5] = (bu_mv_min >> 0) & 0xFF;
	buffer[6] = (bu_mv_max >> 8) & 0xFF;
	buffer[7] = (bu_mv_max >> 0) & 0xFF;
	buffer[8] = (bi_ua_scale >> 8) & 0xFF;
	buffer[9] = (bi_ua_scale >> 0) & 0xFF;

	ble_gatt_cmd_write(tester_sock, 0x000E, buffer, 10);
}

void tester_plot_output(uint16_t bi_ua, uint16_t cu_mv_min, uint16_t cu_mv_max, uint16_t ci_ma_scale)
{
	uint8_t buffer[8];

	if (tester_state != TESTER_STATE_CONNECTED)
		return;

	if (cu_mv_min > cu_mv_max)
		tester_swap(&cu_mv_min, &cu_mv_max);

	buffer[0] = BLUETOOTH_EVENT_START;
	buffer[1] = BLUETOOTH_START_OUTPUT;
	buffer[2] = (bi_ua >> 8) & 0xFF;
	buffer[3] = (bi_ua >> 0) & 0xFF;
	buffer[4] = (cu_mv_min >> 8) & 0xFF;
	buffer[5] = (cu_mv_min >> 0) & 0xFF;
	buffer[6] = (cu_mv_max >> 8) & 0xFF;
	buffer[7] = (cu_mv_max >> 0) & 0xFF;
	buffer[8] = (ci_ma_scale >> 8) & 0xFF;
	buffer[9] = (ci_ma_scale >> 0) & 0xFF;

	ble_gatt_cmd_write(tester_sock, 0x000E, buffer, 10);
}

void tester_plot_beta(uint16_t cu_mv, uint16_t bi_ua_min, uint16_t bi_ua_max, uint16_t ci_ma_scale)
{
	uint8_t buffer[8];

	if (tester_state != TESTER_STATE_CONNECTED)
		return;

	if (bi_ua_min > bi_ua_max)
		tester_swap(&bi_ua_min, &bi_ua_max);

	buffer[0] = BLUETOOTH_EVENT_START;
	buffer[1] = BLUETOOTH_START_BETA;
	buffer[2] = (cu_mv >> 8) & 0xFF;
	buffer[3] = (cu_mv >> 0) & 0xFF;
	buffer[4] = (bi_ua_min >> 8) & 0xFF;
	buffer[5] = (bi_ua_min >> 0) & 0xFF;
	buffer[6] = (bi_ua_max >> 8) & 0xFF;
	buffer[7] = (bi_ua_max >> 0) & 0xFF;
	buffer[8] = (ci_ma_scale >> 8) & 0xFF;
	buffer[9] = (ci_ma_scale >> 0) & 0xFF;

	ble_gatt_cmd_write(tester_sock, 0x000E, buffer, 10);
}

static void *tester_loop(void *p)
{
	uint8_t buffer[32];
	uint8_t *d;
	int scan_fd;
	int i;

	scan_fd = -1;

	while (1) {
		if (tester_state == TESTER_STATE_IDLE) {
			if (scan_fd >= 0) {
				ble_scan_close(scan_fd);
				scan_fd = -1;
			}
			usleep(100000);
			continue;
		} else {
			if (tester_disconnect_flag) {
				if (scan_fd >= 0) {
					ble_scan_close(scan_fd);
					scan_fd = -1;
				}

				tester_state = TESTER_STATE_DISCONNECT;
				tester_disconnect_flag = 0;
			}
		}
		if (tester_state == TESTER_STATE_SCAN_START) {
			if (scan_fd >= 0) {
				ble_scan_close(scan_fd);
				scan_fd = -1;
			}
			tester_mac_list_len = 0;
			scan_fd = ble_scan_open("hci0");
			if (scan_fd >= 0)
				tester_state = TESTER_STATE_SCAN;
		}
		if (tester_state == TESTER_STATE_SCAN) {
			if (tester_sock_poll(scan_fd, 500) < 0) {
				tester_state = TESTER_STATE_IDLE;
			} else {
				i = read(scan_fd, buffer, 32);
				if (i < 0) {
					tester_state = TESTER_STATE_IDLE;
				} else if (i >= 30) {
					if (!memcmp(&buffer[19], "\x54\x52\x41\x4E\x53\x47\x52\x41\x50\x48", 10)) {
						d = ble_scan_ba(buffer, i);
						if (d != NULL) {
							for (i = 0; i < tester_mac_list_len; i++) {
								if (!memcmp(&tester_mac_list[i][0], d, 6))
									break;
							}
							if ((i == tester_mac_list_len) && (tester_mac_list_len < TESTER_MAC_LIST_MAX)) {
								memcpy(&tester_mac_list[tester_mac_list_len][0], d, 6);
								d = g_malloc(8);
								d[0] = 8;
								d[1] = 2;
								d[2] = tester_mac_list[tester_mac_list_len][5];
								d[3] = tester_mac_list[tester_mac_list_len][4];
								d[4] = tester_mac_list[tester_mac_list_len][3];
								d[5] = tester_mac_list[tester_mac_list_len][2];
								d[6] = tester_mac_list[tester_mac_list_len][1];
								d[7] = tester_mac_list[tester_mac_list_len][0];
								tester_mac_list_len++;
								pthread_mutex_lock(&tester_mutex);
								g_queue_push_head(tester_queue, d);
								pthread_mutex_unlock(&tester_mutex);
							}
						}
					}
				}
			}
		}
		if (tester_state == TESTER_STATE_CONNECT) {
			if (scan_fd >= 0) {
				ble_scan_close(scan_fd);
				scan_fd = -1;
			}

			tester_sock = ble_gatt_connect("hci0", (char *)tester_btaddr, BLE_ADDR_TYPE_RANDOM);
			if (tester_sock < 0) {
				tester_state = TESTER_STATE_IDLE;
				tester_tester_callback_set(TESTER_EVENT_DISCONNECTED);
				continue;
			}

			if (tester_ind_not)
				ble_gatt_cmd_indicate_set_req(tester_sock, 0x0010);
			else
				ble_gatt_cmd_notify_set_req(tester_sock, 0x0010);

			for (i = 0; i < 4; i++) {
				if (tester_sock_poll(tester_sock, 1000) <= 0) {
					tester_state = TESTER_STATE_DISCONNECT;
					break;
				}
				if (read(tester_sock, buffer, 32) <= 0) {
					tester_state = TESTER_STATE_DISCONNECT;
					break;
				}
				if (buffer[0] == BLE_GATT_OP_WRITE_RESP)
					break;
			}

			if ((i < 4) && (tester_state != TESTER_STATE_DISCONNECT)) {
				tester_state = TESTER_STATE_CONNECTED;
				tester_tester_callback_set(TESTER_EVENT_CONNECTED);
			}
		}
		if (tester_state == TESTER_STATE_CONNECTED) {
			if (scan_fd >= 0) {
				ble_scan_close(scan_fd);
				scan_fd = -1;
			}

			if (tester_sock_poll(tester_sock, 500) < 0) {
				tester_state = TESTER_STATE_DISCONNECT;
			} else {
				i = read(tester_sock, buffer, 32);
				if (i <= 0) {
					tester_state = TESTER_STATE_DISCONNECT;
				} else {
					if (buffer[0] == BLE_GATT_OP_HANDLE_IND)
						write(tester_sock, "\x1E", 1);

					d = g_malloc(i + 2);
					d[0] = i;
					d[1] = 1;
					memcpy(&d[2], buffer, i);
					pthread_mutex_lock(&tester_mutex);
					g_queue_push_head(tester_queue, d);
					pthread_mutex_unlock(&tester_mutex);
//					tester_parse(buffer, i);
				}
			}
		}
		if (tester_state == TESTER_STATE_DISCONNECT) {
			shutdown(tester_sock, SHUT_RDWR);
			close(tester_sock);
			tester_state = TESTER_STATE_IDLE;
			tester_tester_callback_set(TESTER_EVENT_DISCONNECTED);
		}
	}
	return NULL;
}

static void tester_tester_callback_set(int event)
{
	uint8_t *d;

	d = g_malloc(3);
	d[0] = 2;
	d[1] = 0;
	d[2] = event;
	pthread_mutex_lock(&tester_mutex);
	g_queue_push_head(tester_queue, d);
	pthread_mutex_unlock(&tester_mutex);
}

static void tester_parse(uint8_t *buffer, int l)
{
	uint8_t event;

	if (l < 5)
		return;

	if ((buffer[0] != 0x1B) && (buffer[0] != 0x1D))
		return;

	if (buffer[1] != 0x10)
		return;

	if (buffer[2] != 0x00)
		return;

	l -= 5;
	event = buffer[4];

	if (event == BLUETOOTH_EVENT_UPTIME) {
		if (l < 4)
			return;

		tester_bluetooth_callback(event, 0, 0, ((uint32_t)buffer[5] << 24) | ((uint32_t)buffer[6] << 16) | ((uint32_t)buffer[7] << 8) | buffer[8], 0, 0, 0, 0);
	} else if (event == BLUETOOTH_EVENT_STARTED) {
		if (l < 12)
			return;

		tester_bluetooth_callback(event, buffer[6], buffer[5], ((uint16_t)buffer[7] << 8) | buffer[8], ((uint16_t)buffer[9] << 8) | buffer[10], ((uint16_t)buffer[11] << 8) | buffer[12], ((uint16_t)buffer[13] << 8) | buffer[14], ((uint16_t)buffer[15] << 8) | buffer[16]);
	} else if (event == BLUETOOTH_EVENT_ERROR) {
		if (l < 2)
			return;

		tester_bluetooth_callback(event, 0, buffer[5], buffer[6], 0, 0, 0, 0);
	} else if (event == BLUETOOTH_EVENT_TRANSISTOR_TYPE) {
		if (l < 2)
			return;

		tester_bluetooth_callback(event, 0, buffer[5], buffer[6], 0, 0, 0, 0);
	} else if (event == BLUETOOTH_EVENT_DATA) {
		if (l < 10)
			return;

		tester_bluetooth_callback(event, ((uint16_t)buffer[6] << 8) | buffer[7], buffer[5], buffer[8], ((uint16_t)buffer[9] << 8) | buffer[10], ((uint16_t)buffer[11] << 8) | buffer[12], ((uint16_t)buffer[13] << 8) | buffer[14], 0);
	} else if (event == BLUETOOTH_EVENT_STOPPED) {
		if (l < 1)
			return;

		tester_bluetooth_callback(event, 0, buffer[5], 0, 0, 0, 0, 0);
	}
}

static int tester_sock_poll(int sock, int timeout)
{
	struct pollfd fds;

	fds.fd = sock;
	fds.events = POLLIN;

	return poll(&fds, 1, timeout);
}

static void tester_swap(uint16_t *a, uint16_t *b)
{
	uint16_t c;

	c = *a;
	*a = *b;
	*b = c;
}

