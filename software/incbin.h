// code copied from: https://www.avrfreaks.net/forum/embed-text-file-binary

#ifndef _INCBIN_H_
#define _INCBIN_H_

#if !defined(___STRINGIFY2) 
#define ___STRINGIFY2(__p) #__p 
#define ___STRINGIFY(__p) ___STRINGIFY2(__p) 
#endif 

#define INCLUDE_BINARY_FILE(__variable, __end_variable, __fileName, __section, __attrs) \
__asm__ (                                                                               \
    ".pushsection " __section __attrs                                  "\n\t"           \
    ".global " ___STRINGIFY(__variable)                                "\n\t"           \
    ".global " ___STRINGIFY(__end_variable)                            "\n"             \
    ___STRINGIFY(__variable) ":"                                       "\n\t"           \
    ".align 4"                                                         "\n\t"           \
    ".incbin \"" __fileName "\""                                       "\n"             \
    ___STRINGIFY(__end_variable) ":"                                   "\n\t"           \
    ".size " ___STRINGIFY(__variable) ", .-" ___STRINGIFY(__variable)  "\n\t"           \
    ".align 4"                                                         "\n\t"           \
    ".popsection"                                                                       \
);                                                                                      \
extern char __variable __attribute__((section(__section)));                             \
extern char __end_variable __attribute__((section(__section)))

#endif

