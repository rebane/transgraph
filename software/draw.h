/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _DRAW_H_
#define _DRAW_H_

void draw_init();
void draw_color(int red, int green, int blue);
void draw_cls();
void draw_line(int x1, int y1, int x2, int y2, int red, int green, int blue);
void draw_pset(int x, int y, int red, int green, int blue);
void draw_puts(int x, int y, int size, char *s, int red, int green, int blue);
void draw_scale(char *yn, int ys, int ye, char *xn, int xs, int xe);
void draw_refresh(void *cairo);
int draw_save(char *path);

#endif

