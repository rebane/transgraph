/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include "draw.h"
#include "gui.h"
#include "tester.h"
#include "bluetooth.h"

enum {
	MAIN_PAGE_NONE = 0,
	MAIN_PAGE_INPUT,
	MAIN_PAGE_OUTPUT,
	MAIN_PAGE_BETA
};

static int main_page, main_points, main_start, main_end, main_scale;
static int main_last_x, main_last_y;
static uint8_t main_transaction_id;

void default_input()
{
	gui_page_value_set("input_characteristics", "uc", 300);
	gui_page_value_set("input_characteristics", "ub_start", 750);
	gui_page_value_set("input_characteristics", "ub_end", 980);
	gui_page_value_set("input_characteristics", "ib_scale", 10000);
}

void default_output()
{
	gui_page_value_set("output_characteristics", "ib", 5000);
	gui_page_value_set("output_characteristics", "uc_start", 10);
	gui_page_value_set("output_characteristics", "uc_end", 3000);
	gui_page_value_set("output_characteristics", "ic_scale", 200);
}

void default_beta()
{
	gui_page_value_set("beta", "uc", 3000);
	gui_page_value_set("beta", "ib_start", 0);
	gui_page_value_set("beta", "ib_end", 300);
	gui_page_value_set("beta", "ic_scale", 100);
}

void event_callback(char *widget, char *event)
{
	if (strcmp(event, "idle"))
		printf("NAME: %s, EVENT: %s\n", widget, event);

	if (!strcmp(widget, "connect")) {
		gui_enable(0, 0);
		gui_ble();
		tester_scan();
	} else if (!strcmp(widget, "disconnect")) {
		gui_enable(1, 0);
		tester_disconnect();
	} else if (!strcmp(widget, "clear")) {
		draw_cls();
		main_page = MAIN_PAGE_NONE;
	} else if (!strcmp(widget, "save")) {
		gui_save();
	} else if (!strcmp(event, "ble_cancel")) {
		tester_noscan();
		gui_enable(1, 0);
	} else if (!strcmp(event, "ble_connect")) {
		gui_enable(0, 1);
		tester_connect(widget);
	}
	tester_poll();
}

void page_callback(char *page, char *button)
{
	int s, e, m;

	if (!strcmp(page, "input_characteristics")) {
		if (!strcmp(button, "start")) {
			s = gui_page_value_get(page, "ub_start");
			e = gui_page_value_get(page, "ub_end");
			m = gui_page_value_get(page, "ib_scale");
			main_transaction_id = 0;
			tester_plot_input(gui_page_value_get(page, "uc"), s, e, m);
		} else if (!strcmp(button, "default")) {
			default_input();
		}
	} else if (!strcmp(page, "output_characteristics")) {
		if (!strcmp(button, "start")) {
			s = gui_page_value_get(page, "uc_start");
			e = gui_page_value_get(page, "uc_end");
			m = gui_page_value_get(page, "ic_scale");
			main_transaction_id = 0;
			tester_plot_output(gui_page_value_get(page, "ib"), s, e, m);
		} else if (!strcmp(button, "default")) {
			default_output();
		}
	} else if (!strcmp(page, "beta")) {
		if (!strcmp(button, "start")) {
			s = gui_page_value_get(page, "ib_start");
			e = gui_page_value_get(page, "ib_end");
			m = gui_page_value_get(page, "ic_scale");
			main_transaction_id = 0;
			tester_plot_beta(gui_page_value_get(page, "uc"), s, e, m);
		} else if (!strcmp(button, "default")) {
			default_beta();
		}
	}
}

void draw_callback(void *cairo, int width, int height)
{
	draw_refresh(cairo);
}

void bluetooth_scan_callback(uint8_t *mac)
{
	char buffer[32];

	snprintf(buffer, 32, "%02X:%02X:%02X:%02X:%02X:%02X",
		(unsigned int)mac[0],
		(unsigned int)mac[1],
		(unsigned int)mac[2],
		(unsigned int)mac[3],
		(unsigned int)mac[4],
		(unsigned int)mac[5]
	);
	buffer[31] = 0;
	gui_ble_mac_add(buffer);
}

void bluetooth_callback(int event, int index, uint8_t transaction_id, uint32_t s, uint16_t a, uint16_t b, uint16_t c, uint16_t m)
{
	int y, x, p;

	if (event == BLUETOOTH_EVENT_STARTED) {
		main_transaction_id = transaction_id;
		printf("STARTED: TYPE: %d, TRID: %u, POINTS: %u, A: %u, B: %u, C: %u, SCALE: %u\n", index, (unsigned int)transaction_id, (unsigned int)s, (unsigned int)a, (unsigned int)b, (unsigned int)c, (unsigned int)m);
		if (index == BLUETOOTH_START_INPUT) {
			if ((main_page != MAIN_PAGE_INPUT) || (main_start != b) || (main_end != c) || (main_scale != m))
				draw_scale("Ib (uA)", 0, m, "Ub (mV)", b, c);

			gui_page_value_set("input_characteristics", "uc", a);
			gui_page_value_set("input_characteristics", "ub_start", b);
			gui_page_value_set("input_characteristics", "ub_end", c);
			gui_page_value_set("input_characteristics", "ib_scale", m);
			gui_page_set("input_characteristics");

			main_page = MAIN_PAGE_INPUT;
			main_start = b;
			main_end = c;
			main_scale = m;
			main_last_y = -1;
		} else if (index == BLUETOOTH_START_OUTPUT) {
			if ((main_page != MAIN_PAGE_OUTPUT) || (main_start != b) || (main_end != c) || (main_scale != m))
				draw_scale("Ic (mA)", 0, m, "Uc (mV)", b, c);

			gui_page_value_set("output_characteristics", "ib", a);
			gui_page_value_set("output_characteristics", "uc_start", b);
			gui_page_value_set("output_characteristics", "uc_end", c);
			gui_page_value_set("output_characteristics", "ic_scale", m);
			gui_page_set("output_characteristics");

			main_page = MAIN_PAGE_OUTPUT;
			main_start = b;
			main_end = c;
			main_scale = m;
			main_last_y = -1;
		} else if (index == BLUETOOTH_START_BETA) {
			if ((main_page != MAIN_PAGE_BETA) || (main_start != b) || (main_end != c) || (main_scale != m))
				draw_scale("Ic (mA)", 0, m, "Ib (uA)", b, c);

			gui_page_value_set("beta", "uc", a);
			gui_page_value_set("beta", "ib_start", b);
			gui_page_value_set("beta", "ib_end", c);
			gui_page_value_set("beta", "ic_scale", m);
			gui_page_set("beta");

			main_page = MAIN_PAGE_BETA;
			main_start = b;
			main_end = c;
			main_scale = m;
			main_last_y = -1;
		}
	} else if (event == BLUETOOTH_EVENT_DATA) {
		if (transaction_id != main_transaction_id)
			return;

		p = main_points - 1;
		if (p < 1)
			p = 1;

		x = 15 + ((index * 624) / main_points);
		y = 465 - ((c * 465) / main_scale);

		if (y < 0)
			y = 0;
		if (y > 479)
			y = 479;
		if (x < 0)
			x = 0;
		if (x > 639)
			y = 639;

		if (index == 0) {
			main_last_x = 15;
			main_last_y = y;
		} else {
			if (main_last_y == -1) {
				main_last_x = 15;
				main_last_y = y;
			}

			draw_line(main_last_x, main_last_y, x, y, 0, 255, 0);
			main_last_x = x;
			main_last_y = y;
		}
	}
}

void tester_callback(int event)
{
	if (event == TESTER_EVENT_CONNECTED) {
		gui_enable(0, 1);
		printf("GUI ENABLED\n");
	} else if (event == TESTER_EVENT_DISCONNECTED) {
		gui_enable(1, 0);
		printf("GUI DISABLED\n");
	}
}

int main(int argc, char *argv[])
{
	main_page = MAIN_PAGE_NONE;
	main_points = 128;
	main_start = main_end = -1;
	main_scale = 470;
	main_last_x = 0;
	main_transaction_id = 0;

	gui_init(event_callback, page_callback, draw_callback);
	draw_init();
	default_input();
	default_output();
	default_beta();
	tester_init(0, bluetooth_scan_callback, bluetooth_callback, tester_callback);

	gui_loop();

	return 0;
}

