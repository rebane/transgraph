/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#ifndef _GUI_H_
#define _GUI_H_

void gui_init(void (*event_callback)(char *widget, char *event), void (*page_callback)(char *page, char *button), void (*draw_callback)(void *cairo, int width, int height));
void gui_loop();
void gui_enable(int connect, int disconnect);
void gui_trigger_draw();
int gui_page_value_get(char *page, char *name);
void gui_page_value_set(char *page, char *name, int value);
void gui_page_set(char *page);
void gui_ble();
void gui_ble_mac_add(char *mac);
void gui_save();

#endif

