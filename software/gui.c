/* TalTech
 *
 * Veiko Rütter, 214250IACB
 *
 * Public Domain, 2021
 */

#include "gui.h"
#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <glib.h>
#include "incbin.h"
#include "draw.h"

INCLUDE_BINARY_FILE(glade_start, glade_end, "ui.glade", ".data", "");

double strtod2(const char *string, char **endPtr);

static GtkBuilder *gui_builder;
static GtkWidget *gui_window, *gui_select, *gui_page, *gui_draw, *gui_window_ble, *gui_list_ble;
static int gui_ble_ok;
static void (*gui_event_callback)(char *widget, char *event);
static void (*gui_page_callback)(char *page, char *button);
static void (*gui_draw_callback)(void *cairo, int width, int height);

static void gui_cb_event(GtkWidget *widget, gpointer data);
static void gui_cb_list(gpointer widget, gpointer data);
static void gui_cb_select(GtkWidget *widget, gpointer data);
static gboolean gui_cb_draw(gpointer widget, cairo_t *cr, gpointer data);
static gboolean gui_idle_loop(gpointer data);

void gui_init(void (*event_callback)(char *widget, char *event), void (*page_callback)(char *page, char *button), void (*draw_callback)(void *cairo, int width, int height))
{
	GSList *l;
	int argc;

	argc = 0;
	gui_ble_ok = 0;
	gui_event_callback = event_callback;
	gui_page_callback = page_callback;
	gui_draw_callback = draw_callback;
	gui_window_ble = NULL;

	gtk_init(&argc, NULL);
	g_idle_add(gui_idle_loop, NULL);

	gui_builder = gtk_builder_new();
	// gtk_builder_add_from_file(gui_builder, "ui.glade", NULL);
	gtk_builder_add_from_string(gui_builder, &glade_start, &glade_end - &glade_start, NULL);
	gui_window = GTK_WIDGET(gtk_builder_get_object(gui_builder, "main_window"));
	gtk_window_set_title(GTK_WINDOW(gui_window), "Transgraph");
	gtk_container_set_border_width(GTK_CONTAINER(gui_window), 5);
	gtk_window_set_resizable(GTK_WINDOW(gui_window), FALSE);

	gtk_widget_set_name(GTK_WIDGET(gui_window), "main");
	g_signal_connect(G_OBJECT(gui_window), "destroy", G_CALLBACK(gui_cb_event), "destroy");

	gui_page = GTK_WIDGET(gtk_builder_get_object(gui_builder, "page"));
	gui_select = GTK_WIDGET(gtk_builder_get_object(gui_builder, "select"));

	gtk_container_foreach((GtkContainer *)gui_page, gui_cb_select, gui_select);
	g_signal_connect(G_OBJECT(gui_select), "changed", G_CALLBACK(gui_cb_event), "changed");

	l = gtk_builder_get_objects(gui_builder);
	g_slist_foreach(l, gui_cb_list, NULL);

	gui_draw = GTK_WIDGET(gtk_builder_get_object(gui_builder, "draw"));
	g_signal_connect(G_OBJECT(gui_draw), "draw", G_CALLBACK(gui_cb_draw), NULL);

	gtk_combo_box_set_active((GtkComboBox *)gui_select, 0);

	g_signal_connect(G_OBJECT(gtk_builder_get_object(gui_builder, "connect")), "pressed", G_CALLBACK(gui_cb_event), "pressed");
	g_signal_connect(G_OBJECT(gtk_builder_get_object(gui_builder, "disconnect")), "pressed", G_CALLBACK(gui_cb_event), "pressed");
	g_signal_connect(G_OBJECT(gtk_builder_get_object(gui_builder, "clear")), "pressed", G_CALLBACK(gui_cb_event), "pressed");
	g_signal_connect(G_OBJECT(gtk_builder_get_object(gui_builder, "save")), "pressed", G_CALLBACK(gui_cb_event), "save");
}

void gui_loop()
{
	gtk_widget_show(gui_window);
	gtk_main();
}

void gui_enable(int connect, int disconnect)
{
	if (connect)
		gtk_widget_set_sensitive(GTK_WIDGET(gtk_builder_get_object(gui_builder, "connect")), TRUE);
	else
		gtk_widget_set_sensitive(GTK_WIDGET(gtk_builder_get_object(gui_builder, "connect")), FALSE);

	if (disconnect) {
		gtk_widget_set_sensitive(GTK_WIDGET(gtk_builder_get_object(gui_builder, "pages")), TRUE);
		gtk_widget_set_sensitive(GTK_WIDGET(gtk_builder_get_object(gui_builder, "disconnect")), TRUE);
	} else {
		gtk_widget_set_sensitive(GTK_WIDGET(gtk_builder_get_object(gui_builder, "pages")), FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(gtk_builder_get_object(gui_builder, "disconnect")), FALSE);
	}
}

void gui_trigger_draw()
{
	gtk_widget_queue_draw(GTK_WIDGET(gui_draw));
}

int gui_page_value_get(char *page, char *name)
{
	GObject *o;
	char s[256], *v;
	double d;
	int i;

	snprintf(s, 255, "page_%s_%s", page, name);
	s[255] = 0;

	o = gtk_builder_get_object(gui_builder, s);
	if (GTK_IS_ENTRY(o)) {
		printf("ENTRY: %s\n", s);
		v = strdup(gtk_entry_get_text(GTK_ENTRY(o)));

		for (i = 0; v[i]; i++) {
			if (v[i] == ',')
				v[i] = '.';
		}
		d = strtod2(v, NULL);
		free(v);

		return d;
	} else if (GTK_IS_RANGE(o)) {
		d = gtk_range_get_value(GTK_RANGE(o));
		printf("RANGE: %s, %f\n", s, d);
		return d;
	}

	return 0;
}

void gui_page_value_set(char *page, char *name, int value)
{
	GObject *o;
	char s[256], v[16];
	int p;

	snprintf(s, 255, "page_%s_%s", page, name);
	s[255] = 0;

	p = value;
	if (p < 0)
		p = -p;

	// snprintf(v, 15, "%d.%02d", (value / 1000), (p % 1000));
	snprintf(v, 15, "%d", value);

	o = gtk_builder_get_object(gui_builder, s);
	if (GTK_IS_ENTRY(o)) {
		gtk_entry_set_text(GTK_ENTRY(o), v);
	} else if (GTK_IS_RANGE(o)) {
		gtk_range_set_value(GTK_RANGE(o), value);
	}
}

void gui_page_set(char *page) // TODO: find out how to get combo box strings
{
	if (!strcmp(page, "input_characteristics"))
		gtk_combo_box_set_active((GtkComboBox *)gui_select, 0);
	else if (!strcmp(page, "output_characteristics"))
		gtk_combo_box_set_active((GtkComboBox *)gui_select, 1);
	else if (!strcmp(page, "beta"))
		gtk_combo_box_set_active((GtkComboBox *)gui_select, 2);
}

void gui_ble()
{
	GtkWidget *box_v, *box_h;
	GtkWidget *button_connect, *button_cancel;

	gui_window_ble = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	g_signal_connect(G_OBJECT(gui_window_ble), "destroy", G_CALLBACK(gui_cb_event), "ble_cancel");
	gtk_container_set_border_width(GTK_CONTAINER (gui_window_ble), 5);
	gtk_window_set_title(GTK_WINDOW(gui_window_ble), "Connect");

	box_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);

	gui_list_ble = gtk_list_box_new();
//	gtk_list_box_insert(GTK_LIST_BOX(gui_list_ble), gtk_label_new("44:5C:E9:C9:71:1D"), -1);
//	gtk_list_box_insert(GTK_LIST_BOX(gui_list_ble), gtk_label_new("C8:F7:50:FE:55:70"), -1);
//	gtk_list_box_insert(GTK_LIST_BOX(gui_list_ble), gtk_label_new("00:E0:4C:68:6F:AE"), -1);
//	gtk_list_box_insert(GTK_LIST_BOX(gui_list_ble), gtk_label_new("0C:E8:6C:68:54:35"), -1);

	box_h = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);

	gtk_box_pack_start(GTK_BOX(box_v), gui_list_ble, TRUE, TRUE, 5);
	gtk_box_pack_start(GTK_BOX(box_v), box_h, TRUE, TRUE, 5);

	button_connect = gtk_button_new_with_label("Connect");
	g_signal_connect(G_OBJECT(button_connect), "pressed", G_CALLBACK(gui_cb_event), "ble_connect");

	button_cancel = gtk_button_new_with_label("Cancel");
	g_signal_connect(G_OBJECT(button_cancel), "pressed", G_CALLBACK(gui_cb_event), "ble_cancel");

	gtk_box_pack_start(GTK_BOX(box_h), button_connect, TRUE, TRUE, 5);
	gtk_box_pack_end(GTK_BOX(box_h), button_cancel, TRUE, TRUE, 5);

	gtk_container_add(GTK_CONTAINER(gui_window_ble), box_v);

	gtk_widget_show_all(gui_window_ble);

	gui_ble_ok = 1;
}

void gui_ble_mac_add(char *mac)
{
	if (!gui_ble_ok)
		return;

	gtk_list_box_insert(GTK_LIST_BOX(gui_list_ble), gtk_label_new(mac), -1);
	gtk_widget_show_all(gui_window_ble);
}

void gui_save()
{
	GtkWidget *gui_window_save;
	GtkFileChooser *chooser;
	char *path;
	gint res;

	gui_window_save = gtk_file_chooser_dialog_new("Save", GTK_WINDOW(gui_window), GTK_FILE_CHOOSER_ACTION_SAVE, "Cancel", GTK_RESPONSE_CANCEL, "Save", GTK_RESPONSE_ACCEPT, NULL);
	res = gtk_dialog_run(GTK_DIALOG(gui_window_save));
	if (res == GTK_RESPONSE_ACCEPT) {
		chooser = GTK_FILE_CHOOSER(gui_window_save);
		path = gtk_file_chooser_get_filename(chooser);
		draw_save(path);
		g_free(path);
	}
	gtk_widget_destroy(gui_window_save);
}

static void gui_cb_event(GtkWidget *widget, gpointer data)
{
	char *name, *event, *p;
	GtkListBoxRow *row;
	GtkLabel *label;
	int i;

	name = (char *)gtk_widget_get_name(widget);
	event = (char *)data;

	if (!g_strcmp0(event, "destroy")) {
		gtk_main_quit();
		exit(0);
		return;
	} else if (!g_strcmp0(event, "ble_cancel")) {
		if (!gui_ble_ok)
			return;

		gui_ble_ok = 0;
		gtk_window_close(GTK_WINDOW(gui_window_ble));
	} else if (!g_strcmp0(event, "ble_connect")) {
		if (!gui_ble_ok)
			return;

		gui_ble_ok = 0;

		row = gtk_list_box_get_selected_row(GTK_LIST_BOX(gui_list_ble));
		if (row == NULL) {
			gui_event_callback("", "ble_cancel");
			gtk_window_close(GTK_WINDOW(gui_window_ble));
			return;
		}
		label = GTK_LABEL(gtk_bin_get_child(GTK_BIN(row)));
		if (label == NULL) {
			gui_event_callback("", "ble_cancel");
			gtk_window_close(GTK_WINDOW(gui_window_ble));
			return;
		}
		gui_event_callback((char *)gtk_label_get_text(label), event);
		gtk_window_close(GTK_WINDOW(gui_window_ble));
		return;
	} else if (!strcmp(name, "select")) {
		name = gtk_combo_box_text_get_active_text((GtkComboBoxText *)widget);
		gtk_stack_set_visible_child_name((GtkStack *)gui_page, name);
		return;
	} else if (!strcmp(event, "pressed") && !strncmp(name, "page_", 5)) {
		p = strdup(name);
		for (i = strlen(p) - 1; i >= 0; i--) {
			if (p[i] == '_')
				break;
		}
		p[i] = 0;
		gui_page_callback(&p[5], &p[i + 1]);
		free(p);
		return;
	}

	gui_event_callback(name, event);
}

static void gui_cb_list(gpointer widget, gpointer data)
{
	const gchar *name;
	int l;

	name = gtk_widget_get_name(widget);
	if (GTK_IS_BUTTON(widget) && !strncmp(name, "page_", 5)) {
		l = strlen(name);
		if ((l > 6) && !strncmp(&name[l - 6], "_start", 6)) {
			g_signal_connect(G_OBJECT(widget), "pressed", G_CALLBACK(gui_cb_event), "pressed");
		} else if ((l > 8) && !strncmp(&name[l - 8], "_default", 6)) {
			g_signal_connect(G_OBJECT(widget), "pressed", G_CALLBACK(gui_cb_event), "pressed");
		}
	}
}

static void gui_cb_select(GtkWidget *widget, gpointer data)
{
	GtkComboBoxText *select = data;
	const gchar *name;

	name = gtk_widget_get_name(widget);
	gtk_combo_box_text_append(select, name, name);
}

static gboolean gui_cb_draw(gpointer widget, cairo_t *cr, gpointer data)
{
	gui_draw_callback((void *)cr, gtk_widget_get_allocated_width(widget), gtk_widget_get_allocated_height(widget));

	return FALSE;
}

static gboolean gui_idle_loop(gpointer data)
{
	gui_event_callback("main", "idle");

	return TRUE;
}

